<?php
class Blog extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->library("encrypt");
		$this->load->helper("form");
		$this->load->database();
		$this->load->library('pagination');
		$this->load->helper('url');
	}

	function validate($x)
	{
		return $this->db->escape(str_replace("'","`",str_replace('"',"\"",strip_tags(htmlspecialchars($x)))));
	}

	function index($admin=NULL)
	{
		$this->load->model("user_model");
		$this->load->model("config_model");	
		$this->load->model("icon_model");
		$this->load->model("blog_model");
		$this->load->model("about_model");
		$this->load->model("contact_model");
		$this->load->model("booking_model");
		$this->load->model("blogcategory_model");
		$this->load->model("blogtags_model");
		
		$imgList=array();
		$img=array();
		$paging = array();
		// Get Data
		$urikey =$this->input->get("key");

		$isLogin=$this->session->userdata("id");
		$config=$this->config_model->get_config();

		//konfigurasi pagination
		$paging['base_url'] = base_url().'blog/index'; //site url
        $paging['total_rows'] = $this->db->count_all('msblog'); //total row
        $paging['per_page'] = 5;  //show record per halaman
        $paging["uri_segment"] = 3;  // uri parameter
        $choice = $paging["total_rows"] / $paging["per_page"];
		$paging["num_links"] = floor($choice);

		// Membuat Style pagination untuk BootStrap v4
		$paging['first_link']       = 'First';
        $paging['last_link']        = 'Last';
        $paging['next_link']        = 'Next';
        $paging['prev_link']        = 'Prev';
        $paging['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $paging['full_tag_close']   = '</ul></nav></div>';
        $paging['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $paging['num_tag_close']    = '</span></li>';
        $paging['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $paging['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $paging['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $paging['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $paging['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $paging['prev_tagl_close']  = '</span>Next</li>';
        $paging['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $paging['first_tagl_close'] = '</span></li>';
        $paging['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $paging['last_tagl_close']  = '</span></li>';

		if (!$isLogin)
			$data['id_admin']= 0;
		else
			$data['id_admin']= 2;
		
		$data['error']=0;
		$data['config']=$config;
		$data['fulluserList']=$this->user_model->get_full_user();
		$data['iconList'] = $this->icon_model->get_icon();
		$data['blogList'] = $this->blog_model->get_blog();
		$data['aboutList']=$this->about_model->get_about();
		$data['fullblogList'] = $this->blog_model->get_full_blog();
		$data['bookingList'] = $this->booking_model->get_booking();
		$data['contactList']=$this->contact_model->get_contact();
		$data['blogcategoryList']=$this->blogcategory_model->get_category();
		$data['blogtagsList']=$this->blogtags_model->get_tags();
		$data['fullblogtagsList']=$this->blogtags_model->get_full_category();
		$data['recentblogList']=$this->blog_model->get_recentblog();
		// $data['blogkeyList']=$this->blog_model->get_by_key($urikey);
		$data['fullblogcateList']=$this->blogcategory_model->get_full_category();
		$data['keywords'] = $urikey;

		$this->pagination->initialize($paging);
		$data['page'] = ($this->uri->segment(3) == null) ? 0 : $this->uri->segment(3); 
        $data['blogkeyList'] = $this->blog_model->get_by_key($urikey, $paging["per_page"], $data['page']);           

		$data['pagination'] = $this->pagination->create_links();

		$this->load->view("blog_view",$data);
	}

	function detail($id)
	{
		$this->load->model("user_model");
		$this->load->model("config_model");	
		$this->load->model("icon_model");
		$this->load->model("blog_model");
		$this->load->model("about_model");
		$this->load->model("contact_model");
		$this->load->model("booking_model");
		$this->load->model("blogcategory_model");
		$this->load->model("blogtags_model");
		
		$imgList=array();
		$img=array();
		
		$isLogin=$this->session->userdata("id");
		$config=$this->config_model->get_config();
		$blog = $this->blog_model->get_blog_by_id($id);
		if (!$isLogin)
			$data['id_admin']= 0;
		else
			$data['id_admin']= 2;
		
		$data['error']=0;
		$data['config']=$config;
		$data['fulluserList']=$this->user_model->get_full_user();
		$data['iconList'] = $this->icon_model->get_icon();
		$data['blog'] = $blog;
		$data['next'] = $this->blog_model->get_next_id($blog->ID_blog);
		$data['prev'] = $this->blog_model->get_prev_id($blog->ID_blog);
		$data['bookingList'] = $this->booking_model->get_booking();
		$data['aboutList']=$this->about_model->get_about();
		$data['contactList']=$this->contact_model->get_contact();
		$data['blogcategoryList']=$this->blogcategory_model->get_category();
		$data['blogtagsList']=$this->blogtags_model->get_tags();
		$data['recentblogList']=$this->blog_model->get_recentblog();
		$data['fullblogtagsList']=$this->blogtags_model->get_full_category();
		$data['fullblogcateList']=$this->blogcategory_model->get_full_category();

		$this->load->view("blog_detail_view", $data);
	}

	function category($id)
	{
		$this->load->model("user_model");
		$this->load->model("config_model");	
		$this->load->model("icon_model");
		$this->load->model("blog_model");
		$this->load->model("about_model");
		$this->load->model("contact_model");
		$this->load->model("booking_model");
		$this->load->model("blogcategory_model");
		$this->load->model("blogtags_model");
		
		$imgList=array();
		$img=array();
		
		$isLogin=$this->session->userdata("id");
		$config=$this->config_model->get_config();
		$blog = $this->blog_model->get_blog_by_category($id);
		if (!$isLogin)
			$data['id_admin']= 0;
		else
			$data['id_admin']= 2;
		
		$data['error']=0;
		$data['config']=$config;
		$data['fulluserList']=$this->user_model->get_full_user();
		$data['iconList'] = $this->icon_model->get_icon();
		$data['blog'] = $blog;
		// $data['next'] = $this->blog_model->get_next_id($blog->ID_blog);
		// $data['prev'] = $this->blog_model->get_prev_id($blog->ID_blog);
		$data['bookingList'] = $this->booking_model->get_booking();
		$data['aboutList']=$this->about_model->get_about();
		$data['contactList']=$this->contact_model->get_contact();
		$data['blogcategoryList']=$this->blogcategory_model->get_category();
		$data['blogtagsList']=$this->blogtags_model->get_tags();
		$data['recentblogList']=$this->blog_model->get_recentblog();
		$data['fullblogtagsList']=$this->blogtags_model->get_full_category();
		$data['fullblogcategoryList']=$this->blog_model->get_blog_category($id);
		$data['fullblogcateList']=$this->blogcategory_model->get_full_category();

		$this->load->view("blog_category_view", $data);
	}

	function tags($id)
	{
		$this->load->model("user_model");
		$this->load->model("config_model");	
		$this->load->model("icon_model");
		$this->load->model("blog_model");
		$this->load->model("about_model");
		$this->load->model("contact_model");
		$this->load->model("booking_model");
		$this->load->model("blogcategory_model");
		$this->load->model("blogtags_model");
		
		$imgList=array();
		$img=array();
		
		$isLogin=$this->session->userdata("id");
		$config=$this->config_model->get_config();
		$blog = $this->blog_model->get_blog_by_category($id);
		if (!$isLogin)
			$data['id_admin']= 0;
		else
			$data['id_admin']= 2;
		
		$data['error']=0;
		$data['config']=$config;
		$data['fulluserList']=$this->user_model->get_full_user();
		$data['iconList'] = $this->icon_model->get_icon();
		$data['blog'] = $blog;
		// $data['next'] = $this->blog_model->get_next_id($blog->ID_blog);
		// $data['prev'] = $this->blog_model->get_prev_id($blog->ID_blog);
		$data['bookingList'] = $this->booking_model->get_booking();
		$data['aboutList']=$this->about_model->get_about();
		$data['contactList']=$this->contact_model->get_contact();
		$data['blogcategoryList']=$this->blogcategory_model->get_category();
		$data['blogtagsList']=$this->blogtags_model->get_tags();
		$data['recentblogList']=$this->blog_model->get_recentblog();
		$data['fullblogtagsList']=$this->blogtags_model->get_full_category();
		$data['fullblogcategoryList']=$this->blog_model->get_blog_category($id);
		$data['fullblogcateList']=$this->blogcategory_model->get_full_category();

		$this->load->view("blog_tags_view", $data);
	}

}