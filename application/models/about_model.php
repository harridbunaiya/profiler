<?php
class About_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function get_about()
	{
		$query=$this->db->query("SELECT * FROM msabout");
		return $query->row();
	}

	function get_about_quote()
	{
		$query = $this->db->query("SELECT * FROM msabout_quote");
		return $query->row();
	}

	function get_about_title()
	{
		$query = $this->db->query("SELECT * FROM mstitle_master");
		return $query->row();
	}

	function get_about_home()
	{
		$query = $this->db->query("SELECT * FROM msabout_us");
		return $query->row();
	}
	
	function update_about($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE msabout SET content=".$dataToEdit['content'].",content2=".$dataToEdit['content2']
								);
		return $query;
	}

	function update_about1($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE msabout SET quote_text=".$dataToEdit['quote_text'].",simple_quote=".$dataToEdit['simple_quote']
								);
		return $query;
	}

	function update_about2($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE msabout SET box1=".$dataToEdit['box1'].",box2=".$dataToEdit['box2'].",box3=".$dataToEdit['box3'].",box4=".$dataToEdit['box4']
								);
		return $query;
	}

	function update_about_quote($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE msabout_quote SET quote_text=".$dataToEdit['quote_text'].",
													   content=".$dataToEdit['content'].",
													   title1=".$dataToEdit['title1'].",
													   subtitle1=".$dataToEdit['subtitle1'].",
													   title2=".$dataToEdit['title2'].",
													   subtitle2=".$dataToEdit['subtitle2']
								);
		return $query;
	}

	function update_about_title($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE mstitle_master SET title1=".$dataToEdit['title1'].",
															title2=".$dataToEdit['title2'].",
															title3=".$dataToEdit['title3'].",
															title4=".$dataToEdit['title4'].",
															title5=".$dataToEdit['title5'].",
															title6=".$dataToEdit['title6'].",
															subtitle1=".$dataToEdit['subtitle1']
								);
		return $query;
	}

	function update_about_home($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE msabout_us SET quote_text=".$dataToEdit['quote_text'].",
														simple_quote=".$dataToEdit['simple_quote'].",
													   	content=".$dataToEdit['content']
								);
		return $query;
	}
	
	function update_pic($path)
	{
		$query=$this->db->query("UPDATE msabout SET picture=".$path);
		return $query;		
	}

	function update_about_pict($img)
	{
		$query = $this->db->query("UPDATE msabout SET picture=".$img);
		return $query;
	}

	function update_pic_home($path)
	{
		$query=$this->db->query("UPDATE msabout_us SET picture=".$path);
		return $query;		
	}


}