<?php
class Blog_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_blog()
	{
		$query = $this->db->query("SELECT ID_blog, title, image, link, blog_text, creator, visible, created_date FROM msblog WHERE visible = 1 ORDER BY created_date DESC");
		return $query->result();
	}

	function get_recentblog()
	{
		$query = $this->db->query("SELECT ID_blog, title, image, link, blog_text, creator, visible, created_date FROM msblog WHERE visible = 1 ORDER BY created_date DESC LIMIT 4");
		return $query->result();
	}

	function get_limit_blog($x)
	{
		$query = $this->db->query("SELECT ID_blog, title, image, link, blog_text, creator, visible, created_date FROM msblog WHERE visible = 1 ORDER BY created_date DESC LIMIT ".$x);
		return $query->result();
	}

	function get_blog_by_id($id)
	{
		$query = $this->db->query("SELECT ID_blog, title, image, link, blog_text, creator, visible, created_date FROM msblog WHERE ID_blog = ".$id);
		return $query->row();
	}

	function get_blog_by_category($id)
	{
		$query = $this->db->query("SELECT ID_blog, title, image, link, blog_text, creator, visible, created_date FROM msblog WHERE ID_blogcategory = ".$id);
		return $query->row();
	}

	function get_blog_category($id)
	{
		$query = $this->db->query("SELECT ID_blog, title, image, link, blog_text, creator, visible, created_date FROM msblog WHERE ID_blogcategory = ".$id);
		return $query->result();
	}

	function get_blog_tags($id)
	{
		$query = $this->db->query("SELECT ID_blog, title, image, link, blog_text, creator, visible, created_date FROM msblog WHERE ID_blogcategory = ".$id);
		return $query->result();
	}

	function get_next_id($id)
	{
		$query = $this->db->query("SELECT ID_blog, title, image, link, blog_text, creator, visible, created_date FROM msblog WHERE ID_blog < ".$id." ORDER BY ID_blog DESC LIMIT 1");
		return $query->num_rows() > 0 ? $query->row()->ID_blog : "";
	}

	function get_prev_id($id)
	{
		$query = $this->db->query("SELECT ID_blog, title, image, link, blog_text, creator, visible, created_date FROM msblog WHERE ID_blog > ".$id." ORDER BY ID_blog ASC LIMIT 1");
		return $query->num_rows() > 0 ? $query->row()->ID_blog : "";
	}

	function get_full_blog()
	{
		$query = $this->db->query("SELECT i.ID_blog, i.ID_blogcategory, i.title, i.image, i.link, i.blog_text, i.creator, i.visible, i.created_date, d.blogcategory_name FROM msblog  AS i 
								   LEFT JOIN msblogcategory as d ON i.ID_blogcategory = d.ID_blogcategory
								   ORDER BY created_date DESC");
		return $query->result();
	}

	function get_by_key($key,$limit, $start)
	{
		$query = $this->db->query("SELECT i.ID_blog, i.ID_blogcategory, i.title, i.image, i.link, i.blog_text, i.creator, i.visible, i.created_date, d.blogcategory_name FROM msblog  AS i 
								   LEFT JOIN msblogcategory as d ON i.ID_blogcategory = d.ID_blogcategory
								   WHERE i.title LIKE '%".$key."%'
								   ORDER BY created_date DESC LIMIT ".$start.",".$limit);
								   
		return $query->result();
	}

	function delete_blog($id_blog)
	{
		$query=$this->db->query("DELETE FROM msblog WHERE ID_blog=".$id_blog);
		return $query;
	}

	function insert_blog($dataToInsert)
	{
		$query=$this->db->query("INSERT INTO msblog(title,image,link, ID_blogcategory, blog_text,creator,visible, created_date) VALUES (".$dataToInsert['title'].",".$dataToInsert['image'].",".$dataToInsert['link'].",".$dataToInsert['category'].",".$dataToInsert['blog_text'].",".$dataToInsert['creator'].",".$dataToInsert['visible'].",NOW())");
		return $query;
	}
	
	function update_blog($dataToEdit)
	{
		$query=$this->db->query("UPDATE msblog SET title=".$dataToEdit['title'].", link=".$dataToEdit['link'].", ID_blogcategory=".$dataToEdit['category'].", creator=".$dataToEdit['creator'].", visible=".$dataToEdit['visible'].", blog_text=".$dataToEdit['blog_text']." WHERE ID_blog=".$dataToEdit['ID_blog']);
		// echo "UPDATE msblog SET title=".$dataToEdit['title'].", link=".$dataToEdit['link'].", creator=".$dataToEdit['creator'].", visible=".$dataToEdit['visible'].", blog_text=".$dataToEdit['blog_text']." WHERE ID_blog=".$dataToEdit['ID_blog'];
		return $query;
	}

	function update_image($dataToEdit)
	{
		$query=$this->db->query("UPDATE msblog SET image=".$dataToEdit['image']." WHERE ID_blog=".$dataToEdit['ID_blog']);
		return $query;
	}
}