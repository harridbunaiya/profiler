<?php
class Blogcategory_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_category()
	{
		$query = $this->db->query("SELECT ID_blogcategory, seq_no, blogcategory_code, blogcategory_name, is_active, created_date FROM msblogcategory WHERE is_active = 1 ORDER BY seq_no ASC");
		return $query->result();
	}

	function get_category_by_id($id)
	{
		$query = $this->db->query("SELECT ID_blogcategory, seq_no, blogcategory_code, blogcategory_name, is_active, created_date FROM msblogcategory WHERE ID_blogcategory = ".$id." ORDER BY seq_no ASC");
		return $query->result();
	}

	function get_full_category()
	{
		$query = $this->db->query("SELECT ID_blogcategory, seq_no, blogcategory_code, blogcategory_name, is_active, created_date FROM msblogcategory ORDER BY seq_no ASC");
		return $query->result();
	}

	function insert_category($dataToInsert)
	{
		$index=$this->db->escape($this->get_index());
		$query = $this->db->query("INSERT INTO msblogcategory(seq_no, blogcategory_code, blogcategory_name, is_active, created_date) VALUES (".$index.",".$dataToInsert['blogcategory_code'].",".$dataToInsert['blogcategory_name'].",".$dataToInsert['is_active'].",NOW())");
		return $query;
	}

	function update_category($dataToEdit)
	{
		$query = $this->db->query("UPDATE msblogcategory SET blogcategory_code=".$dataToEdit['blogcategory_code'].",blogcategory_name=".$dataToEdit['blogcategory_name'].",is_active=".$dataToEdit['is_active']." WHERE ID_blogcategory=".$dataToEdit['ID_blogcategory']);
		return $query;
	}

	function delete_category($id)
	{
		$query = $this->db->query("DELETE FROM msblogcategory WHERE ID_blogcategory=".$id);
		return $query;
	}

	function index_up($blogcategoryid,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE msblogcategory SET seq_no=seq_no+1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msblogcategory SET seq_no=seq_no-1 WHERE ID_blogcategory=".$blogcategoryid);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_down($blogcategoryid,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE msblogcategory SET seq_no=seq_no-1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msblogcategory SET seq_no=seq_no+1 WHERE ID_blogcategory=".$blogcategoryid);
			
			return $query;
		}
		else
			return "";
	}

	function get_index()
	{
		$query=$this->db->query("SELECT MAX(seq_no) AS max_index FROM msblogcategory");
		$index=$query->row()->max_index+1;
		return $index;
	}
}