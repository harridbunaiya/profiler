<?php
class Contact_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function get_contact()
	{
		$query=$this->db->query("
									SELECT email, email2, contactMsg,mp1,location,langitude, latitude, corporation_name FROM mscontact
								");
		return $query->row();
	}
	
	function update_contact($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE mscontact SET email=".$dataToEdit['email'].",
														 email2=".$dataToEdit['email2'].",
														 mp1=".$dataToEdit['mp1'].",
														 corporation_name=".$dataToEdit['officeHours'].",
														 location=".$dataToEdit['location']
								);
		return $query;
	}

	function update_contactmsg($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE mscontact SET contactMsg=".$dataToEdit['contactMsg']
								);
		return $query;
	}

	function update_location($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE mscontact SET langitude=".$dataToEdit['langitude'].", latitude=".$dataToEdit["latitude"]
								);
		return $query;
	}
}