<?php
class Event_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	function get_new_id()
	{
		$query=$this->db->query("SELECT RIGHT(ID_events,7) AS ID FROM msevents ORDER BY ID_events DESC LIMIT 0,1");
		$res=$query->row();
		if ($res==NULL)
			$new=1;
		else
			$new=($res->ID+1);
			
		return "EVT".str_pad($new,7,"0",STR_PAD_LEFT);
	}
	
	public function insert_event($dataToInsert)
	{
		$id=$this->db->escape($this->get_new_id());
		$index=$this->db->escape($this->get_index());
		$query=$this->db->query("
									INSERT INTO msevents(ID_events,events_name,events_desc,events_date,visible,events_index, events_img)
									VALUES(".$id.",".$dataToInsert['events_name'].",".$dataToInsert['events_desc'].",NOW(),".$dataToInsert['visible'].",".$index.",".$dataToInsert['events_img'].")
								");				
		return $query;
	}
	
	function insert_layanan($dataToInsert)
	{
		$index=$this->db->escape($this->get_index());
		$query = $this->db->query("INSERT INTO msevents(events_name, events_img, events_desc, events_index, visible, events_date) VALUES (".$dataToInsert['layanan_title'].",".$dataToInsert['layanan_pict'].",".$dataToInsert['layanan_text'].",".$index.",".$dataToInsert['visible'].",NOW())");
		return $query;
	}
	
	function get_new_id_detail()
	{
		$query=$this->db->query("SELECT RIGHT(detailID,7) AS ID FROM trevents_detail ORDER BY detailID DESC LIMIT 0,1");
		$res=$query->row();
		if ($res==NULL)
			$new=1;
		else
			$new=($res->ID+1);
			
		return "IMG".str_pad($new,7,"0",STR_PAD_LEFT);
	}
	
	function get_index_detail($id_events)
	{
		$query=$this->db->query("SELECT MAX(imageIndex) AS max_index FROM trevents_detail WHERE ID_events=".$id_events);
		$index=$query->row()->max_index+1;
		return $index;
	}
	
	function insert_image($dataToInsert)
	{
		$detailid=$this->db->escape($this->get_new_id_detail());
		$index=$this->db->escape($this->get_index_detail($dataToInsert['ID_events']));
		$query=$this->db->query("INSERT INTO trevents_detail(detailID,ID_events,imagePath,imageTitle,imageIndex,imageDesc) VALUES (".$detailid.",".$dataToInsert['ID_events'].",".$dataToInsert['imagePath'].",".$dataToInsert['imageTitle'].",".$index.",".$dataToInsert['imageDesc'].")");
		return $query;
	}
	
	public function delete_event_image($detailid)
	{
		$query=$this->db->query("DELETE FROM trevents_detail WHERE detailID=".$detailid);				
		return $query;
	}
	
	function update_event_image($dataToEdit)
	{
		$query=$this->db->query("UPDATE trevents_detail SET imagePath=".$dataToEdit['imagePath']." WHERE detailID=".$dataToEdit['detailID']);
		return $query;
	}
	
	function update_image_title_desc($dataToEdit)
	{
		$query=$this->db->query("UPDATE trevents_detail SET imageTitle=".$dataToEdit['imageTitle'].", imageDesc=".$dataToEdit['imageDesc']." WHERE detailID=".$dataToEdit['detailID']);

		return $query;
	}
	
	function index_image_up($id_detail,$id_events,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE trevents_detail SET imageIndex=imageIndex+1 WHERE imageIndex=".$index." AND ID_events=".$id_events);
			$query=$this->db->query("UPDATE trevents_detail SET imageIndex=imageIndex-1 WHERE detailID=".$id_detail);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_image_down($id_detail,$id_events,$index)
	{
		if ($index<$this->get_index_detail($id_events))
		{
			$query=$this->db->query("UPDATE trevents_detail SET imageIndex=imageIndex-1 WHERE imageIndex=".$index." AND ID_events=".$id_events);
			$query=$this->db->query("UPDATE trevents_detail SET imageIndex=imageIndex+1 WHERE detailID=".$id_detail);
			
			return $query;
		}
		else
			return "";
	}
	
	public function get_event_detail() 
	{
		$query=$this->db->query("SELECT detailID, ID_events, imagePath, imageTitle, imageDesc,
		imageIndex FROM trevents_detail ORDER BY ID_events ASC");				
		$result = $query->result();
		$prev = "";
		$detail = array();
		foreach ($result as $res) :
			if ($res->ID_events != $prev) {
				$detail[$res->ID_events] = array();
			}
			array_push($detail[$res->ID_events], $res);
			$prev = $res->ID_events; 
		endforeach;
		return $detail;
	}
	
	public function get_all_event()
	{
		$query=$this->db->query("SELECT ID_events,events_name, events_img, events_desc,
		DATE_FORMAT(events_date,'%Y-%m-%d') AS events_date,events_index,visible FROM msevents ORDER BY events_index DESC");				
		return $query->result();

	}
	
	public function get_visible_event()
	{
		$query=$this->db->query("SELECT ID_events,events_name, events_img, events_desc,
		DATE_FORMAT(events_date,'%Y-%m-%d') AS events_date,events_index,visible FROM msevents WHERE visible=1 ORDER BY events_index DESC");				
		return $query->result();
	}
	
	public function get_full_image()
	{
		$query=$this->db->query("SELECT detailID,ID_events,imagePath,imageTitle,imageIndex,imageDesc FROM trevents_detail  ORDER BY ID_events ASC, imageIndex ASC");				
		return $query->result();
	}
	
	public function get_all_image($id_event)
	{
		$query=$this->db->query("SELECT detailID,ID_events,imagePath,imageTitle,imageIndex,imageDesc FROM trevents_detail WHERE ID_events=".$id_event." ORDER BY imageIndex ASC");				
		return $query->result();
	}
	
	public function update_event($dataToEdit)
	{
		$query=$this->db->query("UPDATE msevents SET events_name=".$dataToEdit['events_name'].",events_desc=".$dataToEdit['events_desc'].",visible=".$dataToEdit['visible']." WHERE ID_events=".$dataToEdit['ID_events']);
		
		return $query;
	}

	function update_layanan($dataToEdit)
	{
		$query=$this->db->query("UPDATE msevents SET events_name=".$dataToEdit['name'].", events_img=".$dataToEdit['pict'].", events_desc=".$dataToEdit['text'].", visible=".$dataToEdit['visible']." WHERE ID_events=".$dataToEdit['layananID']);
		return $query;
	}

	function update_event_picture($dataToEdit)
	{
		$query=$this->db->query("UPDATE msevents SET events_img=".$dataToEdit['events_img']." WHERE ID_events=".$dataToEdit['ID_events']);
		return $query;
	}
	
	
	function get_index()
	{
		$query=$this->db->query("SELECT MAX(events_index) AS max_index FROM msevents");
		$index=$query->row()->max_index+1;
		return $index;
	}
	
	function index_up($id_event,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE msevents SET events_index=events_index+1 WHERE events_index=".$index);
			$query=$this->db->query("UPDATE msevents SET events_index=events_index-1 WHERE ID_events=".$id_event);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_down($id_event,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE msevents SET events_index=events_index-1 WHERE events_index=".$index);
			$query=$this->db->query("UPDATE msevents SET events_index=events_index+1 WHERE ID_events=".$id_event);
			
			return $query;
		}
		else
			return "";
	}
	
	public function delete_event($id_event)
	{
		$query=$this->db->query("DELETE FROM msevents WHERE ID_events=".$id_event);	
		$query=$this->db->query("DELETE FROM trevents_detail WHERE ID_events=".$id_event);				
		return $query;
	}

	function delete_layanan($id)
	{
		$query = $this->db->query("DELETE FROM msevents WHERE ID_events=".$id);
		return $query;
	}
	
	public function __destruct()
	{
	}
}