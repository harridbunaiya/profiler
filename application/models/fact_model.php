<?php
class Fact_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_index()
	{
		$query=$this->db->query("SELECT MAX(seq_no) AS max_index FROM msfact");
		$index=$query->row()->max_index+1;
		return $index;
	}

	function get_fact()
	{
		$query = $this->db->query("SELECT * FROM msfact");
		return $query->result();
	}

	function get_visible_fact()
	{
		$query = $this->db->query("SELECT * FROM msfact WHERE is_active=1 ORDER BY seq_no ASC");
		return $query->result();
	}

	function get_full_fact()
	{
		$query=$this->db->query("SELECT * FROM msfact ORDER BY seq_no ASC");
		return $query->result();
	}

	function update_fact($dataToUpdate)
	{
		$query = $this->db->query("UPDATE msfact SET awards = ".$dataToUpdate["awards"].", 
													 clients = ".$dataToUpdate["client"].",
													 projects = ".$dataToUpdate["project"].",
													 teams = ".$dataToUpdate["team"]);
		return $query;
	}

	function update_fact_name($dataToEdit)
	{
		$query=$this->db->query("UPDATE msfact SET fact_name=".$dataToEdit['fact_name']." WHERE ID_fact=".$dataToEdit['ID_fact']);
		return $query;
	}

	function update_image($dataToEdit)
	{
		$query=$this->db->query("UPDATE msfact SET bg_pict=".$dataToEdit['bg_pict']." WHERE ID_fact=".$dataToEdit['ID_fact']);
		return $query;
	}

	function insert_fact($dataToInsert)
	{
		$index=$this->db->escape($this->get_index());
		$query=$this->db->query("INSERT INTO msfact(bg_pict, fact_name, seq_no, is_active, created_date) VALUES (".$dataToInsert['bg_pict'].",".$dataToInsert['fact_name'].", ".$index.",".$dataToInsert['is_active'].", NOW())");
		return $query;
	}

	function delete_fact($id_fact)
	{
		$query=$this->db->query("DELETE FROM msfact WHERE ID_fact=".$id_fact);
		return $query;
	}

	function index_up($factid,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE msfact SET seq_no=seq_no+1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msfact SET seq_no=seq_no-1 WHERE ID_fact=".$factid);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_down($factid,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE msfact SET seq_no=seq_no-1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msfact SET seq_no=seq_no+1 WHERE ID_fact=".$factid);
			
			return $query;
		}
		else
			return "";
	}
}