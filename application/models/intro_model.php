<?php
class Intro_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function get_intro()
	{
		$query=$this->db->query("
									SELECT * FROM msintro_image
								");
		return $query->row();
	}
	
	function update_contact($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE msintro_image SET content=".$dataToEdit['content']
								);
		return $query;
		
	}
}