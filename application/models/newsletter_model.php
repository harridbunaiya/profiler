<?php
class newsletter_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	function get_newslettervisit()
	{
		$query= $this->db->query("SELECT msnewsletter.ID_newsletter, msnewsletter.seq_no, msnewsletter.contact_email, msnewsletter.visible, msnewsletter.created_date, count(1) as coun
			from msnewsletter ORDER BY msnewsletter.ID_newsletter ASC ");
		return $query->result();
	}

	function get_index()
	{
		$query=$this->db->query("SELECT MAX(ID_newsletter) AS max_index FROM msnewsletter");
		$index=$query->row()->max_index+1;
		return $index;
	}


	function insert_newslettervisit($dataToInsert)
	{
		$index=$this->db->escape($this->get_index());
		$query = $this->db->query("INSERT INTO msnewsletter(ID_newsletter, seq_no, contact_email,visible,created_date) VALUES (".$index.",".$index.",".$dataToInsert['contact_email'].",0,NOW())");
		return $query;
	}

	function get_newsletter()
	{
		$query = $this->db->query("SELECT ID_newsletter, seq_no, contact_email, visible, created_date FROM msnewsletter ORDER BY seq_no ASC");
		return $query->result();
	}

	function insert_newsletter($dataToInsert)
	{
		$index=$this->db->escape($this->get_index());
		$query = $this->db->query("INSERT INTO msnewsletter(contact_email, seq_no, visible, created_date) VALUES (".$dataToInsert['contact_email'].",".$index.",".$dataToInsert['visible'].",NOW())");
		return $query;
	}

	function update_newsletter($dataToEdit)
	{
		$query = $this->db->query("UPDATE msnewsletter SET 
			contact_email=".$dataToEdit['contact_email'].",visible=".$dataToEdit['visible']." WHERE ID_newsletter=".$dataToEdit['ID_newsletter']);
		return $query;
	}

	function delete_newsletter($id)
	{
		$query = $this->db->query("DELETE FROM msnewsletter WHERE ID_newsletter=".$id);
		return $query;
	}

	function index_up($howid,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE msnewsletter SET seq_no=seq_no+1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msnewsletter SET seq_no=seq_no-1 WHERE ID_newsletter=".$howid);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_down($howid,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE msnewsletter SET seq_no=seq_no-1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msnewsletter SET seq_no=seq_no+1 WHERE ID_newsletter=".$howid);
			
			return $query;
		}
		else
			return "";
	}
}