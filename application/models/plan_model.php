<?php
class Plan_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_plan()
	{
		$query = $this->db->query("SELECT * FROM msplan_header ORDER BY seq_no ASC");
		return $query->result();
	}

	function get_plan_by_id($id)
	{
		$query = $this->db->query("SELECT * FROM msplan_header WHERE ID_plan = ".$this->db->escape($id));
		return $query->row();
	}

	function get_top_plan($num)
	{
		$query = $this->db->query("SELECT ID_plan, seq_no, plan_name, plan_img, currency, price, per_each, created_date FROM msplan_header ORDER BY seq_no ASC LIMIT ".$num);
		return $query->result();
	}


	function get_plan_detail()
	{
		$query = $this->db->query("SELECT * FROM msplan_detail ORDER BY ID_plan ASC, seq_no ASC");
		$result = $query->result();
		$prev = "";
		$detail = array();
		foreach ($result as $res) :
			if ($res->ID_plan != $prev) {
				$detail[$res->ID_plan] = array();
			}
			array_push($detail[$res->ID_plan], $res);
			$prev = $res->ID_plan; 
		endforeach;
		return $detail;
	}

	function get_plan_detail_by_id($ID_plan)
	{
		$query = $this->db->query("SELECT * FROM msplan_detail WHERE ID_plan = '".$ID_plan."' ORDER BY ID_plan ASC, seq_no ASC");
		$result = $query->result();
		$prev = "";
		$detail = array();
		foreach ($result as $res) :
			if ($res->ID_plan != $prev) {
				$detail[$res->ID_plan] = array();
			}
			array_push($detail[$res->ID_plan], $res);
			$prev = $res->ID_plan; 
		endforeach;
		return $detail;
	}

	function get_plan_detail_by_id2($ID_plan)
	{
		$query = $this->db->query("SELECT * FROM msplan_detail WHERE ID_plan = '".$ID_plan."' ORDER BY ID_plan ASC, seq_no ASC");
		$result = $query->result();
		return $result;
	}

	function get_plan_detail_opsi($ID_plan)
	{
		$query = $this->db->query("SELECT * FROM msplan_detail WHERE ID_plan = '".$ID_plan."' && kategori = 'opsi' ORDER BY ID_plan ASC, seq_no ASC");
		// $result = $query->result();
		return $query->result();
	}

	function get_plan_detail_lebar($ID_plan)
	{
		$query = $this->db->query("SELECT * FROM msplan_detail WHERE ID_plan = '".$ID_plan."' && kategori = 'lebar' ORDER BY ID_plan ASC, seq_no ASC");
		// $result = $query->result();
		return $query->result();
	}

	function get_plan_detail_panjang($ID_plan)
	{
		$query = $this->db->query("SELECT * FROM msplan_detail WHERE ID_plan = '".$ID_plan."' && kategori = 'panjang' ORDER BY ID_plan ASC, seq_no ASC");
		// $result = $query->result();
		return $query->result();
	}

	function get_plan_detail_aksesoris($ID_plan)
	{
		$query = $this->db->query("SELECT * FROM msplan_detail WHERE ID_plan = '".$ID_plan."' && kategori = 'aksesoris' ORDER BY ID_plan ASC, seq_no ASC");
		// $result = $query->result();
		return $query->result();
	}

	function insert_plan($dataToInsert)
	{
		$index=$this->db->escape($this->get_index());
		echo "INSERT INTO msplan_header(seq_no, plan_name, plan_detail, spec_title, spec_desc, created_date, plan_img) VALUES (".$index.",".$dataToInsert['plan_name'].",".$dataToInsert['subtitle'].",".$dataToInsert['spec'].",".$dataToInsert['desc'].",NOW(),".$dataToInsert['plan_image'].")";

		$query = $this->db->query("INSERT INTO msplan_header(seq_no, plan_name, plan_detail, spec_title, spec_desc, created_date, plan_img) VALUES (".$index.",".$dataToInsert['plan_name'].",".$dataToInsert['subtitle'].",".$dataToInsert['spec'].",".$dataToInsert['desc'].",NOW(),".$dataToInsert['plan_image'].")");

		
		return $query;
	}

	function insert_plan_detail($dataToInsert)
	{
		$index=$this->db->escape($this->get_index_detail($dataToInsert["id_plan"]));
		$query = $this->db->query("INSERT INTO msplan_detail(id_plan, seq_no, detail_text, kategori, imagePath, created_date) VALUES (".$dataToInsert['id_plan'].",".$index.",".$dataToInsert['detail_text'].",".$dataToInsert['detailkat'].",".$dataToInsert['imagePath'].",NOW())");
		// echo "INSERT INTO msplan_detail(id_plan, seq_no, detail_text, kategori, imagePath, created_date) VALUES (".$dataToInsert['id_plan'].",".$index.",".$dataToInsert['detail_text'].",".$dataToInsert['detailkat'].",".$dataToInsert['imagePath'].",NOW())";
		return $query;
	}

	function update_plan($dataToEdit)
	{
		$query = $this->db->query("UPDATE msplan_header SET plan_name=".$dataToEdit['plan_name'].",plan_detail=".$dataToEdit['subtitle'].",spec_title=".$dataToEdit['spec'].",spec_desc=".$dataToEdit['desc']." WHERE ID_plan=".$dataToEdit['ID_plan']);
		return $query;
	}

	function update_plan_image($dataToEdit)
	{
		echo "UPDATE msplan_header SET plan_img = ".$dataToEdit['plan_image']." WHERE ID_plan=".$dataToEdit['ID_plan'];
		$query = $this->db->query("UPDATE msplan_header SET plan_img = ".$dataToEdit['plan_image']." WHERE ID_plan=".$dataToEdit['ID_plan']);

		return $query;
	}

	function delete_plan($id)
	{
		$query = $this->db->query("DELETE FROM msplan_header WHERE ID_plan=".$id);
		return $query;
	}

	function delete_detail_plan($id)
	{
		$query = $this->db->query("DELETE FROM msplan_detail WHERE ID_plan_detail=".$id);
		return $query;
	}

	function get_index()
	{
		$query=$this->db->query("SELECT MAX(seq_no) AS max_index FROM msplan_header");
		$index=$query->row()->max_index+1;
		return $index;
	}

	function get_index_detail($id_plan)
	{
		$query=$this->db->query("SELECT MAX(seq_no) AS max_index FROM msplan_detail where id_plan=".$id_plan);
		$index=$query->row()->max_index+1;
		return $index;
	}

	function index_up($planid,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE msplan_header SET seq_no=seq_no+1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msplan_header SET seq_no=seq_no-1 WHERE ID_plan=".$planid);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_down($planid,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE msplan_header SET seq_no=seq_no-1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msplan_header SET seq_no=seq_no+1 WHERE ID_plan=".$planid);
			
			return $query;
		}
		else
			return "";
	}

	function index_detail_up($detailid,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE msplan_detail SET seq_no=seq_no+1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msplan_detail SET seq_no=seq_no-1 WHERE ID_plan_detail=".$detailid);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_detail_down($detailid,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE msplan_detail SET seq_no=seq_no-1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE msplan_detail SET seq_no=seq_no+1 WHERE ID_plan_detail=".$detailid);
			
			return $query;
		}
		else
			return "";
	}
}