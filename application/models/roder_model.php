<?php
class Roder_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function get_roder()
	{
		$query=$this->db->query("SELECT * FROM msroder");
		return $query->row();
	}
	
	function update_roder($dataToEdit)
	{
		$query=$this->db->query("
									UPDATE msroder SET title=".$dataToEdit['title'].",
														subtitle=".$dataToEdit['subtitle'].",
														spec_title=".$dataToEdit['spec_title'].",
														spec_desc=".$dataToEdit['spec_desc']
								);
		return $query;
	}

	function update_pic($path)
	{
		$query=$this->db->query("UPDATE msroder SET picture=".$path);
		return $query;		
	}

	function update_roder_pict($img)
	{
		$query = $this->db->query("UPDATE msroder SET picture=".$img);
		return $query;
	}

	// opsi roder

	function get_new_id_opsi()
	{
		$query=$this->db->query("SELECT RIGHT(id,7) AS ID FROM msroder ORDER BY id DESC LIMIT 0,1");
		$res=$query->row();
		if ($res==NULL)
			$new=1;
		else
			$new=($res->ID+1);
			
		return "IMG".str_pad($new,7,"0",STR_PAD_LEFT);
	}
	
	function get_opsi($imageID)
	{
		$query=$this->db->query("SELECT * FROM msroder WHERE id=".$username);
		return $query;
	}
	
	function get_full_opsi()
	{
		$query=$this->db->query("SELECT * FROM msroder ORDER BY imageIndex");
		return $query->result();
	}	
	
	function get_visible_opsi()
	{
		$query=$this->db->query("SELECT * FROM msroder WHERE visible=1 ORDER BY imageIndex");
		return $query->result();
	}	
	
	function get_index_opsi()
	{
		$query=$this->db->query("SELECT MAX(imageIndex) AS max_index FROM msroder");
		$index=$query->row()->max_index+1;
		return $index;
	}
	
	function insert_opsi($dataToInsert)
	{
		$imageid=$this->db->escape($this->get_new_id_opsi());
		$index=$this->db->escape($this->get_index_opsi());
		$query=$this->db->query("INSERT INTO msroder(id,imagePath,title,imageIndex,visible) VALUES (".$imageid.",".$dataToInsert['imagePath'].",".$dataToInsert['imageTitle'].",".$index.",".$dataToInsert['visible'].")");
		return $query;
	}
	
	function update_opsi($dataToEdit)
	{
		$query=$this->db->query("UPDATE msroder SET title=".$dataToEdit['imageTitle'].", visible=".$dataToEdit['visible']." WHERE id=".$dataToEdit['imageID']);
		return $query;
	}
	
	function update_image_opsi($dataToEdit)
	{
		$query=$this->db->query("UPDATE msroder SET imagePath=".$dataToEdit['imagePath']." WHERE id=".$dataToEdit['imageID']);
		return $query;
	}
		
	function delete_image_opsi($id_image)
	{
		$query=$this->db->query("DELETE FROM msroder WHERE id=".$id_image);
		return $query;
	}
	
	function index_down_opsi($imageid,$index)
	{
		if ($index<$this->get_index_opsi())
		{
			$query=$this->db->query("UPDATE msroder SET imageIndex=imageIndex-1 WHERE imageIndex=".$index);
			$query=$this->db->query("UPDATE msroder SET imageIndex=imageIndex+1 WHERE id=".$imageid);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_up_opsi($imageid,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE msroder SET imageIndex=imageIndex+1 WHERE imageIndex=".$index);
			$query=$this->db->query("UPDATE msroder SET imageIndex=imageIndex-1 WHERE id=".$imageid);
			
			return $query;
		}
		else
			return "";
	}



}