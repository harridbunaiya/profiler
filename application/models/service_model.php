<?php
class Service_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	function get_new_id()
	{
		$query=$this->db->query("SELECT RIGHT(ID_services,7) AS ID FROM msservice ORDER BY ID_services DESC LIMIT 0,1");
		$res=$query->row();
		if ($res==NULL)
			$new=1;
		else
			$new=($res->ID+1);
			
		return "EVT".str_pad($new,7,"0",STR_PAD_LEFT);
	}
	
	public function insert_service($dataToInsert)
	{
		$id=$this->db->escape($this->get_new_id());
		$index=$this->db->escape($this->get_index());
		$query=$this->db->query("
									INSERT INTO msservice(ID_services,services_name,services_desc,services_date,visible,services_index, services_img)
									VALUES(".$id.",".$dataToInsert['service_title'].",".$dataToInsert['service_text'].",NOW(),".$dataToInsert['visible'].",".$index.",".$dataToInsert['service_pict'].")
								");				
		return $query;
	}
	
	
	public function delete_service_image($detailid)
	{
		$query=$this->db->query("DELETE FROM trevents_detail WHERE detailID=".$detailid);				
		return $query;
	}
	
	function update_event_image($dataToEdit)
	{
		$query=$this->db->query("UPDATE trevents_detail SET imagePath=".$dataToEdit['imagePath']." WHERE detailID=".$dataToEdit['detailID']);
		return $query;
	}
	
	function update_image_title_desc($dataToEdit)
	{
		$query=$this->db->query("UPDATE trevents_detail SET imageTitle=".$dataToEdit['imageTitle'].", imageDesc=".$dataToEdit['imageDesc']." WHERE detailID=".$dataToEdit['detailID']);

		return $query;
	}
	
	function index_image_up($id_detail,$id_events,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE trevents_detail SET imageIndex=imageIndex+1 WHERE imageIndex=".$index." AND ID_events=".$id_events);
			$query=$this->db->query("UPDATE trevents_detail SET imageIndex=imageIndex-1 WHERE detailID=".$id_detail);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_image_down($id_detail,$id_events,$index)
	{
		if ($index<$this->get_index_detail($id_events))
		{
			$query=$this->db->query("UPDATE trevents_detail SET imageIndex=imageIndex-1 WHERE imageIndex=".$index." AND ID_events=".$id_events);
			$query=$this->db->query("UPDATE trevents_detail SET imageIndex=imageIndex+1 WHERE detailID=".$id_detail);
			
			return $query;
		}
		else
			return "";
	}
	
	public function get_service_detail() 
	{
		$query=$this->db->query("SELECT detailID, ID_events, imagePath, imageTitle, imageDesc,
		imageIndex FROM trevents_detail ORDER BY ID_events ASC");				
		$result = $query->result();
		$prev = "";
		$detail = array();
		foreach ($result as $res) :
			if ($res->ID_events != $prev) {
				$detail[$res->ID_events] = array();
			}
			array_push($detail[$res->ID_events], $res);
			$prev = $res->ID_events; 
		endforeach;
		return $detail;
	}
	
	public function get_all_service()
	{
		$query=$this->db->query("SELECT ID_services,services_name, services_img, services_desc,
		DATE_FORMAT(services_date,'%Y-%m-%d') AS services_date,services_index,visible FROM msservice ORDER BY services_index ASC");				
		return $query->result();

	}
	
	public function get_visible_service()
	{
		$query=$this->db->query("SELECT ID_services,services_name, services_img, services_desc,
		DATE_FORMAT(services_date,'%Y-%m-%d') AS services_date,services_index,visible FROM msservice WHERE visible=1 ORDER BY services_index ASC");				
		return $query->result();
	}
	
	
	public function update_service($dataToEdit)
	{
		$query=$this->db->query("UPDATE msservice SET services_name=".$dataToEdit['name'].",services_desc=".$dataToEdit['text'].",services_img=".$dataToEdit['pict'].",visible=".$dataToEdit['visible']." WHERE ID_services=".$dataToEdit['serviceID']);
		
		return $query;
	}

	function update_service_picture($dataToEdit)
	{
		$query=$this->db->query("UPDATE msservice SET services_img=".$dataToEdit['services_img']." WHERE ID_services=".$dataToEdit['ID_services']);
		return $query;
	}
	
	
	function get_index()
	{
		$query=$this->db->query("SELECT MAX(services_index) AS max_index FROM msservice");
		$index=$query->row()->max_index+1;
		return $index;
	}
	
	function index_up($id_service,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE msservice SET services_index=services_index+1 WHERE services_index=".$index);
			$query=$this->db->query("UPDATE msservice SET services_index=services_index-1 WHERE ID_services=".$id_service);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_down($id_service,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE msservice SET services_index=services_index-1 WHERE services_index=".$index);
			$query=$this->db->query("UPDATE msservice SET services_index=services_index+1 WHERE ID_services=".$id_service);
			
			return $query;
		}
		else
			return "";
	}
	
	public function delete_service($id_service)
	{
		$query=$this->db->query("DELETE FROM msservice WHERE ID_services=".$id_service);	
		// $query=$this->db->query("DELETE FROM trservices_detail WHERE ID_services=".$id_service);				
		return $query;
	}
	
}