<?php
class Tendaservice_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_tenda_service()
	{
		$query = $this->db->query("SELECT * FROM mstenda_service ORDER BY seq_no ASC");
		return $query->result();
	}

	function delete_tenda_service($id)
	{
		$query = $this->db->query("DELETE FROM mstenda_service WHERE id=".$id);
		return $query;
	}

	function insert_tenda_service($dataToInsert)
	{
		$index=$this->db->escape($this->get_index());
		$query = $this->db->query("INSERT INTO mstenda_service(seq_no, title, subtitle, created_date) VALUES (".$index.",".$dataToInsert['title'].",".$dataToInsert['subtitle'].",NOW())");
		return $query;
	}
	
	function update_tenda_service($dataToEdit)
	{
		$query = $this->db->query("UPDATE mstenda_service SET title=".$dataToEdit['title'].",subtitle=".$dataToEdit['subtitle']." WHERE id=".$dataToEdit['id']);
		return $query;
	}

	function update_image($dataToEdit)
	{
		$query=$this->db->query("UPDATE mstenda_service SET how_pict=".$dataToEdit['how_pict']." WHERE id=".$dataToEdit['id']);
		return $query;
	}

	function get_index()
	{
		$query=$this->db->query("SELECT MAX(seq_no) AS max_index FROM mstenda_service");
		$index=$query->row()->max_index+1;
		return $index;
	}

	function index_up($howid,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE mstenda_service SET seq_no=seq_no+1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE mstenda_service SET seq_no=seq_no-1 WHERE id=".$howid);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_down($howid,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE mstenda_service SET seq_no=seq_no-1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE mstenda_service SET seq_no=seq_no+1 WHERE id=".$howid);
			
			return $query;
		}
		else
			return "";
	}
}