<?php
class Testimoni_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_testimoni()
	{
		$query = $this->db->query("SELECT ID_testimoni, seq_no, person_name, person_photo, testimoni_text, corporation_name, created_date FROM mstestimoni ORDER BY seq_no DESC");
		return $query->result();
	}

	function get_recent_testimoni()
	{
		$query = $this->db->query("SELECT ID_testimoni, seq_no, person_name, person_photo, testimoni_text, corporation_name, created_date FROM mstestimoni ORDER BY seq_no DESC LIMIT 6 ");
		return $query->result();
	}

	function insert_testimoni($dataToInsert)
	{
		$index=$this->db->escape($this->get_index());
		$query = $this->db->query("INSERT INTO mstestimoni(person_name, seq_no, corporation_name, testimoni_text, person_photo, created_date) VALUES (".$dataToInsert['person_name'].",".$index.",".$dataToInsert['corporation_name'].",".$dataToInsert['testimoni_text'].",".$dataToInsert['person_photo'].",NOW())");
		return $query;
	}

	function update_testimoni($dataToEdit)
	{
		$query = $this->db->query("UPDATE mstestimoni SET person_name=".$dataToEdit['person_name'].",corporation_name=".$dataToEdit['corporation_name'].",testimoni_text=".$dataToEdit['testimoni_text']." WHERE ID_testimoni=".$dataToEdit['ID_testimoni']);
		return $query;
	}

	function update_image($dataToEdit)
	{
		$query=$this->db->query("UPDATE mstestimoni SET person_photo=".$dataToEdit['person_photo']." WHERE ID_testimoni=".$dataToEdit['ID_testimoni']);
		return $query;
	}

	function delete_testimoni($id)
	{
		$query = $this->db->query("DELETE FROM mstestimoni WHERE ID_testimoni=".$id);
		return $query;
	}

	function get_index()
	{
		$query=$this->db->query("SELECT MAX(seq_no) AS max_index FROM mstestimoni");
		$index=$query->row()->max_index+1;
		return $index;
	}

	function index_up($howid,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE mstestimoni SET seq_no=seq_no+1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE mstestimoni SET seq_no=seq_no-1 WHERE ID_testimoni=".$howid);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_down($howid,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE mstestimoni SET seq_no=seq_no-1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE mstestimoni SET seq_no=seq_no+1 WHERE ID_testimoni=".$howid);
			
			return $query;
		}
		else
			return "";
	}
}