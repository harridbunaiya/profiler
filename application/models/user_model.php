<?php
class User_model extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		
	}
	
	function get_user($username)
	{
		$query=$this->db->query("SELECT ID_admin, pass,admin_name,email,active FROM msadmin WHERE ID_admin=".$username);	
		return $query->result();
	}
	
	function forgot_user($username,$email)
	{
		$query=$this->db->query("SELECT pass FROM msadmin WHERE ID_admin=".$username." AND email=".$email);	
		
		if ($query->row())
			return $query->row()->pass;
		else
			return '';
	}
	
	function get_full_user()
	{
		$query=$this->db->query("SELECT ID_admin, pass,admin_name,email,created_date,active FROM msadmin");
		return $query->result();
	}
	
	
	function insert_user($dataToInsert)
	{
		$query=$this->db->query("INSERT INTO msadmin(ID_admin,pass,admin_name,email,created_date,active) VALUES (".$dataToInsert['ID_admin'].",".$dataToInsert['pass'].",".$dataToInsert['admin_name'].",".$dataToInsert['email'].",NOW(),".$dataToInsert['active'].")");
		return $query;
	}
	
	function update_user($dataToEdit)
	{
		$query=$this->db->query("UPDATE msadmin SET admin_name=".$dataToEdit['admin_name'].",email=".$dataToEdit['email'].",active=".$dataToEdit['active']." WHERE ID_admin=".$dataToEdit['ID_admin']);
		return $query;
	}
	
	function change_pass($id_user,$pass)
	{
		$query=$this->db->query("UPDATE msadmin SET pass=".$pass." WHERE ID_admin=".$id_user);
		return $query;
	}
	
	function delete_user($id_admin)
	{
		$query=$this->db->query("DELETE FROM msadmin WHERE ID_admin=".$id_admin);
		return $query;
	}
}