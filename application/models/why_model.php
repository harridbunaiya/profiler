<?php
class Why_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();	
	}

	function get_why()
	{
		$query = $this->db->query("SELECT ID_why, seq_no, why_side, why_title, why_pict, why_text, created_date FROM mswhy ORDER BY seq_no ASC");
		return $query->result();
	}

	function insert_why($dataToInsert)
	{
		$index=$this->db->escape($this->get_index());
		$query = $this->db->query("INSERT INTO mswhy(why_pict, seq_no, why_side, why_title, why_text, created_date) VALUES (".$dataToInsert['why_pict'].",".$index.",".$dataToInsert['why_side'].",".$dataToInsert['why_title'].",".$dataToInsert['why_text'].",NOW())");
		return $query;
	}

	function get_index()
	{
		$query=$this->db->query("SELECT MAX(seq_no) AS max_index FROM mswhy");
		$index=$query->row()->max_index+1;
		return $index;
	}

	function update_why($dataToEdit)
	{
		$query = $this->db->query("UPDATE mswhy SET why_pict=".$dataToEdit['why_pict'].",why_side=".$dataToEdit['why_side'].",why_title=".$dataToEdit['why_title'].",why_text=".$dataToEdit['why_text']." WHERE ID_why=".$dataToEdit['ID_why']);
		return $query;
	}

	function delete_why($id)
	{
		$query = $this->db->query("DELETE FROM mswhy WHERE ID_why=".$id);
		return $query;
	}

	function index_up($whyid,$index)
	{
		if ($index>0)
		{
			$query=$this->db->query("UPDATE mswhy SET seq_no=seq_no+1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE mswhy SET seq_no=seq_no-1 WHERE ID_why=".$whyid);
			
			return $query;
		}
		else
			return "";
	}
	
	function index_down($whyid,$index)
	{
		if ($index<$this->get_index())
		{
			$query=$this->db->query("UPDATE mswhy SET seq_no=seq_no-1 WHERE seq_no=".$index);
			$query=$this->db->query("UPDATE mswhy SET seq_no=seq_no+1 WHERE ID_why=".$whyid);
			
			return $query;
		}
		else
			return "";
	}
}