<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title><?php echo $config->title; ?></title>
        <!-- <title>Roker  - Software, Technology , Corporate, Creative, Multi-Purpose, Responsive And Retina Html5/css3 Template</title>  -->
        <meta name="keywords" content="conculta" />
        <meta name="description" content="Roker  - Corporate, Creative, Multi Purpose, Responsive And Retina Template">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <?php include "include/include_css.php" ?>  

        <!-- Head Libs -->
        <script src="<?php echo base_url() ?>js2/modernizr.js"></script>

    </head>
    <body> 

    <?php include "include/cropping_modal.php" ?> 
    <?php include "include/text_modal.php" ?> 

    <!--Preloader-->
    <div id="preloader">
        <!-- <div class="status">&nbsp;</div> -->
    </div>
    <!--End Preloader-->

    <!-- Hidden Value -->
    <input type="hidden" id="hid_login" name="hid_login" value="<?php echo $id_admin;?>" />
    
    <!-- layout-->
    <div id="layout" class="layout-wide">

        <?php include "include/header.php" ?> 
        <main>
        <section class="section-base">
            <div class="container">
                <div class="title text-center">
                    <h1>Tentang Tenda Guna</h1>
                </div>
                <div class="row row-fit-lg">
                    <div class="pull-left div-settings dnone" style="position:absolute;z-index:999; top: 100px;">
                        <img title="Edit" id="btedit-about" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        <img title="Save" id="btsave-about" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                        <img title="Cancel" id="btcancel-about" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                    </div>
                    <div class="col-lg-6">
                        <p class="about-text" id="about-text">
                            <?php echo nl2br(htmlspecialchars_decode($aboutList->content)); ?>
                        </p>
                        <div class="edit-about">
                            <textarea class="text-about fluid" id="txtcontent" name="txtcontent"><?php echo $aboutList->content;?></textarea>                  
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <p class="about-text" id="about-text2">
                            <?php echo nl2br(htmlspecialchars_decode($aboutList->content2)); ?>
                        </p>
                        <div class="edit-about">
                            <textarea class="text-about fluid" id="txtcontent2" name="txtcontent2"><?php echo $aboutList->content2;?></textarea>                  
                        </div>
                    </div>
                </div>
                <hr class="space" />
                <div class="row">
                    <div class="col-lg-6">
                        <div class="pull-left div-settings dnone" style="position:absolute;z-index:999; left: -55px;">
                            <img title="Edit" id="btedit-about1" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                            <img title="Save" id="btsave-about1" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                            <img title="Cancel" id="btcancel-about1" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                        </div>
                        <div class="title">
                            <h2 class="about1-text" id="sp_quote_text"><?php echo nl2br(htmlspecialchars_decode($aboutList->quote_text)); ?></h2>
                            <div class="edit-about1">
                                <textarea class="fluid" id="txtquote" name="txtquote" rows="2"><?php echo $aboutList->quote_text;?></textarea>                  
                            </div>
                        </div>
                        <p class="about1-text" id="sp_simple_quote">
                            <?php echo nl2br(htmlspecialchars_decode($aboutList->simple_quote)); ?>
                        </p>
                        <div class="edit-about1">
                            <textarea class="text-about fluid" id="txtsimple" name="txtsimple"><?php echo $aboutList->simple_quote;?></textarea>                  
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="pull-left div-settings dnone" style="position:absolute; left:-30px;z-index:999">
                            <img title="Edit" id="btedit-about-pict" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                            <input type="file" id="fnabout-pict" class="vnone" name="fnabout-pict" accept="image/*" />
                        </div>
                        <ul class="slider light" data-options="arrows:false,nav:false">
                            <li>
                                <a class="img-box lightbox" href="<?php echo base_url();?>images/<?php echo $aboutList->picture; ?>">
                                    <img id="imgaboutpict" src="<?php echo base_url();?>images/<?php echo $aboutList->picture; ?>" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- <hr class="space-sm" /> -->
                <div class="pull-left div-settings dnone" style="position:absolute;z-index:999; left: 55px;">
                    <img title="Edit" id="btedit-about2" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                    <img title="Save" id="btsave-about2" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                    <img title="Cancel" id="btcancel-about2" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                </div>
                <table class="table table-grid table-border align-left table-6-md">
                    <tbody>
                        <tr>
                            <td>
                                <div class="counter counter-horizontal counter-icon">
                                    <i class="im-globe text-md"></i>
                                    <div>
                                        <h3>Fields Workers</h3>
                                        <div class="value">
                                            <span class="text-md about2-text" id="sp_box1" data-to="<?php echo nl2br(htmlspecialchars_decode($aboutList->box1)); ?>" data-speed="5000"><?php echo nl2br(htmlspecialchars_decode($aboutList->box1)); ?></span>
                                            <span>+</span>
                                            <div class="edit-about2">
                                                <input type="number" class="fluid" id="txtbox1" name="txtbox1" value="<?php echo $aboutList->box1;?>"/>                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="counter counter-horizontal counter-icon">
                                    <i class="im-business-man text-md"></i>
                                    <div>
                                        <h3>Clients</h3>
                                        <div class="value">
                                            <span class="text-md about2-text" id="sp_box2" data-to="<?php echo nl2br(htmlspecialchars_decode($aboutList->box2)); ?>" data-speed="5000"><?php echo nl2br(htmlspecialchars_decode($aboutList->box2)); ?></span>
                                            <span>+</span>
                                            <div class="edit-about2">
                                                <input type="number" class="fluid" id="txtbox2" name="txtbox2" value="<?php echo $aboutList->box2;?>"/>                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="counter counter-horizontal counter-icon">
                                    <i class=" im-charger text-md"></i>
                                    <div>
                                        <h3>Projects</h3>
                                        <div class="value">
                                            <span class="text-md about2-text" id="sp_box3" data-to="<?php echo nl2br(htmlspecialchars_decode($aboutList->box3)); ?>" data-speed="5000"><?php echo nl2br(htmlspecialchars_decode($aboutList->box3)); ?></span>
                                            <span>+</span>
                                            <div class="edit-about2">
                                                <input type="number" class="fluid" id="txtbox3" name="txtbox3" value="<?php echo $aboutList->box3;?>"/>                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="counter counter-horizontal counter-icon">
                                    <i class="im-support text-md"></i>
                                    <div>
                                        <h3>Team members</h3>
                                        <div class="value">
                                            <span class="text-md about2-text" id="sp_box4" data-to="<?php echo nl2br(htmlspecialchars_decode($aboutList->box4)); ?>" data-speed="5000"><?php echo nl2br(htmlspecialchars_decode($aboutList->box4)); ?></span>
                                            <span>+</span>
                                            <div class="edit-about2">
                                                <input type="number" class="fluid" id="txtbox4" name="txtbox4" value="<?php echo $aboutList->box4;?>"/>                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        </main>    

        <?php include "include/footer.php" ?>
        
    </div>
    <!-- End layout-->

     <!-- Scripts -->
     <?php include "include/include_js.php" ?>
        
    </body>
</html>