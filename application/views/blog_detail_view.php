<!DOCTYPE html>
<html lang="en">
<head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title><?php echo $config->title; ?></title>
        <!-- <title>Roker  - Software, Technology , Corporate, Creative, Multi-Purpose, Responsive And Retina Html5/css3 Template</title>  -->
        <meta name="keywords" content="conculta" />
        <meta name="description" content="Roker  - Corporate, Creative, Multi Purpose, Responsive And Retina Template">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <?php include "include/include_css.php" ?>  

        <!-- Head Libs -->
        <script src="<?php echo base_url() ?>js2/modernizr.js"></script>

    </head>
    <body> 

    <!--Preloader-->
    <div class="preloader">
        <div class="status">&nbsp;</div>
    </div>
    <!--End Preloader-->

    <!-- layout-->
    <div id="layout" class="layout-wide">
        
        <?php include "include/header.php" ?>   

        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="index.html">Home</a> / Blog / <?php echo $blog->title; ?>
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1>Berita Terbaru
                            <span class="subtitle-section">
                                Komisi Irigasi
                                <span class="left"></span>
                                <span class="right"></span>
                            </span>
                            <span class="line-title"></span>
                        </h1>
                    </div>
                    <!-- End Title--> 

                </div>
                <!-- End Title -Search --> 
              
            </div>
        </section>   
        <!-- End Title Section --> 


        <!-- Works -->
        <section class="paddings">
            <div class="container">
                <div class="row">   

                    <!-- Blog Post -->
                    <div class="col-md-8">
                       
                        <!-- Item Post -->
                        <article class="post">                                                   
                            <!-- Image Post  --> 
                            <div class="post-image">
                                <img src="<?php echo base_url(); ?>image/blog/<?php echo $blog->image;?>" alt="">
                            </div>
                            <!-- End Image Post  -->  

                            <div class="clearfix"></div>

                            <!-- Post Meta -->                                                             
                            <div class="post-meta">
                                <span><i class="fa fa-calendar"></i> <?php echo date('d M Y', strtotime($blog->created_date)); ?> </span>
                                <span><i class="fa fa-user"></i> By <?php echo $blog->creator; ?> </span>
                                <!-- <span><i class="fa fa-tag"></i> <a href="#">Duis</a>, <a href="#">News</a></span> -->
                                <!-- <span><i class="fa fa-comments"></i> <a href="#">(12)</a></span> -->
                            </div>     
                            <!-- End Post Meta --> 

                            <!-- Info post --> 
                            <div class="info-post">
                                <h2><?php echo $blog->title; ?></h2>
                                <p><?php echo $blog->blog_text; ?></p>
                            </div>
                            <!--End Info post -->                            
                      
                        </article>
                        <!-- End Item Post -->

                         <!-- Content Autor -->
                        <div class="row autor">
                            <h3><i class="fa fa-user"></i> <?php echo $blog->creator; ?></h3>
                                <!-- Image Team  -->
                                <!-- <div class="col-md-3">
                                    <div class="image-autor">
                                        <a href="<?php echo base_url() ?>images/testimonials/1.jpg" class="fancybox">
                                            <img src="<?php echo base_url() ?>images/clients-downloads/1.jpg" alt="" title="View Image">
                                        </a>
                                    </div>
                                </div> -->
                                <!-- End Image Team  -->
                                
                                <!-- Info  Team  -->
                                <!-- <div class="col-md-9">                             
                                   <p>Inani nominati sit eu. Te ubique cotidieque philosophia mel, vix id omnes iudico prompta. Ex usu nihil mediocritatem. Sea quod vituperata no, omittam offendit vel in. Noster voluptua luptatum id mea. Et voluptatum adversarium usu, rebum nominati recteque vix ei.</p> -->
                                    <!-- Social-->
                                    <!-- <ul class="social">
                                        <li data-toggle="tooltip" title data-original-title="Facebook">
                                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                        </li> 
                                        <li data-toggle="tooltip" title data-original-title="Twitter">
                                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                        </li> 
                                        <li data-toggle="tooltip" title data-original-title="Youtube">
                                            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                                        </li>                     
                                    </ul> -->
                                    <!-- End Social-->
                                   
                               <!-- </div> -->
                               <!-- End Info  Team  -->

                           </div> 
                            <!--End Content Autor -->

                    </div>            
                    <!-- End Blog Post -->   

                   
                    <!-- Sidebars -->
                    <div class="col-md-4 sidebars">

                        <aside>
                            <h4>Searh Sidebar</h4>
                            <form class="search" action="#" method="Post">
                                <div class="input-group">
                                    <input class="form-control" placeholder="Search..." name="email"  type="email" required="required">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit" name="subscribe" >Go!</button>
                                    </span>
                                </div>
                            </form>   
                        </aside>

                        <hr>
                        
                        <aside>
                            <h4>Categories</h4>
                            <ul class="list">
                            <?php foreach($blogcategoryList as $category) : ?>
                                <li><i class="fa fa-check"></i><a href="<?php echo base_url();?>blog/category/<?php echo $category->ID_blogcategory; ?>"><?php echo $category->blogcategory_name; ?></a></li>
                            <?php endforeach ?>
                            </ul>
                        </aside>  


                        <!-- <aside class="tags">
                            <h4>Tags</h4>
                            <?php foreach($blogtagsList as $tags) : ?>
                                <a href="#" class="#" title="17 topic"><i class="fa fa-tag"></i> <?php echo $tags->tags_name; ?> </a>
                            <?php endforeach ?>
                        </aside>     -->
               
                        
                        <aside>
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                     <!-- <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Popular</a></li> -->
                                    <li class="active"><a href="#recentPosts" data-toggle="tab">Recent</a></li>
                                 </ul>
                                <div class="tab-content">

                                    <!-- <div class="tab-pane active" id="popularPosts">
                                        <ul class="simple-post-list">
                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="<?php echo base_url(); ?>images/clients-downloads/1.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>

                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="<?php echo base_url(); ?>images/clients-downloads/2.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>

                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="<?php echo base_url(); ?>images/clients-downloads/3.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>
                                              
                                        </ul>
                                    </div> -->

                                    <div class="tab-pane active" id="recentPosts">
                                        <ul class="simple-post-list">
                                            <?php foreach($recentblogList as $recent) : ?>
                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="<?php echo base_url()."blog/detail/".$recent->ID_blog; ?>">
                                                             <img style="width: 70px;" src="<?php echo base_url(); ?>image/blog/<?php echo $recent->image;?>">
                                                        </a>
                                                    </div>
                                                 </div>
                                                <div class="post-info">
                                                    <a href="<?php echo base_url()."blog/detail/".$recent->ID_blog; ?>"><?php echo $recent->title; ?></a>
                                                    <div class="post-meta">
                                                        <?php echo date('d M Y', strtotime($recent->created_date)); ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php endforeach ?>
                                            
                                              
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </aside>

                    </div>
                    <!-- End Sidebars -->


                </div>
            </div>
            <!-- End Container-->
        </section>
        <!-- End Works-->

        <?php include "include/footer.php" ?>

    </div>
    <!-- End layout-->

    <!-- Scripts -->
    <?php include "include/include_js.php" ?>
        
    </body>
</html>