<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title><?php echo $config->title; ?></title>
        <!-- <title>Roker  - Software, Technology , Corporate, Creative, Multi-Purpose, Responsive And Retina Html5/css3 Template</title>  -->
        <meta name="keywords" content="conculta" />
        <meta name="description" content="Roker  - Corporate, Creative, Multi Purpose, Responsive And Retina Template">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <?php include "include/include_css.php" ?>  

        <!-- Head Libs -->
        <script src="<?php echo base_url() ?>js2/modernizr.js"></script>

    </head>
    <body> 

    <!--Preloader-->
    <div class="preloader">
        <div class="status">&nbsp;</div>
    </div>
    <!--End Preloader-->
    
    <!-- layout-->
    <div id="layout" class="layout-wide">
        
        <?php include "include/header.php" ?>
        <?php include "include/cropping_modal.php" ?>   
        <?php include "include/text_modal.php" ?>  

        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="<?php echo base_url(); ?>">Home</a> / Blog 
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1>Blog
                            <span class="subtitle-section">
                                Semua Posting
                                <span class="left"></span>
                                <span class="right"></span>
                            </span>
                            <span class="line-title"></span>
                        </h1>
                    </div>
                    <!-- End Title--> 
                </div>
                <!-- End Title -Search --> 
              
            </div>
        </section>   
        <!-- End Title Section --> 

        


        <!-- Works -->
        
        <section class="paddings blogPage">
            <div class="container">
                <div class="row">   
                    <div class="pull-left div-settings dnone" style="position:absolute;left:70px;top:280px;z-index:999">
                        <img title="Edit" id="btedit-news" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                    </div>
                    <!-- Blog Post -->
                    <div class="col-md-8" >
                        <?php foreach($blogkeyList as $blog) : ?>
                        <!-- Item Post -->
                        <article class="post">
                            <div class="row">
                                <!-- Image and meta post --> 
                                <div class="col-md-5">                                  

                                    <!-- Image Post  --> 
                                    <div class="post-image">
                                       <img src="<?php echo base_url(); ?>image/blog/<?php echo $blog->image;?>" alt="">
                                    </div>
                                            <!-- End Image Post  --> 
                                </div>
                                <!-- End Image and meta post --> 

                                <!-- Info post --> 
                                <div class="col-md-7">
                                     <h3><a href="<?php echo base_url()."blog/detail/".$blog->ID_blog; ?>"><?php echo $blog->title; ?></a></h3>
                                    <!-- Post Meta --> 
                                     <div class="row">
                                        <div class="col-md-12">
                                            <div class="post-meta">
                                                <span><i class="fa fa-calendar"></i><?php echo date('d M Y', strtotime($blog->created_date)); ?> </span>
                                                <span><i class="fa fa-user"></i> By <?php echo $blog->creator; ?> </span>
                                                <span><i class="fa fa-tag"></i> <a href="<?php echo base_url();?>blog/category/<?php echo $blog->ID_blogcategory; ?>"><?php echo $blog->blogcategory_name; ?></a></span>
                                                <!-- <span><i class="fa fa-comments"></i> <a href="#">(12)</a></span> -->
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!-- End Post Meta --> 
                                    <p><?php  echo substr($blog->blog_text, 0,200); ?>[...]</p>
                                    <a href="<?php echo base_url()."blog/detail/".$blog->ID_blog; ?>" class="btn btn-primary">Read more...</a>
                                    <!-- <div class="pull-right">
                                        <span class="label label-success"><?php echo $blog->blogcategory_name; ?></span>
                                    </div> -->
                                </div>
                                <!--End Info post --> 
                            </div>  
                        </article>
                        <?php endforeach ?>
                        <div>
                            <?php echo $pagination; ?>
                        </div>
                        <!-- End Item Post -->

                        <!-- Pagination -->
                        <!-- <ul class="pagination">
                            <li><a href="#">«</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                             <li class="active"><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">»</a></li>
                        </ul> -->
                        <!-- End Pagination -->


                    </div>            
                    <!-- End Blog Post -->   

                   
                    <!-- Sidebars -->
                    <div class="col-md-4 sidebars">

                        <aside>
                            <h4>Searh Sidebar</h4>
                            <form class="formSearch" action="<?php echo base_url() ?>blog">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="key" value="<?php echo $keywords ?>" id="txtsearch" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit" style="padding: 10px;">Go!</button>
                                    </span>
                                </div>
                            </form>   
                        </aside>

                        <hr>
                        
                        <aside>
                            <h4>Categories</h4>
                            <div class="pull-left div-settings dnone" style="position:absolute;left:120px;top:150px;z-index:999">
                                <img title="Edit" id="btedit-blogcategory" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                            </div>
                            <ul class="list" id="blogcategoryList">
                                <?php foreach($blogcategoryList as $blog) : ?>
                                    <li><i class="fa fa-check"></i><a href="<?php echo base_url();?>blog/category/<?php echo $blog->ID_blogcategory; ?>"><?php echo $blog->blogcategory_name; ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        </aside>  


                        <!-- <aside class="tags">
                            <h4>Tags</h4>
                            <div class="pull-left div-settings dnone" style="position:absolute;left:70px;top:445px;z-index:999">
                                <img title="Edit" id="btedit-tags" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                            </div>
                            <div id="tagsList">
                            <?php foreach($blogtagsList as $tags) : ?>
                                <a href="#" class="#" title="17 topic"><i class="fa fa-tag"></i> <?php echo $tags->tags_name; ?> </a>
                            <?php endforeach ?>
                            </div>
                        </aside>     -->
               
                        
                        <aside>
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                     <!-- <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Popular</a></li> -->
                                    <li class="active"><a href="#recentPosts" data-toggle="tab">Recent</a></li>
                                 </ul>
                                <div class="tab-content">

                                    <!-- <div class="tab-pane active" id="popularPosts">
                                        <ul class="simple-post-list">
                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="<?php echo base_url(); ?>images/clients-downloads/1.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>

                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="<?php echo base_url(); ?>images/clients-downloads/2.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>

                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="blog-post.html">
                                                            <img src="<?php echo base_url(); ?>images/clients-downloads/3.jpg" alt="">
                                                        </a>
                                                     </div>
                                                </div>
                                                <div class="post-info">
                                                    <a href="blog-post.html">Pellentesque habitant morbi.</a>
                                                    <div class="post-meta">
                                                            Nov 25 / 7 / 2013
                                                     </div>
                                                 </div>
                                            </li>
                                              
                                        </ul>
                                    </div> -->

                                    <div class="tab-pane active" id="recentPosts">
                                        <ul class="simple-post-list">
                                            <?php foreach($recentblogList as $recent) : ?>
                                            <li>
                                                <div class="post-image">
                                                    <div class="img-thumbnail">
                                                        <a href="<?php echo base_url()."blog/detail/".$recent->ID_blog; ?>">
                                                             <img style="width: 70px;" src="<?php echo base_url(); ?>image/blog/<?php echo $recent->image;?>">
                                                        </a>
                                                    </div>
                                                 </div>
                                                <div class="post-info">
                                                    <a href="<?php echo base_url()."blog/detail/".$recent->ID_blog; ?>"><?php echo $recent->title; ?></a>
                                                    <div class="post-meta">
                                                        <?php echo date('d M Y', strtotime($recent->created_date)); ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php endforeach ?>
                                            
                                              
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </aside>

                    </div>
                    <!-- End Sidebars -->


                </div>
            </div>
            <!-- End Container-->
        </section>

        <!-- End Works-->

        <?php include "include/footer.php" ?>

    </div>
    <!-- End layout-->

    <!-- Scripts -->
    <?php include "include/include_js.php" ?>

    </body>
</html>