<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title><?php echo $config->title; ?></title>
        <!-- <title>Roker  - Software, Technology , Corporate, Creative, Multi-Purpose, Responsive And Retina Html5/css3 Template</title>  -->
        <meta name="keywords" content="conculta" />
        <meta name="description" content="Roker  - Corporate, Creative, Multi Purpose, Responsive And Retina Template">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <?php include "include/include_css.php" ?>  

        <!-- Head Libs -->
        <!-- <script src="<?php echo base_url() ?>js2/modernizr.js"></script> -->

    </head>
    <body> 

    <!--Preloader-->
    <div class="preloader">
        <div class="status">&nbsp;</div>
    </div>
    <!--End Preloader-->
    
    <!-- layout-->
    <div id="layout" class="layout-wide">

        <!-- Maps Coordinate -->
        <input type="hidden" id="hid_langitude" name="hid_langitude" value="<?php echo $contactList->langitude; ?>" />
        <input type="hidden" id="hid_latitude" name="hid_latitude" value="<?php echo $contactList->latitude; ?>" />


        <!-- Hidden Value -->
        <input type="hidden" id="hid_login" name="hid_login" value="<?php echo $id_admin;?>" />

        <?php include "include/header.php" ?>
        <?php include "include/cropping_modal.php" ?>

    <main>
        <section class="section-base">
            <div>
                 <!-- Google Map --> 
                 <div class="sc-google-map" id="sc-google-map">
                    <div class="empty-space"></div>
                    <div class="fluid div-settings dnone" style="min-height:40px;">
                        <img title="Edit" id="btedit-map" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        <img title="Save" id="btsave-map" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                        <img title="Cancel" id="btcancel-map" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                    </div>
            
                    <div id="google-map" style="height:500px;"></div>
                </div>
                <!-- End Google Map -->    
                <hr class="space-lg" />
            </div>
        </section>
        <section class="section-base" style="padding: 0 50px;">
            <div class="">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title">
                            <h1>Write us</h1>
                            <p>Contact us from here</p>
                        </div>
                        <form action="themekit/scripts/contact-form/contact-form.php" class="form-box form-ajax" method="post" data-email="example@domain.com">
                            <div class="row">
                                <div class="col-lg-6">
                                    <p>Name</p>
                                    <input id="name" name="name" placeholder="" type="text" class="input-text" required>
                                </div>
                                <div class="col-lg-6">
                                    <p>Email</p>
                                    <input id="email" name="email" placeholder="" type="email" class="input-text" required>
                                </div>
                            </div>
                            <p>Messagge</p>
                            <textarea id="messagge" name="messagge" class="input-textarea" placeholder="" required></textarea>
                            <div class="form-checkbox">
                                <input type="checkbox" id="check" name="check" value="check" required>
                                <label for="check">You accept the terms of service and the privacy policy</label>
                            </div>
                            <button class="btn btn-xs" type="submit">Send messagge</button>
                            <div class="success-box">
                                <div class="alert alert-success">Congratulations. Your message has been sent successfully</div>
                            </div>
                            <div class="error-box">
                                <div class="alert alert-warning">Error, please retry. Your message has not been sent</div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-6 text-right">
                        <hr class="space visible-md" />
                        <div class="title">
                            <h1>Contact Info</h1>
                            <p>Information</p>
                        </div>
                        <div class="pull-left div-settings dnone" style="z-index: 999; position: absolute; top: 0;">
                            <img title="Edit" id="btedit-contact" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                            <img title="Save" id="btsave-contact" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                            <img title="Cancel" id="btcancel-contact" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                        </div>
                        <ul class="text-list text-list-base">
                            <li>
                                <h3>Office & Workshop</h3>
                                <p class="contact-label" id="p-location"><?php echo $contactList->location; ?></p>
                                <p class="edit-contact">
                                    <textarea id="txtaddress" style="width:100%;text-align:center"><?php echo str_replace("<br/>","\n", $contactList->location); ?></textarea>
                                </p>
                            </li>
                            <li>
                                <h3>Open Office Hours</h3>
                                <p class="contact-label" id="p-officeHours"><?php echo $contactList->corporation_name; ?></p>
                                <p class="edit-contact">
                                    <textarea id="txtofficeHours" style="width:100%;text-align:center"><?php echo str_replace("<br/>","\n", $contactList->contactMsg); ?></textarea>
                                </p>
                            </li>
                            <li>
                                <h3>Email</h3>
                                <p class="contact-label" id="p-email"><?php echo $contactList->email; ?></p>
                                <p class="edit-contact">
                                    <input type="text" id="txtemail" placeholder="Email 1.." style="width:80%;text-align:center" value="<?php echo $contactList->email; ?>" />
                                </p>
                            </li>
                            <li>
                                <h3>Get in Touch</h3>
                                <p class="contact-label" id="p-mp1"><?php echo $contactList->mp1; ?></p>
                                <p class="edit-contact">
                                    <input type="text" id="txtphone" style="width:80%;text-align:center" max-length="20" value="<?php echo $contactList->mp1; ?>" />
                                </p>
                            </li>
                        </ul>
                        <hr class="space-sm" />
                    </div>
                </div>
            </div>
            <hr class="space-md"/>
        </section>
    </main>

        <?php include "include/footer.php" ?>

    </div>
    <!-- End layout-->

    <!-- Scripts -->
    <?php include "include/include_js.php" ?>

    <!--  ======================= Google Map  ============================== -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWINAalJf-rqnjGnUtjfhyVH-Xur74qfM&callback=initMap" type="text/javascript"></script> -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4I_1YfmhjbSwAR0H1dFIkNuUYreOGgU0&callback=init">
    </script> -->
    <!-- <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDl7p8SWg-5kLe7i-usdYCu5m3eVllMDTs&callback=init'></script> -->

    
    </body>
</html>