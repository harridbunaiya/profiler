<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $config->title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "include/include_css.php" ?>
</head>
<body class="page">

<!-- Wrapper content -->
<div id="wrapper-container" class="content-pusher">
    <div class="overlay-close-menu"></div>

    <!-- Header -->
    <?php include "include/header.php" ?>

    <!-- Hidden Value -->
    <input type="hidden" id="hid_login" name="hid_login" value="<?php echo $id_admin;?>" />
    <main>
        <section class="section-base section-color">
            <div class="container">
                <hr class="space-md"/>
                <div class="title text-center">
                    <h1>Our Event Projects</h1>
                </div>
                <div class="text-center">
                    <p>Agung Tent adalah sebuah respons atas tingginya ekspektasi akan kesuksesan pelaksanaan sebuah event. Sebuah badan usaha yang didirikan untuk membangun fondasi sebuah acara dengan menyediakan perlengkapan event berkualitas dan mampu menjawab kebutuhan secara tepat</p>
                </div>
                <hr class="space-md"/>
                <div class="maso-list" data-columns="3" data-columns-md="2"  data-columns-sm="1">
                    <div class="menu-inner">
                        <div class=" div-settings dnone" style="z-index: 999; position: absolute; left: 150px;">
                            <img title="Edit" id="btedit-category" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        </div>
                        <div><i class="menu-btn"></i><span>Menu</span></div>
                        <ul class="categoryList">
                            <li class="active"><a data-filter="maso-item" href="#">All</a></li>
                            <?php foreach($categoryList as $category) : ?>
                            <li><a href="#" data-filter="cat-<?php echo $category->seq_no; ?>"><?php echo $category->category_name; ?></a></li>
                            <?php endforeach ?>
                            <!-- <li><a data-filter="cat-1" href="#">Software</a></li>
                            <li><a data-filter="cat-2" href="#">Finance</a></li>
                            <li><a data-filter="cat-3" href="#">Business</a></li> -->
                            <li><a class="maso-order" data-sort="asc"></a></li>
                        </ul>
                    </div>
                    <div class="pull-left div-settings dnone" style="position: absolute; left: 150px;">
                        <img title="Edit" id="btedit-portfolio" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                    </div>
                    <div class="maso-list list-gallery">
                        <div class="maso-box" id="portfolio-header">
                            <?php foreach($fullimageList as $image) : ?>
                                <div class="maso-item cat-<?php echo $image->ID_category; ?>">
                                    <a class="media-box media-box-full" href="<?php echo base_url() ?>images/works/large/<?php echo $image->imagePath; ?>">
                                        <img style="min-height: 241px" alt="" src="<?php echo base_url() ?>images/works/thumb/<?php echo $image->imagePath; ?>">
                                        <div class="caption">
                                            <h2><?php echo $image->imageDesc; ?></h2>
                                            <div class="extra-field"><?php echo $image->imageTitle; ?></div>
                                        </div>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
    <!-- Footer -->
    <?php include "include/footer.php" ?>

</div><!-- wrapper-container -->

<!-- Modal Dialog -->
<?php include "include/cropping_modal.php"; ?>

<!-- Fixed Button -->
<?php include "include/addition.php" ?>

<!-- Scripts -->
<?php include "include/include_js.php" ?>

</body>
</html>