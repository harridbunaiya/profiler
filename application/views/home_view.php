<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $config->title; ?></title>
<!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->
<?php include "include/include_css.php" ?>
</head>
<body>

<!--=== Wrapper Start ======-->
<div class="wrapper">

<!-- Hidden Value -->
<input type="hidden" id="hid_login" name="hid_login" value="<?php echo $id_admin;?>" />

<!-- Maps Coordinate -->
<input type="hidden" id="hid_langitude" name="hid_langitude" value="<?php echo $contactList->langitude; ?>" />
<input type="hidden" id="hid_latitude" name="hid_latitude" value="<?php echo $contactList->latitude; ?>" />

<!-- Header -->
<?php include "include/header.php" ?>
<?php include "include/cropping_modal.php"; ?>

<main>
    <section class="section-image section-home-one padding-150-0">
        <div class="container-fluid" style="padding: 0;">
            <!-- <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <hr class="space-lg" />
                    <hr style="height: 50px; margin: 0;" />
                    <h1 class="text-color-2">
                        Our Service
                    </h1>
                    <ul class="slider" data-options="arrows:true,nav:true,autoplay:3000,controls:out" id="homeSlideList">
                    <?php $index = 0; foreach($sliderList as $slider) : ?>
                        <li style="background-image:url('<?php echo base_url(); ?>images/slide/slides/<?php echo $sliderList[0]->imagePath; ?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                            <img class="img-box" src="<?php echo base_url(); ?>images/slide/slides/<?php echo $slider->imagePath; ?>" alt="<?php echo $slider->imageTitle; ?>" width="100px;"/>
                            <?php echo $slider->content; ?>
                        </li>
                    <?php $index++; endforeach; ?>
                    </ul>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background: rgb(0, 71, 103) !important; text-align: right; padding: 0 50px;">
                    <hr class="space-lg" />
                    <img data-anima="fade-bottom" data-time="1000" class="slide-image" src="<?php echo base_url();?>images/tenda-roder.jpg" alt="" />
                    <h1 class="text-color-2"> 
                        TENDA GUNA
                    </h1>  

                    <hr class="space-sm" />
                    <h2 class="light">
                        YOUR EVENT OUTDOOR SOLUTION
                    </h2>

                    <hr class="space-sm" />
                    <a href="#" class="btn btn-sm width-190 full-width-sm" style="line-height: 30px; font-size: 15px;">View Our Work</a>

                    <hr class="space-lg"/>
                </div>
            </div> -->
            <div class="pull-left div-settings dnone" style="position:absolute;left:50px;z-index:999">
                <img title="Edit" id="btedit-home" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
            </div>
            <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="home-1"
                 data-source="gallery"
                 style="padding:0px;url('<?php echo base_url(); ?>images/slide/slides/<?php echo $sliderList[0]->imagePath; ?>');background-repeat:no-repeat;background-size:contain;background-position:center center;">
                <!-- START REVOLUTION SLIDER 5.4.7.4 fullwidth mode -->
                <div id="rev_slider_4_1" class="rev_slider fullwidthbanner" style="display:none;" data-version="5.4.7.4">
                    <ul>
                        <?php
                            $index = 0; 
                            foreach($sliderList as $slider) : ?>
                            <!-- SLIDE  -->
                            <li data-index="rs-<?php echo $index ?>" data-transition="fade" data-slotamount="7" data-hideafterloop="0"
                                data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="1500"
                                 data-rotate="0" data-saveperformance="off"
                                data-title="Slide" >
                                <!-- MAIN IMAGE -->
                                
                                <img src="<?php echo base_url(); ?>images/slide/slides/<?php echo $slider->imagePath; ?>" alt="" title="slider tendaguna" data-bgposition="left center" data-kenburns="off" data-duration="10000" data-ease="Linear.easeNone" data-bgfit="contain" data-bgfitend="100" data-bgpositionend="center center">
                                <!-- LAYERS -->
                                <?php echo $slider->content; ?>
                                
                            </li>
                        <?php
                            $index++; 
                            endforeach; ?>
                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-base section-color">
        <div class="container container-profiler">
            <div class="row text-center">
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="box-profiler">
                        <a target="blank_" href="https://google.com">
                            <img src="<?php echo base_url();?>images/services-carousel/1.png" alt="">
                            <h3>Profil</h3>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="box-profiler">
                        <a href="#">
                            <img src="<?php echo base_url();?>images/services-carousel/2.png" alt="">
                            <h3>Bisnis</h3>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="box-profiler">
                        <a href="#">
                            <img src="<?php echo base_url();?>images/services-carousel/3.png" alt="">
                            <h3>Gallery</h3>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="box-profiler">
                        <a href="#">
                            <img src="<?php echo base_url();?>images/services-carousel/4.png" alt="">
                            <h3>Berita</h3>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="box-profiler">
                        <a href="#">
                            <img src="<?php echo base_url();?>images/services-carousel/5.png" alt="">
                            <h3>Saran</h3>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="box-profiler">
                        <a href="#">
                            <img src="<?php echo base_url();?>images/services-carousel/6.png" alt="">
                            <h3>Sosmed</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-base">
        <div class="container container-profiler">
            <div style="margin-bottom: 30px;">
                <h3>Berita Terupdate</h3>
            </div>
            <div class="row blog-row">
                <div class="col-md-6 row" style="margin-bottom: 30px;">
                    <div class="col-md-4 col-sm-5 col-xs-5">
                        <img src="<?php echo base_url(); ?>image/blog/blog1.png" alt="">
                    </div>
                    <div class="col-md-8 col-sm-7 col-xs-7">
                        <h5 style="font-weight:600">SBLF Setuju Terselenggaranya Pilkada Pada Desember 2020</h5>
                        <p style="font-size: 12px; color: #adadad;">Juni 18, 2020</p>
                        <p style="font-size: 10px;line-height: initial;">Suaraindo.id- Lembaga Riset SBLF Setuju dengan keputusan Menteri Dalam Negeri yang memutuskan Pilkada Serentak di Indonesia tetap terselenggara pada Desember</p>
                    </div>
                </div>
                <div class="col-md-6 row" style="margin-bottom: 30px;">
                    <div class="col-md-4 col-sm-5 col-xs-5">
                        <img src="<?php echo base_url(); ?>image/blog/blog2.png" alt="">
                    </div>
                    <div class="col-md-8 col-sm-7 col-xs-7">
                        <h5 style="font-weight:600">Edo Andrefson : “Parpol Kecil” Mesti Siapkan Cawagub Sumbar Terbaik</h5>
                        <p style="font-size: 12px; color: #adadad;">Juni 18, 2020</p>
                        <p style="font-size: 10px;line-height: initial;">Edo Andrefson : “Parpol Kecil” Mesti Siapkan Cawagub Sumbar Terbaik PADANG, HARIANHALUAN.COM—Pengamat Politik sekaligus Direktur SBLF Riset, Edo Andrefson menilai,</p>
                    </div>
                </div>
                <div class="col-md-6 row" style="margin-bottom: 30px;">
                    <div class="col-md-4 col-sm-5 col-xs-5">
                        <img src="<?php echo base_url(); ?>image/blog/blog3.png" alt="">
                    </div>
                    <div class="col-md-8 col-sm-7 col-xs-7">
                        <h5 style="font-weight:600">Pilkada Undur, Elektabilitas Kandidat Direset Ulang</h5>
                        <p style="font-size: 12px; color: #adadad;">Juni 18, 2020</p>
                        <p style="font-size: 10px;line-height: initial;">Padang, Fajarsumbar.com-Pandemi virus korona atau covid-19 yang melanda dunia, termasuk Indonesia membuat Komisi Pemilihan Umum menunda pelaksanaan Pilkada serentak 2020.</p>
                    </div>
                </div>
                <div class="col-md-6 row" style="margin-bottom: 30px;">
                    <div class="col-md-4 col-sm-5 col-xs-5">
                        <img src="<?php echo base_url(); ?>image/blog/blog4.jpg" alt="">
                    </div>
                    <div class="col-md-8 col-sm-7 col-xs-7">
                        <h5 style="font-weight:600">SBLF : Pilkada Undur, Kepedulian Cakada jangan mundur</h5>
                        <p style="font-size: 12px; color: #adadad;">Juni 18, 2020</p>
                        <p style="font-size: 10px;line-height: initial;">Suaraindo.id-Komisi Pemilihan Umum (KPU)  resmi menunda tahapan pemilihan kepala daerah (Pilkada)  serentak 2020. Penundaan dilakukan karena merebaknya penyebaran corona virus</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="section-base section-color">
        <div class="container">
            <div class="cnt-box cnt-call">
                <div class="caption">
                    <div class="pull-left div-settings dnone" style="position:absolute;left:0;top:0;z-index:999">
                        <img title="Edit" src="<?php echo base_url();?>image/Icon_tools.png" class="btedit-title icon mini-icon-settings" />
                        <img title="Save" src="<?php echo base_url();?>image/icon-save.png" class="btsave-title icon mini-icon-settings_30" />
                        <img title="Cancel" src="<?php echo base_url();?>image/icon-delete.png" class="btcancel-title icon mini-icon-settings_32"/>
                    </div>
                    <div class="title title-text">
                        <h1 id="title1-master"><?php echo nl2br(htmlspecialchars_decode($aboutTitleList->title1)); ?></h1>
                    </div>
                    <div class="edit-title">
                        <textarea class="form-control" id="txttitle1_master" name="txttitle1_master" style="width: 100%;"><?php echo $aboutTitleList->title1;?></textarea>                 
                    </div>
                    <a href="#" class="btn btn-sm width-190 btn-text active" style="font-size: 15px;">Free Quote</a>
                </div>
            </div>
            <hr class="space-md"/>
            <table class="table table-grid table-border align-center table-logos table-6-md" style="background: #fff; position: relative;">
                <div class="pull-left div-settings dnone" style="position:absolute;z-index:999">
                    <img title="Edit" id="btedit-home1" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                </div>
                <tbody>
                    <tr class="sliderServices">
                        <?php foreach($sliderserviceList as $feature) : ?>
                            <td style="border-top: none;">
                                <a href="<?php echo $feature->link;?>">
                                    <img src="<?php echo base_url(); ?>images/services-carousel/<?php echo $feature->imagePath; ?>" alt="<?php echo $feature->slider_name;?>" />
                                    <hr class="space-sm" />
                                    <h2><?php echo $feature->slider_name;?></h2>
                                    <p>
                                        <?php echo $feature->content; ?>
                                    </p>
                                </a>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </section> -->
    <!-- <section class="section-base">
        <div class="">
            <div class="row row-fit-lg" data-anima="fade-bottom" data-time="1000">
                <div class="col-lg-5">
                    <div class="pull-left div-settings dnone" style="position:absolute;right:100px;top:0px;z-index:999">
                        <img title="Edit" id="btedit-intro" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        <input class="vnone" type="file" id="introWallpaper" name="introWallpaper" accept="image/*" />
                    </div>
                   <img id="pattern-important-info" src="<?php echo base_url()?>images/patterns/pattern-profile.jpg" alt="">
                </div>
                <div class="col-lg-7" style="padding: 0 50px;">
                    <hr class="space-lg" />
                    <div class="pull-left div-settings dnone" style="position:absolute;left:0;z-index:999">
                        <img title="Edit" id="btedit-aboutquote" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        <img title="Save" id="btsave-aboutquote" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                        <img title="Cancel" id="btcancel-aboutquote" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                    </div>
                    <div class="title aboutquote-text">
                        <h1 id="sp_quote_tenda"><?php echo nl2br(htmlspecialchars_decode($aboutQuoteList->quote_text)); ?></h1>
                    </div>
                    <div class="edit-aboutquote">
                        <textarea class="form-control" id="txtquote_tenda" name="txtquote_tenda" style="width: 100%;"><?php echo $aboutQuoteList->quote_text;?></textarea>                 
                    </div>
                    <div class="aboutquote-text">
                        <p id="aboutquote-text" >
                            <?php echo nl2br(htmlspecialchars_decode($aboutQuoteList->content)); ?>
                        </p>
                    </div>
                    <div class="edit-aboutquote">
                        <textarea class="form-control" id="txtcontent_tenda" name="txtcontent_tenda" style="width: 100%" rows="6"><?php echo $aboutQuoteList->content;?></textarea>                  
                    </div>
                    <hr class="space-sm"/>
                    <table class="table table-grid table-border align-left table-10">
                        <tbody>
                            <tr>
                                <td style="text-align: center">
                                    <div class="aboutquote-text">
                                        <h1 class="text-bold" id="sp_title1_tenda"><?php echo nl2br(htmlspecialchars_decode($aboutQuoteList->title1)); ?></h1>
                                    </div>
                                    <div class="edit-aboutquote">
                                        <input type="text" class="form-control" id="txttitle1_tenda" name="txttitle1_tenda" style="width: 100%;" value="<?php echo $aboutQuoteList->title1;?>">              
                                    </div>
                                    <div class="aboutquote-text">
                                        <h2 style="margin:0" id="sp_subtitle1_tenda"><?php echo nl2br(htmlspecialchars_decode($aboutQuoteList->subtitle1)); ?></h2>
                                    </div>
                                    <div class="edit-aboutquote">
                                        <input type="text" class="form-control" id="txtsubtitle1_tenda" name="txtsubtitle1_tenda" style="width: 100%;" value="<?php echo $aboutQuoteList->subtitle1;?>">              
                                    </div>
                                </td>
                                <td style="text-align: center">
                                    <div class="aboutquote-text">
                                        <h1 class="text-bold" id="sp_title2_tenda"><?php echo nl2br(htmlspecialchars_decode($aboutQuoteList->title2)); ?></h1>
                                    </div>
                                    <div class="edit-aboutquote">
                                        <input type="text" class="form-control" id="txttitle2_tenda" name="txttitle2_tenda" style="width: 100%;" value="<?php echo $aboutQuoteList->title2;?>">              
                                    </div>
                                    <div class="aboutquote-text">
                                        <h2 style="margin:0" id="sp_subtitle2_tenda"><?php echo nl2br(htmlspecialchars_decode($aboutQuoteList->subtitle2)); ?></h2>
                                    </div>
                                    <div class="edit-aboutquote">
                                        <input type="text" class="form-control" id="txtsubtitle2_tenda" name="txtsubtitle2_tenda" style="width: 100%;" value="<?php echo $aboutQuoteList->subtitle2;?>">              
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section> -->
    <!-- <section class="section-base section-color">
        <div class="container">
            <div class="pull-left div-settings dnone" style="position:absolute;left: 135px;z-index:999">
                <img title="Edit" src="<?php echo base_url();?>image/Icon_tools.png" class="btedit-title icon mini-icon-settings" />
                <img title="Save" src="<?php echo base_url();?>image/icon-save.png" class="btsave-title icon mini-icon-settings_30" />
                <img title="Cancel" src="<?php echo base_url();?>image/icon-delete.png" class="btcancel-title icon mini-icon-settings_32"/>
            </div>
            <div class="title text-center title-text">
                <h1 id="title2-master"><?php echo nl2br(htmlspecialchars_decode($aboutTitleList->title2)); ?></h1>
            </div>
            <div class="edit-title">
                <textarea class="form-control" id="txttitle2_master" name="txttitle2_master" style="width: 100%;"><?php echo $aboutTitleList->title2;?></textarea>                 
            </div>
            <hr class="space-sm" />
            <div class="pull-left div-settings dnone">
                <img title="Edit" id="btedit-how" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
            </div>
            <div class="grid-list" data-columns="4" data-columns-md="2" data-columns-sm="1">
                <div class="grid-box" id="howList">
                    <?php foreach($howList as $how) : ?>
                        <div class="grid-item">
                            <div class="cnt-box cnt-box-top-icon boxed text-center">
                                <i class="fa <?php echo $how->how_pict; ?>" style="text-align: center;"></i>
                                <div class="caption">
                                    <h2><?php echo $how->how_title;?></h2>
                                    <p>
                                        <?php echo $how->how_text;?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section> -->
    <!-- <section class="section-base" id="gallery-section">
        <div class="container">
            <div class="pull-left div-settings dnone" style="position:absolute;left: 135px;z-index:999">
                <img title="Edit" src="<?php echo base_url();?>image/Icon_tools.png" class="btedit-title icon mini-icon-settings" />
                <img title="Save" src="<?php echo base_url();?>image/icon-save.png" class="btsave-title icon mini-icon-settings_30" />
                <img title="Cancel" src="<?php echo base_url();?>image/icon-delete.png" class="btcancel-title icon mini-icon-settings_32"/>
            </div>
            <div class="title title-text">
                <h1 id="title3-master"><?php echo nl2br(htmlspecialchars_decode($aboutTitleList->title3)); ?></h1>
            </div>
            <div class="edit-title">
                <textarea class="form-control" id="txttitle3_master" name="txttitle3_master" style="width: 100%;"><?php echo $aboutTitleList->title3;?></textarea>                 
            </div>
            <div class="maso-list" data-columns="3" data-columns-md="2"  data-columns-sm="1">
                <div class="menu-inner">
                    <div class=" div-settings dnone" style="z-index: 999; position: absolute; left: 150px;">
                        <img title="Edit" id="btedit-category" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                    </div>
                    <div><i class="menu-btn"></i><span>Menu</span></div>
                    <ul class="categoryList">
                        <li class="active"><a data-filter="maso-item" href="#">All</a></li>
                        <?php foreach($categoryList as $category) : ?>
                           <li><a href="#" data-filter="cat-<?php echo $category->seq_no; ?>"><?php echo $category->category_name; ?></a></li>
                        <?php endforeach ?>
                        <li><a class="maso-order" data-sort="asc"></a></li>
                    </ul>
                </div>
                <div class="pull-left div-settings dnone" style="position: absolute; left: 150px;">
                    <img title="Edit" id="btedit-portfolio" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                </div>
                <div class="maso-list list-gallery">
                    <div class="maso-box" id="portfolio-header">
                        <?php foreach($imageList as $image) : ?>
                            <div class="maso-item cat-<?php echo $image->ID_category; ?>">
                                <a class="media-box media-box-full" href="<?php echo base_url() ?>images/works/large/<?php echo $image->imagePath; ?>">
                                    <img style="min-height: 241px" alt="" src="<?php echo base_url() ?>images/works/thumb/<?php echo $image->imagePath; ?>">
                                    <div class="caption">
                                        <h2><?php echo $image->imageDesc; ?></h2>
                                        <div class="extra-field"><?php echo $image->imageTitle; ?></div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                
                <hr class="space-sm"/>
                <div class="text-center">
                    <a href="<?php echo base_url();?>gallery" class="btn btn-sm width-190 full-width-sm" style="line-height: 30px; font-size: 15px;">View All</a>
                </div>
            </div>
        </div>
    </section> -->
    <!-- <section class="section-base section-color">
        <div class="">
            <div class="row align-items-center" data-anima="fade-bottom" data-time="1000">
                <div class="col-lg-6" style="padding: 0 0 0 150px;">
                    <hr class="space-md"/>
                    <div class="pull-left div-settings dnone" style="position:absolute;top:20px;z-index:999">
                        <img title="Edit" id="btedit-abouthome" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        <img title="Save" id="btsave-abouthome" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                        <img title="Cancel" id="btcancel-abouthome" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                    </div>
                    <div class="title abouthome-text">
                        <h1 id="sp_quote_text_home"><?php echo nl2br(htmlspecialchars_decode($aboutHomeList->quote_text)); ?></h1>
                    </div>
                    <div class="edit-abouthome">
                        <textarea class="form-control" id="txtquote_home" name="txtquote_home" style="width: 100%;"><?php echo $aboutHomeList->quote_text;?></textarea>                 
                    </div>
                    <div class="abouthome-text">
                        <p id="abouthome-text" >
                            <?php echo nl2br(htmlspecialchars_decode($aboutHomeList->content)); ?>
                        </p>
                    </div>
                    <div class="edit-abouthome">
                        <textarea class="form-control" id="txtcontent_home" name="txtcontent_home" style="width: 100%" rows="6"><?php echo $aboutHomeList->content;?></textarea>                  
                    </div>
                    <hr class="space-sm" />
                    <a href="#" class="btn btn-sm width-190 btn-text active" style="font-size: 15px;">About Us</a>
                    <a id="btDownloadCatalog" href="javascript:void(0)" class="btn btn-border btn-sm width-190 active full-width-sm pull-right" style="font-size: 15px;">Download Brosur</a>
                        <div class="pull-right div-settings dnone" style="position:relative;z-index:999">
                            <img title="Edit" id="btedit-catalog" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" style="width: 25px; height: 25px;"/>
                            <input class="vnone" type="file" id="fileCatalog" name="fileCatalog" accept="application/pdf" />
                        </div>
                    <hr class="space-sm" />

                    <table class="table table-grid table-border align-left table-10">
                        <tbody>
                            <tr>
                                <td style="text-align: center; vertical-align: middle;">
                                    <h2 class="text-bold">Call For a Quote:</h2>
                                </td>
                                <td style="text-align: left; line-height: 25px;">
                                    <div class="abouthome-text">
                                        <h4 class="text-bold" id="sp_simple_quote_home"><?php echo nl2br(htmlspecialchars_decode($aboutHomeList->simple_quote)); ?></h4>
                                    </div>
                                    <div class="edit-abouthome">
                                        <textarea class="form-control" id="txtsimple_home" name="txtsimple_home" style="width: 100%" rows="6"><?php echo $aboutHomeList->simple_quote;?></textarea>                  
                                    </div>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                    <hr class="space-md" />
                </div>
                <div class="col-lg-6 text-right">
                    <div class="pull-left div-settings dnone" style="position:absolute;right:100px;top:0px;z-index:999">
                        <img title="Edit" id="btedit-about-home" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        <input class="vnone" type="file" id="fnabout-home" name="fnabout-home" accept="image/*" />
                    </div>
                    <img id="imgabouthome" src="<?php echo base_url(); ?>images/<?php echo $aboutHomeList->picture;?>" alt="" />
                </div>
            </div>
        </div>
    </section> -->
    <!-- <section class="section-base ">
        <div class="container">
            <div class="row align-items-center" data-anima="fade-bottom" data-time="1000">
                <div class="col-lg-6">
                    <div class="pull-left div-settings dnone" style="position:absolute;left: 0;top: -30px;z-index:999">
                        <img title="Edit" src="<?php echo base_url();?>image/Icon_tools.png" class="btedit-title icon mini-icon-settings" />
                        <img title="Save" src="<?php echo base_url();?>image/icon-save.png" class="btsave-title icon mini-icon-settings_30" />
                        <img title="Cancel" src="<?php echo base_url();?>image/icon-delete.png" class="btcancel-title icon mini-icon-settings_32"/>
                    </div>
                    <div class="title title-text">
                        <h1 id="title4-master"><?php echo nl2br(htmlspecialchars_decode($aboutTitleList->title4)); ?></h1>
                    </div>
                    <div class="edit-title">
                        <textarea class="form-control" id="txttitle4_master" name="txttitle4_master" style="width: 100%;"><?php echo $aboutTitleList->title4;?></textarea>                 
                    </div>
                    <div class="title-text">
                        <p id="subtitle1-master"><?php echo nl2br(htmlspecialchars_decode($aboutTitleList->subtitle1)); ?></p>
                    </div>
                    <div class="edit-title">
                        <textarea class="form-control" id="txtsubtitle1_master" name="txtsubtitle1_master" style="width: 100%;"><?php echo $aboutTitleList->subtitle1;?></textarea>                 
                    </div>
                    <hr class="space-sm" />
                    <a href="#" class="btn btn-sm width-190 btn-text active" style="font-size: 15px;">Learn More    </a>
                    <hr class="space-sm">
                </div>
                <div class="col-lg-6 box-accordion-tenda">
                    <div class="pull-left div-settings dnone" style="position: absolute; top: 0; left: -45px;">
                        <img title="Edit" id="btedit-tenda" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                    </div>
                    <ul class="accordion-list" id="tendaList">
                        <?php foreach($tendaServiceList as $tenda) : ?>
                            <li>
                                <a href="#" style="font-size: 20px;"><?php echo $tenda->title; ?></a>
                                <div class="content">
                                    <p>
                                        <?php echo $tenda->subtitle; ?>
                                    </p>
                                </div>
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </section> -->
    <!-- <section class="section-base section-color">
        <div class="container">
            <div class="pull-left div-settings dnone" style="position:absolute;left: 135px;z-index:999">
                <img title="Edit" src="<?php echo base_url();?>image/Icon_tools.png" class="btedit-title icon mini-icon-settings" />
                <img title="Save" src="<?php echo base_url();?>image/icon-save.png" class="btsave-title icon mini-icon-settings_30" />
                <img title="Cancel" src="<?php echo base_url();?>image/icon-delete.png" class="btcancel-title icon mini-icon-settings_32"/>
            </div>
            <div class="title title-text text-center">
                <h1 id="title5-master"><?php echo nl2br(htmlspecialchars_decode($aboutTitleList->title5)); ?></h1>
            </div>
            <div class="edit-title">
                <textarea class="form-control" id="txttitle5_master" name="txttitle5_master" style="width: 100%;"><?php echo $aboutTitleList->title5;?></textarea>                 
            </div>
            <div class="pull-left div-settings dnone" style="position:absolute;left:110px;z-index:999">
                <img title="Edit" id="btedit-client" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
            </div>
            <div class="partner-slider" id="clientList">
                <?php foreach($customerList as $customer) : ?>
                    <div class="client-logo" style="margin: 20px 15px;"> <img class="img-responsive" style="max-height: 125px" src="<?php echo base_url() ?>images/sponsors/logos/<?php echo $customer->brand_path; ?>" alt="<?php echo $customer->brand_name; ?>"/> </div>
                <?php endforeach ?>
            </div>
        </div>
    </section> -->
    <!-- <section class="section-base">
        <div class="container">
            <div class="row" data-anima="fade-bottom" data-time="1000">
                <div class="col-lg-6">
                    <div class="title">
                        <p>Contact us now</p>
                    </div>
                    <div class="pull-left div-settings dnone" style="position:absolute;left: -60px;z-index:999">
                        <img title="Edit" src="<?php echo base_url();?>image/Icon_tools.png" class="btedit-title icon mini-icon-settings" />
                        <img title="Save" src="<?php echo base_url();?>image/icon-save.png" class="btsave-title icon mini-icon-settings_30" />
                        <img title="Cancel" src="<?php echo base_url();?>image/icon-delete.png" class="btcancel-title icon mini-icon-settings_32"/>
                    </div>
                    <div class="title title-text">
                        <h2 id="title6-master"><?php echo nl2br(htmlspecialchars_decode($aboutTitleList->title6)); ?></h2>
                    </div>
                    <div class="edit-title">
                        <textarea class="form-control" id="txttitle6_master" name="txttitle6_master" style="width: 100%;"><?php echo $aboutTitleList->title6;?></textarea>                 
                    </div>
                    <a href="<?php echo base_url();?>contact" class="btn-text active">Contact Us</a>
                </div>
                <div class="col-lg-6">
                    <form id="UCiFI" action="thtmekit/scripts/php/contact-form.php" class="form-box form-ajax form-ajax-wp" method="post" data-email="">
                        <div class="row">
                            <div class="col-lg-6">
                                <input id="Name" name="Name" placeholder="Name" type="text" class="input-text" required="">
                            </div>
                            <div class="col-lg-6">
                                <input id="Email" name="Email" placeholder="Email" type="email" class="input-text" required="">
                            </div>
                        </div>
                        <hr class="space-xs" />
                        <textarea id="Message" name="Message" placeholder="Message" class="input-textarea" required=""></textarea>
                        <button class="btn btn-xs" type="submit">Send message</button>
                        <div class="success-box">
                            <div class="alert alert-success">
                                Congratulations. Your message has been sent successfully.
                            </div>
                        </div>
                        <div class="error-box">
                            <div class="alert alert-warning">
                                Error, please retry. Your message has not been sent.
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section> -->
</main>
  <?php include "include/footer.php" ?>
  
  <!--=== GO TO TOP  ===-->
  <!-- <a href="#" id="back-to-top" title="Back to top">&uarr;</a> -->
  <!--=== GO TO TOP End ===-->
  
</div>
<!--=== Wrapper End ======-->

<!--=== Javascript Plugins ======-->
<?php include "include/include_js.php" ?>
<!--=== Javascript Plugins End ======-->

 <!--Slider Function-->
 <script type="text/javascript">
        var revapi;
        jQuery(document).ready(function() {
           revapi = jQuery('.tp-banner').revolution(
            {
                delay:9000,
                startwidth:1170,
                startheight:600,
                spinner:"spinner4",
                hideThumbs:10,
                fullWidth:"on",
                navigationType:"none",
                navigationArrows:"solo",
                navigationStyle:"preview4",
                forceFullWidth:"on"
            });
        }); 
    </script>
    <!--End Slider Function-->


</body>
</html>