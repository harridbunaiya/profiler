<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title><?php echo $config->title; ?></title>
        <!-- <title>Roker  - Software, Technology , Corporate, Creative, Multi-Purpose, Responsive And Retina Html5/css3 Template</title>  -->
        <meta name="keywords" content="conculta" />
        <meta name="description" content="Roker  - Corporate, Creative, Multi Purpose, Responsive And Retina Template">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <!-- Head Libs -->
        <script src="<?php echo base_url() ?>js2/modernizr.js"></script>    

        <?php include "include/include_css.php" ?>  

    </head>
    <body> 

    <?php include "include/text_modal.php"; ?>
    <?php include "include/cropping_modal.php"; ?>

    <!--Preloader-->
    <div class="preloader">
        <div class="status">&nbsp;</div>
    </div>
    <!--End Preloader-->

    

    <!-- layout-->
    <div id="layout" class="layout-wide">

        <?php include "include/header.php" ?> 
    
        <!-- Slide Section-->    
        <div class="pull-left div-settings dnone" style="position:absolute;left:100px;top:150px;z-index:999">
            <img title="Edit" id="btedit-home" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
        </div>
        <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="home-1"
                 data-source="gallery" 
                 style="padding:0px;background-image:url('<?php echo base_url(); ?>images/slide/slides/<?php echo $sliderList[0]->imagePath; ?>');background-repeat:no-repeat;background-size:cover;background-position:center center;">
                <!-- START REVOLUTION SLIDER 5.4.7.4 fullwidth mode -->
                <div id="rev_slider_4_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.4">
                    <ul>
                        <?php
                            $index = 0; 
                            foreach($sliderList as $slider) : ?>
                            <!-- SLIDE  -->
                            <li data-index="rs-<?php echo $index ?>" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                                data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                                 data-rotate="0" data-saveperformance="off"
                                data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5=""
                                data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                <!-- MAIN IMAGE -->
                                <img src="<?php echo base_url(); ?>images/slide/slides/<?php echo $slider->imagePath; ?>" alt="" title="hotel-wp-demo3-slider.jpg" width="1422"
                                     height="570" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                                     class="rev-slidebg" data-no-retina>
                                <!-- LAYERS -->
                                <?php echo $slider->content; ?>
                            </li>
                        <?php
                            $index++; 
                            endforeach; ?>
                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div><!-- END REVOLUTION SLIDER -->
        <!-- End Slide Section-->

        <!-- box-action -->
        <section class="box-action">
            <div class="container">
                <div class="title">
                    
                    <p class="lead">
                        <marquee>
                            <?php foreach($testimoniList as $testimoni) : ?>
                                ( <strong><?php echo date('d-M-Y', strtotime($testimoni->created_date));?></strong> ) - 
                                <?php echo $testimoni->testimoni_text;?> |
                            <?php endforeach ?>

                        </marquee>
                    </p>
                   
                </div>
                <!-- <div class="button">
                    <a href="<?php echo base_url() ?>contact">Kontak Kami</a>
                </div> -->
            </div>
        </section>
        <!-- End box-action-->

        <!-- Info About Us -->
        <section class="content_info" id="profil">
            <div class="paddings">
                <div class="container">
                    <!-- <div class="titles-heading">
                    
                        <div class="line"></div>
                        <h1>Kenapa Memilih Kami ?
                            <span>
                              <i class="fa fa-star"></i>  
                              tristique senectus malesuada
                              <i class="fa fa-star"></i>  
                            </span>
                        </h1>
                    </div>  -->
                    <div class="row">
                        <div class="pull-left div-settings dnone" style="position:absolute;left:110px;z-index:999">
                            <img title="Edit" id="btedit-about" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                            <img title="Save" id="btsave-about" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                            <img title="Cancel" id="btcancel-about" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                        </div>
                        <div class="col-md-7">
                             <h2>Tentang Komir</h2>
                             <p class="about-text" id="about-text"><?php echo nl2br(htmlspecialchars_decode($aboutList->content)); ?></p>
                             <div class="edit-about">
                                <textarea class="text-about fluid" id="txtcontent" name="txtcontent"><?php echo nl2br($aboutList->content);?></textarea>                  
                             </div>
                        </div>

                        <div class="col-md-5">
                            <div class="pull-left div-settings dnone" style="position:absolute; left:-30px;z-index:999">
                                <img title="Edit" id="btedit-about-pict" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                                <input type="file" id="fnabout-pict" class="vnone" name="fnabout-pict" accept="image/*" />
                            </div>
                            <img id="imgaboutpict" style="width: 460px" src="<?php echo base_url();?>images/<?php echo $aboutList->picture; ?>"/>    
                        </div>

                    </div>             
                </div>
                <!-- End Container-->
            </div>
        </section>
        <!-- End Info About Us-->


        <!-- Services -->
        <!-- <section class="content_info">
            <div class="paddings">
                <div class="container"> -->
                    <!-- Icon Big -->
                    <!-- <i class="fa fa-cogs icon-section top right"></i>                 -->
                    <!-- End Icon Big -->

                    <!-- Titles Heading --> 
                    <!-- <div class="titles-heading">
                    
                        <div class="line"></div>
                        <h1>Kenapa Memilih Kami ?
                            <span>
                              <i class="fa fa-star"></i>  
                              tristique senectus malesuada
                              <i class="fa fa-star"></i>  
                            </span>
                        </h1>
                    </div>  -->
                    <!-- End Titles Heading --> 
                    <!-- <div class="pull-left div-settings dnone">
                        <img title="Edit" id="btedit-how" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                    </div> -->

                    <!-- Row fuid-->
                    <!-- <div class="row padding-top" id="howList"> -->
                        <!-- Item service - 01 -->
                        <!-- <?php foreach($howList as $how) : ?>
                        <div class="col-md-4">
                            <div class="item-service border-right">
                                <div class="row head-service">
                                    <div class="col-md-2">
                                        <i class="fa <?php echo $how->how_pict; ?>"></i>                             
                                    </div>
                                    <div class="col-md-10">
                                        <h4><?php echo $how->how_title; ?></h4>
                                        <h5><?php echo $how->how_subtitle; ?></h5>
                                    </div>
                                </div>                          
                                <p><?php echo $how->how_text; ?></p>
                            </div>
                        </div>
                        <?php endforeach ?>       -->
                        <!-- End Item service-->
                    <!-- </div> -->
                    <!-- End Row fuid-->
                <!-- </div> -->
                <!-- End Container-->
            <!-- </div>
        </section> -->
        <!-- End Services-->

        <!-- Services Slide -->
        <section class="content_info">
            <div class="pull-left div-settings dnone" style="position:absolute;left:100px;top:150px;z-index:999">
                <img title="Edit" id="btedit-home1" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
            </div>
            <div class="section-gray paddings borders animation-services">
                <!-- Slide Services  -->
                <ul id="slide-services" class="sliderServices" style="text-align: center;">
                <?php foreach($sliderserviceList as $sliderservice) : ?>
                    <!-- Item Slide Services  -->
                    <li>
                        <!-- <div class="container">
                            <div class="row"> -->
                                <!-- image-animations  -->
                                <!-- <div class="col-md-5 image-animations padding-top"> -->
                                   <img style="width: 90%" src="<?php echo base_url() ?>images/services-carousel/<?php echo $sliderservice->imagePath; ?>" alt="" class="image-big">
                                <!-- </div> -->
                                <!-- End image-animations  -->
                                
                                <!-- features-elementns  -->
                               
                               <!-- End feature-elements  -->
                           <!-- </div>  -->
                           <!-- End Row  -->
                       <!-- </div>  -->
                       <!-- End Container  -->
                    </li>
                    <!-- End Item Slide Services  -->                   
                <?php endforeach ?>
                </ul>  
                <!-- End Slide Services  -->    
            </div>  
        </section>
        <!-- End Services Slide-->

        <!-- Clients -->
        <!-- <section class="content_info">
            <div class="paddings clients">
                <div class="container">
                    <div class="row">   -->
                        <!-- title-downloads -->             
                        <!-- <h1 class="title-downloads">
                            <span class="logo-clients">Sudah</span> menyelesaikan
                            <span class="responsive-numbers">
                                <span>2</span>
                                ,
                                <span>3</span>
                                <span>8</span>
                                <span>9</span>
                                ,
                                <span>5</span>
                                <span>1</span>
                                <span>8</span>
                            </span>
                            proyek.
                        </h1>   -->
                        <!-- End title-downloads -->     
                        
                        <!-- subtitle-downloads --> 
                        <!-- <div class="subtitle-downloads">
                            <div class="line"></div>
                            <h4>Memiliki Partner Dengan <i class="fa fa-star"></i></h4>
                        </div>  -->
                        <!-- End subtitle-downloads --> 

                        <!-- <div class="pull-left div-settings dnone" style="position:absolute;left:400px;top:200px;z-index:999">
                            <img title="Edit" id="btedit-fact" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        </div> -->

                        <!-- Image Clients Downloads --> 
                        <!-- <ul class="image-clients-downloads" id="fact-retrieve">
                        <?php foreach($factvisibleList as $fact) : ?>
                            <li><img src="<?php echo base_url() ?>images/clients-downloads/<?php echo $fact->bg_pict; ?>" alt=""></li>
                        <?php endforeach ?>
                        </ul> -->
                        <!-- End Image Clients Downloads --> 
                    <!-- </div>                
                </div>
            </div>
        </section> -->
        <!-- End Clients -->

        <section class="content_info ptb-90">
            <div class="paddings">
                <!-- <div class="container"> -->
                    <div class="row" style="margin: 20px;">
                        <div class="col-md-7" style="border: solid 1px; padding: 0;">
                            <div class="panel">
                                <div class="panel-heading" style="background: #f9f9f9; color: #000;">
                                    <h4 class="panel-title"><span class="text-bold "><i class="fa fa-check"></i></span>
                                        STATISTIK CAPAIAN DAERAH
                                    </h4>
                                </div>
                                <div class="chart" style="padding: 40px 0;">             
                                    <!-- Sales Chart Canvas -->
                                    <div id="chartsa" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                <!-- </div> -->
            </div>
            
        </section>
        

        <!-- Sponsors -->
        <section class="content_info" id="partner">
            <!-- Parallax Background -->
            <div class="bg_parallax image_01_parallax"></div>
            <!-- Parallax Background -->

            <!-- Content Parallax-->
            <section class="opacy_bg_02 paddings sponsors">
                <div class="container wow fadeInUp hide-nav">
                    <div class="pull-left div-settings dnone" style="position:absolute;left:300px;top:80px;z-index:999">
                        <img title="Edit" id="btedit-client" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                    </div>
                    <h2 class="title-resalt" style="padding-bottom: 40px;">Beberapa Partner Kami , Antara Lain</h2>
                    <!-- Sponsors -->
                    <ul id="sponsors" class="client-carousel">
                        <!-- Sponsor -->
                        <?php foreach($customerList as $customer) : ?>
                        <li class="text-center" data-toggle="tooltip" title data-original-title="<?php echo $customer->brand_name; ?>">
                            <a href="#"><img style="max-height: 100px" src="<?php echo base_url() ?>images/sponsors/logos/<?php echo $customer->brand_path; ?>" alt=""></a>
                        </li>  
                        <!-- Item Sponsor -->
                        <?php endforeach ?>
                    </ul>
                    <!-- End Sponsors --> 

                    <!-- <div class="circle">
                        <i class="fa fa-thumbs-up"></i>
                    </div> -->
                </div> 
            </section>  
            <!-- End Content Parallax--> 
        </section>
        <!-- End Sponsors -->

        <!-- post-testimonials -->
        <section class="content_info" >
            <div class="paddings post-testimonials">
                <div class="container wow bounceIn" data-wow-offset="10" data-wow-duration="1.5s" data-wow-delay="0.5s">
                   <div class="row">  
                        <!-- Post --> 
                        <div class="col-md-6">
                            <h3>Berita Terbaru</h3>
                            <div class="pull-left div-settings dnone" style="position:absolute;left:200px;top:5px;z-index:999">
                                <img title="Edit" id="btedit-news" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                            </div>
                             <!-- Box -->
                            <ul class="boxNew" id="blogList">
                                <?php foreach($blogList as $blog) : ?>
                                <!-- Item Post --> 
                                <li class="row" >
                                    <!-- Date --> 
                                    <div class="col-md-3">
                                        <div class="photo">
                                            <img src="<?php echo base_url() ?>image/blog/<?php echo $blog->image; ?>" alt="">
                                        </div>
                                        <p class="text-center"><?php echo date('d-M-Y', strtotime($blog->created_date)); ?></p>
                                    </div>
                                    <!-- End Date --> 
                                    <div class="col-md-7">
                                        <div class="info">
                                            <h5><span style="font-weight: 600;"><?php echo $blog->title; ?></span></h5>
                                            <!-- <p><?php  echo substr($blog->blog_text, 0,45); ?> </p> -->
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="link">
                                        <a href="<?php echo base_url()."blog/detail/".$blog->ID_blog; ?>"><i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </li>
                                <!-- End Item Post --> 
                                <?php endforeach ?>

                            </ul>
                            <!-- End Box -->
                        </div>
                        <!-- End Post --> 

                        <!-- Testimonial --> 
                        <div class="col-md-6">
                            <h3>Pengumuman Terbaru</h3>
                            <div class="pull-left div-settings dnone" style="position:absolute;left:220px;top:5px;z-index:999">
                                <img title="Edit" id="btedit-testimoni" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                            </div>
                            <!-- Box -->
                            <ul class="boxNew" id="testimoniList">
                                
                                <!-- Item testimonial --> 
                                <?php foreach($testimoniList as $testimoni) : ?>
                                <li class="row">
                                    <!-- Date --> 
                                    <div class="col-md-3">
                                        <div class="date"><span><i class="fa fa-calendar-o"></i><?php echo date('M', strtotime($testimoni->created_date)); ?></span><?php echo date('d', strtotime($testimoni->created_date)); ?></div>
                                    </div>
                                    <!-- End Date -->
                                    <div class="col-md-9">
                                        <div class="name">
                                            <h4><?php echo $testimoni->person_name; ?>
                                                <span><?php echo $testimoni->corporation_name; ?></span>
                                            </h4>                                        
                                        </div>
                                        <p><?php echo $testimoni->testimoni_text; ?></p>
                                    </div>
                                </li>
                                <?php endforeach ?>
                                <!-- End Item testimonial --> 
                            </ul>
                            <!-- End Box -->
                        </div>
                        <!-- End Testimonial --> 
                   </div>                
                </div>
            </div>
        </section>
        <!-- End post-testimonials -->

        <section class="content_info" id="kontak-kami" style="padding-top: 90px; padding-bottom: 90px; background: #f7f7f7 url('../images/patterns/cubes.png');">
            <!-- Content Right -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="text-center text-bold">KONTAK KAMI</h2>
                        <hr class="tall">
                        
                        <!-- Tabs -->
                        <div class="tabs">
                            <!-- Tab nav -->
                            <!-- <ul class="nav nav-tabs">
                                <li class="active"><a href="#contactUs" data-toggle="tab">
                                    <i class="fa fa-envelope"></i> Contact Us</a>
                                </li>
                            </ul> -->
                            <!-- End Tab nav -->

                            <!-- Tab Content -->
                            <div class="tab-content" style="background-color: #fff;">
                                <!-- Tab item -->
                                <div class="tab-pane active" id="contactUs">
                                    
                                    <h4>Contact Us</h4>

                                    <!-- Form Contact -->
                                    <form class="form-contact" action="php/send-mail-contact.php">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label>Your name *</label>
                                                    <input type="text"  required="required" value="" maxlength="100" class="form-control" name="Name" id="name">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Your email address *</label>
                                                    <input type="email"  required="required" value="" maxlength="100" class="form-control" name="Email" id="email">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label>Comment *</label>
                                                    <textarea maxlength="5000" rows="10" class="form-control" name="message"  style="height: 138px;" required="required" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="submit" value="Send Message" class="btn btn-lg btn-primary">
                                            </div>
                                        </div>
                                    </form>
                                    <!-- End Form Contact --> 
                                    <div class="result"></div>
                                </div>
                                <!-- End Tab item -->

                            </div>
                            <!-- End Tab Content -->
                        </div>  
                        <!-- End Tabs -->                     

                    </div>
                </div>
            </div>
            <!-- End Content Right -->
        </section>

        <!-- Important Info -->
        <!-- <section class="content_info">
            <div class="pull-left div-settings dnone" style="position:absolute;right:100px;top:60px;z-index:999">
                <img title="Edit" id="btedit-intro" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                <input class="vnone" type="file" id="introWallpaper" name="introWallpaper" accept="image/*" />
            </div>
            <div class="pull-left div-settings dnone" style="position: absolute; left: 70px; top: 45px;">
                <img title="Edit" id="btedit-introContent" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                <img title="Save" id="btsave-introContent" src="<?php echo base_url();?>image/icon-save.png" class="dnone icon mini-icon-settings_30" />
                <img title="Cancel" id="btcancel-introContent" src="<?php echo base_url();?>image/icon-delete.png" class="dnone icon mini-icon-settings_32"/>
            </div>
            <div id="pattern-important-info" class="paddings pattern-important-info important-info">
                <div class="container">
                   <div class="row">   
                        <div class="col-md-9 introContent-label" id="introContent">
                            <?php echo $introList->content; ?>
                        </div>
                        <div class="dnone edit-introContent">
                            <textarea id="txtintroContent" style="width:100%; height: 300px;"><?php echo $introList->content; ?></textarea>
                        </div>
                   </div>                
                </div>
            </div>
        </section> -->
        <!-- End Important Info -->

        

        <?php include "include/footer.php" ?>

    </div>
    <!-- End layout-->


    <!-- Scripts -->
    <?php include "include/include_js.php" ?>

    <!--Slider Function-->
    <script type="text/javascript">
        var revapi;
        jQuery(document).ready(function() {
           revapi = jQuery('.tp-banner').revolution(
            {
                delay:9000,
                startwidth:1170,
                startheight:580,
                spinner:"spinner4",
                hideThumbs:10,
                fullWidth:"on",
                navigationType:"none",
                navigationArrows:"solo",
                navigationStyle:"preview4",
                forceFullWidth:"on"
            });
        }); 
    </script>
    <!--End Slider Function-->
        
    </body>
</html>