<!-- === Footer Start ======-->
<div style="position: fixed; bottom: 10px; left: 10px; width: 90px;">
    <a href="https://wa.me/628112220205" target="_blank">
        <img src="<?php echo base_url(); ?>images/whatsapp.png" alt="whatsapp number">
    </a>
</div>

<i class="scroll-top-btn scroll-top show"></i>
    <footer class="light">
        <!-- <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <img class="logo-default scroll-show logo-light" style="width: 250px; margin-bottom: 20px;" src="<?php echo base_url();?>images/<?php echo $config->logo_light; ?>" alt="logo" />
                    <div class="div-settings dnone" style="min-height:40px; position: absolute; left: -55px;">
                        <img title="Edit" id="btedit-contactmsg" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        <img title="Save" id="btsave-contactmsg" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                        <img title="Cancel" id="btcancel-contactmsg" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                    </div>
                    <p class="span-contact" id="span-contact"><?php echo nl2br(htmlspecialchars_decode($contactList->contactMsg)); ?></p>
                    <div class="edit-contactmsg">
                        <textarea class="text-contact" id="txtcontact" name="txtcontact" style="color: #000; width: 100%;"><?php echo str_replace("<br/>","\n",$contactList->contactMsg);?></textarea>                  
                    </div>
                </div>
                <div class="col-lg-4">
                    <h3>Resources</h3>
                    <ul class="icon-list icon-line">
                        <li><a href="#">Partners and advertising</a></li>
                        <li><a href="#">About us and the company</a></li>
                        <li><a href="#">Services and projects</a></li>
                        <li><a href="#">Contact us online</a></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <div class="pull-left div-settings dnone" style="position:absolute; left:-55px; z-index:999; margin-top: 20px;">
                        <img title="Edit" id="btedit-setting" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        <img title="Save" id="btsave-setting" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                        <img title="Cancel" id="btcancel-setting" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                    </div>
                    <div class="text-center">
                        <h2>Follow Us</h2>    
                    </div>
                    <div class="icon-links icon-social icon-links-grid social-colors setting-text" style="margin-top: 20px;">
                        <a class="facebook lnk-facebook" href="<?php echo $config->facebook_link; ?>" target="_blank"><i class="icon-facebook"></i></a>
                        <a class="twitter lnk-twitter" href="<?php echo $config->twitter_link; ?>" target="_blank"><i class="icon-twitter"></i></a>
                        <a class="instagram lnk-google" href="<?php echo $config->google_link; ?>" target="_blank"><i class="icon-instagram"></i></a>
                    </div>
                    <div class="attr-nav">
                        <table class="edit-setting social-media-dark" border="1" style="position: absolute; width: 300px; color: #000; z-index: 999;">
                            <tr>
                                <td class="text-center"><i class="fa fa-facebook" style="font-size: 29px; color: #fff;"></i></td>
                                <td><input type="text" value="<?php echo $config->facebook_link; ?>" class="fluid" placeholder="Insert facebook link here.." name="txtFacebook" id="txtFacebook" maxlength="250" /></td>
                            </tr>
                            <tr>
                                <td class="text-center"><i class="fa fa-whatsapp" style="font-size: 29px; color: #fff;"></i></td>
                                <td><input type="text" value="<?php echo $config->twitter_link; ?>" class="fluid" placeholder="Insert Whatsapp link here.." name="txtTwitter" id="txtTwitter" maxlength="250" /></td>
                            </tr>
                            <tr>
                                <td class="text-center"><i class="fa fa-instagram" style=" font-size: 29px; color: #fff;"></i></td>
                                <td><input type="text" value="<?php echo $config->google_link; ?>" class="fluid" placeholder="Insert Google Plus link here.." name="txtGooglePlus" id="txtGooglePlus" maxlength="250" /></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="footer-bar" style="background:#900A0A;">
            <div class="container text-center">
                <div class="pull-left div-settings dnone" style="position:absolute; left:-55px; z-index:999; margin-top: 20px;">
                    <img title="Edit" id="btedit-setting" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                    <img title="Save" id="btsave-setting" src="<?php echo base_url();?>image/icon-save.png" class="icon mini-icon-settings_30" />
                    <img title="Cancel" id="btcancel-setting" src="<?php echo base_url();?>image/icon-delete.png" class="icon mini-icon-settings_32"/>
                </div>
                <div class="social-colors setting-text sosmed-profiler" style="margin-top: 20px;">
                    <a class="" href="<?php echo $config->facebook_link; ?>" target="_blank"><i class="icon-facebook"></i></a>
                    <a class="" href="<?php echo $config->twitter_link; ?>" target="_blank"><i class="icon-twitter"></i></a>
                    <a class="" href="<?php echo $config->google_link; ?>" target="_blank"><i class="icon-instagram"></i></a>
                </div>
                <div class="attr-nav">
                    <table class="edit-setting social-media-dark" border="1" style="position: absolute; width: 300px; color: #000; z-index: 999;">
                        <tr>
                            <td class="text-center"><i class="fa fa-facebook" style="font-size: 29px; color: #fff;"></i></td>
                            <td><input type="text" value="<?php echo $config->facebook_link; ?>" class="fluid" placeholder="Insert facebook link here.." name="txtFacebook" id="txtFacebook" maxlength="250" /></td>
                        </tr>
                        <tr>
                            <td class="text-center"><i class="fa fa-whatsapp" style="font-size: 29px; color: #fff;"></i></td>
                            <td><input type="text" value="<?php echo $config->twitter_link; ?>" class="fluid" placeholder="Insert Whatsapp link here.." name="txtTwitter" id="txtTwitter" maxlength="250" /></td>
                        </tr>
                        <tr>
                            <td class="text-center"><i class="fa fa-instagram" style=" font-size: 29px; color: #fff;"></i></td>
                            <td><input type="text" value="<?php echo $config->google_link; ?>" class="fluid" placeholder="Insert Google Plus link here.." name="txtGooglePlus" id="txtGooglePlus" maxlength="250" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="container text-center">
                <span style="float: none;">© 2020 Hak cipta Edo andrefson. All Rights Reserved.</span>
                <!-- <span><a href="<?php echo base_url(); ?>">tendaguna.com</a></span> -->
            </div>
        </div>
    </footer>
  <!--=== Footer End ======