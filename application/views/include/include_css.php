<!-- Favicone Icon -->
<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>image/<?php echo $config->favicon; ?>" />
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>image/<?php echo $config->favicon; ?>" />
<link rel="apple-touch-icon" href="<?php echo base_url(); ?>image/<?php echo $config->favicon; ?>" />



<!-- Cropper -->
<link rel="stylesheet" href="<?php echo base_url(); ?>plugin/cropper-plugin/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>plugin/cropper-plugin/css/tether.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>plugin/cropper-plugin/dist/cropper.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>plugin/cropper-plugin/css/main.css">

<!-- Skins Theme -->
<link rel="stylesheet" href="<?php echo base_url() ?>media/icons/iconsmind/line-icons.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css2/bootstrap-grid.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css2/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css2/style.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css2/glide.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css2/magnific-popup.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css2/content-box.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css2/contact-form.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css2/media-box.css">
<link rel="stylesheet" href="<?php echo base_url() ?>css2/skin.css">
<link rel="stylesheet" href="<?php echo base_url() ?>js2/rs-plugin/css/settings.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style-admin.css"><!-- Admin CSS -->
<!-- <link rel="stylesheet" href="<?php echo base_url() ?>/css/font-awesome_icon-font.css"> -->

<link rel="stylesheet" href="<?php echo base_url() ?>css/slick/slick.css">

<script type="text/javascript">
    var base_url="<?php echo base_url();?>";
    var max_size="<?php echo $this->config->item("max_size_upload");?>";
    var title="<?php echo $config->title;?>";
</script>


