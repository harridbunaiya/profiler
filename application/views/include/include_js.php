<!--  Javascript Document -->
<script src="<?php echo base_url() ?>js2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/plugins/bootstrap.js"></script>
<script src="<?php echo base_url() ?>js2/isotope.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/main.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/parallax.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/glide.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/magnific-popup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/tab-accordion.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/imagesloaded.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/progress.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/custom.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/contact-form/contact-form.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js2/pagination.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js/slick/slick.js" type="text/javascript"></script>
<!-- <script src="<?php echo base_url() ?>js2/maps.min.js"></script> -->

<!--Slide Revolution-->
<script type="text/javascript" src="<?php echo base_url() ?>js2/rs-plugin/js/jquery.themepunch.tools.min.js" ></script>      
<script type='text/javascript' src='<?php echo base_url() ?>js2/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>  
	
<script src="<?php echo base_url() ?>js/home.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js/main_admin.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js/manage_events.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>js/manage_file.js" type="text/javascript"></script>



<script type="text/javascript" src="<?php echo base_url(); ?>js/plugins/ckeditor/ckeditor.js"></script>

<!-- Cropper -->
<script src="<?php echo base_url(); ?>plugin/cropper-plugin/js/tether.min.js"></script> 
<script src="<?php echo base_url(); ?>plugin/cropper-plugin/dist/cropper.js"></script>
<script src="<?php echo base_url(); ?>plugin/cropper-plugin/js/main.js"></script>

<!-- Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-141283177-1"></script>
<script src="<?php echo base_url(); ?>js/gtag.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>js/plugins/jquery.dataTables.min.js"></script>

<!-- <script src="<?php echo base_url(); ?>js/map.js"></script> -->

<!-- Google Maps -->
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4I_1YfmhjbSwAR0H1dFIkNuUYreOGgU0&callback=sc_Map">
</script>

<!-- Google Map Script -->
<script>
    var map = null;
    var marker = null;
    var title = "Purepack Office";
    // Initialize and add the map
    function sc_Map() {
        var langitude = $("#hid_langitude").val();
        var latitude = $("#hid_latitude").val();

        // The location of London
        var manutd = {
            lat: parseFloat(latitude), 
            lng: parseFloat(langitude)
        };

        var style = 
		[
			{
				"featureType": "administrative",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#444444"
					}
				]
			},
			{
				"featureType": "landscape",
				"elementType": "all",
				"stylers": [
					{
						"color": "#f2f2f2"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"lightness": 45
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "all",
				"stylers": [
					{
						"color": "#505a65"
					},
					{
						"visibility": "on"
					}
				]
			}
		]

        // The map, centered at London
        map = new google.maps.Map(
            document.getElementById('google-map'), {zoom: 16, center: manutd, zoomControl : true});
        // The marker, positioned at London
        marker = new google.maps.Marker({position: manutd, map: map, title: title});

        // Create the DIV to hold the control and call the CenterControl()
        // constructor passing in this DIV.
        var centerControlDiv = document.createElement('div');
        var centerControl = new CenterControl(centerControlDiv, map);

        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
    }

    function CenterControl(controlDiv, map) {

        var langitude = $("#hid_langitude").val();
        var latitude = $("#hid_latitude").val();

        var manutd = {
            lat: parseFloat(latitude), 
            lng: parseFloat(langitude)
        }
        
    // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click to recenter the map';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = '<span class="fa fa-map-marker"></span> &nbsp;Back to Current Pin';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
            map.setCenter(manutd);
        });

    }

    function placeMarker(position)
    {
        if (marker != null) marker.setMap(null);
        marker = new google.maps.Marker({
            position: position,
            map : map,
            title : title
        });
        position2 = position;
    }
</script>

<script>function setREVStartSize(e) {
    try {
        e.c = jQuery(e.c);
        var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
        if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
            f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
        }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
            var u = (e.c.width(), jQuery(window).height());
            if (void 0 != e.fullScreenOffsetContainer) {
                var c = e.fullScreenOffsetContainer.split(",");
                if (c) jQuery.each(c, function (e, i) {
                    u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
            }
            f = u
        } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
        e.c.closest(".rev_slider_wrapper").css({height: f})
    } catch (d) {
        console.log("Failure at Presize of Slider:" + d)
    }
};</script>

<script>
    var revapi4,
        tpj;
    (function () {
        if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad); else onLoad();

        function onLoad() {
            if (tpj === undefined) {
                tpj = jQuery;
                if ("off" == "on") tpj.noConflict();
            }
            if (tpj("#rev_slider_4_1").revolution == undefined) {
                revslider_showDoubleJqueryError("#rev_slider_4_1");
            } else {
                revapi4 = tpj("#rev_slider_4_1").show().revolution({
                    sliderType: "standard",
                    sliderLayout: "fullwidth",
                    dottedOverlay: "none",
                    startheight:500,
                    delay: 9000,
                    navigation: {
                        keyboardNavigation: "off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation: "off",
                        mouseScrollReverse: "default",
                        onHoverStop: "off",
                        arrows: {
                            style: "zeus",
                            enable: true,
                            hide_onmobile: false,
                            hide_onleave: false,
                            tmp: '<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 30,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 20,
                                v_offset: 0
                            }
                        }
                        ,
                        bullets: {
                            enable: true,
                            hide_onmobile: false,
                            style: "hermes",
                            hide_onleave: false,
                            direction: "horizontal",
                            h_align: "center",
                            v_align: "bottom",
                            h_offset: 0,
                            v_offset: 100,
                            space: 10,
                            tmp: ''
                        }
                    },
                    viewPort: {
                        enable: true,
                        outof: "wait",
                        visible_area: "80%",
                        presize: false
                    },
                    responsiveLevels: [1240, 1024, 778, 480],
                    visibilityLevels: [1240, 1024, 778, 480],
                    gridwidth: [1240, 1024, 778, 480],
                    gridheight: [672, 600, 500, 400],
                    lazyType: "none",
                    shadow: 0,
                    spinner: "off",
                    stopLoop: "off",
                    stopAfterLoops: -1,
                    stopAtSlide: -1,
                    shuffle: "off",
                    autoHeight: "off",
                    disableProgressBar: "on",
                    hideThumbsOnMobile: "off",
                    hideSliderAtLimit: 0,
                    hideCaptionAtLimit: 0,
                    hideAllCaptionAtLilmit: 0,
                    debugMode: false,
                    fallbacks: {
                        simplifyAll: "off",
                        nextSlideOnWindowFocus: "off",
                        disableFocusListener: false,
                    }
                });
            }
            ;
            /* END OF revapi call */

        };
        /* END OF ON LOAD FUNCTION */
    }());
    /* END OF WRAPPING FUNCTION */
</script>


<script>
    // var d = new Date();
    // document.getElementById("day").setAttribute('value',(d.getDate())+'.');
    // document.getElementById("month").setAttribute('value',format_month());
    // document.getElementById("multidate").setAttribute('value',format_full());

    // document.getElementById("day2").setAttribute('value',(d.getDate()+1)+'.');
    // document.getElementById("month2").setAttribute('value',format_month());

    function format_full() {
        var months = ['January','February','March','April','May','June','July','August','September','September','November','December'];
        return months[d.getMonth()]+' '+d.getDate()+', '+d.getFullYear();
    }
    function format_month() {
        var months = ['Jan', 'Feb', 'March', 'April', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return months[d.getMonth()]+'. '+d.getFullYear();
    }

    // Format Number
    function formatThousand(x, char) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, char);
    }

</script>