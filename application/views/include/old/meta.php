<!-- Update your html tag to include the itemscope and itemtype attributes. -->
<html itemscope itemtype="http://schema.org/Gallery">

<!-- Add the following three tags inside head. -->
<meta content="We are an online platform for those of you who love to travel..">
<meta name="keywords" content="holidaymood,agency,design,indonesia,jordi,edbert,art,portfolio,work,ability" />
<meta itemprop="name" content="Tan Jordy">
<meta itemprop="description" content="We are an online platform for those of you who love to travel..">
<meta itemprop="image" content="http://tanjordy.com/image/favicon.png">
<meta name="description" content="Author: Tan Jordy"  />
<meta property="og:title" content="Tan Jordy" />
<meta property="og:type" content="article" />
<meta property="og:image" content="http://tanjordy.com/image/favicon.png" />
<meta property="og:description" content="" />
<meta property="og:url" content="http://www.tanjordy.com/" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />