<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title><?php echo $config->title; ?></title>
        <!-- <title>Roker  - Software, Technology , Corporate, Creative, Multi-Purpose, Responsive And Retina Html5/css3 Template</title>  -->
        <meta name="keywords" content="conculta" />
        <meta name="description" content="Roker  - Corporate, Creative, Multi Purpose, Responsive And Retina Template">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <?php include "include/include_css.php" ?>  

        <!-- Head Libs -->
        <script src="<?php echo base_url() ?>js2/modernizr.js"></script>

    </head>
    <body> 

    <?php include "include/text_modal.php"; ?>
    <?php include "include/cropping_modal.php"; ?>

    <!--Preloader-->
    <div class="preloader">
        <div class="status">&nbsp;</div>
    </div>
    <!--End Preloader-->
    
    <!-- layout-->
    <div id="layout" class="layout-wide">
        <?php include "include/header.php" ?>


        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="<?php echo base_url(); ?>">Home</a> / Layanan
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1>Layanan
                            <span class="subtitle-section">
                                Produk Kami
                                <span class="left"></span>
                                <span class="right"></span>
                            </span>
                            <span class="line-title"></span>
                        </h1>
                    </div>
                    <!-- End Title--> 
                </div>
                <!-- End Title -Search --> 

            </div>
        </section>   
        <!-- End Title Section --> 

        <!-- box-action -->
        <section class="box-action">
            <div class="container">
                <div class="title">
                    <p class="lead">Implement an action bar at any section of the page!</p>
                </div>
                <div class="button">
                    <a href="<?php echo base_url() ?>contact" target="_blank">Kontak</a>
                </div>
            </div>
        </section>
        <!-- End box-action-->


        <!-- Carousel Services -->
        <section class="padding-top services-carousel position-relative">      

                <i class="fa fa-cogs icon-section right"></i>

                <!-- Title Heading --> 
                <div class="titles-heading">
                    <div class="line"></div>
                     <h1>Layanan Kami
                        <span>
                          <i class="fa fa-star"></i>  
                          tristique senectus malesuada
                          <i class="fa fa-star"></i>  
                        </span>
                        <div class="pull-left div-settings dnone" style="position:absolute;left:15px;top:22px;z-index:999">
                            <img title="Edit" id="btedit-layanan" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                        </div>
                     </h1>
                </div> 
                <!-- End Title Heading --> 

                           <!-- Items Services -->
                <ul id="services-carousel" class="layananList">
                    <!-- Item Service -->

                    <?php foreach($eventsList as $event) : ?>
                    <li class="item-service-carousel">
                        <div class="row color">
                            <div class="col-md-3">
                                <i class="fa <?php echo $event->events_img; ?>"></i>
                            </div>
                            <div class="col-md-9">
                               <h4><?php echo $event->events_name; ?></h4>
                            </div>                           
                        </div>
                        <div class="row">
                            <p><?php echo $event->events_desc; ?></p>
                        </div>
                        <div class="row color-small"><a href="#">Read More..</a></div>
                    </li>  
                    <!-- Item Service -->

                    <?php endforeach ?>
                </ul>
                <!-- End Items Services -->
        </section>
        <!-- End Carousel Services-->

        <!-- Box Services--> 
        <section class="padding-bottom padding-top">
            <div class="container">
               <div class="row">  
                    <!-- More services --> 
                    <div class="col-md-12">
                        
                        <!-- Title Heading --> 
                        <div class="titles-heading">
                            <div class="line"></div>
                             <h1>Box Services
                                <span>
                                  <i class="fa fa-star"></i>  
                                  tristique senectus malesuada
                                  <i class="fa fa-star"></i>  
                                </span>
                                <div class="pull-left div-settings dnone" style="position:absolute;top:22px;z-index:999">
                                    <img title="Edit" id="btedit-service" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                                </div>
                             </h1>
                        </div> 
                        <!-- End Title Heading --> 

                         <!-- Box -->
                        <ul class="boxNew" id="serviceList">

                            <!-- Item More-service --> 
                            <?php foreach($servicesList as $service) : ?>
                                <li class="row">
                                    <!-- Image Services --> 
                                    <div class="col-md-3">
                                        <div class="image-more-service">
                                            <i class="fa <?php echo $service->services_img; ?>"></i>
                                        </div>
                                    </div>
                                    <!-- End Image Services --> 
                                    <div class="col-md-7">
                                        <div class="info">
                                            <h4><?php echo $service->services_name; ?></h4>
                                            <p><?php echo $service->services_desc; ?></p>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="link">
                                        <a href="#" title="Read More"><i class="fa fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach ?>
                            <!-- End Item More-service --> 

                        </ul>
                        <!-- End Box -->
                    </div>
                    <!-- End More services --> 

               
               </div>                
            </div>
        </section>
        <!-- End Box Services -->       

        <?php include "include/footer.php" ?>

    </div>
    <!-- End layout-->

    <!-- Scripts -->
    <?php include "include/include_js.php" ?>
        
    </body>
</html>