<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title><?php echo $config->title; ?></title>
        <!-- <title>Roker  - Software, Technology , Corporate, Creative, Multi-Purpose, Responsive And Retina Html5/css3 Template</title>  -->
        <meta name="keywords" content="conculta" />
        <meta name="description" content="Roker  - Corporate, Creative, Multi Purpose, Responsive And Retina Template">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <?php include "include/include_css.php" ?>  

        <!-- Head Libs -->
        <script src="<?php echo base_url() ?>js2/modernizr.js"></script>

    </head>
    <body> 

    <?php include "include/text_modal.php"; ?>
    <?php include "include/cropping_modal.php"; ?>

    <!--Preloader-->
    <div class="preloader">
        <div class="status">&nbsp;</div>
    </div>
    <!--End Preloader-->
    
    <!-- layout-->
    <div id="layout" class="layout-wide">

        <?php include "include/header.php" ?>

        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="index.html">Home</a> / Portofolio Detail
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1><?php echo $image->imageTitle; ?>
                            <span class="subtitle-section">
                                Original Works
                                <span class="left"></span>
                                <span class="right"></span>
                            </span>
                            <span class="line-title"></span>
                        </h1>
                    </div>
                    <!-- End Title--> 

                </div>
                <!-- End Title -Search --> 
              
            </div>
        </section>   
        <!-- End Title Section --> 

        <!-- Services Carousel -->
        <section class="paddings">
            <div class="container">
                <div class="row position-relative">

                    <i class="fa fa-cogs icon-section top right"></i> 

                    <div class="col-md-7">
                        <div class="hover">
                            <img style="width: 100%;" src="<?php echo base_url(); ?>images/works/<?php echo $image->imagePath; ?>" alt=""/>                               
                            <a href="<?php echo base_url(); ?>images/works/<?php echo $image->imagePath; ?>" class="fancybox" title="Zoom Image"><div class="overlay"></div></a>
                        </div>
                    </div>

                    <div class="col-md-5">
                         

                        <h3 class="title-subtitle">
                            <span><?php echo $image->imageTitle; ?></span>
                        </h3>
                         <p><?php echo $image->imageDesc; ?></p>
                        
                    </div>                  
                </div>             
            </div>
             <!-- End Container--> 
        </section>
        <!-- End Services Carousel-->


        <!-- Slides Team -->
        <!-- <section class="section-gray paddings borders">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3>The Challenge</h3>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                    </div>
                    <div class="col-md-6">
                        <h3>The Solution</h3>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- End Services Slide-->

        <!-- Clients -->
        <section class="paddings shadow-top clients">        

                <!-- Title Heading --> 
                <div class="titles-heading">
                    <div class="line"></div>
                     <h1>Recent Works
                        <span>
                          <i class="fa fa-star"></i>  
                          tristique senectus malesuada
                          <i class="fa fa-star"></i>  
                        </span>
                     </h1>
                </div> 
                <!-- End Title Heading --> 

                <!-- Items Works -->
                <ul id="works" class="works padding-top-mini">
                    <?php foreach($recentimageList as $recent) : ?>
                    <!-- Item Work -->
                    <li class="item-work">
                       <div class="hover">
                            <img src="<?php echo base_url();?>images/works/1.jpg" alt="Image"/>                               
                             <a href="<?php echo base_url();?>images/works/1.jpg" class="fancybox" title="Image"><div class="overlay"></div></a>
                        </div>                                   
                        <div class="info-work">
                            <h4><?php echo $recent->imageTitle; ?></h4>
                            <p><?php echo $recent->imageDesc; ?></p>
                            <div class="icons-work">
                                <i class="fa fa-tablet" data-toggle="tooltip" title data-original-title="Responsive Desing"></i>
                                <i class="fa fa-desktop" data-toggle="tooltip" title data-original-title="Retina Display"></i> 
                            </div>
                        </div>  
                    </li>  
                    <!-- Item Work -->
                    <?php endforeach ?>

                </ul>
                <!-- End Items Works -->
             
        </section>
        <!-- End Clients -->

        <?php include "include/footer.php" ?>

    </div>
    <!-- End layout-->

    <!-- Scripts -->
    <?php include "include/include_js.php" ?>
        
    </body>
</html>