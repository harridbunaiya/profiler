<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic -->
        <meta charset="utf-8">
        <title><?php echo $config->title; ?></title>
        <!-- <title>Roker  - Software, Technology , Corporate, Creative, Multi-Purpose, Responsive And Retina Html5/css3 Template</title>  -->
        <meta name="keywords" content="conculta" />
        <meta name="description" content="Roker  - Corporate, Creative, Multi Purpose, Responsive And Retina Template">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <?php include "include/include_css.php" ?>  

        <!-- Head Libs -->
        <script src="<?php echo base_url() ?>js2/modernizr.js"></script>

    </head>
    <body> 

    <!--Preloader-->
    <div class="preloader">
        <div class="status">&nbsp;</div>
    </div>
    <!--End Preloader-->
    
    <!-- layout-->
    <div id="layout" class="layout-wide">
        <?php include "include/header.php" ?>
        <?php include "include/cropping_modal.php" ?>


        <!-- Title Section -->           
        <section class="title-section">
            <div class="container">
                <!-- crumbs --> 
                <div class="row crumbs">
                   <div class="col-md-12">
                        <a href="index.html">Home</a> / <a href="#">Portofolio </a>
                   </div>
                </div>
                <!-- End crumbs --> 

                <!-- Title - Search--> 
                <div class="row title">
                    <!-- Title --> 
                    <div class="col-md-9">
                        <h1>Portofolio
                            <span class="subtitle-section">
                                Original Works
                                <span class="left"></span>
                                <span class="right"></span>
                            </span>
                            <span class="line-title"></span>
                        </h1>
                    </div>
                    <!-- End Title--> 

                </div>
                <!-- End Title -Search --> 
              
            </div>
        </section>   
        <!-- End Title Section --> 


        <!-- Works -->
        <section class="paddings">
            <div class="container">
                <div class="pull-left div-settings dnone" style="z-index:999">
                    <img title="Edit" id="btedit-portfolio" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                </div>
                <!-- Nav Filters -->
                <div class="portfolioFilter" id="categoryList">
                        <a href="#" data-filter="*" class="current">Show All</a>
                    <?php foreach($categoryList as $category) : ?>
                        <a href="#<?php echo $category->ID_category; ?>" data-filter=".<?php echo $category->ID_category; ?>"><?php echo $category->category_name; ?></a>
                    <?php endforeach ?>
                </div>
                <div class=" div-settings dnone" style="z-index: 999;display: block; position: absolute; right: 110px; top: 300px;">
                    <img title="Edit" id="btedit-category" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                </div>
                <!-- End Nav Filters -->

                <!-- Items Works filters-->
                <div class="row works portfolioContainer" id="portfolio-header">
                    
                    <!-- Item Work-->
                    <?php foreach($fullimageList as $image) : ?>
                    <div class="col-md-4 <?php echo $image->ID_category; ?>">
                        <div class="item-work">
                            <div class="hover">
                                <img src="<?php echo base_url(); ?>images/works/<?php echo $image->imagePath; ?>" alt=""/>                               
                                 <a href="<?php echo base_url(); ?>images/works/<?php echo $image->imagePath; ?>" class="fancybox" title="Zoom Image"><div class="overlay"></div></a>
                            </div>                                   
                            <div class="info-work">
                                <a href="<?php echo base_url()."portofolio/detail/".$image->imageID; ?>">
                                    <h4><?php echo $image->imageTitle; ?></h4>
                                </a>
                                    <p><?php echo $image->imageDesc; ?></p>
                                    <div class="icons-work">
                                        <!-- <i class="fa fa-tablet" data-toggle="tooltip" title data-original-title="Responsive Desing"></i>
                                        <i class="fa fa-desktop" data-toggle="tooltip" title data-original-title="Retina Display"></i> -->
                                    </div>
                            </div>  
                        </div>
                    </div>
                    <?php endforeach ?>
                    <!-- End Item Work-->
                                
                </div>   
                <!-- End Items Works filters-->       
            </div>
            <!-- End Container-->
        </section>
        <!-- End Works-->

        <?php include "include/footer.php" ?>
        
    </div>
    <!-- End layout-->

    <!-- Scripts -->
    <?php include "include/include_js.php" ?>
        
    </body>
</html>