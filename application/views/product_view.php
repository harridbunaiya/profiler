<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $config->title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "include/include_css.php" ?>
</head>
<body class="page">

<!-- Wrapper content -->
<div id="wrapper-container" class="content-pusher">
    <div class="overlay-close-menu"></div>

    <!-- Header -->
    <?php include "include/header.php" ?>

    <!-- Hidden Value -->
    <input type="hidden" id="hid_login" name="hid_login" value="<?php echo $id_admin;?>" />
    <input type="hidden" id="hid_id_detail" name="hid_id_detail" value="<?php echo $product->ID_plan;?>" />

    <main>
        <section class="section-base section-color">
            <div class="container" id="div-product">
                <div class="pull-left div-settings dnone" style="position: absolute; left: 150px;">
                    <img title="Edit" id="btedit-plan" src="<?php echo base_url();?>image/Icon_tools.png" class="icon mini-icon-settings" />
                </div>
                <div class="title text-center">
                    <h1><?php echo $product->plan_name ?></h1>
                </div>
                <div>
                    <p><?php echo $product->plan_detail ?></p>
                </div>
                <hr class="space-md"/>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title">
                            <h2><?php echo $product->spec_title ?></h2>
                        </div>
                        <div>
                            <p><?php echo $product->spec_desc ?></p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="cnt-box cnt-box-top boxed">
                            <a href="#" class="img-box" style="max-height: 300px; width: auto;"><img src="<?php echo base_url(); ?>image/plan/<?php echo $product->plan_img;?>" alt=""/></a>
                        </div>
                    </div>
                </div>
                <hr class="space-md"/>
            </div>
        </section>

        <?php if (sizeof($planOpsiList) > 0) : ?>
        <section class="section-base section-color">
            <div class="container" style="padding-top: 0;">
                <div class="title">
                    <h2>Opsi <?php echo $product->plan_name;?></h2>           
                </div>
                <div class="row" id="opsiListing">
                    <?php foreach($planOpsiList as $opsi) : ?>
                    <div class="col-lg-6" style="padding-top: 15px;">
                        <div class="cnt-box cnt-box-top boxed">
                            <a href="#" class="img-box"><img src="<?php echo base_url(); ?>image/plan/<?php echo $opsi->imagePath;?>" alt="" style="max-height: 375px; width: 100%;"/></a>
                            <div class="caption text-center">
                                <h3><?php echo $opsi->detail_text; ?></h3>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php if (sizeof($planLebarList) > 0) : ?>
        <section class="section-base section-color">
            <div class="container" style="padding-top: 0;">
                <div class="title">
                    <h2>Bentangan / Lebar <?php echo $product->plan_name;?></h2>           
                </div>
                <div class="row" id="lebarListing">
                    <?php foreach($planLebarList as $lebar) : ?>
                    <div class="col-lg-3" style="padding-top: 15px;">
                        <div class="cnt-box cnt-box-top boxed">
                            <a href="#" class="img-box"><img src="<?php echo base_url(); ?>image/plan/<?php echo $lebar->imagePath;?>" alt="" /></a>
                            <div class="caption text-center">
                                <h3><?php echo $lebar->detail_text; ?></h3>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php if (sizeof($planPanjangList) > 0) : ?>
        <section class="section-base section-color">
            <div class="container" style="padding-top: 0;">
                <div class="title">
                    <h2>Panjang <?php echo $product->plan_name;?></h2>           
                </div>
                <div class="row" id="panjangListing">
                    <?php foreach($planPanjangList as $panjang) : ?>
                    <div class="col-lg-4" style="padding-top: 15px;">
                        <div class="cnt-box cnt-box-top boxed">
                            <a href="#" class="img-box"><img src="<?php echo base_url(); ?>image/plan/<?php echo $panjang->imagePath;?>" alt="" /></a>
                            <div class="caption text-center">
                                <h3><?php echo $panjang->detail_text; ?></h3>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php if (sizeof($planAksesorisList) > 0) : ?>
        <section class="section-base section-color">
            <div class="container" style="padding-top: 0;">
                <div class="title">
                    <h2>Opsi Aksesoris <?php echo $product->plan_name;?></h2>           
                </div>
                <div class="row" id="aksesorisListing">
                    <?php foreach($planAksesorisList as $aksesoris) : ?>
                    <div class="col-lg-4" style="padding-top: 15px;">
                        <div class="cnt-box cnt-box-top boxed">
                            <a href="#" class="img-box"><img src="<?php echo base_url(); ?>image/plan/<?php echo $aksesoris->imagePath;?>" alt="" /></a>
                            <div class="caption text-center">
                                <h3><?php echo $aksesoris->detail_text; ?></h3>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <?php endif; ?>
    </main>
    
    <!-- Footer -->
    <?php include "include/footer.php" ?>

</div><!-- wrapper-container -->

<!-- Modal Dialog -->
<?php include "include/cropping_modal.php"; ?>

<!-- Fixed Button -->
<?php include "include/addition.php" ?>

<!-- Scripts -->
<?php include "include/include_js.php" ?>

</body>
</html>