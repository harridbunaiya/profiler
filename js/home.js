// Javascript Document
// Global Variable
var admin=parseInt($("#hid_login").val());
var blobURL;
var tempID, tempFile, tempImg, tempImageID;
var flagAdd = false, flagDetail = false;
var max_size = 3 * 1024;
function show_error(x)
{
	$(".errorContent").fadeIn("slow");
	$(".errorMsg").html(x);
	window.setTimeout(function(){ $(".errorContent").fadeOut("slow"); },3500);
}

function show_success(x)
{
	$(".errorContent").hide();
	$(".successContent").fadeIn("slow");
	$(".success").html(x);
	window.setTimeout(function(){ $(".successContent").fadeOut("slow"); },3500);
}

function clearAll()
{
    for ( instance in CKEDITOR.instances ){
        CKEDITOR.instances[instance].updateElement();
        CKEDITOR.instances[instance].setData('');
    }
}

function decodeHtml(html) {
	var txt = document.createElement('textarea');
    txt.innerHTML = html;
    return txt.value;
}

function escapeHtml(text) {
	var map = {
	  '&': '&amp;',
	  '<': '&lt;',
	  '>': '&gt;',
	  '"': '&quot;',
	  "'": '&#039;'
	};
  
	return text.replace(/[&<>"']/g, function(m) { return map[m]; });
  }

// Function for stripping tags
function is_array(obj) {
	if (obj.constructor.toString().indexOf('Array') == -1) {
		return false;
	}
	return true;
}

function strip_tags(input) {
	if (input) {
		var tags = /(<([^>]+)>)/ig;
		if (!is_array(input)) {
			input = input.replace(tags,'');
		}
		else {
			var i = input.length;
			var newInput = new Array();
			while(i--) {
				input[i] = input[i].replace(tags,'');
			}
		}
		return input;
	}
	return false;
}

function nl2br(varTest){
    return varTest.replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
};

function br2nl(varTest){
    return varTest.replace(/<br>/g, "\r");
};

function getShortMonth(idx) {
	var month = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	return month[idx];
}

function getMonth(x) {
	var dt = new Date(x);
	return getShortMonth(dt.getMonth()-1);
}

function getDate(x) {
	var showDate = new Date(x);
	return showDate.getDate();
}

function pad(n) {
    return (parseInt(n) < 10) ? ("0" + n) : n;
}

function formatDate(str, withHour) {
	withHour = typeof withHour == 'undefined' ? 0 : withHour;
	var date = new Date(str);
	var hour = (withHour == 1) ? (" " + pad(date.getHours()) + ":" + pad(date.getMinutes())) : "";
	return date.getDate() + " " + getShortMonth(date.getMonth()-1) + " " + date.getFullYear() + hour;
}

//Collapse All
function collapse_all()
{	
	//$(".welcome-planc").hide();
    $("#div-change-logo").css("left:","-280px");
	$("#div-change-logo").hide();
    $("#div-change-pass").css("left:","-280px");
	$("#div-change-pass").hide();
	$("#manage-home-box").css("left:","-280px");
	$("#manage-home-box").hide();
	$("#manage-opsi-box").css("left:","-280px");
	$("#manage-opsi-box").hide();
	$("#manage-home1-box").css("left:","-280px");
	$("#manage-home1-box").hide();
	$("#manage-how-box").css("left:","-280px");
	$("#manage-how-box").hide();
	$("#manage-testimoni-box").css("left:","-280px");
	$("#manage-testimoni-box").hide();
	$("#manage-events-box").css("left:","-280px");
	$("#manage-events-box").hide();
	$("#manage-events-detail-box").css("left:","-280px");
	$("#manage-events-detail-box").hide();
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function thousandFormat(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


// Other Function

var uploadFileWhyPict = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_why_pict", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  $("#imgwhypict").attr("src", base_url + "image/mockup/" + xhr.responseText);
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFileAboutPict = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_about_pict", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  $("#imgaboutpict").attr("src", base_url + "images/" + xhr.responseText);
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFileAboutHome = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_pic_home", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  $("#imgabouthome").attr("src", base_url + "images/" + xhr.responseText);
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFileTestimoniEdit = function(file,data_id)
{
	var id=data_id;
	
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_testimoni_image?id="+id, true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  retrieve_testimoni();
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}


var uploadFilePictureEdit = function(file,data_id)
{
	var id=data_id;
	
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_slider_image?id="+id, true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  retrieve_home();
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFileSliderEdit = function(file,data_id)
{
	var id=data_id;
	
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_slider_service_image?id="+id, true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  retrieve_slider();
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFilePlanEdit = function(file,data_id)
{
	var id=data_id;
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_plan_image?id="+id, true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  retrieve_plan();
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFilePictureEditClient = function(file,data_id)
{
	var id=data_id;
	
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_client_image?id="+id, true);
		
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  retrieve_client();
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFilePictureEditFact = function(file,data_id)
{
	var id=data_id;
	
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_fact_image?id="+id, true);
		
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  retrieve_fact();
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFileImageEdit = function(file,data_id)
{
	var id=data_id;
	
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_news_image?id="+id, true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  retrieve_news();
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFilePhotoEdit = function(file,data_id)
{
	var id=data_id;
	
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_team_photo?id="+id, true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  retrieve_team();
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadFilePortEdit = function(file,data_id)
{
	var id=data_id;
	tempID = id;
	tempFile = file;
	flagDetail = false;
	var URL = window.URL || window.webkitURL;

	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			document.getElementById("imgCrop").src = reader.result;
			blobURL = URL.createObjectURL(file);
          	$("#cropping").modal("show");
			//alert(e.target.result);
			//$('#right_pic').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
	}	
}

var uploadFileDetPortEdit = function(file,data_id)
{
	var id=data_id;
	tempID = id;
	tempFile = file;
	flagDetail = true;
	var URL = window.URL || window.webkitURL;

	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			document.getElementById("imgCrop").src = reader.result;
			blobURL = URL.createObjectURL(file);
          	$("#cropping").modal("show");
			//alert(e.target.result);
			//$('#right_pic').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
	}	
}

var uploadCV = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/update_cv", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  $("#btDownload").attr("href", base_url + "image/cv/cv.pdf");
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

// Ajax Retrieve

function retrieve_booking()
{
	$.ajax({
		url:base_url+"home/retrieve_booking",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img' >";
                html+="<div class='img-polaroid wid_240'>";
                html+="<div class='div-evt' style='min-height:68px'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_booking + "'>";
                html+="<i data-id='" + json[i].ID_booking + "' class='btsend-mail icon icon-envelope' title='Send Email'></i><br/>";
                html+="</span>";
                if (json[i].status != "R") {
                    html+="<span class='btreject-booking spclose back-white' data-value='R' data-id='" + json[i].ID_booking + "'>X</span>";
                }
                if (json[i].status != "A") {
                    html+="<span class='btaccept-booking spclose back-white' data-value='A' data-id='" + json[i].ID_booking + "'>&#10003;</span>";
                }
                html+="<span class='events_box' data-id='" + json[i].ID_booking + "' data-title='" + json[i].places + "'>";                               
                html+="<strong>" + json[i].places + "</strong>";
                html+="<br/>";
                html+="By <em>" + json[i].contact_name + "</em> - (" + json[i].contact_phone + ")";
                html+="<br/>";
                html+="<font style='font-size:9pt'>" + formatDate(json[i].book_date, 1) + "</font><br/>";
                html+="<font style='font-size:9pt'>" + json[i].notes + "</font><br/>";
                html+="<b>(<font class='small-font'>";
                if (json[i].status == "P") {
                	html+="Pending";
                } else if (json[i].status == "A") {
                	html+="Accepted";
                } else if (json[i].status == "R") {
                	html+="Rejected";
                }
                html+="</font>)</b></span></div></div></div>";
			}
			
			$("#booking-scroll").html(html);

	        // Button Event
			$(".btsend-mail").click(function(e){
				var id = $(this).attr("data-id");

				$("#booking-scroll").hide();
				$("#edit_booking").fadeIn("fast");
			});

			$(".btreject-booking, .btaccept-booking").click(function(e)
			{
				var id = $(this).attr("data-id");
				var status = $(this).attr("data-value");

				var conf = confirm("Are you sure about your decision ?");
				if (conf == true) {
					$.ajax({
						url :base_url + "home/update_book_status",
						dataType : "html",
						type : "POST",
						data : "id=" + id + "&status=" + status,
						success : function(result) {
							alert("Booking status has been saved successfully.");
							retrieve_booking();
						}, error : function(result) {
							alert("AJAX Error when change status.");
						}
					});
				}
			});
		}
	});
}

function retrieve_detail_portfolio(id)
{
	$.ajax({
		url:base_url+"home/retrieve_full_detail_image?imageid=" + id,
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img no-border'>";
                html+="<div class='img-thumbnail wid' style='margin-bottom:-8px'>";
                html+="<div style=\"background-image:url('" + base_url + "images/works/thumb/" + json[i].imagePath + "')\" class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].detailID + "'>";
                html+="<i data-visible='" + json[i].visible + "' data-title='" + json[i].imageTitle + "' data-image='" + json[i].imageID + "' data-desc='" + json[i].imageDesc + "'  data-id='" + json[i].detailID + "' class='btedit-detail-portfolio icon fa fa-pencil' title='Edit Portfolio'></i>";
                html+="<br/>";
                html+="<i data-id='" + json[i].detailID + "' class='btchangepic-detport icon fa fa-picture-o' title='Edit Image'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].detailID + "' data-value='" + json[i].imageIndex + "' class='btdetail-portfolio-up icon mini'/><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].detailID + "' data-value='" + json[i].imageIndex + "' class='btdetail-portfolio-down icon mini' />";
                html+="<input type='file' class='fnchdetport dnone' style='display:none !important;' data-id='" + json[i].detailID + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-detail-portfolio spclose back-white' data-value='" + json[i].imagePath + "' data-id='" + json[i].detailID + "'>X</span>";
                html+="</div></div>";
                html+="<span class='splabel' data-id='" + json[i].detailID + "' style='margin-top:-30px'>" + json[i].imageTitle + "</span>";
            	html+="</div>";
			}
			
			$("#detail-portfolio-scroll").html(html);
			if (tempImg == id) retrieve_detail_img(id);
     
	        // Button Event
	        $(".btdetail-portfolio-up").click(function(e) 
			{
		        var detailID=$(this).attr("data-id");
				var index=$(this).attr("data-value");

				$.ajax({
					url:base_url+"home/move_det_img_up?detailid="+detailID+"&imageid="+id+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_detail_portfolio(id);
					}
				});
		    });
			
			$(".btdetail-portfolio-down").click(function(e) 
			{
		        var detailID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_det_img_down?detailid="+detailID+"&imageid="+id+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_detail_portfolio(id);
					}
				});
		    });

			$(".btedit-detail-portfolio").click(function(e) {
				flagAdd = false;
				var id=$(this).attr("data-id");
				var title=$(this).attr("data-title");
				var desc = $(this).attr("data-desc");
				var visible = $(this).attr("data-visible");
				var image = $(this).attr("data-image");

				$("#btadd_detail_portfolio").hide();
				$("#detail-portfolio-scroll").hide();
				$("#edit_detail_portfolio").fadeIn("fast");
				
				//set data to component
				$("#hid_id_detail_portfolio").val(id);
				$("#txteddetimgtitle").val(title);
				$("#hid_id_image_port").val(image);
				$("#txteddetimgdesc").val(desc);
				$("#hid_id_image_port").val(image);
				$("#chkeddetImgVisible").prop("checked", (visible == "1"));
		    });

			$(".btdelete-detail-portfolio ").click(function(e) {
				var ID_detail = $(this).attr("data-id");
				var filename = $(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this detail portfolio ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_detail_portfolio?id="+ID_detail+"&filename=" + filename,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Detail portfolio removed successfully.");
								retrieve_detail_portfolio(id);
							}
						}
					});
				}
			});

			$(".btchangepic-detport").click(function(e)
			{
				flagAdd = false;
				var id=$(this).attr("data-id");
				var fnchpic=$(".fnchdetport[data-id='"+id+"']");
				fnchpic.click();
			});

			$(".fnchdetport").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFileDetPortEdit(file,id);
				}
			});
		}
	});
}

function retrieve_portfolio()
{
	$.ajax({
		url:base_url+"home/retrieve_portfolio",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			$("#portfolioContainer").html("");
			html2 += "<ul id='portfolio-grid' class='three-column hover-two portfolio-header'>";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img no-border'>";
                html+="<div class='img-thumbnail wid' style='margin-bottom:-8px'>";
                html+="<div style=\"background-image:url('" + base_url + "images/works/thumb/"+ json[i].imagePath + "')\" class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].imageID + "'>";
                html+="<i data-visible='" + json[i].visible + "' data-title='" + json[i].imageTitle + "' data-category='" + json[i].ID_category + "' data-desc='" + json[i].imageDesc + "'  data-id='" + json[i].imageID + "' class='btedit-portfolio icon fa fa-pencil' title='Edit Portfolio'></i>";
                html+="<br/>";
                html+="<i data-id='" + json[i].imageID + "' class='btchangepic-portfolio icon fa fa-picture-o' title='Edit Image'></i><br/>";
                html+="<i data-id='" + json[i].imageID + "' class='btndetail-portfolio icon fa fa-file-text' title='View Detail'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].imageID + "' data-value='" + json[i].imageIndex + "' class='btportfolio-up icon mini'/><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].imageID + "' data-value='" + json[i].imageIndex + "' class='btportfolio-down icon mini' />";
                html+="<input type='file' class='fnchport vnone' data-id='" + json[i].imageID + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-portfolio spclose back-white' data-value='" + json[i].imagePath + "' data-id='" + json[i].imageID + "'>X</span>";
                html+="</div></div>";
                html+="<span class='splabel' data-id='" + json[i].imageID + "' style='margin-top:-30px'>" + json[i].imageTitle + "</span>";
            	html+="</div>";

				if (json[i].visible == 1) {
            		// html2+="<div class='col-lg-4 col-md-6 item-folder item " + json[i].ID_category + "' data-count='" + json[i].jml_detail + "' data-id='" + json[i].imageID + "'>" + 
                    //         	"<div class='item-content'>" + 
                    //             	"<img style='height:214px' src='" + base_url + "images/works/thumb/" + json[i].imagePath + "' alt=''>" + 
		            //                     "<div class='item-overlay'>" + 
		            //                         "<h6>" + json[i].imageTitle + "</h6>" + 
		            //                         "<div class='icons'>" + 
		            //                             "<span class='icon link'>";
		            //                                 if (json[i].jml_detail <= 1) { 
		            //                                 	html2 += "<a href='" + base_url + "images/works/" + json[i].imagePath + "'>"; 
		            //                                 }
		            //                                 html2 += "<i class='fa fa-search'></i>";
		            //                                 if (json[i].jml_detail <= 1) {
		            //                                  	html2 += "</a>"; 
		            //                                  }
		            //                             html2 += "</span>" + 
		            //                         "</div>" + 
		            //                     "</div>" + 
		            //                 "</div>" + 
					//             "</div>";
					
					html2+="<div class='maso-item cat-"+ json[i].ID_category +">"+
                                "<a class='media-box media-box-full' href='" + base_url + "images/works/large/"+ json[i].imagePath + "'>"+
                                    "<img style='min-height: 241px' alt='' src='" + base_url + "images/works/thumb/" + json[i].imagePath + "'>"+
                                    "<div class='caption'>"+
                                        "<h2>" + json[i].imageDesc + "</h2>"+
                                        "<div class='extra-field'>" + json[i].imageTitle + "</div>"+
                                    "</div>"+
                                "</a>"+
                            "</div>";

				
				}
                
			}
			html2 += "</ul>";
			
			// $(".maso-box").hide();
			// $("#portfolio-filter").show();
			// $("#portfolio-header").show();
			
			$("#portfolio-scroll").html(html);

			$("#portfolio-header").html(html2);
			
			
			// Reinit Isotop
	        $('.maso-box').isotope('reloadItems');
	        window.setTimeout(function() {
	        	$("li.sel-item").click();
	    	}, 320);

			
	        // Button Event
	        $(".btndetail-portfolio").click(function(e){
	        	var id = $(this).attr("data-id");
	        	tempImageID = id;
				collapse_all();
				retrieve_detail_portfolio(id);
				$("#manage-detail-portfolio-box").show();
		        $("#manage-detail-portfolio-box").animate({left:'0'},140);
			});

			$(".item-folder").click(function(e)
			{
				var id = $(this).data("id");
				var count = parseInt($(this).attr("data-count"));

				if (count > 0) {
					retrieve_detail_img(id);
					$("#portfolio-header").hide();
					$(".portfolio-filter").hide();
					$("#portfolio-detail").show();
				}
			});

			$(".btportfolio-up").click(function(e) 
			{
		        var imageID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_img_up?imageid="+imageID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_portfolio();
					}
				});
		    });
			
			$(".btportfolio-down").click(function(e) 
			{
		        var imageID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_img_down?imageid="+imageID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_portfolio();
					}
				});
		    });

			$(".btedit-portfolio").click(function(e) {
				flagAdd = false;
				var id=$(this).attr("data-id");
				var title=$(this).attr("data-title");
				var category=$(this).attr("data-category");
				var desc = $(this).attr("data-desc");
				var visible = $(this).attr("data-visible");

				$("#btadd_portfolio").hide();
				$("#portfolio-scroll").hide();
				$("#edit_portfolio").fadeIn("fast");
				
				//set data to component
				$("#hid_id_portfolio").val(id);
				$("#txtedimgtitle").val(title);
				$("#cbedImgCategory").val(category);
				$("#txtedimgdesc").val(desc);
				$("#chkedImgVisible").prop("checked", (visible == "1"));
			});

			$(".btdelete-portfolio").click(function(e) {
				var ID_portfolio = $(this).attr("data-id");
				var filename = $(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this portfolio ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_portfolio?id="+ID_portfolio+"&filename=" + filename,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
								alert("Portfolio removed successfully.");
								retrieve_portfolio();
						}
					});
				}
			});

			$(".btchangepic-portfolio").click(function(e)
			{
				flagAdd = false;
				var id=$(this).attr("data-id");
				var fnchpic=$(".fnchport[data-id='"+id+"']");
				fnchpic.click();
			});

			$(".fnchport").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
					  return false;	
					} 
					var size=file.size/1024;

					uploadFilePortEdit(file,id);
				}
			});
		}
	});
}

function retrieve_client()
{
	$.ajax({
		url:base_url+"home/retrieve_client",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img no-border'>";
                html+="<div class='img-thumbnail wid' style='margin-bottom:-8px'>";
                html+="<div style='background-color:#b5b5b5;background-size: contain;background-image:url(\""+ base_url + "images/sponsors/logos/" + json[i].brand_path + "\")' class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_customer + "'>";
                html+="<i data-name='" + json[i].brand_name + "' data-id='" + json[i].ID_customer + "' class='btedit-client icon fa fa-pencil' title='Edit Item'></i>";
                html+="<br/>";
                html+="<i data-id='" + json[i].ID_customer+ "' class='btchangepic-client icon fa fa-image' title='Edit Picture'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_customer + "' data-value='" + json[i].seq_no + "' class='btclient-up icon mini'/><br/><img src='" + base_url +"assets/arrow-bottom.png' data-id='" + json[i].ID_customer + "' data-value='" + json[i].seq_no + "' class='btclient-down icon mini' />";
                html+="<input type='file' class='fnchpic_client vnone' data-id='" + json[i].ID_customer + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-client spclose back-white' data-value='" + json[i].brand_path + "' data-id='" + json[i].ID_customer + "'>X</span>";
                html+="</div></div>";
                html+="<div class='input-prepend input-append dnone' data-id='" + json[i].ID_customer + "'>";
                html+="<span class='add-on mini icon btclose-edit-client' data-id='" + json[i].ID_customer + "'><i class='icon-remove'></i></span>";
                html+="<input type='text' class='txttitle-client mini-input span2' placeholder='Title..' value='" + json[i].brand_name + "' data-id='" + json[i].ID_customer + "' />";
                html+="<span class='add-on mini icon btsave-edit-client' data-id='" + json[i].ID_customer + "'><i class='icon-ok'></i></span>";
                html+="</div>";
                html+="<span class='splabel' data-id='" + json[i].ID_customer + "' style='margin-top:-30px'>" + json[i].brand_name + "</span></div>";


				// html2+="<li><img src='" + base_url + "image/logos/" + json[i].brand_path + "' alt=''></li>";
				html2+="<div class='client-logo'> <img class='img-responsive' style='max-height: 125px' src='" + base_url + "images/sponsors/logos/" + json[i].brand_path + "' alt='" + json[i].brand_name + "'/> </div>";
			}
			
			$("#client-scroll").html(html);
			

			$(".partner-slider").slick('unslick');

			$("#clientList").html(html2);

			$(".partner-slider").slick({
				dots: false,
				infinite: true,
				centerMode: true,
				slidesToShow: 5,
				slidesToScroll: 1,
				centerPadding: '0',
				rows: 1,
				autoplay: true,
				autoplaySpeed: 2000,
				touchThreshold: 1000,
				draggable: true
			});

	        // Button Event
		    $(".btclient-up").click(function(e) 
			{
		        var clientID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_client_up?ID_customer="+clientID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_client();
					}
				});
		    });
			
			$(".btclient-down").click(function(e) 
			{
		        var clientID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_client_down?ID_customer="+clientID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_client();
					}
				});
		    });

		    $(".btchangepic-client").click(function(e)
			{
				var id=$(this).attr("data-id");
				var fnchpic=$(".fnchpic_client[data-id='"+id+"']");
				fnchpic.click();
			});

			$(".fnchpic_client").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum 700 MB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFilePictureEditClient(file,id);
				}
			});

			$(".btedit-client").click(function(e) {
				var id=$(this).attr("data-id");
				var item_name=$(this).attr("data-name");

				$("#btadd_client").hide();
				$("#client-scroll").hide();
				$("#edit_client").fadeIn("fast");
				
				//set data to component
				$("#hid_id_client").val(id);
				$("#txtedclient_name").val(item_name);
		    });

			$(".btdelete-client").click(function(e) {
				var ID_customer=$(this).attr("data-id");
				var filename = $(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this slider item ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_client?id="+ID_customer+"&filename=" + filename,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							alert("Client item removed successfully.");
							retrieve_client();
						}
					});
				}
			});
			
		}
	});
}

function retrieve_team()
{
	$.ajax({
		url:base_url+"home/retrieve_team",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img no-border'>";
                html+="<div class='img-thumbnail wid' style='margin-bottom:-8px'>";
                html+="<div style=\"background-image:url('" + base_url + "image/team/" + json[i].photo_path + "')\" class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_team + "'>";
                html+="<i data-name='" + json[i].name + "' data-position='" + json[i].position_name + "' data-fb='" + json[i].facebook_link + "' data-twitter='" + json[i].twitter_link + "' data-linkedin='" + json[i].linkedin_link + "' data-id='" + json[i].ID_team + "' class='btedit-team icon fa fa-pencil' title='Edit Member'></i>";
                html+="<br/>";
                html+="<i data-id='" + json[i].ID_team + "' class='btchangepic-team icon fa fa-picture-o' title='Edit Photo'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_team + "' data-value='" + json[i].seq_no + "' class='btteam-up icon mini'/><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_team + "' data-value='" + json[i].seq_no + "' class='btteam-down icon mini' />";
                html+="<input type='file' class='fnchphoto dnone' data-id='" + json[i].ID_team + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-team spclose back-white' data-value='" + json[i].photo_path + "' data-id='" + json[i].ID_team + "'>X</span>";
                html+="</div>";
                html+="</div>";
                html+="<span class='splabel' data-id='" + json[i].ID_team + "' style='margin-top:-30px'>" + json[i].name + "</span>";
            	html+="</div>";


                html2+="<div class='item wow fadeInUp'>";
                html2+="<div class='team-item'>";
                html2+="<div class='team-item-img'>";
                html2+="<img src='" + base_url + "image/team/" + json[i].photo_path + "' alt='' />";
                html2+="<div class='team-item-detail'>";
                html2+="<div class='team-item-detail-inner light-color'>";
                html2+="<ul class='social'>";
                if (json[i].facebook_link != "") {
                	html2+="<li><a href='" + json[i].facebook_link + "' target='_blank'><i class='fa fa-facebook'></i></a></li>";
                }
				if (json[i].twitter_link != "") {
					html2+="<li><a href='" + json[i].twitter_link + "' target='_blank'><i class='fa fa-whatsapp'></i></a></li>";
				}
                if (json[i].linkedin_link != "") {
                	html2+="<li><a href='" + json[i].linkedin_link + "' target='_blank'><i class='fa fa-linkedin'></i></a></li>";
                }
                html2+="</ul></div></div></div>";
                html2+="<div class='team-item-info'>";
                html2+="<h5 class='alt-title'>" + json[i].name + "</h5>";
                html2+="<p class=''>" + json[i].position_name + "</p>";
                html2+="</div></div></div>";
			}
			
			$("#team-scroll").html(html);
			$(".team-carousel").html(html2);

			// Reinit Carousel
	        $(".team-carousel").data('owlCarousel').reinit({
			    autoPlay: false,
	            stopOnHover: true,
	            items: 3,
	            itemsDesktop: [1170, 3],
	            itemsDesktopSmall: [1000, 2],
	            itemsTabletSmall: [768, 1],
	            itemsMobile: [480, 1],
	            pagination: false,  // Hide pagination buttons
	            navigation: false,  // Hide next and prev buttons
	            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
			});
			

	        // Button Event
			$(".btteam-up").click(function(e) 
			{
		        var teamID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_team_up?ID_team="+teamID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_team();
					}
				});
		    });
			
			$(".btteam-down").click(function(e) 
			{
		        var teamID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_team_down?ID_team="+teamID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_team();
					}
				});
		    });

			$(".btedit-team").click(function(e) {
				var id=$(this).attr("data-id");
				var name=$(this).attr("data-name");
				var position=$(this).attr("data-position");
				var fb_link = $(this).attr("data-fb");
				var twitter_link = $(this).attr("data-twitter");
				var linkedin_link = $(this).attr("data-linkedin");

				$("#btadd_team").hide();
				$("#team-scroll").hide();
				$("#edit_team").fadeIn("fast");
				
				//set data to component
				$("#hid_id_team").val(id);
				$("#txtedteam_name").val(name);
				$("#txtedposition").val(position);
				$("#txtedfb_link").val(fb_link);
				$("#txtedtwitter_link").val(twitter_link);
				$("#txtedlinkedin_link").val(linkedin_link);
		    });

			$(".btdelete-team").click(function(e) {
				var ID_team = $(this).attr("data-id");
				var filename = $(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this member ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_team?id="+ID_team+"&filename=" + filename,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Team member removed successfully.");
								retrieve_team();
							}
						}
					});
				}
			});

			$(".btchangepic-team").click(function(e)
			{
				var id=$(this).attr("data-id");
				var fnchpic=$(".fnchphoto[data-id='"+id+"']");
				fnchpic.click();
			});

			$(".fnchphoto").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFilePhotoEdit(file,id);
				}
			});
		}
	});
}

function retrieve_news()
{
	$.ajax({
		url:base_url+"home/retrieve_news",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img' >";
                html+="<div class='img-polaroid wid_240'>";
                html+="<div class='div-evt' style='min-height:68px;'>";                  
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_blog + "'>";
                html+="<i data-id='" + json[i].ID_blog + "' data-text='" + json[i].blog_text.replace("'", '"') + "' data-visible='" + json[i].visible + "' data-image='" + json[i].image + "' data-category='" + json[i].category + "' data-creator='" + json[i].creator + "' data-title='" + json[i].title + "' data-link='" + json[i].link + "' class='btedit-news icon fa fa-pencil' title='Edit News'></i><br/>";
                html+="<i data-id='" + json[i].ID_blog + "' class='btchangepic-news icon fa fa-picture-o' title='Edit Picture'></i><br/>";
                html+="<input type='file' class='fnchimg dnone' data-id='" + json[i].ID_blog + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-news spclose back-white' data-id='" + json[i].ID_blog +"' data-value='" + json[i].image + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_blog + "' data-title='" + json[i].title + "'>";                               
                html+="<strong>" + json[i].title + "</strong>";
                html+="<br/>";
                html+="By <em>" + json[i].creator + "</em>";
                html+="<br/>";
                html+="<b>(<font class='small-font'>" + (json[i].visible == "1" ? "Active" : "Not Active") + "</font>)</b><br/>";
                html+="<font style='font-size:9pt'>" + formatDate(json[i].created_date) + "</font>";
                html+="</span></div></div></div>";

				html2+="<li class='row'>";
                html2+="<div class='col-md-3'>";
				html2+="<div class='photo'>";
				html2+="<img src='" + base_url + "image/blog/" + json[i].image + "' alt=''>";
				html2+="</div></div>";
                html2+="<div class='col-md-7'>";
                html2+="<div class='info'>";
                html2+="<h5><span style='font-weight: 600'>" + json[i].title +"</span></h5>";
                // html2+="<p>" + json[i].blog_text.substring(0, 45) + "</p>";
                html2+="</div>";
                html2+="</div>"
                html2+="<div class='col-md-2'>";
                html2+="<div class='link'>";
                html2+="<a href='"+ base_url +"blog/detail/"+ json[i].ID_blog +"'><i class='fa fa-chevron-right'></i></a>";
				html2+="</div></div></li>";

			}
			
			$("#news-scroll").html(html);
			$("#blogList").html(html2);

			$('.boxNew').mCustomScrollbar("destroy")
			$(".boxNew").mCustomScrollbar({
				theme:"dark-2",
				scrollButtons:{ enable: true }
			});



			// Setting up in Owl Carousel
			// Reinit Carousel
	        // $(".blogs .owl-carousel").owlCarousel('destroy'); 
			// $(".blogs .owl-carousel").owlCarousel({
		    //     loop: true,
		    //     margin: 30,
		    //     autoplay: true,
		    //     smartSpeed: 500,
		    //     responsiveClass: true,
		    //     dots: false,
		    //     responsive: {
		    //         0: {
		    //             items: 1,
		    //         },
		    //         700: {
		    //             items: 2,
		    //         },
		    //         1000: {
		    //             items: 3,
		    //         },
		    //     },
		    // });

	        // Button Event
			$(".btedit-news").click(function(e) {
				var id=$(this).attr("data-id");
				var title=$(this).attr("data-title");
				var link=$(this).attr("data-link");
				var creator=$(this).attr("data-creator");
				var visible=$(this).attr("data-visible");

				$("#btadd_news").hide();
				$("#news-scroll").hide();
				$("#edit_news").fadeIn("fast");
				
				//set data to component
				$("#hid_id_news").val(id);
				$("#txtedtitle").val(title);
				$("#txtedlink").val(link);
				$("#txtedcreator").val(creator);
				$("#chkedblogVisible").prop("checked", (visible == "1"));
		    });

			$(".btdelete-news").click(function(e) {
				var ID_blog=$(this).attr("data-id");
				var filename = $(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this news ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_news?id="+ID_blog+"&filename=" + filename,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("News removed successfully.");
								retrieve_news();
							}
						}
					});
				}
			});

			$(".btchangepic-news").click(function(e)
			{
				var id=$(this).attr("data-id");
				var fnchpic=$(".fnchimg[data-id='"+id+"']");
				fnchpic.click();
			});

			$(".fnchimg").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFileImageEdit(file,id);
				}
			});
		}
	});
}

function retrieve_detail_img(id)
{
	tempImg = id;
	$.ajax({
		url:base_url+"home/retrieve_detail_image?imageid=" + id,
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";

			html += "<div class='col-lg-4 col-md-6 item-folder cursor' id='btnBackTo'>" + 
						"<div class='divBackBtn'>" + 
                            "<a class='porfolio-popup gallery-popup-link' id='btnBackDetail' title='Back to category'>" + 
                                "<div class='item-content'>" + 
                                    "<center class='backBtn'>Back</center>" +
                                "</div>" + 
                            "</a>" + 
                        "</div>" + 
                    "</div>";

			for (var i=0;i<json.length;i++)
			{
				html += "<div class='col-lg-4 col-md-6 item-folder item'>" + 
                            "<div class='item-content'>" + 
                                "<img style='height:228px' src='" + base_url + "images/works/thumb/" + json[i].imagePath + "' alt=''>" +
                                "<div class='item-overlay'>" + 
                                    "<h6>" + json[i].imageTitle + "</h6>" + 
                                    "<div class='icons'>" + 
                                        "<span class='icon link'>" + 
                                            "<a href='" + base_url + "images/works/" +  json[i].imagePath + "'>" + 
                                                "<i class='fa fa-search'></i>" + 
                                            "</a>" + 
                                        "</span>" + 
                                    "</div>" + 
                                "</div>" + 
                            "</div>" + 
                        "</div>";
			}
			html += "<div class='clear'></div>";

			$("#portfolio-detail").html(html);

			 /*========Magnific Popup Setup========*/
		     $('.portfolio .link').magnificPopup({
		        delegate: 'a',
		        type: 'image',
		        gallery: {
		            enabled: true
		        }
		    });

			// Back Button Clicked
			$("#btnBackTo").click(function(e)
			{
				$("#portfolio-detail").hide();
				$("#portfolio-header").show();
				$(".portfolio-filter").show();
			});
		}
	});
}

function retrieve_category()
{
	$.ajax({
		url:base_url+"home/retrieve_category",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			// html2+="<li><a href='javascript:;' class='filter active' id='all'>All</a></li>";
			html2+="<li class='active'><a data-filter='maso-item' href='#'>All</a></li>";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img'>";
				html+="<div class='img-polaroid wid_240'>";
                html+="<div class='div-evt' style='min-height:68px'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_category + "'>";
                html+="<i data-id='" + json[i].ID_category + "' data-visible='" + json[i].is_active + "' data-code='" + json[i].category_code + "' data-name='" + json[i].category_name + "' class='btedit-category icon fa fa-pencil' title='Edit Category'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_category + "' data-value='" + json[i].seq_no + "' class='btcategory-up icon mini'/><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_category + "' data-value='" + json[i].seq_no + "' class='btcategory-down icon mini' />";
                html+="</span>";
                html+="<span class='btdelete-category spclose back-white' data-id='" + json[i].ID_category + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_category + "' data-title='" + json[i].category_name + "'>";                               
                html+="<strong>" + json[i].category_name + "</strong>";
                html+="<br/>";
                html+="(<em>" + json[i].category_code + "</em>)<br/>";
                html+="<b>(<font class='small-font'>" + (json[i].is_active == "1" ? "Active" : "Not Active") + "</font>)</b>";
                html+="</span></div></div></div>";

                if (json[i].is_active == "1") {
                	// html2+="<li><a href='javascript:;' class='filter' data-filter='." + json[i].ID_category + "'>" + json[i].category_name + "</a></li>";
					// html2+="<li><a data-filter='." + json[i].ID_category + "' class='categories'>" + json[i].category_name + "</a></li>";
					
					html2+="<li><a href='#'' data-filter='cat-" + json[i].seq_no + "'>" + json[i].category_name + "</a></li>";
					
				} 

			}
			html2+="<li><a class='maso-order' data-sort='asc'></a></li>";
			
			$("#category-scroll").html(html);
			$(".categoryList").html(html2);
			// $("#portfolio-filter").click();
			// $("#portfolio-grid").html();

			if ($('#portfolio-grid').length > 0) {
				/* initialize shuffle plugin */
				var $grid = $('#portfolio-grid');
		
				$grid.shuffle({
					itemSelector: '.portfolio-item' // the selector for the items in the grid
				});
		
				/* reshuffle when user clicks a filter item */
				$('#portfolio-filter li').on('click', function (e) {
					e.preventDefault();
		
					// set active class
					$('#portfolio-filter li').removeClass('active');
					$(this).addClass('active');
		
					// get group name from clicked item
					var groupName = $(this).attr('data-group');
		
					// reshuffle grid
					$grid.shuffle('shuffle', groupName );
				});
			}
			
			// $("#all").click();
			
			// Button Event
		    $(".btcategory-up").click(function(e) 
			{
		        var categoryID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_category_up?categoryid="+categoryID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_category();
					}
				});
		    });
			
			$(".btcategory-down").click(function(e) 
			{
		        var categoryID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_category_down?categoryid="+categoryID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_category();
					}
				});
		    });

		    $(".btedit-category").click(function(e) {
				var id=$(this).attr("data-id");
				var code=$(this).attr("data-code");
				var name=$(this).attr("data-name");
				var visible = $(this).attr("data-visible");
				
				$("#btadd_category").hide();
				$("#category-scroll").hide();
				$("#edit_category").fadeIn("fast");
				
				//set data to component
				$("#hid_id_category").val(id);
				$("#txtedcategory_code").val(code);
				$("#txtedcategory_name").val(name);
				$("#chkedVisible").prop("checked",visible == "1");
		    });

		    
		    $(".btdelete-category").click(function(e) {
		    	var id_category=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this category?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/remove_category?id="+id_category,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							console.log(result);
							if (result=="")
							{
								alert("Category removed successfully.");
								retrieve_category();
							}
						}
					});
				}
		    }); 
		}
	});
}

function retrieve_blogcategory()
{
	$.ajax({
		url:base_url+"home/retrieve_blogcategory",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			// html2+="<li><a href='javascript:;' class='filter active' id='all'>All</a></li>";
			// html2+="<a href='#' data-filter='*' class='current'>Show All</a>";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img'>";
				html+="<div class='img-polaroid wid_240'>";
                html+="<div class='div-evt' style='min-height:68px'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_blogcategory + "'>";
                html+="<i data-id='" + json[i].ID_blogcategory + "' data-visible='" + json[i].is_active + "' data-code='" + json[i].blogcategory_code + "' data-name='" + json[i].blogcategory_name + "' class='btedit-blogcategory icon fa fa-pencil' title='Edit Blog Category'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_blogcategory + "' data-value='" + json[i].seq_no + "' class='btblogcategory-up icon mini'/><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_blogcategory + "' data-value='" + json[i].seq_no + "' class='btblogcategory-down icon mini' />";
                html+="</span>";
                html+="<span class='btdelete-blogcategory spclose back-white' data-id='" + json[i].ID_blogcategory + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_blogcategory + "' data-title='" + json[i].blogcategory_name + "'>";                               
                html+="<strong>" + json[i].blogcategory_name + "</strong>";
                html+="<br/>";
                html+="(<em>" + json[i].blogcategory_code + "</em>)<br/>";
                html+="<b>(<font class='small-font'>" + (json[i].is_active == "1" ? "Active" : "Not Active") + "</font>)</b>";
                html+="</span></div></div></div>";

                if (json[i].is_active == "1") {
                html2+="<li><i class='fa fa-check'></i><a href='#'>" + json[i].blogcategory_name + "</a></li>";
            	} 
			}
			
			$("#blogcategory-scroll").html(html);
			$("#blogcategoryList").html(html2);
			$("#all").click();
			
			// Button Event
			$(".btblogcategory-up").click(function(e) 
			{
				var blogcategoryID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_blogcategory_up?blogcategoryid="+blogcategoryID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_blogcategory();
					}
				});
			});
			
			$(".btblogcategory-down").click(function(e) 
			{
				var blogcategoryID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_blogcategory_down?blogcategoryid="+blogcategoryID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_blogcategory();
					}
				});
			});
		
			$(".btedit-blogcategory").click(function(e) {
				var id=$(this).attr("data-id");
				var code=$(this).attr("data-code");
				var name=$(this).attr("data-name");
				var visible = $(this).attr("data-visible");
				
				$("#btadd_blogcategory").hide();
				$("#blogcategory-scroll").hide();
				$("#edit_blogcategory").fadeIn("fast");
				
				//set data to component
				$("#hid_id_blogcategory").val(id);
				$("#txtedblogcategory_code").val(code);
				$("#txtedblogcategory_name").val(name);
				$("#chkedblogVisible").prop("checked",visible == "1");
			});
		
			
			$(".btdelete-blogcategory").click(function(e) {
				var id_blogcategory=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this category?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/remove_blogcategory?id="+id_blogcategory,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							console.log(result);
							if (result=="")
							{
								alert("Category removed successfully.");
								retrieve_blogcategory();
							}
						}
					});
				}
			}); 
		}
	});
}

function retrieve_tags()
{
	$.ajax({
		url:base_url+"home/retrieve_blogtags",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			// html2+="<li><a href='javascript:;' class='filter active' id='all'>All</a></li>";
			// html2+="<a href='#' data-filter='*' class='current'>Show All</a>";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img'>";
				html+="<div class='img-polaroid wid_240'>";
                html+="<div class='div-evt' style='min-height:68px'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_tags + "'>";
                html+="<i data-id='" + json[i].ID_tags + "' data-visible='" + json[i].is_active + "' data-code='" + json[i].tags_code + "' data-name='" + json[i].tags_name + "' class='btedit-tags icon fa fa-pencil' title='Edit Blog Tags'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_tags + "' data-value='" + json[i].seq_no + "' class='bttags-up icon mini'/><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_tags + "' data-value='" + json[i].seq_no + "' class='bttags-down icon mini' />";
                html+="</span>";
                html+="<span class='btdelete-tags spclose back-white' data-id='" + json[i].ID_tags + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_tags + "' data-title='" + json[i].tags_name + "'>";                               
                html+="<strong>" + json[i].tags_name + "</strong>";
                html+="<br/>";
                html+="(<em>" + json[i].tags_code + "</em>)<br/>";
                html+="<b>(<font class='small-font'>" + (json[i].is_active == "1" ? "Active" : "Not Active") + "</font>)</b>";
                html+="</span></div></div></div>";

                if (json[i].is_active == "1") {
                html2+="<a href='#' class='#' title='17 topic'><i class='fa fa-tag'></i> " + json[i].tags_name+ " </a>";
            	} 
			}
			
			$("#tags-scroll").html(html);
			$("#tagsList").html(html2);
			$("#all").click();
			
			// Button Event
			$(".bttags-up").click(function(e) 
			{
				var tagsID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_tags_up?tagsid="+tagsID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_tags();
					}
				});
			});
	
			$(".bttags-down").click(function(e) 
			{
				var tagsID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_tags_down?tags="+tagsID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_tags();
					}
				});
			});

			$(".btedit-tags").click(function(e) {
				var id=$(this).attr("data-id");
				var code=$(this).attr("data-code");
				var name=$(this).attr("data-name");
				var visible = $(this).attr("data-visible");
				
				$("#btadd_tags").hide();
				$("#tags-scroll").hide();
				$("#edit_tags").fadeIn("fast");
				
				//set data to component
				$("#hid_id_tags").val(id);
				$("#txtedtags_code").val(code);
				$("#txtedtags_name").val(name);
				$("#chkedtagsVisible").prop("checked",visible == "1");
			});

			$(".btdelete-tags").click(function(e) {
				var id_tags=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this category?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/remove_tags?id="+id_tags,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							console.log(result);
							if (result=="")
							{
								alert("Category removed successfully.");
								retrieve_tags();
							}
						}
					});
				}
			}); 
		}
	});
}

function retrieve_detail(id)
{
	$.ajax({
		url:base_url+"home/retrieve_detail_plan?planid=" + id,
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2="";
			var html3="";
			var html4="";
			var html5="";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img'>";
				html+="<div class='img-thumbnail wid' style='margin-bottom:-8px'>"; 
				html+="<div style='background-image:url(\"" + base_url + "image/plan/" + json[i].imagePath + "\");height:130px' class='div-img'>";
				// html+="<div class='img-polaroid wid_240'>";
				// html+="<div class='div-evt' style='min-height:50px'>";   
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_plan_detail +"'>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_plan_detail + "' data-value='" + json[i].seq_no + "' class='btdetail-plan-up icon mini'/><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_plan_detail + "' data-value='" + json[i].seq_no + "' class='btdetail-plan-down icon mini' />";
                html+="</span>";
                html+="<span class='btdelete-detail spclose back-white' data-id='" + json[i].ID_plan_detail + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_plan_detail + "' data-text='" + json[i].detail_text + "' >";
				html+="</span></div></div>";
				html+=json[i].detail_text;
				html+="</div>";

				html2+="<div class='col-lg-6'>";
                html2+="<div class='cnt-box cnt-box-top boxed'>";
                html2+="<a href='#' class='img-box'><img src='" + base_url + "image/plan/" + json[i].imagePath +"' alt='' style='max-height: 375px; width: 100%;'/></a>";
                html2+="<div class='caption text-center'>";
                html2+="<h3>" + json[i].detail_text + "</h3>";
				html2+="</div></div></div>";
				
				html3+="<div class='col-lg-4'>";
                html3+="<div class='cnt-box cnt-box-top boxed'>";
                html3+="<a href='#' class='img-box'><img src='" + base_url + "image/plan/" + json[i].imagePath +"' alt='' style='max-height: 375px; width: 100%;'/></a>";
                html3+="<div class='caption text-center'>";
                html3+="<h3>" + json[i].detail_text + "</h3>";
				html3+="</div></div></div>";
				
				html4+="<div class='col-lg-4'>";
                html4+="<div class='cnt-box cnt-box-top boxed'>";
                html4+="<a href='#' class='img-box'><img src='" + base_url + "image/plan/" + json[i].imagePath +"' alt='' style='max-height: 375px; width: 100%;'/></a>";
                html4+="<div class='caption text-center'>";
                html4+="<h3>" + json[i].detail_text + "</h3>";
				html4+="</div></div></div>";
				
				html5+="<div class='col-lg-4'>";
                html5+="<div class='cnt-box cnt-box-top boxed'>";
                html5+="<a href='#' class='img-box'><img src='" + base_url + "image/plan/" + json[i].imagePath +"' alt='' style='max-height: 375px; width: 100%;'/></a>";
                html5+="<div class='caption text-center'>";
                html5+="<h3>" + json[i].detail_text + "</h3>";
                html5+="</div></div></div>";
			}
			
			$("#detail-plan-scroll").html(html);
			$("#opsiListing").html(html2);
			$("#lebarListing").html(html3);
			$("#panjangListing").html(html4);
			$("#aksesorisListing").html(html5);
			
			// Events Button
			$(".btdetail-plan-up").click(function(e) 
			{
		        var detailID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_detail_up?detailid="+detailID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_detail(id);
						retrieve_plan();
					}
				});
		    });
			
			$(".btdetail-plan-down").click(function(e) 
			{
		        var detailID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_detail_down?detailid="+detailID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_detail(id);
						retrieve_plan();
					}
				});
		    });

			$(".btdelete-detail").click(function(e) {
				var ID_detail=$(this).attr("data-id");
				var conf=confirm("Are you sure want to delete this detail item ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_detail_plan?id="+ID_detail,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Detail item removed successfully.");
								retrieve_detail(id);
								retrieve_plan();
							}
						}
					});
				}
			});
		}
	});
}

function retrieve_plan()
{
	$.ajax({
		url:base_url+"home/retrieve_plan",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img no-border'>" + 
                    "<div class='img-thumbnail wid' style='margin-bottom:-8px'>" + 
                        "<div style='background-image:url(\"" + base_url + "image/plan/" + json[i].plan_img + "\");height:130px' class='div-img'>" + 
                            "<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_plan + "'>" +
                                "<i data-id='" + json[i].ID_plan + "' data-name='" + json[i].plan_name + "' class='btedit-plan icon fa fa-pencil' data-currency='" + json[i].currency + "' data-price='" + json[i].price + "' data-pereach='" + json[i].per_each + "' title='Edit Pricing Plan'></i><br/>" + 
                                "<i data-id='" + json[i].ID_plan + "' data-name='" + json[i].plan_name + "' class='btedit-detail icon fa fa-book' title='Edit Detail Plan'></i><br/>" + 
                                "<i data-id='" + json[i].ID_plan + "' class='btchangepic-plan icon fa fa-picture-o' title='Edit Picture'></i><br/>" + 
                                "<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_plan + "' data-value='" + json[i].seq_no + "' class='btplan-up icon mini'/><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_plan + "' data-value='" + json[i].seq_no + "' class='btplan-down icon mini' />" + 
                                "<input type='file' class='fnchplan dnone' data-id='" + json[i].ID_plan + "' accept='image/*' style='display:none;'/>" +
                            "</span>" + 
                            "<span class='btdelete-plan spclose back-white' data-value='" + json[i].plan_name + "' data-id='" + json[i].ID_plan + "'>X</span>" + 
                        "</div>" + 
                    "</div>" + 
                    "<div class='input-prepend input-append dnone' data-id='" + json[i].ID_plan + "'>" + 
                        "<span class='add-on mini icon btdelete-plan' data-id='" + json[i].ID_plan + "'><i class='icon-remove'></i></span>" + 
                        "<input type='text' class='txttitle-plan mini-input span2' placeholder='Title..' value='" + json[i].plan_name + "' data-id='" + json[i].ID_plan + "' />" + 
                        "<span class='add-on mini icon btsave-edit-plan' data-id='" + json[i].ID_plan + "'><i class='icon-ok'></i></span>" + 
                    "</div>" + 
                    "<span class='splabel' data-id='" + json[i].ID_plan + "' style='margin-top:-30px'>" + json[i].plan_name + "</span>" + 
                "</div>";
                
                html2+="<div class='pull-left div-settings dnone' style='position: absolute; left: 150px;'>"+
						"<img title='Edit' id='btedit-plan' src='" + base_url + "image/Icon_tools.png' class='icon mini-icon-settings' />"+
						"</div>"+
						"<div class='title text-center'>"+
						"<h1>"+ json[i].plan_name + "</h1>"+
						"</div>"+
						"<div>"+
						"<p>" + json[i].plan_detail + "</p>"+
						"</div>"+
						"<hr class='space-md'/>"+
						"<div class='row'>"+
						"<div class='col-lg-6'>"+
						"<div class='title'>"+
						"<h2>" + json[i].spec_title + "</h2>"+
						"</div>"+
						"<div>"+
						"<p>" + json[i].spec_desc + "</p>"+
						"</div>"+
						"</div>"+
						"<div class='col-lg-6'>"+
						"<div class='cnt-box cnt-box-top boxed'>"+
						"<a href='#' class='img-box' style='max-height: 300px; width: auto;'><img src='" + base_url + "image/plan/" + json[i].plan_img +"' alt=''/></a>"+
						"</div></div></div>";
			}
			$("#plan-scroll").html(html);
			$("#div-product").html(html2);
			
			// Events Button
			$(".btedit-detail").click(function(e) {
				var id = $(this).attr("data-id");
				var name = $(this).attr("data-name");
				
				//collapse_all();
				retrieve_detail(id);
				$("#hid_id_detail").val(id);
				$("#planc").html($(this).attr("data-name"));
				$("#manage-detail-plan-box").show();
		        $("#manage-detail-plan-box").animate({left:'0'},140);
		    });


			$(".btplan-up").click(function(e) 
			{
		        var planID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_plan_up?planid="+planID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_plan();
					}
				});
		    });
			
			$(".btplan-down").click(function(e) 
			{
		        var planID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_plan_down?planid="+planID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_plan();
					}
				});
		    });

			$(".btedit-plan").click(function(e) {
				var id=$(this).attr("data-id");
				var plan_name=$(this).attr("data-name");
				var subtitle=$(this).attr("data-subtitle");
				var spec=$(this).attr("data-spec");
				var desc=$(this).attr("data-desc")
				
				$("#btadd_plan").hide();
				$("#plan-scroll").hide();
				$("#edit_plan").fadeIn("fast");
				
				//set data to component
				$("#hid_id_plan").val(id);
				$("#txtedplan_name").val(plan_name);
				$("#txtedproduct_subtitle").val(subtitle);
				$("#txtedproduct_spec").val(spec);
				$("#txtedproduct_desc").val(desc);
		    });

			$(".btdelete-plan").click(function(e) {
				var ID_plan=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this pricing plan ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/remove_plan?id="+ID_plan,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Pricing plan removed successfully.");
								retrieve_plan();
							}
						}
					});
				}
			});
		}
	});
}

function retrieve_why()
{
	$.ajax({
		url:base_url+"home/retrieve_why",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "", html3 ="";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img' >";
                html+="<div class='img-polaroid wid_240'>";
                html+="<div class='div-evt'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_why + "'>";
                html+="<i data-id='" + json[i].ID_why + "' data-side='" + json[i].why_side + "' data-title='" + json[i].why_title + "' class='btedit-why icon fa fa-pencil' data-pict='" + json[i].why_pict + "' data-text='" + json[i].why_text + "' title='Edit Why'></i><br/>";
                html+="<br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_why + "' data-value='" + json[i].seq_no + "' class='btwhy-up icon mini'/><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_why + "' data-value='" + json[i].seq_no + "' class='btwhy-down icon mini' />";
                html+="</span>";
                html+="<span class='btdelete-why spclose back-white' data-id='" + json[i].ID_why + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_why + "' data-title='" + json[i].why_title + "'>";
                html+="<i style='font-size:30px' class='" + json[i].why_pict + "'></i>";
                html+="<br/>";
                html+="(<em>" + ((json[i].why_side == "L") ? "Left" : "Right") + "</em>)";
                html+="<br/>";
                html+="<strong>" + json[i].why_title + "</strong>";
                html+="<br/>";
                html+="<span class='small-font'>" + json[i].why_text + "</span>";
                html+="</span></div></div></div>";

                if (json[i].why_side == "L") {
	                html2+="<div class='content-box alt-right'>";
                    html2+="<div class='alt-icon-right'>";
                    html2+="<i class='" + json[i].why_pict + "'></i>";
                    html2+="</div>";
                    html2+="<h4 class='alt-title'>" + json[i].why_title + "</h4>";
                    html2+="<p>" + json[i].why_text + "</p>";
                    html2+="</div>";
            	} else {
            		html3+="<div class='content-box alt-left'>";
                    html3+="<div class='alt-icon-left'>";
                    html3+="<i class='" + json[i].why_pict + "'></i>";
                    html3+="</div>";
                    html3+="<h4 class='alt-title'>" + json[i].why_title + "</h4>";
                    html3+="<p>" + json[i].why_text + "</p>";
                    html3+="</div>";
            	}
			}
			
			$("#why-scroll").html(html);
			$("#left-bar").html(html2);
			$("#right-bar").html(html3);
			
			// Button Event
		    $(".btwhy-up").click(function(e) 
			{
		        var whyID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_why_up?whyid="+whyID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_why();
					}
				});
		    });
			
			$(".btwhy-down").click(function(e) 
			{
		        var whyID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_why_down?whyid="+whyID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_why();
					}
				});
		    });

		    $(".btedit-why").click(function(e) {
				var id=$(this).attr("data-id");
				var pict=$(this).attr("data-pict");
				var title=$(this).attr("data-title");
				var side=$(this).attr("data-side");
				var text=$(this).attr("data-text");
				
				$("#btadd_why").hide();
				$("#why-scroll").hide();
				$("#edit_why").fadeIn("fast");
				
				//set data to component
				$("#hid_id_why").val(id);
				$("#cbedwhyIcon").val(pict);
				$("#iedwhyIcon").attr("class",pict);
				$("#cbedSide").val(side);
				$("#txtedwhy_title").val(title);
				$("#txtedwhy_text").val(text);
		    });
		    
		    $(".btdelete-why").click(function(e) {
		    	var id_why=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this points?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/remove_why?id="+id_why,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							console.log(result);
							if (result=="")
							{
								alert("Point removed successfully.");
								retrieve_why();
							}
						}
					});
				}
		    });  	
		}
	});
}

function retrieve_slider()
{
	$.ajax({
		url:base_url+"home/retrieve_slider_service",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img no-border'>";
                html+="<div class='img-thumbnail wid' style='margin-bottom:-8px'>";
                html+="<div style='background-image:url(\""+ base_url + "images/services-carousel/" + json[i].imagePath + "\"); background-size: contain;' class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_slider + "'>";
                html+="<i class='btedit-home1 icon fa fa-pencil' data-html='" + htmlEntities(json[i].content) + "' data-name='" + json[i].slider_name + "' data-id='" + json[i].ID_slider + "' data-link='" + json[i].link+ "' title='Edit Item'></i>";
                html+="<br/>";
                html+="<i data-id='" + json[i].ID_slider+ "' class='btchangepic-home1 icon fa fa-picture-o' title='Edit Picture'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_slider + "' data-value='" + json[i].imageIndex + "' class='bthome1-up icon mini'/><br/><img src='" + base_url +"assets/arrow-bottom.png' data-id='" + json[i].ID_slider + "' data-value='" + json[i].imageIndex + "' class='bthome1-down icon mini' />";
                html+="<input type='file' class='fnchpic1 vnone' data-id='" + json[i].ID_slider + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-home1 spclose back-white' data-value='" + json[i].imagePath + "' data-id='" + json[i].ID_slider + "'>X</span>";
                html+="</div></div>";
                html+="<div class='input-prepend input-append dnone' data-id='" + json[i].ID_slider + "'>";
                html+="<span class='add-on mini icon btclose-edit-home1' data-id='" + json[i].ID_slider + "'><i class='icon-remove'></i></span>";
                html+="<input type='text' class='txttitle-home1 mini-input span2' placeholder='Title..' value='" + json[i].slider_name + "' data-id='" + json[i].ID_slider + "' />";
                html+="<span class='add-on mini icon btsave-edit-home1' data-id='" + json[i].ID_slider + "'><i class='icon-ok'></i></span>";
                html+="</div>";
                html+="<span class='splabel' data-id='" + json[i].ID_slider + "' style='margin-top:-30px'>" + json[i].slider_name + "</span></div>";

				html2+="<td style='border-top: none;'>";
				html2+="<a href='"+ json[i].link +"'>";
				html2+="<img src='" + base_url + "images/services-carousel/"+ json[i].imagePath +"' alt='"+ json[i].slider_name +"' />";
				html2+="<hr class='space-sm' />";
				html2+="<h2>"+ json[i].slider_name +"</h2>";
				html2+="<p>"+ json[i].content + "</p></a></td>";
					
			}
			// html2 += "</ul><div class='tp-bannertimer tp-bottom' style='visibility: hidden !important;'></div>";
			
			$("#home1-scroll").html(html);
			// $("#home1").css("background-image", "url('" + base_url + "images/services-carousel/" + json[0].imagePath + "')");
			// $("#content").html(json[0].content);
			
			// Reinit Carousel
			$(".sliderServices").html(html2);
			 
			// $("#slide-services").owlCarousel;
			// $('#slide-services').owlCarousel({
			// 	autoPlay: false,
			// 	items : 1,
			// 	navigation : true,
			// 	pagination : true
			// });
			   
	        // Button Event
		    $(".bthome1-up").click(function(e) 
			{
		        var sliderID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_slider_up?ID_slider="+sliderID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_slider();
					}
				});
		    });
			
			$(".bthome1-down").click(function(e) 
			{
		        var sliderID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_slider_down?ID_slider="+sliderID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_slider();
					}
				});
		    });

		    $(".btchangepic-home1").click(function(e)
			{
				var id=$(this).attr("data-id");
				var fnchpic1=$(".fnchpic1[data-id='"+id+"']");
				fnchpic1.click();
			});

			$(".fnchpic1").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum 700 MB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFileSliderEdit(file,id);
				}
			});

			$(".btedit-home1").click(function(e) {
				var id=$(this).attr("data-id");
				var item_name=$(this).attr("data-name");
				var content=$(this).attr("data-html");
				var link=$(this).attr("data-link");

				$("#btadd_home1").hide();
				$("#home1-scroll").hide();
				$("#edit_home1").fadeIn("fast");
				
				//set data to component
				$("#hid_id_home").val(id);
				$("#txtedsliderservice_name").val(item_name);
				$("#txtedhtmlservice_element").val(content);
				$("#txtedslider_url").val(link);
		    });

			$(".btdelete-home1").click(function(e) {
				var ID_slider=$(this).attr("data-id");
				var filename = $(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this slider item ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_slider_service?id="+ID_slider+"&filename=" + filename,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							// if (result=="")
							// {
								alert("Slider item removed successfully.");
								retrieve_slider();
							// }
						}
					});
				}
			});
			
		}
	});
}

function retrieve_opsi()
{
	$.ajax({
		url:base_url+"home/retrieve_opsi_service",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img no-border'>";
                html+="<div class='img-thumbnail wid' style='margin-bottom:-8px'>";
                html+="<div style='background-image:url(\""+ base_url + "images/roder/" + json[i].imagePath + "\"); background-size: contain;' class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].id + "'>";
                html+="<i class='btedit-opsi icon fa fa-pencil' data-name='" + json[i].title + "' data-id='" + json[i].id + "' title='Edit Opsi'></i>";
                html+="<br/>";
                html+="<i data-id='" + json[i].id+ "' class='btchangepic-opsi icon fa fa-picture-o' title='Edit Picture'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].id + "' data-value='" + json[i].imageIndex + "' class='btopsi-up icon mini'/><br/><img src='" + base_url +"assets/arrow-bottom.png' data-id='" + json[i].id + "' data-value='" + json[i].imageIndex + "' class='btopsi-down icon mini' />";
                html+="<input type='file' class='fnchopsi vnone' data-id='" + json[i].id + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-opsi spclose back-white' data-value='" + json[i].imagePath + "' data-id='" + json[i].id + "'>X</span>";
                html+="</div></div>";
                html+="<div class='input-prepend input-append dnone' data-id='" + json[i].id + "'>";
                html+="<span class='add-on mini icon btclose-edit-opsi' data-id='" + json[i].id + "'><i class='icon-remove'></i></span>";
                html+="<input type='text' class='txttitle-opsi mini-input span2' placeholder='Title..' value='" + json[i].title + "' data-id='" + json[i].id + "' />";
                html+="<span class='add-on mini icon btsave-edit-opsi' data-id='" + json[i].id + "'><i class='icon-ok'></i></span>";
                html+="</div>";
                html+="<span class='splabel' data-id='" + json[i].id + "' style='margin-top:-30px'>" + json[i].title + "</span></div>";

				html2+="<td style='border-top: none;'>";
				html2+="<img src='" + base_url + "images/roder/"+ json[i].imagePath +"' alt='"+ json[i].title +"' />";
				html2+="<hr class='space-sm' />";
				html2+="<h2>"+ json[i].title +"</h2>";
					
			}
			// html2 += "</ul><div class='tp-bannertimer tp-bottom' style='visibility: hidden !important;'></div>";
			
			$("#home1-scroll").html(html);
			// $("#home1").css("background-image", "url('" + base_url + "images/services-carousel/" + json[0].imagePath + "')");
			// $("#content").html(json[0].content);
			
			// Reinit Carousel
			$(".sliderServices").html(html2);
			 
			// $("#slide-services").owlCarousel;
			// $('#slide-services').owlCarousel({
			// 	autoPlay: false,
			// 	items : 1,
			// 	navigation : true,
			// 	pagination : true
			// });
			   
	        // Button Event
		    $(".bthome1-up").click(function(e) 
			{
		        var sliderID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_slider_up?ID_slider="+sliderID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_slider();
					}
				});
		    });
			
			$(".bthome1-down").click(function(e) 
			{
		        var sliderID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_slider_down?ID_slider="+sliderID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_slider();
					}
				});
		    });

		    $(".btchangepic-home1").click(function(e)
			{
				var id=$(this).attr("data-id");
				var fnchpic1=$(".fnchpic1[data-id='"+id+"']");
				fnchpic1.click();
			});

			$(".fnchpic1").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum 700 MB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFileSliderEdit(file,id);
				}
			});

			$(".btedit-home1").click(function(e) {
				var id=$(this).attr("data-id");
				var item_name=$(this).attr("data-name");
				var content=$(this).attr("data-html");
				var link=$(this).attr("data-link");

				$("#btadd_home1").hide();
				$("#home1-scroll").hide();
				$("#edit_home1").fadeIn("fast");
				
				//set data to component
				$("#hid_id_home").val(id);
				$("#txtedsliderservice_name").val(item_name);
				$("#txtedhtmlservice_element").val(content);
				$("#txtedslider_url").val(link);
		    });

			$(".btdelete-home1").click(function(e) {
				var ID_slider=$(this).attr("data-id");
				var filename = $(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this slider item ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_slider_service?id="+ID_slider+"&filename=" + filename,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							// if (result=="")
							// {
								alert("Slider item removed successfully.");
								retrieve_slider();
							// }
						}
					});
				}
			});
			
		}
	});
}

function retrieve_home()
{
	$.ajax({
		url:base_url+"home/retrieve_slider",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			// var html2 = "<div id='rev_slider_4_1' class='rev_slider fullwidthabanner' style='display:none;'' data-version='5.4.7.4'><ul>";
			var html2="";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img no-border'>";
                html+="<div class='img-thumbnail wid' style='margin-bottom:-8px'>";
                html+="<div style='background-image:url(\""+ base_url + "images/slide/slides/" + json[i].imagePath + "\")' class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].imageID + "'>";
                html+="<i class='btedit-home icon fa fa-pencil' data-html='" + htmlEntities(json[i].content) + "' data-name='" + json[i].imageTitle + "' data-id='" + json[i].imageID + "' title='Edit Item'></i>";
                html+="<br/>";
                html+="<i data-id='" + json[i].imageID+ "' class='btchangepic-home icon fa fa-picture-o' title='Edit Picture'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].imageID + "' data-value='" + json[i].imageIndex + "' class='bthome-up icon mini'/><br/><img src='" + base_url +"assets/arrow-bottom.png' data-id='" + json[i].imageID + "' data-value='" + json[i].imageIndex + "' class='bthome-down icon mini' />";
                html+="<input type='file' class='fnchpic dnone' data-id='" + json[i].imageID + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-home spclose back-white' data-value='" + json[i].imagePath + "' data-id='" + json[i].imageID + "'>X</span>";
                html+="</div></div>";
                html+="<div class='input-prepend input-append dnone' data-id='" + json[i].imageID + "'>";
                html+="<span class='add-on mini icon btclose-edit-home' data-id='" + json[i].imageID + "'><i class='icon-remove'></i></span>";
                html+="<input type='text' class='txttitle-home mini-input span2' placeholder='Title..' value='" + json[i].imageTitle + "' data-id='" + json[i].imageID + "' />";
                html+="<span class='add-on mini icon btsave-edit-home' data-id='" + json[i].imageID + "'><i class='icon-ok'></i></span>";
                html+="</div>";
                html+="<span class='splabel' data-id='" + json[i].imageID + "' style='margin-top:-30px'>" + json[i].imageTitle + "</span></div>";

						
				html2+="<li data-index='rs-" + i + "' data-transition='fade' data-slotamount='7' data-hideafterloop='0' " + 
                            "data-hideslideonmobile='off' data-easein='default' data-easeout='default' data-masterspeed='1500' " + 
                            "data-rotate='0' data-saveperformance='off'" + 
                            "data-title='Slide'>" + 
                            "<img src='" + base_url + "images/slide/slides/" + json[i].imagePath + "' alt='' title=slider tendaguna' " +
                                "data-bgposition='left center' data-kenburns='on' data-duration='10000' data-ease='Linear.easeNone' data-bgfit='130' data-bgfitend='100' data-bgpositionend='right center' " + 
                                // "class='rev-slidebg' data-no-retina>" + 
                            json[i].content + 
						"</li>";
			}
			
			$("#home-scroll").html(html);
			// $("#home").css("background-image", "url('" + base_url + "images/slide/slides/" + json[0].imagePath + "')");
			$("#content").html(json[0].content);
			
			$("#rev_slider_4_1_wrapper").html(html2);
			$("#rev_slider_4_1").show().revolution({
                sliderType: "standard",
                sliderLayout: "fullwidth",
				dottedOverlay: "none",
				startheight:500,
                delay: 9000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    mouseScrollReverse: "default",
                    onHoverStop: "off",
                    arrows: {
                        style: "zeus",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: false,
                        tmp: '<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 20,
                            v_offset: 0
                        }
                    }
                    ,
                    bullets: {
                        enable: true,
                        hide_onmobile: false,
                        style: "hermes",
                        hide_onleave: false,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 100,
                        space: 10,
                        tmp: ''
                    }
                },
                viewPort: {
                    enable: true,
                    outof: "wait",
                    visible_area: "80%",
                    presize: false
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1240, 1024, 778, 480],
                gridheight: [672, 600, 500, 400],
                lazyType: "none",
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                disableProgressBar: "on",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
			

	        // Button Event
		    $(".bthome-up").click(function(e) 
			{
		        var sliderID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_home_up?ID_slider="+sliderID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_home();
					}
				});
		    });
			
			$(".bthome-down").click(function(e) 
			{
		        var sliderID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_home_down?ID_slider="+sliderID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_home();
					}
				});
		    });

		    $(".btchangepic-home").click(function(e)
			{
				var id=$(this).attr("data-id");
				var fnchpic=$(".fnchpic[data-id='"+id+"']");
				fnchpic.click();
			});

			$(".fnchpic").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum 700 MB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFilePictureEdit(file,id);
				}
			});

			$(".btedit-home").click(function(e) {
				var id=$(this).attr("data-id");
				var item_name=$(this).attr("data-name");
				var content=$(this).attr("data-html");

				$("#btadd_home").hide();
				$("#home-scroll").hide();
				$("#edit_home").fadeIn("fast");
				
				//set data to component
				$("#hid_id_home").val(id);
				$("#txtedslider_name").val(item_name);
				$("#txtedhtml_element").val(content);
		    });

			$(".btdelete-home").click(function(e) {
				var ID_slider=$(this).attr("data-id");
				var filename = $(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this slider item ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_slider?id="+ID_slider+"&filename=" + filename,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Slider item removed successfully.");
								retrieve_home();
							}
						}
					});
				}
			});
			
		}
	});
}

function retrieve_testimoni()
{
	$.ajax({
		url:base_url+"home/retrieve_testimoni",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img' >";
                html+="<div class='img-polaroid wid_240'  >";
                html+="<div class='div-evt' style='line-height: 20px;'>";   
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_testimoni + "'>";
                html+="<i data-id='" + json[i].ID_testimoni + "' data-person=" + json[i].person_name + "' class='btedit-testimoni icon fa fa-pencil' data-corp='" + json[i].corporation_name + "' data-text='" + json[i].testimoni_text +"' title='Edit Testimoni'></i><br/>";
                html+="<i data-id='" + json[i].ID_testimoni + "' class='btchangepic-test icon fa fa-picture-o' title='Edit Picture'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_testimoni + "' data-value='" + json[i].seq_no + "' class='bttestimoni-up icon mini' /><br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_testimoni + "' data-value='" + json[i].seq_no + "' class='bttestimoni-down icon mini' />";
                html+="<input type='file' class='fnchtest vnone' data-id='" + json[i].ID_testimoni + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-testimoni spclose back-white' data-id='" + json[i].ID_testimoni + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_testimoni + "' data-person='" + json[i].person_name + "'>";
                html+="<strong>" + json[i].person_name + "</strong>";
                if (json[i].corporation_name != "") {
	                html+="<br/>";
	                html+="(<em>" + json[i].corporation_name + "</em>)";
            	}
                html+="<br/>";
                html+="<center><img style='max-height:65px' src='" + base_url + "images/testimonials/" + json[i].person_photo + "' alt='' /></center>";
                html+="<br/>";
                html+="<span class='small-font'>" + json[i].testimoni_text + "</span>";
                html+="</span></div></div></div>";


                //  html2+="<div class='testimonial-item'>" + 
		        //             "<div class='author-img'>" + 
		        //                 "<img style='width:120px;height:120px' src='" + base_url + "image/photo/" + json[i].person_photo + "' alt=''>" + 
		        //             "</div>" + 
		        //             "<h5>" + json[i].person_name + "</h5>" + 
		        //             "<span>" +  (json[i].corporation_name != "" ? json[i].corporation_name : "") + "</span>" + 
		        //             "<p>" + json[i].testimoni_text + "</p>" + 
				// 		"</div>";
						
				html2+="<li class='row'>";
				html2+="<div class='col-md-3'>";
                html2+="<div class='date'><span><i class='fa fa-calendar-o'></i>"+ getMonth(json[i].created_date) +"</span>"+ getDate(json[i].created_date) +"</div>";
                html2+="</div>";
				html2+="<div class='col-md-9'>";
				html2+="<div class='name'>";
				html2+="<h4>"+ json[i].person_name +" <span>" + json[i].corporation_name + "</span></h4>";                                        
				html2+="</div><p>" + json[i].testimoni_text + "</p>";
				html2+="</div></li>";

			}
			
			$("#testimoni-scroll").html(html);
			$("#testimoniList").html(html2);

			$('.boxNew').mCustomScrollbar("destroy")
			$(".boxNew").mCustomScrollbar({
				theme:"dark-2",
				scrollButtons:{ enable: true }
			});

		    $(".btchangepic-test").click(function(e)
			{
				var id=$(this).attr("data-id");
				var fnchpic=$(".fnchtest[data-id='"+id+"']");
				fnchpic.click();
			});

			$(".fnchtest").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFileTestimoniEdit(file,id);
				}
			});

			$(".bttestimoni-up").click(function(e) 
			{
		        var testimoniID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_testimoni_down?ID_testimoni="+testimoniID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_testimoni();
					}
				});
		    });
			
			$(".bttestimoni-down").click(function(e) 
			{
		        var testimoniID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_testimoni_up?ID_testimoni="+testimoniID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_testimoni();
					}
				});
		    });
    
			$(".btedit-testimoni").click(function(e) {
				var id=$(this).attr("data-id");
				var person_name=$(this).attr("data-person");
				var corp_name=$(this).attr("data-corp");
				var testimoni_text=$(this).attr("data-text");
				
				$("#btadd_testimoni").hide();
				$("#testimoni-scroll").hide();
				$("#edit_testimoni").fadeIn("fast");
				
				//set data to component
				$("#hid_id_testimoni").val(id);
				$("#txtedperson_name").val(person_name);
				$("#txtedcorp_name").val(corp_name);
				$("#txtedtestimoni_text").val(testimoni_text);
		    });

			$(".btdelete-testimoni").click(function(e) {
				var ID_testimoni=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this testimonial ?");
				if (conf==true)
				{

					console.log(result);
					$.ajax({
						url:base_url+"home/remove_testimoni?id="+ID_testimoni,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Testimonial removed successfully.");
								retrieve_testimoni();
							}
						}
					});
				}
			});
		}
	});
}

function retrieve_newsletter()
{
	$.ajax({
		url:base_url+"home/retrieve_newsletter",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{

				html+="<div class='child-scrollpanel' >";
                html+="    <div class='slide-img' >";
                html+="    <div class='img-polaroid wid_240'  >";
                html+="        <div class='div-evt' >";
                html+="            <span class='spleft back-white-left' style='text-align:center' data-id='"+json[i].ID_newsletter+"'>";
                html+="                <i data-id='"+json[i].ID_newsletter+"' data-email='"+json[i].contact_email+"' class='btedit-newsletter icon icon-pencil' data-visible='"+json[i].visible+"' title='Edit newsletter'>";
                html+="                </i><br/>";
                html+="                <img src='"+base_url+"assets/arrow-up.png' data-id='"+json[i].ID_newsletter+"' data-value='"+json[i].seq_no+"' class='btnewsletter-up icon mini'/>";
                html+="                <br/>";
                html+="                <img src='"+base_url+"assets/arrow-bottom.png' data-id='"+json[i].ID_newsletter+"' data-value='"+json[i].seq_no+"' class='btnewsletter-down icon mini' />";
                html+="            </span>";
                html+="            <span class='btdelete-newsletter spclose back-white' data-id='"+json[i].ID_newsletter+"'>X";
                html+="            </span>";
                html+="            <span class='events_box' data-id='"+json[i].ID_newsletter+"' data-email='"+json[i].contact_email+"'>";
                html+="                <strong>"+json[i].contact_email+"</strong><br/>";
                                        switch (json[i].visible)
                                        {
                                            case '0' : 
                html+="                            (<em style='color:red;'>Disable</em>)"; 
                                            break;
                                            case '1' : 
                html+="                        (<em style='color:green;'>Enable</em>)"; 
                                            break;
                                        }
                html+="                <br/>";
                html+="                <span class='small-font'>"+json[i].created_date+"</span>";
                html+="            </span>";
                html+="        </div>";
                html+="    </div>";
                html+="	</div>";        
				html+="</div>";

			}
			$("#newsletter-scroll").html(html);
			$(".btnewsletter-up").click(function(e)
			{
		        var newsletterID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_newsletter_up?ID_newsletter="+newsletterID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_newsletter();
					}
				});
		    });

			$(".btnewsletter-down").click(function(e)
			{
		        var newsletterID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_newsletter_down?ID_newsletter="+newsletterID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_newsletter();
					}
				});
		    });

			$(".btedit-newsletter").click(function(e) {
				var id=$(this).attr("data-id");
				var person_name=$(this).attr("data-person");
				var corp_name=$(this).attr("data-corp");
				var newsletter_text=$(this).attr("data-text");

				$("#btadd_newsletter").hide();
				$("#newsletter-scroll").hide();
				$("#edit_newsletter").fadeIn("fast");

				//set data to component
				$("#hid_id_newsletter").val(id);
				$("#txtedcontact_email").val(contact_email);
				$("#txtedcreated_date").val(created_date);
		    });

			$(".btdelete-newsletter").click(function(e) {
				var ID_newsletter=$(this).attr("data-id");

				var conf=confirm("Are you sure want to delete this newsletter ?");
				if (conf==true)
				{

					console.log(result);
					$.ajax({
						url:base_url+"home/remove_newsletter?id="+ID_newsletter,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("newsletteral removed successfully.");
								retrieve_newsletter();
							}
						}
					});
				}
			});
		}
	});
}


function retrieve_how()
{
	$.ajax({
		url:base_url+"home/retrieve_how",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img' >";
                html+="<div class='img-polaroid wid_240'  >";
                html+="<div class='div-evt' >";
                html+="<span class='spleft back-white-left' style='text-align:center; line-height: 1.42857143; padding:0;' data-id='" + json[i].ID_hows + "'>";
                html+="<i data-id='" + json[i].ID_hows + "' data-title='" + json[i].how_title + "' class='btedit-how icon fa fa-pencil' data-pict='" + json[i].how_pict + "' data-text='" + json[i].how_text + "' title='Edit How'></i>";
                // html+="<i data-id='" + json[i].ID_hows + "' class='btchangepic-how icon fa fa-picture-o' title='Edit Picture'></i>";
                html+="<br/><img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_hows + "' data-value='" + json[i].seq_no + "' class='bthow-up icon mini'/>";
                html+="<br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_hows + "' data-value='" + json[i].seq_no + "' class='bthow-down icon mini' />";
                html+="<input type='file' class='fnchhow vnone' data-id='" + json[i].ID_hows + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-how spclose back-white' data-id='" + json[i].ID_hows + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_hows + "' data-title='" + json[i].how_title + "'>";                               
				html+="<i class='fa "+ json[i].how_pict +"'></i>";
				html+="<br/><strong>" + json[i].how_title + "</strong><br/>";
                html+="<span class='small-font'>" + json[i].how_text + "</span></span>";
                html+="</div></div></div>";
						
				html2+="<div class='grid-item'>";
                html2+="<div class='cnt-box cnt-box-top-icon boxed text-center'>";
                html2+="<i class='fa " + json[i].how_pict + "' style='text-align: center;'></i>";
                html2+="<div class='caption'>";
                html2+="<h2>" + json[i].how_title + "</h2>";
                html2+="<p>" + json[i].how_text + "</p></div></div></div>";
			}
			
			$("#how-scroll").html(html);
			$("#howList").html(html2);

			// Reinitialize Owl Carousel
			// $(".list-box-slider").owlCarousel('destroy'); 
			// $('.list-box-slider').owlCarousel({
            //     loop:false,
            //     margin:10,
            //     nav: false,
            //     responsive:{
            //         0:{
            //             items:1
            //         },
            //         600:{
            //             items:2
            //         },
            //         1000:{
            //             items:4
            //         }
            //     }
            // });

			$(".bthow-up").click(function(e) 
			{
		        var howID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_how_up?howid="+howID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_how();
					}
				});
		    });
			
			$(".bthow-down").click(function(e) 
			{
		        var howID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_how_down?howid="+howID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_how();
					}
				});
		    });
    
			$(".btedit-how").click(function(e) {
				var id=$(this).attr("data-id");
				var pict=$(this).attr("data-pict");
				var title=$(this).attr("data-title");
				var text=$(this).attr("data-text");
				
				$("#btadd_how").hide();
				$("#how-scroll").hide();
				$("#edit_how").fadeIn("fast");
				
				//set data to component
				$("#hid_id_how").val(id);
				$("#cbedIcon").val(pict);
				$("#iedIcon").attr("class","fa " + pict);
				$("#txtedhow_title").val(title);
				$("#txtedhow_text").val(text);
		    });

			$(".btdelete-how").click(function(e) {
				var id_how=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this points?");
				if (conf==true)
				{

					console.log(result);
					$.ajax({
						url:base_url+"home/remove_how?id="+id_how,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Points removed successfully.");
								retrieve_how();
							}
						}
					});
				}
			});
		}
	});
}

function retrieve_tenda()
{
	$.ajax({
		url:base_url+"home/retrieve_tenda",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img' >";
                html+="<div class='img-polaroid wid_240'  >";
                html+="<div class='div-evt' >";
                html+="<span class='spleft back-white-left' style='text-align:center; line-height: 1.42857143; padding:0;' data-id='" + json[i].id + "'>";
                html+="<i data-id='" + json[i].id + "' data-title='" + json[i].title + "' class='btedit-tenda icon fa fa-pencil' title='Edit Service'></i>";
                // html+="<i data-id='" + json[i].ID_hows + "' class='btchangepic-how icon fa fa-picture-o' title='Edit Picture'></i>";
                html+="<br/><img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].id + "' data-value='" + json[i].seq_no + "' class='bttenda-up icon mini'/>";
                html+="<br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].id + "' data-value='" + json[i].seq_no + "' class='bttenda-down icon mini' />";
                // html+="<input type='file' class='fnchhow vnone' data-id='" + json[i].ID_hows + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-tenda spclose back-white' data-id='" + json[i].id + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].id + "' data-title='" + json[i].title + "'>";                               
				// html+="<i class='fa "+ json[i].how_pict +"'></i>";
				html+="<br/><strong>" + json[i].title + "</strong><br/>";
                html+="<span class='small-font'>" + json[i].subtitle + "</span></span>";
                html+="</div></div></div>";
				
				html2+="<li>";
                html2+="<a href='#' style='font-size: 20px;'>" + json[i].title + "</a>";
                html2+="<div class='content'>";
                html2+="<p>" + json[i].subtitle + "</p></div></li>";
			}
			
			$("#tenda-scroll").html(html);
			$("#tendaList").html(html2);

			$(".bttenda-up").click(function(e) 
			{
				var tendaID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_tenda_up?tendaid="+tendaID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_tenda();
					}
				});
			});
			
			$(".bttenda-down").click(function(e) 
			{
				var tendaID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_tenda_down?tendaid="+tendaID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_tenda();
					}
				});
			});

			$(".btedit-tenda").click(function(e) {
				var id=$(this).attr("data-id");
				var title=$(this).attr("data-title");
				var subtitle=$(this).attr("data-subtitle");
				
				// alert(pict);
				$("#btadd_tenda").hide();
				$("#tenda-scroll").hide();
				$("#edit_tenda").fadeIn("fast");
				
				//set data to component
				$("#hid_id_tenda").val(id);
				$("#txtedtenda_title").val(title);
				$("#txtedtenda_subtitle").val(subtitle);
			});

			
			$(".btdelete-tenda").click(function(e) {
				var id_tenda=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this points?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/remove_tenda?id="+id_tenda,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							console.log(result);
							if (result=="")
							{
								alert("Point removed successfully.");
								retrieve_tenda();
							}
						}
					});
				}
			});
		}
	});
}

function retrieve_layanan()
{
	$.ajax({
		url:base_url+"home/retrieve_event",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img' >";
                html+="<div class='img-polaroid wid_240'  >";
                html+="<div class='div-evt' >";
                html+="<span class='spleft back-white-left' style='text-align:center; line-height: 1.42857143; padding:0;' data-id='" + json[i].ID_events + "'>";
                html+="<i data-id='" + json[i].ID_events + "' data-title='" + json[i].events_name + "' class='btedit-layanan icon fa fa-pencil' data-pict='" + json[i].events_img + "' data-text='" + json[i].events_desc + "' title='Edit Layanan'></i><br/>";
                // html+="<i data-id='" + json[i].ID_hows + "' class='btchangepic-how icon fa fa-picture-o' title='Edit Picture'></i>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_events + "' data-value='" + json[i].events_index + "' class='btlayanan-up icon mini'/>";
                html+="<br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_events + "' data-value='" + json[i].events_index + "' class='btlayanan-down icon mini' />";
                html+="<input type='file' class='fnchlayanan vnone' data-id='" + json[i].ID_events + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-layanan spclose back-white' data-id='" + json[i].ID_events + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_events + "' data-title='" + json[i].events_name + "'>";                               
				html+="<i class='fa "+ json[i].events_img +"'></i>";
				html+="<br/><strong>" + json[i].events_name + "</strong><br/>";
				html+="<span class='small-font'>" + json[i].events_desc + "</span></span>";
				// <?php echo substr($events->events_desc, 0,40); ?>
				html+="</div></div></div>";
				
				html2+="<div class='col-md-12 feature-box text-left mb-50 col-sm-6 wow fadeTop' data-wow-delay='0.1s'>";
              	html2+="<div class='pull-left'><i class='fa " + json[i].service_img + " font-60px blue-icon'></i></div>";
              	html2+="<div class='pull-right'>";
                html2+="<h5 class='mt-0 upper-case'>" + json[i].services_name + "</h5>";
                html2+="<p>" + json[i].services_desc + " </p>";
              	html2+="</div></div>";
						

			}
			
			$("#layanan-scroll").html(html);
			$(".layananList").html(html2);

			// $(".layananList").owlCarousel; 
			// $(".layananList").owlCarousel({
			// 	autoPlay: 3200,      
			// 	items : 4,
			// 	navigation: true,
			// 	itemsDesktop : [1600,3],
			// 	itemsDesktopSmall : [1024,2],
			// 	itemsMobile : [800,1],
			// 	pagination: false
			   
			// });



			$(".btlayanan-up").click(function(e) 
			{
				var layananID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_layanan_down?ID_layanan="+layananID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_layanan();
					}
				});
			});
	
			$(".btlayanan-down").click(function(e) 
			{
				var layananID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_layanan_up?ID_layanan="+layananID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_layanan();
					}
				});
			});
    
			$(".btedit-layanan").click(function(e) {
				var id=$(this).attr("data-id");
				var pict=$(this).attr("data-pict");
				var title=$(this).attr("data-title");
				var text=$(this).attr("data-text");
				
				$("#btadd_layanan").hide();
				$("#layanan-scroll").hide();
				$("#edit_layanan").fadeIn("fast");
				
				//set data to component
				$("#hid_id_layanan").val(id);
				$("#cbedlayananIcon").val(pict);
				$("#iedLayananIcon").attr("class","fa " + pict);
				$("#txtedlayanan_name").val(title);
				$("#txtedlayanan_text").val(text);
			});

			$(".btdelete-layanan").click(function(e) {
				var id_layanan=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this points?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/remove_layanan?id="+id_layanan,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Points removed successfully.");
								retrieve_layanan();
							}
						}
					});
				}
			});
		}
	});
}

function retrieve_service()
{
	$.ajax({
		url:base_url+"home/retrieve_service",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img' >";
                html+="<div class='img-polaroid wid_240'  >";
                html+="<div class='div-evt' >";
                html+="<span class='spleft back-white-left' style='text-align:center; line-height: 1.42857143; padding:0;' data-id='" + json[i].ID_services + "'>";
                html+="<i data-id='" + json[i].ID_services + "' data-title='" + json[i].services_name + "' class='btedit-service icon fa fa-pencil' data-pict='" + json[i].services_img + "' data-text='" + json[i].services_desc + "' title='Edit Service'></i><br/>";
                // html+="<i data-id='" + json[i].ID_hows + "' class='btchangepic-how icon fa fa-picture-o' title='Edit Picture'></i>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_services + "' data-value='" + json[i].services_index + "' class='btservice-up icon mini'/>";
                html+="<br/><img src='" + base_url + "assets/arrow-bottom.png' data-id='" + json[i].ID_services + "' data-value='" + json[i].services_index + "' class='btservice-down icon mini' />";
                html+="<input type='file' class='fnchservice vnone' data-id='" + json[i].ID_services + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-service spclose back-white' data-id='" + json[i].ID_services + "'>X</span>";
                html+="<span class='events_box' data-id='" + json[i].ID_services + "' data-title='" + json[i].services_name + "'>";                               
				html+="<i class='fa "+ json[i].services_img +"'></i>";
				html+="<br/><strong>" + json[i].services_name + "</strong><br/>";
				html+="<span class='small-font'>" + json[i].services_desc + "</span></span>";
				// <?php echo substr($events->events_desc, 0,40); ?>
				html+="</div></div></div>";
				
				html2+="<div class='col-md-12 feature-box text-left mb-50 col-sm-6 wow fadeTop' data-wow-delay='0.1s'>";
              	html2+="<div class='pull-left'><i class='fa " + json[i].services_img + " font-60px blue-icon'></i></div>";
              	html2+="<div class='pull-right'>";
                html2+="<h5 class='mt-0 upper-case'>" + json[i].services_name + "</h5>";
                html2+="<p>" + json[i].services_desc + " </p>";
              	html2+="</div></div>";
						

			}
			
			$("#service-scroll").html(html);
			$("#serviceList").html(html2);

			// $('.boxNew').mCustomScrollbar("destroy")
			// $(".boxNew").mCustomScrollbar({
			// 	theme:"dark-2",
			// 	scrollButtons:{ enable: true }
			// });

			$(".btservice-up").click(function(e) 
			{
				var serviceID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_service_up?ID_service="+serviceID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_service();
					}
				});
			});
	
			$(".btservice-down").click(function(e) 
			{
				var serviceID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_service_down?ID_service="+serviceID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_service();
					}
				});
			});
    
			$(".btedit-service").click(function(e) {
				var id=$(this).attr("data-id");
				var pict=$(this).attr("data-pict");
				var title=$(this).attr("data-title");
				var text=$(this).attr("data-text");
				
				$("#btadd_service").hide();
				$("#service-scroll").hide();
				$("#edit_service").fadeIn("fast");
				
				//set data to component
				$("#hid_id_service").val(id);
				$("#cbedserviceIcon").val(pict);
				$("#iedserviceIcon").attr("class","fa " + pict);
				$("#txtedservice_name").val(title);
				$("#txtedservice_text").val(text);
			});

			$(".btdelete-service").click(function(e) {
				var id_service=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this points?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/remove_service?id="+id_service,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Points removed successfully.");
								retrieve_service();
							}
						}
					});
				}
			});
		}
	});
}

function retrieve_fact()
{
	$.ajax({
		url:base_url+"home/retrieve_fact",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			var html2 = "";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img no-border'>";
                html+="<div class='img-thumbnail wid' style='margin-bottom:-8px'>";
                html+="<div style='background-color:#ffffff;background-size: contain;background-image:url(\""+ base_url + "images/clients-downloads/" + json[i].bg_pict + "\")' class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='" + json[i].ID_fact + "'>";
                html+="<i data-name='" + json[i].fact_name + "' data-id='" + json[i].ID_fact + "' class='btedit-fact icon fa fa-pencil' title='Edit Item'></i>";
                html+="<br/>";
                html+="<i data-id='" + json[i].ID_fact+ "' class='btchangepic-fact icon fa fa-image' title='Edit Picture'></i><br/>";
                html+="<img src='" + base_url + "assets/arrow-up.png' data-id='" + json[i].ID_fact + "' data-value='" + json[i].seq_no + "' class='btfact-up icon mini'/><br/><img src='" + base_url +"assets/arrow-bottom.png' data-id='" + json[i].ID_fact + "' data-value='" + json[i].seq_no + "' class='btfact-down icon mini' />";
				html+="<input type='file' class='fnchpic_fact vnone' data-id='" + json[i].ID_fact + "' accept='image/*' />";
                html+="</span>";
                html+="<span class='btdelete-fact spclose back-white' data-value='" + json[i].bg_pict + "' data-id='" + json[i].ID_fact + "'>X</span>";
                html+="</div></div>";
                html+="<div class='input-prepend input-append dnone' data-id='" + json[i].ID_fact + "'>";
                html+="<span class='add-on mini icon btclose-edit-fact' data-id='" + json[i].ID_fact + "'><i class='icon-remove'></i></span>";
                html+="<input type='text' class='txttitle-fact mini-input span2' placeholder='Title..' value='" + json[i].fact_name + "' data-id='" + json[i].ID_fact + "' />";
                html+="<span class='add-on mini icon btsave-edit-fact' data-id='" + json[i].ID_fact + "'><i class='icon-ok'></i></span>";
                html+="</div>";
                html+="<span class='splabel' data-id='" + json[i].ID_fact + "' style='margin-top:-30px'>" + json[i].fact_name + "</span></div>";

				
                html2+="<li><img src='" + base_url + "images/clients-downloads/" + json[i].bg_pict + "' alt=''></li>";
			}
			
			$("#fact-scroll").html(html);
			$("#fact-retrieve").html(html2);
			
			
	        // Button Event
		    $(".btfact-up").click(function(e) 
			{
		        var factID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_fact_up?ID_fact="+factID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_fact();
					}
				});
		    });
			
			$(".btfact-down").click(function(e) 
			{
		        var factID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_fact_down?ID_fact="+factID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_fact();
					}
				});
		    });

		    $(".btchangepic-fact").click(function(e)
			{
				var id=$(this).attr("data-id");
				var fnchpic=$(".fnchpic_fact[data-id='"+id+"']");
				fnchpic.click();
			});

			$(".fnchpic_fact").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum 700 MB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFilePictureEditFact(file,id);
				}
			});

			$(".btedit-fact").click(function(e) {
				var id=$(this).attr("data-id");
				var item_name=$(this).attr("data-name");

				$("#btadd_fact").hide();
				$("#fact-scroll").hide();
				$("#edit_fact").fadeIn("fast");
				
				//set data to component
				$("#hid_id_fact").val(id);
				$("#txtedfact_name").val(item_name);
		    });

			$(".btdelete-fact").click(function(e) {
				var ID_fact=$(this).attr("data-id");
				var filename = $(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this slider item ?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/delete_fact?id="+ID_fact+"&filename=" + filename,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Fact item removed successfully.");
								retrieve_fact();
							}
						}
					});
				}
			});
			
		}
	});
}

/*========Window Load Function========*/
$(window).on("load", function() {

    /*========Preloader Setup========*/
    setTimeout(function(){
        $('.preloader').addClass('loaded');
    }, 1000);

    /*========Portfolio Isotope Setup========*/
    if ($(".portfolio-items").length) {
        var $elements = $(".portfolio-items");
        $elements.isotope();
        $(".portfolio-filter ul li").on("click", function() {
            $(".portfolio-filter ul li").removeClass("sel-item");
            $(this).addClass("sel-item");
            var selector = $(this).attr("data-filter");
            $(".portfolio-items").isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: "linear",
                    queue: false,
                },
            });
        });
	}
	


});


// Event Handler
$(document).ready(function(e)
{

	$("body").on("click", "#btDownloadCatalog", function(e) {
		var href = base_url + "images/upload/catalog.pdf"
		window.open(href, '_blank');
	});

	$("body").on("click", "#btDownloadTerms", function(e) {
		var href = base_url + "images/upload/terms.pdf"
		window.open(href, '_blank');
	});

	$('#blogPage').DataTable( {
		searching: false,
		ordering: false,
		sorting: false
	} );

	// Picture Folder Click
	$(".item-folder").click(function(e)
	{
		var id = $(this).data("id");
		var count = parseInt($(this).attr("data-count"));
		if (count > 0) {
			retrieve_detail_img(id);
			$("#portfolio-header").fadeOut("fast");
			$(".portfolio-filter").hide();
			$("#portfolio-detail").fadeIn("slow");
		}
	});

	// Link Facility & Activity
	$("body").on("click", ".link", function(e) {
		var href = $(this).attr("href");
		$(".divHeads").hide();
		$(".divDets").hide();
		$(href).fadeIn("slow");
		$("." + href.replace("#","")).fadeIn("slow");
		$("#div-back").fadeIn("slow");
	});

	$("#btn-back").click(function(e) {
		$(".divHeads").fadeIn("slow");
		$(".divDets").hide();
		$("#div-back").fadeOut("fast");
	});

	try { 
		CKEDITOR.config.toolbar_MA = [
			{ name: 'document', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
			{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
			{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
			{ name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
		        'HiddenField' ] },
			'/',
			{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
			'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
			{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
			{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
			'/',
			{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
			{ name: 'colors', items : [ 'TextColor','BGColor' ] },
			{ name: 'tools', items : [ 'ShowBlocks','-','About' ] }
		];
		 
		var editor = CKEDITOR.replace("txtblogText", {
			toolbar: 'MA',
			filebrowserUploadUrl: base_url + 'js/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
			filebrowserBrowseUrl: base_url + 'js/plugins/ckfinder/ckfinder.html?resourceType=Files'
		}); 

		CKFinder.setupCKEditor(editor);
	} catch(e) { }

	$("#contact-form").submit(function(e)
	{
		return false;
	});

    $("#btsend_email").click(function(e) 
	{

		alert("send mail");
     	var xmlhttp;
		var status;
		
		var s_name=$("#txts_name").val();
		var s_email=$("#txts_email").val();
		var s_subject=$("#txts_subject").val();
		var s_message=$("#txts_message").val();
		var s_telp=$("#txts_telp").val();
		
		var regex=new RegExp("^.+@.+\..+$");
		
		if (s_name=="")
			show_error("Please fill your name first.");
		else if (s_name.length<3)
			show_error("Name contains at least 3 characters.");
		else if (s_email=="")
			show_error("Please fill your email first.");
		else if (!regex.test(s_email))
			show_error("Please enter a valid email.");
		else if (s_subject == "")
			show_error("Please fill subject first.");
		else if (s_telp.length=="")
			show_error("Please fill your phone number.");
		else if (s_message.length=="")
			show_error("Please give your message.");
		else
		{
			if (window.XMLHttpRequest)
				xmlhttp=new XMLHttpRequest();
			else
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.status==200 && xmlhttp.readyState==4)
				{
					status=xmlhttp.responseText;
					show_success("Your message successfully sent.");
					
					$("#txts_name").val("");
					$("#txts_subject").val("");
					$("#txts_email").val("");
					$("#txts_message").val("");
					$("#txts_telp").val("");
				}
			}
			xmlhttp.open("POST",base_url+"home/send_email",true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("s_name="+s_name+"&s_subject="+s_subject+"&s_email="+s_email+"&s_message="+s_message+"&s_telp="+s_telp);  
		}
    });

	$("#btnSaveText").click(function(e)
	{
		$("#textModal").modal("hide");
	});

	$("#btsave_newslettervisit").click(function(e)
	{
		var regex = new RegExp("^.+@.+\..+$");
		var email = $("#txtnewsletter_email").val();
		if (email==""){
			$("#result-newsletter").html("<red>Please fill email.</red>");
		}else if (!regex.test(email)){
			$("#result-newsletter").html("<red>email is invaid.</red>");
		}else
		{

			$.ajax({
				url:base_url+"home/insert_newslettervisit",
				dataType:"html",
				type:"POST",
				data:"email="+email,
				success: function(result)
				{
					//Clear Insert
					$("#txtnewsletter_email").val("");
					$("#error-save-newsletter").html("");
					$("#result-newsletter").html("<green>Thank u for sending email</green>");

				}
			});
		}
    });

	// Manage File Why Pict
	$("#btedit-why-pict").click(function(e)
	{
		$("#fnwhy-pict").click();
	});

	$("#fnwhy-pict").change(function(e)
	{
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileWhyPict(file);
		}
	});
	
	// Manage File Why Pict
	$("#btedit-about-pict").click(function(e)
	{
		$("#fnabout-pict").click();
	});

	$("#fnabout-pict").change(function(e)
	{
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileAboutPict(file);
		}
	});

	// Manage File about home pic
	$("#btedit-about-home").click(function(e)
	{
		$("#fnabout-home").click();
	});

	$("#fnabout-home").change(function(e)
	{
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileAboutHome(file);
		}
	});

	// Manage Logo

	$("#btedit-logo").click(function(e) {
		collapse_all();
		$("#div-change-logo").show();
        $("#div-change-logo").animate({left:'0'},140);
    });

    $("#close-chlogo").click(function(e) {	
        $("#div-change-logo").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#div-change-logo").hide();},140);
    });

    $("#btuploadlogo").click(function(e)
	{
		var logolight = document.getElementById("fnlogolight").files[0];
		var logodark = document.getElementById("fnlogodark").files[0];
		var formData = new FormData();

		if ($("#fnlogolight").val() == "") {
			$("#error-chlogo").html("Please attach logo light first.");
		} else if ($("#fnlogodark").val() == "") {
			$("#error-chlogo").html("Please attach logo dark first.");
		} else { 
			formData.append('logodark', logodark);
			formData.append('logolight', logolight);
			// Use `jQuery.ajax` method
			$.ajax(base_url + 'home/update_logo', {
			    method: "POST",
			    data: formData,
			    processData: false,
			    contentType: false,
			    success: function (result) {
			      	console.log('Upload success');
			      	//Clean everything..
				  	$("#fnlogolight").val("");
				  	$("#fnlogodark").val("");
				  	$("#error-chlogo").html("");

				  	$("#close-chlogo").click();
				  	var dt = new Date();
				  	$(".logo-light").attr("src",base_url + "images/logo-light.png?" + dt.getTime());
				  	$(".logo-dark").attr("src",base_url + "images/logo-dark.png?" + dt.getTime());
			    },
			    error: function (result) {
			      console.log('Upload error');
			    }
			});
		}
	});

	// Manage Booking
	$("#btnBook").click(function(e)
	{
		var places = $("#txtPlaces").val();
		var date = $("#txtBookDate").val();
		var category = $("#cbCategory").val();
		if (places.trim() == "") {
			$("#error-book").html("Please fill places first.");
			$("#error-book").show();
		} else if(date.trim() == "") {
			$("#error-book").html("Please fill booking date first.");
			$("#error-book").show();
		} else {
			$("#dialog").modal("show");
		}
		window.setTimeout(function(){ $("#error-book").hide(); }, 3200);
	});

	$("#btnRequest").click(function(e){
		var contactName = $("#txtContactName").val();
		var contactPhone = $("#txtContactPhone").val();
		var contactEmail = $("#txtContactEmail").val();
		var notes = $("#txtnotes").val();
		var places = $("#txtPlaces").val();
		var date = $("#txtBookDate").val();
		var category = $("#cbCategory").val();
		var regex = new RegExp("^.+@.+\..+$");

		if (contactName.trim() == "") {
			$("#error-request").html("Please fill your name first.");
			$("#error-request").show();
		} else if (contactPhone.trim() == "") {
			$("#error-request").html("Please fill your phone first.");
			$("#error-request").show();
		} else if (contactEmail.trim() == "") {
			$("#error-request").html("Please fill your email first.");
			$("#error-request").show();
		} else if (!regex.test(contactEmail)) {
			$("#error-request").html("Your email format is not valid!");
			$("#error-request").show();
		} else {
			$.ajax({
				url : base_url + "home/booking",
				type : "POST",
				dataType : "html",
				data : "places=" + places + "&date=" + date + "&category=" + category + "&name=" + contactName + "&phone=" + contactPhone + "&email=" + contactEmail + "&notes="+notes,
				success:function(result) {
					alert("Booking has been saved successfully. We will contact you soon, so stay tune.");
					$("#dialog").modal("hide");
				}, error : function(result) {
					alert("Booking failed : " + result);
				}
			});
		}
		window.setTimeout(function(){ $("#error-request").hide(); }, 3200);
	});

	// Manage newsletter Box
    $("#btedit-newsletter").click(function(e)
    {
    	collapse_all();
		$("#manage-newsletter-box").show();
        $("#manage-newsletter-box").animate({left:'0'},140);
    });

    $("#close-newsletter").click(function(e)
    {
    	$("#manage-newsletter-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-newsletter-box").hide();},140);
    });

	// Manage Booking
	$("#btmanage-booking").click(function(e) {
		collapse_all();
		$("#manage-booking-box").show();
        $("#manage-booking-box").animate({left:'0'},140);
    });

    $("#close-booking").click(function(e) {	
        $("#manage-booking-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-booking-box").hide();},140);
    });

	// Manage Portfolio
	$("#btedit-portfolio").click(function(e) {
		collapse_all();
		$("#manage-portfolio-box").show();
        $("#manage-portfolio-box").animate({left:'0'},140);
    });

    $("#close-portfolio").click(function(e) {	
        $("#manage-portfolio-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-portfolio-box").hide();},140);
    });

	// Manage Team
	$("#btedit-team").click(function(e) {
		collapse_all();
		$("#manage-team-box").show();
        $("#manage-team-box").animate({left:'0'},140);
    });

    $("#close-team").click(function(e) {	
        $("#manage-team-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-team-box").hide();},140);
    });


	// Manage News
	$("#btedit-news").click(function(e) {
		collapse_all();
		$("#manage-news-box").show();
        $("#manage-news-box").animate({left:'0'},140);
    });

    $("#close-news").click(function(e) {	
        $("#manage-news-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-news-box").hide();},140);
    });

	// Manage Category
	$("#btedit-category").click(function(e) {
		collapse_all();
		$("#manage-category-box").show();
        $("#manage-category-box").animate({left:'0'},140);
    });

    $("#close-category").click(function(e) {	
        $("#manage-category-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-category-box").hide();},140);
	});
	
	// Manage Blog Category
	$("#btedit-blogcategory").click(function(e) {
		collapse_all();
		$("#manage-blogcategory-box").show();
        $("#manage-blogcategory-box").animate({left:'0'},140);
    });

    $("#close-blogcategory").click(function(e) {	
        $("#manage-blogcategory-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-blogcategory-box").hide();},140);
	});
	
	// Manage Blog Category
	$("#btedit-tags").click(function(e) {
		collapse_all();
		$("#manage-tags-box").show();
        $("#manage-tags-box").animate({left:'0'},140);
    });

    $("#close-tags").click(function(e) {	
        $("#manage-tags-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-tags-box").hide();},140);
	});

	// Manage Pricing Plan
	$(".btedit-detail").click(function(e) {
		var id = $(this).attr("data-id");
		var name = $(this).attr("data-name");
		
		//collapse_all();
		retrieve_detail(id);
		$("#hid_id_detail").val(id);
		$("#planc").html($(this).attr("data-name"));
		$("#manage-detail-plan-box").show();
        $("#manage-detail-plan-box").animate({left:'0'},140);
    });

    $("#close-detail-plan").click(function(e) {	
        $("#form-input-detail").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-detail-plan-box").hide();},140);
	});
	
	$("#button-hide-detail").click(function(e) {	
        $("#upload_detail").toggle();
		// window.setTimeout(function(){ $("#form-input-detail").hide();},140);
	})


	$("#btedit-plan").click(function(e) {
		collapse_all();
		$("#manage-plan-box").show();
        $("#manage-plan-box").animate({left:'0'},140);
    });

    $("#close-plan").click(function(e) {	
        $("#manage-plan-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-plan-box").hide();},140);
    });

	// Manage Box Why
	$("#btedit-why").click(function(e) {
		collapse_all();
		$("#manage-why-box").show();
        $("#manage-why-box").animate({left:'0'},140);
    });

    $("#close-why").click(function(e) {	
        $("#manage-why-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-why-box").hide();},140);
    });

	// Manage Box Home
	$("#btedit-home").click(function(e) {
		collapse_all();
		$("#manage-home-box").show();
        $("#manage-home-box").animate({left:'0'},140);
    });

    $("#close-home").click(function(e) {	
        $("#manage-home-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-home-box").hide();},140);
	});

	// Manage Opsi Roder
	$("#btedit-opsi").click(function(e) {
		collapse_all();
		$("#manage-opsi-box").show();
        $("#manage-opsi-box").animate({left:'0'},140);
    });

    $("#close-opsi").click(function(e) {	
        $("#manage-opsi-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-opsi-box").hide();},140);
	});
	
	// Manage Box Home1
	$("#btedit-home1").click(function(e) {
		collapse_all();
		$("#manage-home1-box").show();
        $("#manage-home1-box").animate({left:'0'},140);
    });

    $("#close-home1").click(function(e) {	
        $("#manage-home1-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-home1-box").hide();},140);
	});
	
	// Manage Box Layanan
	$("#btedit-layanan").click(function(e) {
		collapse_all();
		$("#manage-layanan-box").show();
        $("#manage-layanan-box").animate({left:'0'},140);
    });

    $("#close-layanan").click(function(e) {	
        $("#manage-layanan-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-layanan-box").hide();},140);
	});
	
	// Manage Box Service
	$("#btedit-service").click(function(e) {
		collapse_all();
		$("#manage-service-box").show();
        $("#manage-service-box").animate({left:'0'},140);
    });

    $("#close-service").click(function(e) {	
        $("#manage-service-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-service-box").hide();},140);
    });

    // Manage Box Client
	$("#btedit-client").click(function(e) {
		collapse_all();
		$("#manage-client-box").show();
        $("#manage-client-box").animate({left:'0'},140);
    });

    $("#close-client").click(function(e) {	
        $("#manage-client-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-client-box").hide();},140);
	});
	
	// Manage Box Fact
	$("#btedit-fact").click(function(e) {
		collapse_all();
		$("#manage-fact-box").show();
        $("#manage-fact-box").animate({left:'0'},140);
    });

    $("#close-fact").click(function(e) {	
        $("#manage-fact-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-fact-box").hide();},140);
    });

	// Manage Box Event
	$("#btedit-how").click(function(e) {
		collapse_all();
		$("#manage-how-box").show();
        $("#manage-how-box").animate({left:'0'},140);
    });

    $("#close-how").click(function(e) {	
        $("#manage-how-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-how-box").hide();},140);
	});
	
	// Manage Box Tenda Service
	$("#btedit-tenda").click(function(e) {
		collapse_all();
		$("#manage-tenda-box").show();
        $("#manage-tenda-box").animate({left:'0'},140);
    });

    $("#close-tenda").click(function(e) {	
        $("#manage-tenda-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-tenda-box").hide();},140);
    });

    // Manage Testimoni Box
    $("#btedit-testimoni").click(function(e)
    {
    	collapse_all();
		$("#manage-testimoni-box").show();
        $("#manage-testimoni-box").animate({left:'0'},140);
    });

    $("#close-testimoni").click(function(e)
    {
    	$("#manage-testimoni-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-testimoni-box").hide();},140);
    });

    // var today = new Date();
    // $(".date").datepicker({
    // 	minDate : today
    // });
    
	// Validation
	$(".num").keydown(function(e) {
        if ((e.keyCode<48 || e.keyCode>57) && e.keyCode!=8 &&e.keyCode!=46 &&e.keyCode!=37 &&e.keyCode!=38 &&e.keyCode!=39 &&e.keyCode!=40 )
		{
			return false;
		}
    });

	$("#btlogin").click(function(e) 
	{
    	var username=$("#txtusername").val(); 
		var password=$("#txtpassword").val();
		
		if (username=="") 
			$("#error-login").html("Please fill your username first.");
		else if (password=="")
			$("#error-login").html("Please fill your password first.");
		else
		{
			$.ajax({
				url:base_url+"home/do_login",
				data:"id_admin="+username+"&password="+password,
				dataType:"html",
				type:"POST",
				success: function(result)
				{
					switch (parseInt(result))
					{
						case 0:window.location.reload();break;
						case 1:$("#error-login").html("Your username is not registered yet.");break;
						case 2:$("#error-login").html("Your password is incorrect.");break;
						case 3:$("#error-login").html("Your account is not active.");break;
					}				
				}
			});
		}
		window.setTimeout(function(){$("#error-login").html("");}, 2400);
    });
	
	$(document).keydown(function(e) 
	{
        if(e.keyCode==13)
		{
			$("#btlogin").click();		
		}
    });

    $("#cropping").on("shown.bs.modal", function(e)
    {
    	cropper.reset().replace(blobURL);
    });

    $("#cropping").on("hide.bs.modal", function(e)
    {
    	$(".fnchport").val("");
    });

	// Booking
	$("#btsend_booking").click(function(e){
		var id = $("#hid_id_booking").val();
		var title = $("#txtmsgtitle").val();
		var msg = $("#txtSendMessage").val();

		if (title == "") {
			$("#error-booking").html("Please fill title first."); 
		} else if (msg == "") {
			$("#error-booking").html("Please fill message first."); 
		} else {
			$.ajax({
				url : base_url + "home/send_booking",
				dataType : "html",
				type : "POST",
				data : "id=" + id + "&title=" + title + "&msg=" + msg,
				success : function(result) {
					alert("Mail has been sent successfully.");
					$("#txtmsgtitle").val("");
					$("#txtSendMessage").val("");
					$("#error-booking").html("");

					$("#btcancel_booking").click();
				}, error : function(result) {
					alert("AJAX Error when send email.");
				}
			});
		}
	});

	$(".btsend-mail").click(function(e){
		var id = $(this).attr("data-id");

		$("#booking-scroll").hide();
		$("#hid_id_booking").val(id);
		$("#edit_booking").fadeIn("fast");
	});

	$("#btcancel_booking").click(function(e)
	{
		$("#booking-scroll").fadeIn("fast");
		$("#edit_booking").hide();
	});

	$(".btreject-booking, .btaccept-booking").click(function(e)
	{
		var id = $(this).attr("data-id");
		var status = $(this).attr("data-value");

		var conf = confirm("Are you sure about your decision ?");
		if (conf == true) {
			$.ajax({
				url :base_url + "home/update_book_status",
				dataType : "html",
				type : "POST",
				data : "id=" + id + "&status=" + status,
				success : function(result) {
					alert("Booking status has been saved successfully.");
					retrieve_booking();
				}, error : function(result) {
					alert("AJAX Error when change status.");
				}
			});
		}
	});

	// Maps Form
	$("#btedit-map").click(function(e)
	{
		// $(".setting-text").slideUp("fast");
		alert("Place the marker to the location you want.");
		google.maps.event.addListener(map, 'click', function(event){
			placeMarker(event.latLng);
		});
		$(".edit-map").slideDown("slow");
		$("#btsave-map").fadeIn("fast");
		$("#btcancel-map").fadeIn("fast");
		$("#btedit-map").hide();
	});
	
	$("#btcancel-map").click(function(e)
	{
		var conf = confirm("Are you sure want to remove all your marker updates ?");
		if (conf == true) {
			marker.setMap(null);
			marker = new google.maps.Marker({
		        position: positions,
		        map: map,
		        title: title,
		        // icon: base_url + 'image/map-marker.png'
		    });
			$(".edit-map").slideUp("slow");
			$("#btsave-map").hide();
			$("#btcancel-map").hide();
			$("#btedit-map").fadeIn("fast");
		}
	});

	$("#btsave-map").click(function(e)
	{
		var conf = confirm("Are you sure want to save this location ?");
		if (conf == true) {
			positions = position2;
			$.ajax({
				url : base_url + "home/update_location",
				type : "POST",
				dataType : "html",
				data : "langitude=" + positions.lng() + "&latitude=" + positions.lat(),
				success: function(result) {
					$("#btcancel-map").click();
				}, error : function(result) {
					alert("Error when update location.");
				}
			});
		}

	});

	// Blog Link Form
	$("#btedit-bloglink").click(function(e)
	{
		$(".bloglink-text").slideUp("fast");
		$(".edit-bloglink").slideDown("slow");
		$("#btsave-bloglink").fadeIn("fast");
		$("#btcancel-bloglink").fadeIn("fast");
		$("#btedit-bloglink").hide();
	});
	
	$("#btcancel-bloglink").click(function(e)
	{
		$(".bloglink-text").slideDown("fast");
		$(".edit-bloglink").slideUp("slow");
		$("#btsave-bloglink").hide();
		$("#btcancel-bloglink").hide();
		$("#btedit-bloglink").fadeIn("fast");
	});

	$("#btsave-bloglink").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var blog_link=$("#txtblogLink").val();
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;

				$("#newLink").attr("href", blog_link);

				$("#btcancel-bloglink").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_bloglink",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("link="+blog_link);
	});


	// Setting Form
	$("#btedit-setting").click(function(e)
	{
		$(".setting-text").slideUp("fast");
		$(".edit-setting").slideDown("slow");
		$("#btsave-setting").fadeIn("fast");
		$("#btcancel-setting").fadeIn("fast");
		$("#btedit-setting").hide();
	});
	
	$("#btcancel-setting").click(function(e)
	{
		$(".setting-text").slideDown("slow");
		$(".edit-setting").slideUp("fast");
		$("#btsave-setting").hide();
		$("#btcancel-setting").hide();
		$("#btedit-setting").fadeIn("fast");
	});
	
	$("#btsave-setting").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var facebook=$("#txtFacebook").val();
		var twitter=$("#txtTwitter").val();
		var linkedin=$("#txtLinkedIn").val();
		var google=$("#txtGooglePlus").val();
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;

				$(".lnk-facebook").attr("href", facebook);
				$(".lnk-twitter").attr("href", twitter);
				$(".lnk-linkedin").attr("href", linkedin);
				$(".lnk-google").attr("href", google);

				if (facebook == "") 
					$(".li-facebook").addClass("d_none");
				else
					$(".li-facebook").removeClass("d_none");

				if (twitter == "") 
					$(".li-twitter").addClass("d_none");
				else
					$(".li-twitter").removeClass("d_none");

				if (linkedin == "") 
					$(".li-linkedin").addClass("d_none");
				else
					$(".li-linkedin").removeClass("d_none");

				if (google == "") 
					$(".li-google").addClass("d_none");
				else
					$(".li-google").removeClass("d_none");

				$("#btcancel-setting").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_social",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("facebook="+facebook+"&twitter="+twitter+"&linkedin="+linkedin+"&google="+google);
	});

	// Video Form
	$("#btedit-video").click(function(e)
	{
		$(".video-text").slideUp("fast");
		$(".edit-video").slideDown("slow");
		$("#btsave-video").fadeIn("fast");
		$("#btcancel-video").fadeIn("fast");
		$("#btedit-video").hide();
	});
	
	$("#btcancel-video").click(function(e)
	{
		$(".video-text").slideDown("slow");
		$(".edit-video").slideUp("fast");
		$("#btsave-video").hide();
		$("#btcancel-video").hide();
		$("#btedit-video").fadeIn("fast");
	});
	
	$("#btsave-video").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var link=$("#txtVideoLink").val();
		var text=$("#txtVideoText").val();
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;

				$("#video-popup").attr("href", link);
				
				var c=nl2br(text);
				$("#hV-text").html(c);

				//$("#sp_content").html(content);
				$("#btcancel-video").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_video",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("link="+link+"&text="+text);
	});

	// About Form
	$("#btedit-about").click(function(e)
	{
		$(".about-text").slideUp("fast");
		$(".edit-about").slideDown("slow");
		$("#btsave-about").fadeIn("fast");
		$("#btcancel-about").fadeIn("fast");
		$("#btedit-about").hide();
	});
	
	$("#btcancel-about").click(function(e)
	{
		$(".about-text").slideDown("slow");
		$(".edit-about").slideUp("fast");
		$("#btsave-about").hide();
		$("#btcancel-about").hide();
		$("#btedit-about").fadeIn("fast");
	});
	
	$("#btsave-about").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var quote=$("#txtquote").val();
		var simple=$("#txtsimple").val();
		//var content=CKEDITOR.instances.txtcontent.getData();
		var content=$("#txtcontent").val();
		var content2=$("#txtcontent2").val();
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;
				$("#sp_quote_text").html(quote);
				$("#sp_simple_quote").html(simple);
				
				var c=nl2br(content);				
				$("#about-text").html(c);
				$("#txtcontent").val(br2nl(c));

				var c=nl2br(content2);				
				$("#about-text2").html(c);
				$("#txtcontent2").val(br2nl(c));

				//$("#sp_content").html(content);
				$("#btcancel-about").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_about",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("quote="+quote+"&simple="+escape(simple)+"&content="+encodeURIComponent(content)+"&content2="+encodeURIComponent(content2));
	});

	// Roder Form
	$("#btedit-roder").click(function(e)
	{
		$(".roder-text").slideUp("fast");
		$(".edit-roder").slideDown("slow");
		$("#btsave-roder").fadeIn("fast");
		$("#btcancel-roder").fadeIn("fast");
		$("#btedit-roder").hide();
	});
	
	$("#btcancel-roder").click(function(e)
	{
		$(".roder-text").slideDown("slow");
		$(".edit-roder").slideUp("fast");
		$("#btsave-roder").hide();
		$("#btcancel-roder").hide();
		$("#btedit-roder").fadeIn("fast");
	});
	
	$("#btsave-roder").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var title=$("#txtroder_title").val();
		var subtitle=$("#txtroder_subtitle").val();
		//var content=CKEDITOR.instances.txtcontent.getData();
		var spec_title=$("#txtroder_spec").val();
		var spec_desc=$("#txtroder_desc").val();
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;
				$("#sp_roder_title").html(title);
				$("#sp_roder_spec").html(spec_title);
				
				var c=nl2br(subtitle);				
				$("#sp_roder_subtitle").html(c);
				$("#txtroder_subtitle").val(br2nl(c));

				var c=nl2br(spec_desc);				
				$("#sp_roder_desc").html(c);
				$("#txtroder_desc").val(br2nl(c));

				//$("#sp_content").html(content);
				$("#btcancel-roder").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_roder",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("title="+title+"&subtitle="+encodeURIComponent(subtitle)+"&spec_title="+spec_title+"&spec_desc="+encodeURIComponent(spec_desc));
	});

	// About1 Form
	$("#btedit-about1").click(function(e)
	{
		$(".about1-text").slideUp("fast");
		$(".edit-about1").slideDown("slow");
		$("#btsave-about1").fadeIn("fast");
		$("#btcancel-about1").fadeIn("fast");
		$("#btedit-about1").hide();
	});
	
	$("#btcancel-about1").click(function(e)
	{
		$(".about1-text").slideDown("slow");
		$(".edit-about1").slideUp("fast");
		$("#btsave-about1").hide();
		$("#btcancel-about1").hide();
		$("#btedit-about1").fadeIn("fast");
	});
	
	$("#btsave-about1").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var quote=$("#txtquote").val();
		var simple=$("#txtsimple").val();
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;
				$("#sp_quote_text").html(quote);
				$("#sp_simple_quote").html(simple);

				//$("#sp_content").html(content);
				$("#btcancel-about1").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_about1",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("quote="+quote+"&simple="+escape(simple));
	});

	// About2 Form
	$("#btedit-about2").click(function(e)
	{
		$(".about2-text").slideUp("fast");
		$(".edit-about2").slideDown("slow");
		$("#btsave-about2").fadeIn("fast");
		$("#btcancel-about2").fadeIn("fast");
		$("#btedit-about2").hide();
	});
	
	$("#btcancel-about2").click(function(e)
	{
		$(".about2-text").slideDown("slow");
		$(".edit-about2").slideUp("fast");
		$("#btsave-about2").hide();
		$("#btcancel-about2").hide();
		$("#btedit-about2").fadeIn("fast");
	});
	
	$("#btsave-about2").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var box1=$("#txtbox1").val();
		var box2=$("#txtbox2").val();
		//var content=CKEDITOR.instances.txtcontent.getData();
		var box3=$("#txtbox3").val();
		var box4=$("#txtbox4").val();
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;
				$("#sp_box1").html(box1);
				$("#sp_box2").html(box2);
				$("#sp_box3").html(box3);
				$("#sp_box4").html(box4);

				//$("#sp_content").html(content);
				$("#btcancel-about2").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_about2",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("box1="+box1+"&box2="+box2+"&box3="+box3+"&box4="+box4);
	});

	// Title Master Form
	
	$(".btedit-title").click(function(e)
	{
		$(".title-text").slideUp("fast");
		$(".edit-title").slideDown("slow");
		$(".btsave-title").fadeIn("fast");
		$(".btcancel-title").fadeIn("fast");
		$(".btedit-title").hide();
	});
	
	$(".btcancel-title").click(function(e)
	{
		$(".title-text").slideDown("slow");
		$(".edit-title").slideUp("fast");
		$(".btsave-title").hide();
		$(".btcancel-title").hide();
		$(".btedit-title").fadeIn("fast");
	});
	
	$(".btsave-title").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var title1=$("#txttitle1_master").val();
		var title2=$("#txttitle2_master").val();
		var title3=$("#txttitle3_master").val();
		var title4=$("#txttitle4_master").val();
		var title5=$("#txttitle5_master").val();
		var title6=$("#txttitle6_master").val();
		var subtitle1=$("#txtsubtitle1_master").val();
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;
				
				// $("#span-contact").html(nl2br(contact));
				// var c=nl2br(title1);				
				$("#title1-master").html(nl2br(title1));
				// $("#txttitle1_master").val(br2nl(title1));

				var c2=nl2br(title2);				
				$("#title2-master").html(c2);
				$("#txttitle2_master").val(br2nl(c2));

				var c3=nl2br(title3);				
				$("#title3-master").html(c3);
				$("#txttitle3_master").val(br2nl(c3));

				var c4=nl2br(title4);				
				$("#title4-master").html(c4);
				$("#txttitle4_master").val(br2nl(c4));

				var c5=nl2br(title5);				
				$("#title5-master").html(c5);
				$("#txttitle5_master").val(br2nl(c5));

				var c6=nl2br(title6);				
				$("#title6-master").html(c6);
				$("#txttitle6_master").val(br2nl(c6));

				var c7=nl2br(subtitle1);				
				$("#subtitle1-master").html(c7);
				$("#txtsubtitle1_master").val(br2nl(c7));

				//$("#sp_content").html(content);
				$(".btcancel-title").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_about_title",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("title1="+encodeURIComponent(title1)+"&title2="+encodeURIComponent(title2)+"&title3="+encodeURIComponent(title3)
		+"&title4="+encodeURIComponent(title4)+"&title5="+encodeURIComponent(title5)+"&title6="+encodeURIComponent(title6)
		+"&subtitle1="+encodeURIComponent(subtitle1));
	});

	// About Quote Form
	$("#btedit-aboutquote").click(function(e)
	{
		$(".aboutquote-text").slideUp("fast");
		$(".edit-aboutquote").slideDown("slow");
		$("#btsave-aboutquote").fadeIn("fast");
		$("#btcancel-aboutquote").fadeIn("fast");
		$("#btedit-aboutquote").hide();
	});
	
	$("#btcancel-aboutquote").click(function(e)
	{
		$(".aboutquote-text").slideDown("slow");
		$(".edit-aboutquote").slideUp("fast");
		$("#btsave-aboutquote").hide();
		$("#btcancel-aboutquote").hide();
		$("#btedit-aboutquote").fadeIn("fast");
	});
	
	$("#btsave-aboutquote").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var quote=$("#txtquote_tenda").val();
		var content=$("#txtcontent_tenda").val();
		var title1=$("#txttitle1_tenda").val();
		var subtitle1=$("#txtsubtitle1_tenda").val();
		var title2=$("#txttitle2_tenda").val();
		var subtitle2=$("#txtsubtitle2_tenda").val();
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;
				$("#sp_quote_tenda").html(quote);
				$("#sp_title1_tenda").html(title1);
				$("#sp_subtitle1_tenda").html(subtitle1);
				$("#sp_title2_tenda").html(title2);
				$("#sp_subtitle2_tenda").html(subtitle2);
				
				var c=nl2br(content);				
				$("#aboutquote-text").html(c);
				$("#txtcontent_tenda").val(br2nl(c));

				//$("#sp_content").html(content);
				$("#btcancel-aboutquote").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_about_quote",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("quote="+quote+"&content="+encodeURIComponent(content)+"&title1="+title1+"&subtitle1="+subtitle1
		+"&title2="+title2+"&subtitle2="+subtitle2);
	});

	// About Home Form
	$("#btedit-abouthome").click(function(e)
	{
		$(".abouthome-text").slideUp("fast");
		$(".edit-abouthome").slideDown("slow");
		$("#btsave-abouthome").fadeIn("fast");
		$("#btcancel-abouthome").fadeIn("fast");
		$("#btedit-abouthome").hide();
	});
	
	$("#btcancel-abouthome").click(function(e)
	{
		$(".abouthome-text").slideDown("slow");
		$(".edit-abouthome").slideUp("fast");
		$("#btsave-abouthome").hide();
		$("#btcancel-abouthome").hide();
		$("#btedit-abouthome").fadeIn("fast");
	});
	
	$("#btsave-abouthome").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var quote=$("#txtquote_home").val();
		var simple=$("#txtsimple_home").val();
		//var content=CKEDITOR.instances.txtcontent.getData();
		var content=$("#txtcontent_home").val();
		
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;
				$("#sp_quote_text_home").html(quote);
				$("#sp_simple_quote_home").html(simple);
				
				var c=nl2br(content);				
				$("#abouthome-text").html(c);
				$("#txtcontent_home").val(br2nl(c));

				//$("#sp_content").html(content);
				$("#btcancel-abouthome").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_about_home",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("quote="+quote+"&simple="+escape(simple)+"&content="+encodeURIComponent(content));
	});


	// Change CV Document
	$("#btedit-cv").click(function(e)
	{
		$("#fnchcv").click();
	});

	$("#fnchcv").change(function(e)
	{
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];

			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadCV(file);
		}
	});

	// Fact-----------
	//|_______________|

	// $("#btedit-fact").click(function(e)
	// {
	// 	$(".counter-title").slideUp("fast");
	// 	$(".h-fact").slideDown("slow");
	// 	$("#btsave-fact").fadeIn("fast");
	// 	$("#btcancel-fact").fadeIn("fast");
	// 	$("#btedit-fact").hide();
	// });
	
	// $("#btcancel-fact").click(function(e)
	// {
	// 	$(".counter-title").slideDown("slow");
	// 	$(".h-fact").slideUp("fast");
	// 	$("#btsave-fact").hide();
	// 	$("#btcancel-fact").hide();
	// 	$("#btedit-fact").fadeIn("fast");
	// });

	$(".btedit-fact").click(function(e) {
		var id=$(this).attr("data-id");
		var item_name=$(this).attr("data-name");

		$("#btadd_fact").hide();
		$("#fact-scroll").hide();
		$("#edit_fact").fadeIn("fast");
		
		//set data to component
		$("#hid_id_fact").val(id);
		$("#txtedfact_name").val(item_name);
	});

	$("#btadd_fact").click(function(e) {
        $("#btadd_fact").hide();
		$("#fact-scroll").hide();
        $("#upload_fact").fadeIn("fast");
    });
	
	$("#btcancel_fact").click(function(e) {
        $("#btadd_fact").fadeIn("fast");
		$("#fact-scroll").fadeIn("fast");
        $("#upload_fact").hide();
    });
	
	$("#btcanceled_fact").click(function(e) {
        $("#btadd_fact").fadeIn("fast");
		$("#fact-scroll").fadeIn("fast");
        $("#edit_fact").hide();
	});

	$("#btsaveed_fact").click(function(e) 
	{
    	var id = $("#hid_id_fact").val();
		var slider_name = $("#txtedfact_name").val();
		var visible = ($("#chkedfact_visible").prop("checked") == true) ? "1" : "0";
		
		if (slider_name=="")
			$("#error-edit-fact").html("Please fill fact name first."); 
		else
		{
			$.ajax({
				url:base_url+"home/edit_fact",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&name="+slider_name+"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_fact();
					//Close
					$("#btcanceled_fact").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_fact").click(function(e) 
	{
		var filename=$("#fnfact_image").val();
		var name=$("#txtpartner_name").val();
		var visible=(document.getElementById("chkfact_visible").checked)?1:0;
		
		//Clean error
		$("#error-fact").html("");
		if (filename=="")
			$("#error-fact").html("Please attach the file first.");
		else if (name=="")
			$("#error-fact").html("Please fill the Partner name first.");
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fnfact_image").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-fact").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-fact").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePictureFact(file);
		}
    });


    var uploadFilePictureFact = function(file)
	{
		var filename=$("#fnfact_image").val();
		var name=$("#txtpartner_name").val();
		var visible=(document.getElementById("chkfact_visible").checked)?1:0;
		
		$("#td-percent").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_fact?name="+name+"&visible="+visible, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-fact").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent").html("100%");
				  
				  //Clean everything..
				  $("#error-fact").html("");
				  $("#txtpartner_name").val("");
				  $("#fnfact_image").val("");
				  document.getElementById("chkfact_visible").checked = true;
				  $("#td-percent").hide();

				  if (xhr.responseText!="0")
				  	$("#error-fact").html("You can have maximum 4 images.");
				  else
				  {
				  	retrieve_fact();
				  	$("#btcancel_fact").trigger("click");
				  }
				} else {  
				  $("#error-fact").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_fact").html("Your browser doesnt support FileReader object");
		} 		
	}

	$(".btdelete-fact").click(function(e) {
		var ID_fact=$(this).attr("data-id");
		var filename = $(this).attr("data-value");
		var conf=confirm("Are you sure want to delete this fact item ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/delete_fact?id="+ID_fact+"&filename=" + filename,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("Fact item removed successfully.");
						retrieve_fact();
					}
				}
			});
		}
	});

	// Button Event
	$(".btfact-up").click(function(e) 
	{
		var factID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_fact_up?ID_fact="+factID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_fact();
			}
		});
	});
	
	$(".btfact-down").click(function(e) 
	{
		var factID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_fact_down?ID_fact="+factID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_fact();
			}
		});
	});

	$(".btchangepic-fact").click(function(e)
	{
		var id=$(this).attr("data-id");
		var fnchpic=$(".fnchpic_fact[data-id='"+id+"']");
		fnchpic.click();
	});

	$(".fnchpic_fact").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePictureEditFact(file,id);
		}
	});

	// $("#btsave-fact").click(function(e)
	// {
	// 	var xmlhttp;
	// 	var status;
		
	// 	var award=$("#txtaward").val();
	// 	var client=$("#txtclient").val();
	// 	var project=$("#txtproject").val();
	// 	var team=$("#txtteam").val();
		
	// 	if (window.XMLHttpRequest)
	// 		xmlhttp=new XMLHttpRequest();
	// 	else
	// 		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
	// 	xmlhttp.onreadystatechange=function()
	// 	{
	// 		if (xmlhttp.status==200 && xmlhttp.readyState==4)
	// 		{
	// 			status=xmlhttp.responseText;

	// 			$("#h-award").html(award);
	// 			$("#h-client").html(client);
	// 			$("#h-project").html(project);
	// 			$("#h-team").html(team);

	// 			//$("#sp_content").html(content);
	// 			$("#btcancel-fact").trigger("click");
	// 		}
	// 	}
	// 	xmlhttp.open("POST",base_url+"home/update_fact",true);
	// 	xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	// 	xmlhttp.send("award="+award+"&client="+client+"&project="+project+"&team="+team);
	// });

	// intro content

	$("#btedit-introContent").click(function(e)
	{
		$(".edit-introContent").slideDown("slow");
		$(".introContent-label").slideUp("fast");
		$("#btsave-introContent").fadeIn("fast");
		$("#btcancel-introContent").fadeIn("fast");
		$("#btedit-introContent").fadeOut("fast");
	});
	
	$("#btcancel-introContent").click(function(e)
	{
		$(".edit-introContent").slideUp("fast");
		$(".introContent-label").slideDown("slow");
		$("#btsave-introContent").fadeOut("fast");
		$("#btcancel-introContent").fadeOut("fast");
		$("#btedit-introContent").fadeIn("fast");
	});
	
	$("#btsave-introContent").click(function(e) {
        var xmlhttp;
		var status;
		
		var content=$("#txtintroContent").val();
		
				
		if (content == "")
			alert("Please fill your content first.");
		else
		{
			if (window.XMLHttpRequest)
				xmlhttp=new XMLHttpRequest();
			else
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.status==200 && xmlhttp.readyState==4)
				{
					status=xmlhttp.responseText;
					
					$("#introContent").html(content);
					
					$("#btcancel-introContent").trigger("click");

					alert(content);
				}
			}
			xmlhttp.open("POST",base_url+"home/update_intro",true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("content="+content);
		}
	});
	
	// Contact

	$("#btedit-contact").click(function(e)
	{
		$(".edit-contact").slideDown("slow");
		$(".contact-label").slideUp("fast");
		$("#btsave-contact").fadeIn("fast");
		$("#btcancel-contact").fadeIn("fast");
		$("#btedit-contact").fadeOut("fast");
	});
	
	$("#btcancel-contact").click(function(e)
	{
		$(".edit-contact").slideUp("fast");
		$(".contact-label").slideDown("slow");
		$("#btsave-contact").fadeOut("fast");
		$("#btcancel-contact").fadeOut("fast");
		$("#btedit-contact").fadeIn("fast");
	});
	
	$("#btsave-contact").click(function(e) {
        var xmlhttp;
		var status;
		
		var email=$("#txtemail").val();
		var email2=$("#txtemail2").val();
		var address = $("#txtaddress").val();
		var phone=$("#txtphone").val();
		var msg=$("#txtofficeHours").val();
				
		if (address == "")
			alert("Please fill your address first.");
		else if (phone == "")
			alert("Please fill your phone first.");	
		else if (email=="")
			alert("Please fill your email first.");
		else
		{
			if (window.XMLHttpRequest)
				xmlhttp=new XMLHttpRequest();
			else
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.status==200 && xmlhttp.readyState==4)
				{
					status=xmlhttp.responseText;
					
					$("#p-mp1").html(phone);
					$("#p-mp1Header").html(phone);
					$("#p-mp1Footer").html(phone);
					$("#p-email").html(email);
					$("#p-emailFooter").html(email);
					$("#p-email2").html(email2);
					$("#p-location").html(nl2br(address));
					$("#p-locationHeader").html(nl2br(address));
					$("#p-locationFooter").html(nl2br(address));
					$("#p-officeHours").html(nl2br(msg));
					
					$("#btcancel-contact").trigger("click");
				}
			}
			xmlhttp.open("POST",base_url+"home/update_contact",true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("email="+email+"&email2="+ email2 +"&phone="+encodeURIComponent(phone)+"&address="+address+"&officeHours="+msg);
		}
    });


    // Contact Msg

    // Contact

	$("#btedit-contactmsg").click(function(e)
	{
		$(".edit-contactmsg").slideDown("slow");
		$(".span-contact").slideUp("fast");
		$("#btsave-contactmsg").fadeIn("fast");
		$("#btcancel-contactmsg").fadeIn("fast");
		$("#btedit-contactmsg").hide();
	});
	
	$("#btcancel-contactmsg").click(function(e)
	{
		$(".edit-contactmsg").slideUp("fast");
		$(".span-contact").slideDown("slow");
		$("#btsave-contactmsg").hide();
		$("#btcancel-contactmsg").hide();
		$("#btedit-contactmsg").fadeIn("fast");
	});
	
	$("#btsave-contactmsg").click(function(e) {
        var xmlhttp;
		var status;
		
		var contact=$("#txtcontact").val();
				
				
		if (contact=="")
			alert("Please fill your contact text first.");
		else
		{
			if (window.XMLHttpRequest)
				xmlhttp=new XMLHttpRequest();
			else
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.status==200 && xmlhttp.readyState==4)
				{
					status=xmlhttp.responseText;
					
					$("#span-contact").html(nl2br(contact));
					
					$("#btcancel-contactmsg").trigger("click");

					alert(contact);
				}
			}
			xmlhttp.open("POST",base_url+"home/update_contactmsg",true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("contact="+encodeURIComponent(contact));
		}
    });

	$("#close-admin").click(function(e) {	
        $("#manage-admin-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-admin-box").hide();},140);
    });

    // How It Works

	$(".bthow-up").click(function(e) 
	{
        var howID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_how_up?howid="+howID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_how();
			}
		});
    });
	
	$(".bthow-down").click(function(e) 
	{
        var howID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_how_down?howid="+howID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_how();
			}
		});
    });

    $(".btedit-how").click(function(e) {
		var id=$(this).attr("data-id");
		var pict=$(this).attr("data-pict");
		var title=$(this).attr("data-title");
		var subtitle=$(this).attr("data-subtitle");
		var text=$(this).attr("data-text");
		
		// alert(pict);
		$("#btadd_how").hide();
		$("#how-scroll").hide();
		$("#edit_how").fadeIn("fast");
		
		//set data to component
		$("#hid_id_how").val(id);
		$("#cbedIcon").val(pict);
		$("#iedIcon").attr("class","fa " + pict);
		$("#txtedhow_title").val(title);
		$("#txtedhow_subtitle").val(subtitle);
		$("#txtedhow_text").val(text);
    });

    
    $(".btdelete-how").click(function(e) {
    	var id_how=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this points?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_how?id="+id_how,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					console.log(result);
					if (result=="")
					{
						alert("Point removed successfully.");
						retrieve_how();
					}
				}
			});
		}
	}); 

	$("#cbIcon").change(function(e)
	{
		$("#iIcon").attr("class", "fa " + $("#cbIcon").val());
	});

	$("#cbedIcon").change(function(e)
	{
		$("#iedIcon").attr("class", "fa " + $("#cbedIcon").val());
	});

	$("#cbLayananIcon").change(function(e)
	{
		$("#iLayananIcon").attr("class", "fa " + $("#cbLayananIcon").val());
	});

	$("#cbedLayananIcon").change(function(e)
	{
		$("#iedLayananIcon").attr("class", "fa " + $("#cbedLayananIcon").val());
	});

	$("#cbserviceIcon").change(function(e)
	{
		$("#iserviceIcon").attr("class", "fa " + $("#cbserviceIcon").val());
	});

	$("#cbedserviceIcon").change(function(e)
	{
		$("#iedserviceIcon").attr("class", "fa " + $("#cbedserviceIcon").val());
	});

	$("#btadd_how").click(function(e) {
        $("#btadd_how").hide();
		$("#how-scroll").hide();
        $("#upload_how").fadeIn("fast");
    });
	
	$("#btcancel_how").click(function(e) {
        $("#btadd_how").fadeIn("fast");
		$("#how-scroll").fadeIn("fast");
        $("#upload_how").hide();
    });
	
	$("#btcanceled_how").click(function(e) {
        $("#btadd_how").fadeIn("fast");
		$("#how-scroll").fadeIn("fast");
        $("#edit_how").hide();
    });
	
	$("#btsaveed_how").click(function(e) 
	{
    	var id = $("#hid_id_how").val();
		var pict = $("#cbedIcon").val();
		var title = $("#txtedhow_title").val();
		var subtitle = $("#txtedhow_subtitle").val();
		var text = $("#txtedhow_text").val();
		
		if (pict=="")
			$("#error-edit-how").html("Please fill icon first."); 
		else if (title=="")
			$("#error-edit-how").html("Please fill title first.");
		else if (subtitle=="")
			$("#error-edit-how").html("Please fill subtitle first.");
		else if (text=="")
			$("#error-edit-how").html("Please fill how-text first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_how",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&pict="+pict+"&title="+title+"&text="+text+"&subtitle="+subtitle,
				success: function(result)
				{
					retrieve_how();
					//Close
					$("#btcanceled_how").trigger("click");
				}
			});
		}
    });

    $("body").on("click",".btchangepic-how", function(e)
	{
		var id=$(this).attr("data-id");
		var fnchhow=$(".fnchhow[data-id='"+id+"']");
		fnchhow.click();
	});

	$("body").on("change", ".fnchhow", function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileHowEdit(file,id);
		}
	});
	
	$("#btsave_how").click(function(e) 
	{
		var pict = $("#cbIcon").val();
		var title = $("#txthow_title").val();
		var subtitle = $("#txthow_subtitle").val();
		var text = $("#txthow_text").val();;
		
		if (pict=="")
			$("#error-how").html("Please attach the file first.");
		else if (title=="")
			$("#error-how").html("Please fill title first.");
		else if (subtitle=="")
			$("#error-how").html("Please fill title first.");	
		else if (text=="")
			$("#error-how").html("Please fill how-text first.");
		else
		{
			
			$.ajax({
				url:base_url+"home/insert_how",
				dataType:"html",
				type:"POST",
				data:"pict="+pict+"&title="+title+"&text="+text+"&subtitle="+subtitle,
				success: function(result)
				{
					//Clear Insert
					$("#txthow_title").val("");
					$("#cbIcon").val("icon-mobile");
					$("#iIcon").attr("class","icon-mobile");
					$("#txthow_text").val("");
					$("#txthow_subtitle").val("");
										
					//retrieve
					retrieve_how();
					
					//Close
					$("#btcancel_how").trigger("click");
				}
			});
		}
	});
	
	// Tenda Service

	$(".bttenda-up").click(function(e) 
	{
        var tendaID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_tenda_up?tendaid="+tendaID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_tenda();
			}
		});
    });
	
	$(".bttenda-down").click(function(e) 
	{
        var tendaID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_tenda_down?tendaid="+tendaID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_tenda();
			}
		});
    });

    $(".btedit-tenda").click(function(e) {
		var id=$(this).attr("data-id");
		var title=$(this).attr("data-title");
		var subtitle=$(this).attr("data-subtitle");
		
		// alert(pict);
		$("#btadd_tenda").hide();
		$("#tenda-scroll").hide();
		$("#edit_tenda").fadeIn("fast");
		
		//set data to component
		$("#hid_id_tenda").val(id);
		$("#txtedtenda_title").val(title);
		$("#txtedtenda_subtitle").val(subtitle);
    });

    
    $(".btdelete-tenda").click(function(e) {
    	var id_tenda=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this points?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_tenda?id="+id_tenda,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					console.log(result);
					if (result=="")
					{
						alert("Point removed successfully.");
						retrieve_tenda();
					}
				}
			});
		}
	});

	$("#btadd_tenda").click(function(e) {
        $("#btadd_tenda").hide();
		$("#tenda-scroll").hide();
        $("#upload_tenda").fadeIn("fast");
    });
	
	$("#btcancel_tenda").click(function(e) {
        $("#btadd_tenda").fadeIn("fast");
		$("#tenda-scroll").fadeIn("fast");
        $("#upload_tenda").hide();
    });
	
	$("#btcanceled_tenda").click(function(e) {
        $("#btadd_tenda").fadeIn("fast");
		$("#tenda-scroll").fadeIn("fast");
        $("#edit_tenda").hide();
    });
	
	$("#btsaveed_tenda").click(function(e) 
	{
    	var id = $("#hid_id_tenda").val();
		var title = $("#txtedtenda_title").val();
		var subtitle = $("#txtedtenda_subtitle").val();
		if (title=="")
			$("#error-edit-tenda").html("Please fill title first.");
		else if (subtitle=="")
			$("#error-edit-tenda").html("Please fill subtitle first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_tenda",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&title="+title+"&subtitle="+subtitle,
				success: function(result)
				{
					retrieve_tenda();
					//Close
					$("#btcanceled_tenda").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_tenda").click(function(e) 
	{
		var title = $("#txttenda_title").val();
		var subtitle = $("#txttenda_subtitle").val();
		
		if (title=="")
			$("#error-tenda").html("Please fill title first.");
		else if (subtitle=="")
			$("#error-tenda").html("Please fill title first.");	
		else
		{
			
			$.ajax({
				url:base_url+"home/insert_tenda",
				dataType:"html",
				type:"POST",
				data:"title="+title+"&subtitle="+subtitle,
				success: function(result)
				{
					//Clear Insert
					$("#txttenda_title").val("");
					$("#txttenda_subtitle").val("");
										
					//retrieve
					retrieve_tenda();
					
					//Close
					$("#btcancel_tenda").trigger("click");
				}
			});
		}
    });


    // Testimonial Section

    $(".bttestimoni-up").click(function(e) 
	{
        var testimoniID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_testimoni_down?ID_testimoni="+testimoniID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_testimoni();
			}
		});
    });
	
	$(".bttestimoni-down").click(function(e) 
	{
        var testimoniID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_testimoni_up?ID_testimoni="+testimoniID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_testimoni();
			}
		});
    });

	$(".btedit-testimoni").click(function(e) {
		var id=$(this).attr("data-id");
		var person_name=$(this).attr("data-person");
		var corp_name=$(this).attr("data-corp");
		var testimoni_text=$(this).attr("data-text");
		
		$("#btadd_testimoni").hide();
		$("#testimoni-scroll").hide();
		$("#edit_testimoni").fadeIn("fast");
		
		//set data to component
		$("#hid_id_testimoni").val(id);
		$("#txtedperson_name").val(person_name);
		$("#txtedcorp_name").val(corp_name);
		$("#txtedtestimoni_text").val(testimoni_text);
    });

	$(".btdelete-testimoni").click(function(e) {
		var ID_testimoni=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this testimonial ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_testimoni?id="+ID_testimoni,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("Testimonial removed successfully.");
						retrieve_testimoni();
					}
				}
			});
		}
	});

	$("#btadd_testimoni").click(function(e) {
        $("#btadd_testimoni").hide();
		$("#testimoni-scroll").hide();
        $("#upload_testimoni").fadeIn("fast");
    });
	
	$("#btcancel_testimoni").click(function(e) {
        $("#btadd_testimoni").fadeIn("fast");
		$("#testimoni-scroll").fadeIn("fast");
        $("#upload_testimoni").hide();
    });
	
	$("#btcanceled_testimoni").click(function(e) {
        $("#btadd_testimoni").fadeIn("fast");
		$("#testimoni-scroll").fadeIn("fast");
        $("#edit_testimoni").hide();
    });
	
	$("#btsaveed_testimoni").click(function(e) 
	{
    	var id = $("#hid_id_testimoni").val();
		var person_name = $("#txtedperson_name").val();
		var corp_name = $("#txtedcorp_name").val();
		var testimoni_text = $("#txtedtestimoni_text").val();
		
		if (person_name=="")
			$("#error-edit-testimoni").html("Please fill person name first."); 
		else if (testimoni_text=="")
			$("#error-edit-testimoni").html("Please fill testimoni-text first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_testimoni",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&person_name="+escape(person_name)+"&corp_name="+corp_name+"&testimoni_text="+testimoni_text,
				success: function(result)
				{
					console.log(result);
					retrieve_testimoni();
					//Close
					$("#btcanceled_testimoni").trigger("click");
				}
			});
		}
    });

    $("#btsave_testimoni").click(function(e) 
	{
		var person_name = $("#txtperson_name").val();
		var corp_name = $("#txtcorp_name").val();
		var testimoni_text = $("#txttestimoni_text").val();
		
		if (person_name=="")
			$("#error-testimoni").html("Please fill person name first."); 
		else if (testimoni_text=="")
			$("#error-testimoni").html("Please fill testimoni-text first.");
		else
		{
			// var imageType = /image.*/;  
			// var file = document.getElementById("fnperson_photo").files[0];
			
			
			// // check file type
			// if (!file.type.match(imageType)) {  
			//   $("#error-home").html("File \""+file.name+"\" is not a valid image file.");
			//   return false;	
			// } 
			// // check file size
			// if (parseInt(file.size / 1024) > max_size) {  
			//   $("#error-home").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			//   return false;	
			// } 
			// var size=file.size/1024;
			uploadFileTestimoni();
		}
    });

    var uploadFileHowEdit = function(file,data_id)
	{
		var id=data_id;
		
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			//$('#right_pic').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/update_how_image?id="+id, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

			
			}
			else {
				alert("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  alert("Changes saved successfully.");
				  retrieve_how();
				} else {  
				  alert("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			alert("Your browser doesn't support FileReader object");
		} 		
	}


    var uploadFileTestimoni = function()
	{
		var person_name = $("#txtperson_name").val();
		var corp_name = $("#txtcorp_name").val();
		var testimoni_text = $("#txttestimoni_text").val();
		
		$("#td-percent-test").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		// reader = new FileReader();
		// reader.onload = function(e){
		// }
		// reader.readAsDataURL();
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_testimoni?person_name="+escape(person_name)+"&corp_name="+corp_name+"&testimoni_text="+testimoni_text, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent_test").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-home").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent_test").html("100%");
				  
				  //Clean everything..
				  $("#txtperson_name").val("");
				  $("#txtcorp_name").val("");
				  $("#txttestimoni_text").val("");
				//   $("#fnperson_photo").val("");
				  $("#td-percent-test").hide();

				  //retrieve
				  retrieve_testimoni();
					
				  //Close
				  $("#btcancel_testimoni").trigger("click");

				} else {  
				  $("#error-home").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			// xhr.setRequestHeader("X-File-Name", file.fileName);
			// xhr.setRequestHeader("X-File-Size", file.fileSize);
			// xhr.setRequestHeader("X-File-Type", file.type);
			
			// // Send the file (doh)
			xhr.send();
		
		}else{
			$("#error_home").html("Your browser doesnt support FileReader object");
		} 		
	}

	var uploadFilePictureHow = function(file)
	{
		$.ajax({
			url:base_url+"home/insert_how",
			dataType:"html",
			type:"POST",
			data:"pict="+pict+"&title="+title+"&text="+text,
			success: function(result)
			{
				//Clear Insert
				$("#txthow_title").val("");
				$("#cbIcon").val("icon-mobile");
				$("#iIcon").attr("class","icon-mobile");
				$("#txthow_text").val("");
									
				//retrieve
				retrieve_how();
				
				//Close
				$("#btcancel_how").trigger("click");
			}
		});

		var filename=$("#fnhow_image").val();
		var name=$("#txthow_title").val();
		var url=$("#txthow_url").val();
		var content=$("#txthow_text").val();
		
		$("#td-percent-how").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_how?title="+name+"&text="+content+"&url="+url, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent-how").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-home").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			if (xhr.readyState === 4) 
			{  
				if (xhr.status === 200) 
				{  
				  $("#percent-how").html("100%");
				  
				  //Clean everything..
				  $("#txthow_title").val("");
				  $("#txthow_url").val("");
				  $("#fnhow_image").val("");
				  $("#txthow_text").val("");
				  $("#td-percent-how").hide();
				  retrieve_how();
				  $("#btcancel_how").trigger("click");
				} else {  
				  $("#error-how").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_home").html("Your browser doesnt support FileReader object");
		} 		
	}

	var uploadFilePlan = function(file)
	{
		var name = $("#txtplan_name").val();
		var subtitle = $("#txtproduct_subtitle").val();
		var spec = $("#txtproduct_spec").val();
		var desc = $("#txtproduct_desc").val();
		
		$("#td-percent-plan").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_plan?name="+escape(name)+"&subtitle="+subtitle+"&spec="+spec+"&desc=" + desc, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent-plan").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-plan").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			console.log(xhr.responseText);
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent-plan").html("100%");
				  
				  //Clean everything..
				  $("#txtplan_name").val("");
				  $("#txtproduct_subtitle").val("");
				  $("#txtproduct_spec").val("");
				  $("#txtproduct_desc").val("");
				  $("#fnplan_image").val("");
				  $("#td-percent-plan").hide();

				  //retrieve
				  retrieve_plan();
					
				  //Close
				  $("#btcancel_plan").trigger("click");

				} else {  
				  $("#error-plan").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_plan").html("Your browser doesnt support FileReader object");
		} 		
	}

	var uploadFileDetail = function(file)
	{
		var id_plan = $("#hid_id_detail").val();
		var detail_text = $("#txtdetail_text").val();
		var detail_kat = $("#txtdetail_kategori").val();
		
		$("#td-percent-detail").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_detail_plan?id="+id_plan+"&detail_text="+detail_text+"&detail_kat="+detail_kat, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent-detail").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-detail").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			console.log(xhr.responseText);
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent-detail").html("100%");
				  
				  //Clean everything..
				  $("#txtdetail_text").val("");
				  $("#txtdetail_kategori").val("");
				  $("#fndetail_image").val("");
				  $("#td-percent-detail").hide();

				  //retrieve
				  retrieve_detail();
					
				  //Close
				  $("#close-detail-plan").trigger("click");
				//   $("#manage-detail-plan-box").show();
		        //   $("#manage-detail-plan-box").animate({left:'0'},140);

				} else {  
				  $("#error-detail").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_detail").html("Your browser doesnt support FileReader object");
		} 		
	}

	// newsletter Section

    $(".btnewsletter-up").click(function(e)
	{
        var newsletterID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_newsletter_up?ID_newsletter="+newsletterID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_newsletter();
			}
		});
    });

	$(".btnewsletter-down").click(function(e)
	{
        var newsletterID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_newsletter_down?ID_newsletter="+newsletterID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_newsletter();
			}
		});
    });

	$(".btedit-newsletter").click(function(e) {
		var id=$(this).attr("data-id");
		var contact_email=$(this).attr("data-email");
		var visible=$(this).attr("data-visible");

		$("#btadd_newsletter").hide();
		$("#newsletter-scroll").hide();
		$("#edit_newsletter").fadeIn("fast");

		//set data to component
		$("#hid_id_newsletter").val(id);
		$("#txtedcontact_email").val(contact_email);
		$("#txtedvisible").val(visible).selected;
    });

	$(".btdelete-newsletter").click(function(e) {
		var ID_newsletter=$(this).attr("data-id");

		var conf=confirm("Are you sure want to delete this newsletteral ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_newsletter?id="+ID_newsletter,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("newsletteral removed successfully.");
						retrieve_newsletter();
					}
				}
			});
		}
	});

	$("#btadd_newsletter").click(function(e) {
        $("#btadd_newsletter").hide();
		$("#newsletter-scroll").hide();
        $("#upload_newsletter").fadeIn("fast");
    });

	$("#btcancel_newsletter").click(function(e) {
        $("#btadd_newsletter").fadeIn("fast");
		$("#newsletter-scroll").fadeIn("fast");
        $("#upload_newsletter").hide();
    });

	$("#btcanceled_newsletter").click(function(e) {
        $("#btadd_newsletter").fadeIn("fast");
		$("#newsletter-scroll").fadeIn("fast");
        $("#edit_newsletter").hide();
    });


	$("#btsaveed_newsletter").click(function(e)
	{
    	var id = $("#hid_id_newsletter").val();
		var contact_email = $("#txtedcontact_email").val();
		var visible = $("#txtedvisible").val();

		if (contact_email=="")
			$("#error-edit-newsletter").html("Please fill contact emailfirst.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_newsletter",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&contact_email="+escape(contact_email)+"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_newsletter();
					//Close
					$("#btcanceled_newsletter").trigger("click");
				}
			});
		}
    });

	$("#btsave_newsletter").click(function(e)
	{

		var contact_email = $("#txtcontact_email").val();
		var visible = $("#txtvisible").val();

		if (contact_email=="")
			$("#error-edit-newsletter").html("Please fill contact emailfirst.");
		else
		{
			$.ajax({
				url:base_url+"home/insert_newsletter",
				dataType:"html",
				type:"POST",
				data:"contact_email="+escape(contact_email)+"&visible="+visible,
				success: function(result)
				{
					//Clear Insert
					$("#txtcontact_email").val("");
					$("#txtvisible").val("0");

					//retrieve
					retrieve_newsletter();

					//Close
					$("#btcancel_newsletter").trigger("click");
				}
			});
		}
    });

	
	// Slider Home Section

    $(".bthome-up").click(function(e) 
	{
        var sliderID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_home_up?ID_slider="+sliderID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_home();
			}
		});
    });
	
	$(".bthome-down").click(function(e) 
	{
        var sliderID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_home_down?ID_slider="+sliderID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_home();
			}
		});
    });

	$(".btedit-home").click(function(e) {
		var id=$(this).attr("data-id");
		var item_name=$(this).attr("data-name");
		var content=$(this).attr("data-html");
		var visible=$(this).attr("data-visible");

		$("#btadd_home").hide();
		$("#home-scroll").hide();
		$("#edit_home").fadeIn("fast");
		
		//set data to component
		$("#hid_id_home").val(id);
		$("#txtedslider_name").val(item_name);
		$("#txtedhtml_element").val(content);
		$("#chkedslider_visible").prop("checked", (visible == "1"));
    });

	$(".btdelete-home").click(function(e) {
		var ID_slider=$(this).attr("data-id");
		var filename = $(this).attr("data-value");
		var conf=confirm("Are you sure want to delete this slider item ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/delete_slider?id="+ID_slider+"&filename=" + filename,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("Slider item removed successfully.");
						retrieve_home();
					}
				}
			});
		}
	});

	$(".btchangepic-home").click(function(e)
	{
		var id=$(this).attr("data-id");
		var fnchpic=$(".fnchpic[data-id='"+id+"']");
		fnchpic.click();
	});

	$(".fnchpic").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePictureEdit(file,id);
		}
	});

	$("#btadd_home").click(function(e) {
        $("#btadd_home").hide();
		$("#home-scroll").hide();
        $("#upload_home").fadeIn("fast");
    });
	
	$("#btcancel_home").click(function(e) {
        $("#btadd_home").fadeIn("fast");
		$("#home-scroll").fadeIn("fast");
        $("#upload_home").hide();
    });
	
	$("#btcanceled_home").click(function(e) {
        $("#btadd_home").fadeIn("fast");
		$("#home-scroll").fadeIn("fast");
        $("#edit_home").hide();
    });
	
	$("#btsaveed_home").click(function(e) 
	{
    	var id = $("#hid_id_home").val();
		var slider_name = $("#txtedslider_name").val();
		var content = $("#txtedhtml_element").val();
		var visible = ($("#chkedslider_visible").prop("checked") == true) ? "1" : "0";
		
		if (slider_name=="")
			$("#error-edit-home").html("Please fill slider name first."); 
		else
		{
			$.ajax({
				url:base_url+"home/edit_home",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&name="+slider_name+"&content="+escape(content)+"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_home();
					//Close
					$("#btcanceled_home").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_home").click(function(e) 
	{
		var filename=$("#fnslider_image").val();
		var name=$("#txtslider_name").val();
		var content=$("#txthtml_element").val();
		var visible=(document.getElementById("chkslider_visible").checked)?1:0;
		
		//Clean error
		$("#error-home").html("");
		if (filename=="")
			$("#error-home").html("Please attach the file first.");
		else if (name=="")
			$("#error-home").html("Please fill the slider name first.");
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fnslider_image").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-home").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-home").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePicture(file);
		}
    });


    var uploadFilePicture = function(file)
	{
		var filename=$("#fnslider_image").val();
		var name=$("#txtslider_name").val();
		var content=$("#txthtml_element").val();
		var visible=(document.getElementById("chkslider_visible").checked)?1:0;
		
		$("#td-percent").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_slider?name="+name+"&content="+content+"&visible="+visible, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-home").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent").html("100%");
				  
				  //Clean everything..
				  $("#error-home").html("");
				  $("#txtslider_name").val("");
				  $("#fnslider_image").val("");
				  document.getElementById("chkslider_visible").checked = true;
				  $("#txthtml_element").val("");
				  $("#td-percent").hide();

				  if (xhr.responseText!="0")
				  	$("#error-home").html("You can have maximum 4 images.");
				  else
				  {
				  	retrieve_home();
				  	$("#btcancel_home").trigger("click");
				  }
				} else {  
				  $("#error-home").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_home").html("Your browser doesnt support FileReader object");
		} 		
	}

	// Our Clients Section
	$(".btclient-up").click(function(e) 
	{
        var sliderID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_client_up?ID_customer="+sliderID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_client();
			}
		});
    });
	
	$(".btclient-down").click(function(e) 
	{
        var sliderID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_client_down?ID_customer="+sliderID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_client();
			}
		});
    });

	$(".btedit-client").click(function(e) {
		var id=$(this).attr("data-id");
		var item_name=$(this).attr("data-name");
		var visible=$(this).attr("data-visible");

		$("#btadd_client").hide();
		$("#client-scroll").hide();
		$("#edit_client").fadeIn("fast");
		
		//set data to component
		$("#hid_id_client").val(id);
		$("#txtedclient_name").val(item_name);
		$("#chkedclient_visible").prop("checked", (visible == "1"));
    });

	$(".btdelete-client").click(function(e) {
		var ID_customer=$(this).attr("data-id");
		var filename = $(this).attr("data-value");
		var conf=confirm("Are you sure want to delete this client item ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/delete_client?id="+ID_customer+"&filename=" + filename,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					alert("Client item removed successfully.");
					retrieve_client();
				}
			});
		}
	});

	$(".btchangepic-client").click(function(e)
	{
		var id=$(this).attr("data-id");
		var fnchpic=$(".fnchpic_client[data-id='"+id+"']");
		fnchpic.click();
	});

	$(".fnchpic_client").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePictureEditClient(file,id);
		}
	});

	$("#btadd_client").click(function(e) {
        $("#btadd_client").hide();
		$("#client-scroll").hide();
        $("#upload_client").fadeIn("fast");
    });
	
	$("#btcancel_client").click(function(e) {
        $("#btadd_client").fadeIn("fast");
		$("#client-scroll").fadeIn("fast");
        $("#upload_client").hide();
    });
	
	$("#btcanceled_client").click(function(e) {
        $("#btadd_client").fadeIn("fast");
		$("#client-scroll").fadeIn("fast");
        $("#edit_client").hide();
    });
	
	$("#btsaveed_client").click(function(e) 
	{
    	var id = $("#hid_id_client").val();
		var slider_name = $("#txtedclient_name").val();
		var visible = ($("#chkedclient_visible").prop("checked") == true) ? "1" : "0";
		
		if (slider_name=="")
			$("#error-edit-client").html("Please fill client name first."); 
		else
		{
			$.ajax({
				url:base_url+"home/edit_client",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&name="+slider_name+"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_client();
					//Close
					$("#btcanceled_client").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_client").click(function(e) 
	{
		var filename=$("#fnclient_image").val();
		var name=$("#txtclient_name").val();
		var visible=(document.getElementById("chkclient_visible").checked)?1:0;
		
		//Clean error
		$("#error-client").html("");
		if (filename=="")
			$("#error-client").html("Please attach the file first.");
		else if (name=="")
			$("#error-client").html("Please fill the client name first.");
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fnclient_image").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-client").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-client").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePictureClient(file);
		}
    });


    var uploadFilePictureClient = function(file)
	{
		var filename=$("#fnclient_image").val();
		var name=$("#txtclient_name").val();
		var visible=(document.getElementById("chkclient_visible").checked)?1:0;
		
		$("#td-percent").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_client?name="+name+"&visible="+visible, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-client").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent").html("100%");
				  
				  //Clean everything..
				  $("#error-client").html("");
				  $("#txtclient_name").val("");
				  $("#fnclient_image").val("");
				  document.getElementById("chkclient_visible").checked = true;
				  $("#td-percent").hide();

				  if (xhr.responseText!="0")
				  	$("#error-client").html("You can have maximum 4 images.");
				  else
				  {
				  	retrieve_client();
				  	$("#btcancel_client").trigger("click");
				  }
				} else {  
				  $("#error-client").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_client").html("Your browser doesnt support FileReader object");
		} 		
	}


	 // Why Us ?

	$(".btwhy-up").click(function(e) 
	{
        var whyID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_why_up?whyid="+whyID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_why();
			}
		});
    });
	
	$(".btwhy-down").click(function(e) 
	{
        var whyID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_why_down?whyid="+whyID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_why();
			}
		});
    });

    $(".btedit-why").click(function(e) {
		var id=$(this).attr("data-id");
		var pict=$(this).attr("data-pict");
		var title=$(this).attr("data-title");
		var side=$(this).attr("data-side");
		var text=$(this).attr("data-text");
		
		$("#btadd_why").hide();
		$("#why-scroll").hide();
		$("#edit_why").fadeIn("fast");
		
		//set data to component
		$("#hid_id_why").val(id);
		$("#cbedwhyIcon").val(pict);
		$("#iedwhyIcon").attr("class",pict);
		$("#cbedSide").val(side);
		$("#txtedwhy_title").val(title);
		$("#txtedwhy_text").val(text);
    });

    
    $(".btdelete-why").click(function(e) {
    	var id_why=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this points?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_why?id="+id_why,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					console.log(result);
					if (result=="")
					{
						alert("Point removed successfully.");
						retrieve_why();
					}
				}
			});
		}
    }); 

	$("#cbwhyIcon").change(function(e)
	{
		$("#iwhyIcon").attr("class", $("#cbwhyIcon").val());
	});

	$("#cbedwhyIcon").change(function(e)
	{
		$("#iedwhyIcon").attr("class", $("#cbedwhyIcon").val());
	});

	$("#btadd_why").click(function(e) {
        $("#btadd_why").hide();
		$("#why-scroll").hide();
        $("#upload_why").fadeIn("fast");
    });
	
	$("#btcancel_why").click(function(e) {
        $("#btadd_why").fadeIn("fast");
		$("#why-scroll").fadeIn("fast");
        $("#upload_why").hide();
    });
	
	$("#btcanceled_why").click(function(e) {
        $("#btadd_why").fadeIn("fast");
		$("#why-scroll").fadeIn("fast");
        $("#edit_why").hide();
    });
	
	$("#btsaveed_why").click(function(e) 
	{
    	var id = $("#hid_id_why").val();
		var pict = $("#cbedwhyIcon").val();
		var side = $("#cbedSide").val();
		var title = $("#txtedwhy_title").val();
		var text = $("#txtedwhy_text").val();
		
		if (pict=="")
			$("#error-edit-why").html("Please choose icon first."); 
		else if (title=="")
			$("#error-edit-why").html("Please fill title first.");
		else if (side=="")
			$("#error-edit-why").html("Please choose side first.");
		else if (text=="")
			$("#error-edit-why").html("Please fill why-text first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_why",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&pict="+pict+"&side="+ side + "&title="+title+"&text="+text,
				success: function(result)
				{
					retrieve_why();
					//Close
					$("#btcanceled_why").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_why").click(function(e) 
	{
		// var pict = $("#cbIcon").val();
		var side = $("#cbSide").val();
		var title = $("#txtwhy_title").val();
		var text = $("#txtwhy_text").val();;
		
		if (pict=="")
			$("#error-why").html("Please fill icon first."); 
		else if (side=="")
			$("#error-why").html("Please fill side first."); 
		else if (title=="")
			$("#error-why").html("Please fill title first.");
		else if (text=="")
			$("#error-why").html("Please fill why-text first.");
		else
		{
			// $.ajax({
			// 	url:base_url+"home/insert_why",
			// 	dataType:"html",
			// 	type:"POST",
			// 	data:"pict="+pict+"&side=" + side + "&title="+title+"&text="+text,
			// 	success: function(result)
			// 	{
			// 		//Clear Insert
			// 		$("#txtwhy_title").val("");
			// 		$("#cbIcon").val("icon-mobile");
			// 		$("#iIcon").attr("class","icon-mobile");
			// 		$("#cbside").val("L");
			// 		$("#txtwhy_text").val("");
										
			// 		//retrieve
			// 		retrieve_why();
					
			// 		//Close
			// 		$("#btcancel_why").trigger("click");
			// 	}
			// });
		}
    });

    // Pricing Plan Section

    $(".btplan-up").click(function(e) 
	{
        var planID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_plan_up?planid="+planID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_plan();
			}
		});
    });
	
	$(".btplan-down").click(function(e) 
	{
        var planID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_plan_down?planid="+planID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_plan();
			}
		});
    });

	$(".btedit-plan").click(function(e) {
		var id=$(this).attr("data-id");
		var plan_name=$(this).attr("data-name");
		var subtitle=$(this).attr("data-subtitle");
		var spec=$(this).attr("data-spec");
		var desc=$(this).attr("data-desc")
		
		$("#btadd_plan").hide();
		$("#plan-scroll").hide();
		$("#edit_plan").fadeIn("fast");
		
		//set data to component
		$("#hid_id_plan").val(id);
		$("#txtedplan_name").val(plan_name);
		$("#txtedproduct_subtitle").val(subtitle);
		$("#txtedproduct_spec").val(spec);
		$("#txtedproduct_desc").val(desc);
    });

	$(".btdelete-plan").click(function(e) {
		var ID_plan=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this pricing plan ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_plan?id="+ID_plan,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("Pricing plan removed successfully.");
						retrieve_plan();
					}
				}
			});
		}
	});

	$("#btadd_plan").click(function(e) {
        $("#btadd_plan").hide();
		$("#plan-scroll").hide();
        $("#upload_plan").fadeIn("fast");
    });
	
	$("#btcancel_plan").click(function(e) {
        $("#btadd_plan").fadeIn("fast");
		$("#plan-scroll").fadeIn("fast");
        $("#upload_plan").hide();
    });
	
	$("#btcanceled_plan").click(function(e) {
        $("#btadd_plan").fadeIn("fast");
		$("#plan-scroll").fadeIn("fast");
        $("#edit_plan").hide();
    });
	
	$("#btsaveed_plan").click(function(e) 
	{
    	var id = $("#hid_id_plan").val();
		var name = $("#txtedplan_name").val();
		var subtitle = $("#txtedproduct_subtitle").val();
		var spec = $("#txtedproduct_spec").val();
		var desc = $("#txtedproduct_desc").val();
		
		if (name=="")
			$("#error-edit-plan").html("Please fill plan name first."); 
		else if (subtitle=="")
			$("#error-edit-plan").html("Please fill product subtitle first.");
		else if (spec=="")
			$("#error-edit-plan").html("Please fill product spec title first.");
		else if (desc=="")
			$("#error-edit-plan").html("Please fill product spec description first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_plan",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&name="+name+"&subtitle="+subtitle+"&spec="+spec+"&desc=" + desc,
				success: function(result)
				{
					console.log(result);
					retrieve_plan();
					//Close
					$("#btcanceled_plan").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_plan").click(function(e) 
	{
		var name = $("#txtplan_name").val();
		var subtitle = $("#txtproduct_subtitle").val();
		var spec = $("#txtproduct_spec").val();
		var desc = $("#txtproduct_desc").val();
		
		if (name=="")
			$("#error-edit-plan").html("Please fill product name first."); 
		else if (subtitle=="")
			$("#error-edit-plan").html("Please fill subtitle first.");
		else if (spec=="")
			$("#error-edit-plan").html("Please fill spesification title first.");
		else if (desc=="")
			$("#error-edit-plan").html("Please fill spesification desc first.");
		else
		{
			// $.ajax({
			// 	url:base_url+"home/insert_plan",
			// 	dataType:"html",
			// 	type:"POST",
			// 	data:"name="+name+"&currency="+currency+"&price="+price+"&pereach=" + pereach,
			// 	success: function(result)
			// 	{
			// 		//Clear Insert
			// 		$("#txtplan_name").val("");
			// 		$("#txtcurrency").val("");
			// 		$("#txtplan_price").val("");
			// 		$("#txtper_each").val("");

			// 		//retrieve
			// 		retrieve_plan();
					
			// 		//Close
			// 		$("#btcancel_plan").trigger("click");
			// 	}
			// });

			var imageType = /image.*/;  
			var file = document.getElementById("fnplan_image").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-news").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-news").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePlan(file);
		}
    });

    $("body").on("click",".btchangepic-plan",function(e)
	{
		var id=$(this).attr("data-id");
		var fnchpic=$(".fnchplan[data-id='"+id+"']");
		fnchpic.click();
	});

	$("body").on("change",".fnchplan", function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePlanEdit(file,id);
		}
	});

	// Detail Item
	$("#btsave_detail").click(function(e) 
	{

		var id_plan = $("#hid_id_detail").val();
		var detail_text = $("#txtdetail_text").val();
		var detail_kat = $("#txtdetail_kategori").val();
		
		if (detail_text=="")
			$("#error-detail").html("Please fill detail text first.");
		else if (detail_kat=="")
		$("#error-detail").html("Please fill detail text first.");
		else
		{
			// $.ajax({
			// 	url:base_url+"home/insert_detail_plan",
			// 	dataType:"html",
			// 	type:"POST",
			// 	data:"id="+id_plan+"&detail_text="+detail_text,
			// 	success: function(result)
			// 	{
			// 		//Clear Insert
			// 		$("#txtdetail_text").val("");

			// 		//retrieve
			// 		retrieve_detail(id_plan);
			// 		retrieve_plan();
			// 	}
			// });

			var imageType = /image.*/;  
			var file = document.getElementById("fndetail_image").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-detail").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-detail").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileDetail(file);
		}
    });

	$("#btclear_detail").click(function(e) 
	{
		$("#txtdetail_text").val("");
    });

    // Portfolio Category

	$(".btcategory-up").click(function(e) 
	{
        var categoryID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_category_up?categoryid="+categoryID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_category();
			}
		});
    });
	
	$(".btcategory-down").click(function(e) 
	{
        var categoryID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_category_down?categoryid="+categoryID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_category();
			}
		});
    });

    $(".btedit-category").click(function(e) {
		var id=$(this).attr("data-id");
		var code=$(this).attr("data-code");
		var name=$(this).attr("data-name");
		var visible = $(this).attr("data-visible");
		
		$("#btadd_category").hide();
		$("#category-scroll").hide();
		$("#edit_category").fadeIn("fast");
		
		//set data to component
		$("#hid_id_category").val(id);
		$("#txtedcategory_code").val(code);
		$("#txtedcategory_name").val(name);
		$("#chkedVisible").prop("checked",visible == "1");
    });

    
    $(".btdelete-category").click(function(e) {
    	var id_category=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this category?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_category?id="+id_category,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					console.log(result);
					if (result=="")
					{
						alert("Category removed successfully.");
						retrieve_category();
					}
				}
			});
		}
    }); 

	$("#btadd_category").click(function(e) {
        $("#btadd_category").hide();
		$("#category-scroll").hide();
        $("#upload_category").fadeIn("fast");
    });
	
	$("#btcancel_category").click(function(e) {
        $("#btadd_category").fadeIn("fast");
		$("#category-scroll").fadeIn("fast");
        $("#upload_category").hide();
    });
	
	$("#btcanceled_category").click(function(e) {
        $("#btadd_category").fadeIn("fast");
		$("#category-scroll").fadeIn("fast");
        $("#edit_category").hide();
    });
	
	$("#btsaveed_category").click(function(e) 
	{
    	var id = $("#hid_id_category").val();
		var code = $("#txtedcategory_code").val();
		var name = $("#txtedcategory_name").val();
		var visible = ($("#chkedVisible").prop("checked") == true) ? "1" : "0";
		
		if (code=="")
			$("#error-edit-category").html("Please fill category code first."); 
		else if (name=="")
			$("#error-edit-category").html("Please fill category name first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_category",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&code="+code+"&name="+ name + "&is_active="+visible,
				success: function(result)
				{
					retrieve_category();
					//Close
					$("#btcanceled_category").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_category").click(function(e) 
	{
		var code = $("#txtcategory_code").val();
		var name = $("#txtcategory_name").val();
		var visible = ($("#chkVisible").prop("checked") == true) ? "1" : "0";
		
		if (code=="")
			$("#error-edit-category").html("Please fill category code first."); 
		else if (name=="")
			$("#error-edit-category").html("Please fill category name first.");
		else
		{
			$.ajax({
				url:base_url+"home/insert_category",
				dataType:"html",
				type:"POST",
				data:"code="+code+"&name=" + name + "&is_active="+visible,
				success: function(result)
				{
					//Clear Insert
					$("#txtcategory_code").val("");
					$("#txtcategory_name").val("");
					$("#chkVisible").prop("checked", true);
										
					//retrieve
					retrieve_category();
					
					//Close
					$("#btcancel_category").trigger("click");
				}
			});
		}
	});
	
	// Blog Category

	$(".btblogcategory-up").click(function(e) 
	{
        var blogcategoryID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_blogcategory_up?blogcategoryid="+blogcategoryID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_blogcategory();
			}
		});
    });
	
	$(".btblogcategory-down").click(function(e) 
	{
        var blogcategoryID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_blogcategory_down?blogcategoryid="+blogcategoryID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_blogcategory();
			}
		});
    });

    $(".btedit-blogcategory").click(function(e) {
		var id=$(this).attr("data-id");
		var code=$(this).attr("data-code");
		var name=$(this).attr("data-name");
		var visible = $(this).attr("data-visible");
		
		$("#btadd_blogcategory").hide();
		$("#blogcategory-scroll").hide();
		$("#edit_blogcategory").fadeIn("fast");
		
		//set data to component
		$("#hid_id_blogcategory").val(id);
		$("#txtedblogcategory_code").val(code);
		$("#txtedblogcategory_name").val(name);
		$("#chkedblogcatVisible").prop("checked",visible == "1");
    });

    $(".btdelete-blogcategory").click(function(e) {
    	var id_blogcategory=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this category?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_blogcategory?id="+id_blogcategory,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					console.log(result);
					if (result=="")
					{
						alert("Category removed successfully.");
						retrieve_blogcategory();
					}
				}
			});
		}
    }); 

	$("#btadd_blogcategory").click(function(e) {
        $("#btadd_blogcategory").hide();
		$("#blogcategory-scroll").hide();
        $("#upload_blogcategory").fadeIn("fast");
    });
	
	$("#btcancel_blogcategory").click(function(e) {
        $("#btadd_blogcategory").fadeIn("fast");
		$("#blogcategory-scroll").fadeIn("fast");
        $("#upload_blogcategory").hide();
    });
	
	$("#btcanceled_blogcategory").click(function(e) {
        $("#btadd_blogcategory").fadeIn("fast");
		$("#blogcategory-scroll").fadeIn("fast");
        $("#edit_blogcategory").hide();
    });
	
	$("#btsaveed_blogcategory").click(function(e) 
	{
    	var id = $("#hid_id_blogcategory").val();
		var code = $("#txtedblogcategory_code").val();
		var name = $("#txtedblogcategory_name").val();
		var visible = ($("#chkedblogcatVisible").prop("checked") == true) ? "1" : "0";
		
		if (code=="")
			$("#error-edit-blogcategory").html("Please fill category code first."); 
		else if (name=="")
			$("#error-edit-blogcategory").html("Please fill category name first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_blogcategory",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&code="+code+"&name="+ name + "&is_active="+visible,
				success: function(result)
				{
					retrieve_blogcategory();
					//Close
					$("#btcanceled_blogcategory").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_blogcategory").click(function(e) 
	{
		var code = $("#txtblogcategory_code").val();
		var name = $("#txtblogcategory_name").val();
		var visible = ($("#chkblogVisible").prop("checked") == true) ? "1" : "0";
		
		if (code=="")
			$("#error-edit-blogcategory").html("Please fill category code first."); 
		else if (name=="")
			$("#error-edit-blogcategory").html("Please fill category name first.");
		else
		{
			$.ajax({
				url:base_url+"home/insert_blogcategory",
				dataType:"html",
				type:"POST",
				data:"code="+code+"&name=" + name + "&is_active="+visible,
				success: function(result)
				{
					//Clear Insert
					$("#txtblogcategory_code").val("");
					$("#txtblogcategory_name").val("");
					$("#chkblogVisible").prop("checked", true);
										
					//retrieve
					retrieve_blogcategory();
					
					//Close
					$("#btcancel_blogcategory").trigger("click");
				}
			});
		}
	});
	
	// Blog Tags

	$(".bttags-up").click(function(e) 
	{
        var tagsID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_tags_up?tagsid="+tagsID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_tags();
			}
		});
    });
	
	$(".bttags-down").click(function(e) 
	{
        var tagsID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_tags_down?tags="+tagsID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_tags();
			}
		});
    });

    $(".btedit-tags").click(function(e) {
		var id=$(this).attr("data-id");
		var code=$(this).attr("data-code");
		var name=$(this).attr("data-name");
		var visible = $(this).attr("data-visible");
		
		$("#btadd_tags").hide();
		$("#tags-scroll").hide();
		$("#edit_tags").fadeIn("fast");
		
		//set data to component
		$("#hid_id_tags").val(id);
		$("#txtedtags_code").val(code);
		$("#txtedtags_name").val(name);
		$("#chkedtagsVisible").prop("checked",visible == "1");
    });

    $(".btdelete-tags").click(function(e) {
    	var id_tags=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this category?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_tags?id="+id_tags,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					console.log(result);
					if (result=="")
					{
						alert("Category removed successfully.");
						retrieve_tags();
					}
				}
			});
		}
    }); 

	$("#btadd_tags").click(function(e) {
        $("#btadd_tags").hide();
		$("#tags-scroll").hide();
        $("#upload_tags").fadeIn("fast");
    });
	
	$("#btcancel_tags").click(function(e) {
        $("#btadd_tags").fadeIn("fast");
		$("#tags-scroll").fadeIn("fast");
        $("#upload_tags").hide();
    });
	
	$("#btcanceled_tags").click(function(e) {
        $("#btadd_tags").fadeIn("fast");
		$("#tags-scroll").fadeIn("fast");
        $("#edit_tags").hide();
    });
	
	$("#btsaveed_tags").click(function(e) 
	{
    	var id = $("#hid_id_tags").val();
		var code = $("#txtedtags_code").val();
		var name = $("#txtedtags_name").val();
		var visible = ($("#chkedtagsVisible").prop("checked") == true) ? "1" : "0";
		
		if (code=="")
			$("#error-edit-tags").html("Please fill tags code first."); 
		else if (name=="")
			$("#error-edit-tags").html("Please fill tags name first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_blogtags",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&code="+code+"&name="+ name + "&is_active="+visible,
				success: function(result)
				{
					retrieve_tags();
					//Close
					$("#btcanceled_tags").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_tags").click(function(e) 
	{
		var code = $("#txttags_code").val();
		var name = $("#txttags_name").val();
		var visible = ($("#chktagsVisible").prop("checked") == true) ? "1" : "0";
		
		if (code=="")
			$("#error-edit-tags").html("Please fill tags code first."); 
		else if (name=="")
			$("#error-edit-tags").html("Please fill tags name first.");
		else
		{
			$.ajax({
				url:base_url+"home/insert_blogtags",
				dataType:"html",
				type:"POST",
				data:"code="+code+"&name=" + name + "&is_active="+visible,
				success: function(result)
				{
					//Clear Insert
					$("#txttags_code").val("");
					$("#txttags_name").val("");
					$("#chktagsVisible").prop("checked", true);
										
					//retrieve
					retrieve_tags();
					
					//Close
					$("#btcancel_tags").trigger("click");
				}
			});
		}
    });


	// Blog Section

	$(".btedit-news").click(function(e) {
		var id=$(this).attr("data-id");
		var title=$(this).attr("data-title");
		var link=$(this).attr("data-link");
		var creator=$(this).attr("data-creator");
		var category=$(this).attr("data-category");
		var visible=$(this).attr("data-visible");
		var blogText = $(this).attr("data-text");

		$("#btadd_news").hide();
		$("#news-scroll").hide();
		$("#edit_news").fadeIn("fast");
		
		//set data to component
		$("#hid_id_news").val(id);
		$("#txtedtitle").val(title);
		$("#txtedlink").val(link);
		$("#cbedblogCategory").val(category);
		$("#txtedcreator").val(creator);
		CKEDITOR.instances.txtblogText.setData(blogText);
		$("#chkedblogVisible").prop("checked", (visible == "1"));
    });

	$(".btdelete-news").click(function(e) {
		var ID_blog=$(this).attr("data-id");
		var filename = $(this).attr("data-value");
		var conf=confirm("Are you sure want to delete this news ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/delete_news?id="+ID_blog+"&filename=" + filename,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("News removed successfully.");
						retrieve_news();
					}
				}
			});
		}
	});

	$(".btchangepic-test").click(function(e)
	{
		var id=$(this).attr("data-id");
		var fnchpic=$(".fnchtest[data-id='"+id+"']");
		fnchpic.click();
	});

	$(".fnchtest").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileTestimoniEdit(file,id);
		}
	});

	$(".btchangepic-news").click(function(e)
	{
		var id=$(this).attr("data-id");
		var fnchpic=$(".fnchimg[data-id='"+id+"']");
		fnchpic.click();
	});

	$(".fnchimg").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileImageEdit(file,id);
		}
	});

	$("#btadd_news").click(function(e) {
        $("#btadd_news").hide();
		$("#news-scroll").hide();
        $("#upload_news").fadeIn("fast");
        // CKEDITOR.instances.txtblogText.setData("");
    });
	
	$("#btcancel_news").click(function(e) {
        $("#btadd_news").fadeIn("fast");
		$("#news-scroll").fadeIn("fast");
        $("#upload_news").hide();
    });
	
	$("#btcanceled_news").click(function(e) {
        $("#btadd_news").fadeIn("fast");
		$("#news-scroll").fadeIn("fast");
        $("#edit_news").hide();
    });
	
	$("#btsaveed_news").click(function(e) 
	{
    	var id = $("#hid_id_news").val();
		var title = $("#txtedtitle").val();
		var link = $("#txtedlink").val();
		var category = $("#cbedblogCategory").val();
		var creator = $("#txtedcreator").val();
		var visible = ($("#chkedblogVisible").prop("checked") == true) ? "1" : "0";
		var blogText = CKEDITOR.instances.txtblogText.getData();
		
		if (title=="")
			$("#error-edit-news").html("Please fill news title first."); 
		else if (creator == "") 
			$("#error-edit-news").html("Please fill news creator first."); 
		else
		{
			$.ajax({
				url:base_url+"home/edit_news",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&title="+title+"&link="+link+"&category="+category+"&creator="+creator+"&visible="+visible+"&blogText=" + escape(blogText),
				success: function(result)
				{
					console.log(result);
					retrieve_news();
					//Close
					$("#btcanceled_news").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_news").click(function(e) 
	{
		var filename=$("#fnnews_image").val();
		var title = $("#txttitle").val();
		var link = $("#txtlink").val();
		var category = $("#cbblogCategory").val();
		var creator = $("#txtcreator").val();
		var visible = ($("#chkblogVisible").prop("checked") == true) ? "1" : "0";
		var blogText = CKEDITOR.instances.txtblogText.getData();

		//Clean error
		$("#error-news").html("");
		if (filename=="")
			$("#error-news").html("Please attach the picture first.");
		else if (title=="")
			$("#error-news").html("Please fill news title first."); 
		else if (category=="")
			$("#error-news").html("Please fill news category first.");
		else if (creator == "") 
			$("#error-news").html("Please fill news creator first."); 
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fnnews_image").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-news").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-news").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileImage(file);
		}
    });


    var uploadFileImage = function(file)
	{
		var filename=$("#fnnews_image").val();
		var title = $("#txttitle").val();
		var link = $("#txtlink").val();
		var category = $("#cbblogCategory").val();
		var creator = $("#txtcreator").val();
		var visible = ($("#chkblogVisible").prop("checked") == true) ? "1" : "0";
		var blogText = CKEDITOR.instances.txtblogText.getData();
		
		$("#td-percent-news").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_blog?title="+title+"&link="+link+"&category="+category+"&creator="+creator+"&visible="+visible+"&blogText="+escape(blogText), true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent-news").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-news").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent-news").html("100%");
				  
				  //Clean everything..
				  $("#error-news").html("");
				  $("#txttitle").val("");
				  $("#txtlink").val("");
				  $("#cbblogCategory").val("");
				  $("#txtcreator").val("");
				  $("#fnnews_image").val("");
				  clearAll();
				  document.getElementById("chkblogVisible").checked = true;
				  $("#txthtml_element").val("");
				  $("#td-percent-news").hide();

			  	  retrieve_news();
			  	  $("#btcancel_news").trigger("click");
				} else {  
				  $("#error-news").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_news").html("Your browser doesnt support FileReader object");
		} 		
	}


	// Our Team Section

	$(".btteam-up").click(function(e) 
	{
        var teamID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_team_up?ID_team="+teamID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_team();
			}
		});
    });
	
	$(".btteam-down").click(function(e) 
	{
        var teamID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_team_down?ID_team="+teamID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_team();
			}
		});
    });

	$(".btedit-team").click(function(e) {
		var id=$(this).attr("data-id");
		var name=$(this).attr("data-name");
		var position=$(this).attr("data-position");
		var fb_link = $(this).attr("data-fb");
		var twitter_link = $(this).attr("data-twitter");
		var linkedin_link = $(this).attr("data-linkedin");

		$("#btadd_team").hide();
		$("#team-scroll").hide();
		$("#edit_team").fadeIn("fast");
		
		//set data to component
		$("#hid_id_team").val(id);
		$("#txtedteam_name").val(name);
		$("#txtedposition").val(position);
		$("#txtedfb_link").val(fb_link);
		$("#txtedtwitter_link").val(twitter_link);
		$("#txtedlinkedin_link").val(linkedin_link);
    });

	$(".btdelete-team").click(function(e) {
		var ID_team = $(this).attr("data-id");
		var filename = $(this).attr("data-value");
		var conf=confirm("Are you sure want to delete this member ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/delete_team?id="+ID_team+"&filename=" + filename,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("Team member removed successfully.");
						retrieve_team();
					}
				}
			});
		}
	});

	$(".btchangepic-team").click(function(e)
	{
		var id=$(this).attr("data-id");
		var fnchpic=$(".fnchphoto[data-id='"+id+"']");
		fnchpic.click();
	});

	$(".fnchphoto").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePhotoEdit(file,id);
		}
	});

	$("#btadd_team").click(function(e) {
        $("#btadd_team").hide();
		$("#team-scroll").hide();
        $("#upload_team").fadeIn("fast");
    });
	
	$("#btcancel_team").click(function(e) {
        $("#btadd_team").fadeIn("fast");
		$("#team-scroll").fadeIn("fast");
        $("#upload_team").hide();
    });
	
	$("#btcanceled_team").click(function(e) {
        $("#btadd_team").fadeIn("fast");
		$("#team-scroll").fadeIn("fast");
        $("#edit_team").hide();
    });
	
	$("#btsaveed_team").click(function(e) 
	{
    	var id = $("#hid_id_team").val();
		var name = $("#txtedteam_name").val();
		var position = $("#txtedposition").val();
		var fb = $("#txtedfb_link").val();
		var twitter = $("#txtedtwitter_link").val();
		var linkedin = $("#txtedlinkedin_link").val();
		
		if (name=="")
			$("#error-edit-team").html("Please fill member name first."); 
		else if (position == "") 
			$("#error-edit-team").html("Please fill position name first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_team",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&name="+name+"&position="+position+"&fb="+fb+"&twitter="+twitter+"&linkedin="+linkedin,
				success: function(result)
				{
					console.log(result);
					retrieve_team();
					//Close
					$("#btcanceled_team").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_team").click(function(e) 
	{
		var filename=$("#fnteam_photo").val();
		var name = $("#txtteam_name").val();
		var position = $("#txtposition").val();
		var fb = $("#txtfb_link").val();
		var twitter = $("#txttwitter_link").val();
		var linkedin = $("#txtlinkedin_link").val();
		
		//Clean error
		$("#error-team").html("");
		if (name=="")
			$("#error-team").html("Please fill member name first."); 
		else if (position == "") 
			$("#error-team").html("Please fill position name first.");
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fnteam_photo").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-team").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-team").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePhoto(file);
		}
    });


    var uploadFilePhoto = function(file)
	{
		var filename=$("#fnteam_photo").val();
		var name = $("#txtteam_name").val();
		var position = $("#txtposition").val();
		var fb = $("#txtfb_link").val();
		var twitter = $("#txttwitter_link").val();
		var linkedin = $("#txtlinkedin_link").val();
		
		$("#td-percent-team").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_team?name="+name+"&position="+position+"&fb="+fb+"&twitter="+twitter+"&linkedin="+linkedin, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent-team").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-team").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent-team").html("100%");
				  
				  //Clean everything..
				  $("#error-team").html("");
				  $("#txtteam_name").val("");
				  $("#txtposition").val("");
				  $("#txtfb_link").val("");
				  $("#txttwitter_link").val("");
				  $("#txtlinkedin_link").val("");
				  $("#td-percent-team").hide();

			  	  retrieve_team();
			  	  $("#btcancel_team").trigger("click");
				} else {  
				  $("#error-team").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_team").html("Your browser doesnt support FileReader object");
		} 		
	}


	// Our Portfolio Section
	$(".btndetail-portfolio").click(function(e){
		var id = $(this).attr("data-id");
		tempImageID = id;
		collapse_all();
		retrieve_detail_portfolio(id);
		$("#manage-detail-portfolio-box").show();
        $("#manage-detail-portfolio-box").animate({left:'0'},140);
	});

    $("#close-detail-portfolio").click(function(e)
    {
    	$("#manage-detail-portfolio-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-detail-portfolio-box").hide();},140);
    });

	$(".btportfolio-up").click(function(e) 
	{
        var imageID=$(this).attr("data-id");
		var index=$(this).attr("data-value");

		$.ajax({
			url:base_url+"home/move_img_up?imageid="+imageID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_portfolio();
			}
		});
    });
	
	$(".btportfolio-down").click(function(e) 
	{
        var imageID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_img_down?imageid="+imageID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_portfolio();
			}
		});
    });

	$(".btedit-portfolio").click(function(e) {
		flagAdd = false;
		flagDetail = false;
		var id=$(this).attr("data-id");
		var title=$(this).attr("data-title");
		var category=$(this).attr("data-category");
		var desc = $(this).attr("data-desc");
		var visible = $(this).attr("data-visible");

		$("#btadd_portfolio").hide();
		$("#portfolio-scroll").hide();
		$("#edit_portfolio").fadeIn("fast");
		
		//set data to component
		$("#hid_id_portfolio").val(id);
		$("#txtedimgtitle").val(title);
		$("#cbedImgCategory").val(category);
		$("#txtedimgdesc").val(desc);
		$("#chkedImgVisible").prop("checked", (visible == "1"));
    });

	$(".btdelete-portfolio").click(function(e) {
		var ID_portfolio = $(this).attr("data-id");
		var filename = $(this).attr("data-value");
		var conf=confirm("Are you sure want to delete this portfolio ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/delete_portfolio?id="+ID_portfolio+"&filename=" + filename,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
						alert("Portfolio removed successfully.");
						retrieve_portfolio();
				}
			});
		}
	});

	$(".btchangepic-portfolio").click(function(e)
	{
		flagAdd = false;
		var id=$(this).attr("data-id");
		var fnchpic=$(".fnchport[data-id='"+id+"']");
		fnchpic.click();
	});

	$(".fnchport").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePortEdit(file,id);
		}
	});

	$("#btadd_portfolio").click(function(e) {
		flagAdd = true;
		flagDetail = false;
        $("#btadd_portfolio").hide();
		$("#portfolio-scroll").hide();
        $("#upload_portfolio").fadeIn("fast");
    });
	
	$("#btcancel_portfolio").click(function(e) {
        $("#btadd_portfolio").fadeIn("fast");
		$("#portfolio-scroll").fadeIn("fast");
        $("#upload_portfolio").hide();
    });
	
	$("#btcanceled_portfolio").click(function(e) {
        $("#btadd_portfolio").fadeIn("fast");
		$("#portfolio-scroll").fadeIn("fast");
        $("#edit_portfolio").hide();
    });
	
	$("#btsaveed_portfolio").click(function(e) 
	{
    	var id = $("#hid_id_portfolio").val();
		var title=$("#txtedimgtitle").val();
		var category=$("#cbedImgCategory").val();
		var desc = $("#txtedimgdesc").val();
		var visible = ($("#chkedImgVisible").prop("checked") == true) ? 1 : 0;
		
		if (title=="")
			$("#error-edit-portfolio").html("Please fill title first."); 
		else if (category == "") 
			$("#error-edit-portfolio").html("Please fill category first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_portfolio",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&title="+title+"&category="+category+"&desc="+desc+"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_portfolio();
					//Close
					$("#btcanceled_portfolio").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_portfolio").click(function(e) 
	{
		var filename=$("#fnimg_photo").val();
		var title=$("#txtimgtitle").val();
		var category=$("#cbImgCategory").val();
		var desc = $("#txtimgdesc").val();
		var visible = ($("#chkImgVisible").prop("checked") == true) ? 1 : 0;
		
		//Clean error
		$("#error-portfolio").html("");
		if (title=="")
			$("#error-portfolio").html("Please fill title first."); 
		else if (category == "") 
			$("#error-portfolio").html("Please fill category first.");
		else if (filename == "")
			$("#error-portfolio").html("Please attach photo first.");
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fnimg_photo").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-portfolio").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-portfolio").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePort(file);
		}
    });

	function addCrop() { 
		var title=$("#txtimgtitle").val();
		var category=$("#cbImgCategory").val();
		var desc = $("#txtimgdesc").val();
		var visible = ($("#chkImgVisible").prop("checked") == true) ? 1 : 0;

		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_img?title="+title+"&category="+category+"&desc="+desc+"&visible="+visible, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent-portfolio").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-portfolio").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent-portfolio").html("100%");
				  
				  // Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`
				 cropper.getCroppedCanvas({ width: 780, height: 508 }).toBlob(function (blob) {
				 var formData = new FormData();

				 formData.append('croppedImage', blob);
				 // Use `jQuery.ajax` method
				 $.ajax(base_url + 'home/update_thumb_image?name=' + xhr.responseText, {
					    method: "POST",
					    data: formData,
					    processData: false,
					    contentType: false,
					    success: function (result) {
					      	console.log('Upload success');

					      	//Clean everything..
						  	$("#error-portfolio").html("");
						  	$("#txtimgtitle").val("");
						  	$("#fnimg_photo").val("");
						  	$("#cbImgCategory option:nth(0)").prop("selected", true);
						  	$("#txtimgdesc").val("");
						  	$("#chkImgVisible").prop("checked", true);
						  	$("#td-percent-portfolio").hide();

					      	retrieve_portfolio();
					      	$("#cropping").modal("hide");
					    },
					    error: function (result) {
					      console.log('Upload error');
					    }
					  });
					});

				  
			  	  $("#btcancel_portfolio").trigger("click");
				} else {  
				  $("#error-portfolio").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", tempFile.fileName);
			xhr.setRequestHeader("X-File-Size", tempFile.fileSize);
			xhr.setRequestHeader("X-File-Type", tempFile.type);
			
			// Send the file (doh)
			xhr.send(tempFile);
	}

	function editCrop() {
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/update_image?id="+tempID, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

			}
			else {
				alert("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  

				 // Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`
				 cropper.getCroppedCanvas({ width: 780, height: 508 }).toBlob(function (blob) {
				 var formData = new FormData();

				 formData.append('croppedImage', blob);
				 // Use `jQuery.ajax` method
				 $.ajax(base_url + 'home/update_thumb_image?name=' + xhr.responseText, {
					    method: "POST",
					    data: formData,
					    processData: false,
					    contentType: false,
					    success: function (result) {
					      	console.log('Upload success');
					      	retrieve_portfolio();
					      	$("#cropping").modal("hide");
					    },
					    error: function (result) {
					      console.log('Upload error');
					    }
					  });
					});

			  alert("Changes saved successfully.");
			  
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", tempFile.fileName);
		xhr.setRequestHeader("X-File-Size", tempFile.fileSize);
		xhr.setRequestHeader("X-File-Type", tempFile.type);
		
		// Send the file (doh)
		xhr.send(tempFile);
	}

	$("#btnCrop").click(function(e) {
		if (flagDetail == false) {
			if (flagAdd == true) 
				addCrop();
			else
				editCrop();
		} else {
			if (flagAdd == true) 
				addCropDetail();
			else
				editCropDetail();
		}
		alert("Data saved successfully.");
	});

    var uploadFilePort = function(file)
	{
		var filename=$("#fnimg_photo").val();
		var title=$("#txtimgtitle").val();
		var category=$("#cbImgCategory").val();
		var desc = $("#txtimgdesc").val();
		var visible = ($("#chkImgVisible").prop("checked") == true) ? 1 : 0;
		tempFile = file;

		$("#td-percent-portfolio").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
			//alert("uploading "+file.name);  
			reader = new FileReader();
			reader.onload = function(e){
				document.getElementById("imgCrop").src = reader.result;
				blobURL = URL.createObjectURL(file);
	          	$("#cropping").modal("show");
			}
			reader.readAsDataURL(file);
		} else{
			$("#error_portfolio").html("Your browser doesnt support FileReader object");
		}
	}


	// Manage Detail Portfolio

	$("#btadd_detail_portfolio").click(function(e) {
		flagDetail = true;
		flagAdd = true;
        $("#btadd_detail_portfolio").hide();
		$("#detail-portfolio-scroll").hide();
        $("#upload_detail_portfolio").fadeIn("fast");
    });
	
	$("#btcancel_detail_portfolio").click(function(e) {
        $("#btadd_detail_portfolio").fadeIn("fast");
		$("#detail-portfolio-scroll").fadeIn("fast");
        $("#upload_detail_portfolio").hide();
    });
	
	$("#btcanceled_detail_portfolio").click(function(e) {
        $("#btadd_detail_portfolio").fadeIn("fast");
		$("#detail-portfolio-scroll").fadeIn("fast");
        $("#edit_detail_portfolio").hide();
    });
	
	$("#btsaveed_detail_portfolio").click(function(e) 
	{
    	var id = $("#hid_id_detail_portfolio").val();
		var title=$("#txteddetimgtitle").val();
		var image=$("#hid_id_image_port").val();
		var desc = $("#txteddetimgdesc").val();
		var visible = ($("#chkeddetImgVisible").prop("checked") == true) ? 1 : 0;
		
		if (title=="")
			$("#error-edit-detail-portfolio").html("Please fill title first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_detail_portfolio",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&title="+title+"&image="+image+"&desc="+desc+"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_detail_portfolio(image);
					//Close
					$("#btcanceled_detail_portfolio").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_detail_portfolio").click(function(e) 
	{
		var filename=$("#fndetimg_photo").val();
		var title=$("#txtdetimgtitle").val();
		var image=tempImageID;
		var desc = $("#txtdetimgdesc").val();
		var visible = ($("#chkDetImgVisible").prop("checked") == true) ? 1 : 0;
		
		//Clean error
		$("#error-detail-portfolio").html("");
		if (title=="")
			$("#error-detail-portfolio").html("Please fill title first.");
		else if (filename == "")
			$("#error-detail-portfolio").html("Please attach photo first.");
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fndetimg_photo").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-detail-portfolio").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-detail-portfolio").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileDetPort(file);
		}
    });

	var uploadFileDetPort = function(file)
	{
		var filename=$("#fndetimg_photo").val();
		var title=$("#txtdetimgtitle").val();
		var image = tempImageID;
		var desc = $("#txtdetimgdesc").val();
		var visible = ($("#chkDetImgVisible").prop("checked") == true) ? 1 : 0;
		tempFile = file;

		$("#td-percent-detail-portfolio").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
			//alert("uploading "+file.name);  
			reader = new FileReader();
			reader.onload = function(e){
				document.getElementById("imgCrop").src = reader.result;
				blobURL = URL.createObjectURL(file);
	          	$("#cropping").modal("show");
			}
			reader.readAsDataURL(file);
		} else{
			$("#error_detail_portfolio").html("Your browser doesnt support FileReader object");
		}
	}

	function addCropDetail() { 
		var title=$("#txtdetimgtitle").val();
		var image = tempImageID;
		var desc = $("#txtdetimgdesc").val();
		var visible = ($("#chkDetImgVisible").prop("checked") == true) ? 1 : 0;

		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_detail_img?title="+title+"&image="+image+"&desc="+desc+"&visible="+visible, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent-detail-portfolio").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-detail-portfolio").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#percent-detail-portfolio").html("100%");

				  // Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`
				 cropper.getCroppedCanvas({ width: 780, height: 540 }).toBlob(function (blob) {
				 var formData = new FormData();

				 formData.append('croppedImage', blob);
				 // Use `jQuery.ajax` method
				 $.ajax(base_url + 'home/update_thumb_image?name=' + xhr.responseText, {
					    method: "POST",
					    data: formData,
					    processData: false,
					    contentType: false,
					    success: function (result) {
					      	console.log('Upload success');
					      	//Clean everything..
						  	$("#error-detail-portfolio").html("");
						  	$("#txtdetimgtitle").val("");
						  	$("#fndetimg_photo").val("");
						  	$("#txtdetimgdesc").val("");
						  	$("#chkDetImgVisible").prop("checked", true);
						  	$("#td-percent-detail-portfolio").hide();

					      	retrieve_detail_portfolio(tempImageID);
					      	$("#cropping").modal("hide");
					    },
					    error: function (result) {
					      console.log('Upload error');
					    }
					  });
					});

				  
			  	  $("#btcancel_detail_portfolio").trigger("click");
				} else {  
				  $("#error-detail-portfolio").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", tempFile.fileName);
			xhr.setRequestHeader("X-File-Size", tempFile.fileSize);
			xhr.setRequestHeader("X-File-Type", tempFile.type);
			
			// Send the file (doh)
			xhr.send(tempFile);
	}

	function editCropDetail() {
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/update_detail_image?id="+tempID, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

			}
			else {
				alert("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  

				 // Upload cropped image to server if the browser supports `HTMLCanvasElement.toBlob`
				 cropper.getCroppedCanvas({ width: 780, height: 540 }).toBlob(function (blob) {
				 var formData = new FormData();

				 formData.append('croppedImage', blob);
				 // Use `jQuery.ajax` method
				 $.ajax(base_url + 'home/update_thumb_image?name=' + xhr.responseText, {
					    method: "POST",
					    data: formData,
					    processData: false,
					    contentType: false,
					    success: function (result) {
					      	console.log('Upload success');
					      	retrieve_detail_portfolio(tempImageID);
					      	$("#cropping").modal("hide");
					    },
					    error: function (result) {
					      console.log('Upload error');
					    }
					  });
					});

			  alert("Changes saved successfully.");
			  
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", tempFile.fileName);
		xhr.setRequestHeader("X-File-Size", tempFile.fileSize);
		xhr.setRequestHeader("X-File-Type", tempFile.type);
		
		// Send the file (doh)
		xhr.send(tempFile);
	}



	// Slider Home1 Section

	$(".bthome1-up").click(function(e) 
	{
		var sliderID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_slider_up?ID_slider="+sliderID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_slider();
			}
		});
	});
	
	$(".bthome1-down").click(function(e) 
	{
		var sliderID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_slider_down?ID_slider="+sliderID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_slider();
			}
		});
	});

	$(".btedit-home1").click(function(e) {
		var id=$(this).attr("data-id");
		var item_name=$(this).attr("data-name");
		var content=$(this).attr("data-html");
		var visible=$(this).attr("data-visible");
		var link=$(this).attr("data-link");

		$("#btadd_home1").hide();
		$("#home1-scroll").hide();
		$("#edit_home1").fadeIn("fast");
		
		//set data to component
		$("#hid_id_home1").val(id);
		$("#txtedsliderservice_name").val(item_name);
		$("#txtedhtmlservice_element").val(content);
		$("#txtedslider_url").val(link);
		$("#chkedsliderservice_visible").prop("checked", (visible == "1"));
	});

	$(".btdelete-home1").click(function(e) {
		var ID_slider=$(this).attr("data-id");
		var filename = $(this).attr("data-value");
		var conf=confirm("Are you sure want to delete this slider item ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/delete_slider_service?id="+ID_slider+"&filename=" + filename,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("Slider item removed successfully.");
						retrieve_slider();
					}
				}
			});
		}
	});

	$(".btchangepic-home1").click(function(e)
	{
		var id=$(this).attr("data-id");
		var fnchpic1=$(".fnchpic1[data-id='"+id+"']");
		fnchpic1.click();
	});

	$(".fnchpic1").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
				alert("File \""+file.name+"\" is not a valid image file.");
				return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
				alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
				return false;	
			} 
			var size=file.size/1024;
			uploadFileSliderEdit(file,id);
		}
	});

	$("#btadd_home1").click(function(e) {
		$("#btadd_home1").hide();
		$("#home1-scroll").hide();
		$("#upload_home1").fadeIn("fast");
	});
	
	$("#btcancel_home1").click(function(e) {
		$("#btadd_home1").fadeIn("fast");
		$("#home1-scroll").fadeIn("fast");
		$("#upload_home1").hide();
	});
	
	$("#btcanceled_home1").click(function(e) {
		$("#btadd_home1").fadeIn("fast");
		$("#home1-scroll").fadeIn("fast");
		$("#edit_home1").hide();
	});
	
	$("#btsaveed_home1").click(function(e) 
	{
		var id = $("#hid_id_home1").val();
		var slider_name = $("#txtedsliderservice_name").val();
		var content = $("#txtedhtmlservice_element").val();
		var link = $("#txtedslider_url").val();
		var visible = ($("#chkedsliderservice_visible").prop("checked") == true) ? "1" : "0";
		
		if (slider_name=="")
			$("#error-edit-home1").html("Please fill slider name first."); 
		else
		{
			$.ajax({
				url:base_url+"home/edit_slider_service",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&name="+slider_name+"&content="+escape(content)+"&link="+ link +"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_slider();
					//Close
					$("#btcanceled_home1").trigger("click");
				}
			});
		}
	});
	
	$("#btsave_home1").click(function(e) 
	{
		var filename=$("#fnslider_image1").val();
		var name=$("#txtslider_name1").val();
		var content=$("#txthtml_element1").val();
		var link=$("#txtslider_url").val();
		var visible=(document.getElementById("chkslider_visible1").checked)?1:0;
		
		//Clean error
		$("#error-home1").html("");
		if (filename=="")
			$("#error-home1").html("Please attach the file first.");
		else if (name=="")
			$("#error-home1").html("Please fill the slider name first.");
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fnslider_image1").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
				$("#error-home1").html("File \""+file.name+"\" is not a valid image file.");
				return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
				$("#error-home1").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
				return false;	
			} 
			var size=file.size/1024;
			uploadFilePicture1(file);
		}
	});


	var uploadFilePicture1 = function(file)
	{
		var filename=$("#fnslider_image1").val();
		var name=$("#txtslider_name1").val();
		var content=$("#txthtml_element1").val();
		var link=$("#txtslider_url").val();
		var visible=(document.getElementById("chkslider_visible1").checked)?1:0;
		
		$("#td-percent").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_slider_service?name="+name+"&content="+content+"&link="+link+"&visible="+visible, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-home1").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
				if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
					$("#percent").html("100%");
					
					//Clean everything..
					$("#error-home1").html("");
					$("#txtslider_name1").val("");
					$("#fnslider_image1").val("");
					document.getElementById("chkslider_visible1").checked = true;
					$("#txthtml_element1").val("");
					$("#txtslider_url").val("");
					$("#td-percent").hide();

					if (xhr.responseText!="0")
						$("#error-home1").html("You can have maximum 4 images.");
					else
					{
						retrieve_slider();
						$("#btcancel_home1").trigger("click");
					}
				} else {  
					$("#error-home1").html("Error "+ xhr.statusText);  
				}  
				}  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_home1").html("Your browser doesnt support FileReader object");
		} 		
	}

	// Slider Opsi Roder

	$(".btopsi-up").click(function(e) 
	{
		var opsiID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_opsi_up?ID_slider="+opsiID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_opsi();
			}
		});
	});
	
	$(".btopsi-down").click(function(e) 
	{
		var opsiID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_opsi_down?ID_slider="+opsiID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_opsi();
			}
		});
	});

	$(".btedit-opsi").click(function(e) {
		var id=$(this).attr("data-id");
		var item_name=$(this).attr("data-name");
		var visible=$(this).attr("data-visible");

		$("#btadd_opsi").hide();
		$("#opsi-scroll").hide();
		$("#edit_opsi").fadeIn("fast");
		
		//set data to component
		$("#hid_id_opsi").val(id);
		$("#txtedopsi_name").val(item_name);
		$("#chkedopsi_visible").prop("checked", (visible == "1"));
	});

	$(".btdelete-opsi").click(function(e) {
		var ID_slider=$(this).attr("data-id");
		var filename = $(this).attr("data-value");
		var conf=confirm("Are you sure want to delete this opsi item ?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/delete_opsi_service?id="+ID_slider+"&filename=" + filename,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("opsi item removed successfully.");
						retrieve_opsi();
					}
				}
			});
		}
	});

	$(".btchangepic-opsi").click(function(e)
	{
		var id=$(this).attr("data-id");
		var fnchopsi=$(".fnchopsi[data-id='"+id+"']");
		fnchopsi.click();
	});

	$(".fnchopsi").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
				alert("File \""+file.name+"\" is not a valid image file.");
				return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
				alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
				return false;	
			} 
			var size=file.size/1024;
			uploadFileOpsiEdit(file,id);
		}
	});

	$("#btadd_opsi").click(function(e) {
		$("#btadd_opsi").hide();
		$("#opsi-scroll").hide();
		$("#upload_opsi").fadeIn("fast");
	});
	
	$("#btcancel_opsi").click(function(e) {
		$("#btadd_opsi").fadeIn("fast");
		$("#opsi-scroll").fadeIn("fast");
		$("#upload_opsi").hide();
	});
	
	$("#btcanceled_opsi").click(function(e) {
		$("#btadd_opsi").fadeIn("fast");
		$("#opsi-scroll").fadeIn("fast");
		$("#edit_opsi").hide();
	});
	
	$("#btsaveed_opsi").click(function(e) 
	{
		var id = $("#hid_id_opsi").val();
		var slider_name = $("#txtedopsi_name").val();
		var visible = ($("#chkedopsi_visible").prop("checked") == true) ? "1" : "0";
		
		if (slider_name=="")
			$("#error-edit-opsi").html("Please fill opsi name first."); 
		else
		{
			$.ajax({
				url:base_url+"home/edit_opsi_service",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&name="+slider_name+"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_opsi();
					//Close
					$("#btcanceled_opsi").trigger("click");
				}
			});
		}
	});
	
	$("#btsave_opsi").click(function(e) 
	{
		var filename=$("#fnslider_opsi").val();
		var name=$("#txtslider_opsi").val();
		var visible=(document.getElementById("chkopsi_visible").checked)?1:0;
		
		//Clean error
		$("#error-opsi").html("");
		if (filename=="")
			$("#error-opsi").html("Please attach the file first.");
		else if (name=="")
			$("#error-opsi").html("Please fill the opsi name first.");
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fnslider_opsi").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
				$("#error-opsi").html("File \""+file.name+"\" is not a valid image file.");
				return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
				$("#error-opsi").html("File \""+file.name+"\" maximum "+ (max_size/1024) +" KB.");
				return false;	
			} 
			var size=file.size/1024;
			uploadFileOpsi(file);
		}
	});


	var uploadFileOpsi = function(file)
	{
		var filename=$("#fnslider_opsi").val();
		var name=$("#txtslider_opsi").val();
		var visible=(document.getElementById("chkopsi_visible").checked)?1:0;
		
		$("#td-percent").show();
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_opsi_service?name="+name+"&visible="+visible, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-opsi").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
				if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
					$("#percent").html("100%");
					
					//Clean everything..
					$("#error-opsi").html("");
					$("#txtslider_opsi").val("");
					$("#fnslider_opsi").val("");
					document.getElementById("chkopsi_visible").checked = true;
					$("#td-percent").hide();

					if (xhr.responseText!="0")
						$("#error-opsi").html("You can have maximum 4 images.");
					else
					{
						retrieve_slider();
						$("#btcancel_opsi").trigger("click");
					}
				} else {  
					$("#error-opsi").html("Error "+ xhr.statusText);  
				}  
				}  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_opsi").html("Your browser doesnt support FileReader object");
		} 		
	}

	// Service Section

	$(".btservice-up").click(function(e) 
	{
		var serviceID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_service_up?ID_service="+serviceID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_service();
			}
		});
	});
	
	$(".btservice-down").click(function(e) 
	{
		var serviceID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_service_down?ID_service="+serviceID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_service();
			}
		});
	});

	$(".btedit-service").click(function(e) {
		var id=$(this).attr("data-id");
		var pict=$(this).attr("data-pict");
		var title=$(this).attr("data-title");
		var text=$(this).attr("data-text");

		// alert(pict);

		$("#btadd_service").hide();
		$("#service-scroll").hide();
		$("#edit_service").fadeIn("fast");
		
		//set data to component
		$("#hid_id_service").val(id);
		$("#cbedserviceIcon").val(pict);
		$("#iedserviceIcon").attr("class","fa " + pict);
		$("#txtedservice_name").val(title);
		$("#txtedservice_text").val(text);
	});

	$(".btdelete-service").click(function(e) {
		var id_service=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this points?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_service?id="+id_service,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("Points removed successfully.");
						retrieve_service();
					}
				}
			});
		}
	});

	$("#btadd_service").click(function(e) {
		$("#btadd_service").hide();
		$("#service-scroll").hide();
		$("#upload_service").fadeIn("fast");
	});
	
	$("#btcancel_service").click(function(e) {
		$("#btadd_service").fadeIn("fast");
		$("#service-scroll").fadeIn("fast");
		$("#upload_service").hide();
	});
	
	$("#btcanceled_service").click(function(e) {
		$("#btadd_service").fadeIn("fast");
		$("#service-scroll").fadeIn("fast");
		$("#edit_service").hide();
	});
	
	$("#btsaveed_service").click(function(e) 
	{
		var id = $("#hid_id_service").val();
		var pict = $("#cbedserviceIcon").val();
		var title = $("#txtedservice_name").val();
		var text = $("#txtedservice_text").val();
		var visible = ($("#chkedservice_visible").prop("checked") == true) ? "1" : "0";
		
		if (title=="")
			$("#error-edit-service").html("Please fill slider name first."); 
		else
		{
			$.ajax({
				url:base_url+"home/edit_service",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&pict="+pict+"&name="+title+"&text="+text+"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_service();
					//Close
					$("#btcanceled_service").trigger("click");
				}
			});
		}
	});
	
	$("#btsave_service").click(function(e) 
	{
		var pict = $("#cbserviceIcon").val();
		var title = $("#txtservice_name").val();
		var text = $("#txtservice_text").val();
		var visible=(document.getElementById("chkservice_visible").checked)?1:0;
		
		if (pict=="")
			$("#error-service").html("Please attach the file first.");
		else if (title=="")
			$("#error-service").html("Please fill title first.");
		else if (text=="")
			$("#error-service").html("Please fill text first.");
		else
		{
			
			$.ajax({
				url:base_url+"home/insert_service",
				dataType:"html",
				type:"POST",
				data:"pict="+pict+"&title="+title+"&text="+text+"&visible="+visible,
				success: function(result)
				{
					//Clear Insert
					$("#txtservice_name").val("");
					$("#cbserviceIcon").val("icon-mobile");
					$("#iserviceIcon").attr("class","icon-mobile");
					$("#txtservice_text").val("");
					document.getElementById("chkservice_visible").checked = true;
										
					//retrieve
					retrieve_service();
					
					//Close
					$("#btcancel_service").trigger("click");
				}
			});
		}
	});

	// Layanan Section

	$(".btlayanan-up").click(function(e) 
	{
		var layananID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_layanan_down?ID_layanan="+layananID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_layanan();
			}
		});
	});
	
	$(".btlayanan-down").click(function(e) 
	{
		var layananID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_layanan_up?ID_layanan="+layananID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_layanan();
			}
		});
	});

	$(".btedit-layanan").click(function(e) {
		var id=$(this).attr("data-id");
		var pict=$(this).attr("data-pict");
		var title=$(this).attr("data-title");
		var text=$(this).attr("data-text");

		// alert(pict);

		$("#btadd_layanan").hide();
		$("#layanan-scroll").hide();
		$("#edit_layanan").fadeIn("fast");
		
		//set data to component
		$("#hid_id_layanan").val(id);
		$("#cbedlayananIcon").val(pict);
		$("#iedLayananIcon").attr("class","fa " + pict);
		$("#txtedlayanan_name").val(title);
		$("#txtedlayanan_text").val(text);
	});

	$(".btdelete-layanan").click(function(e) {
		var id_layanan=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this points?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_layanan?id="+id_layanan,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("Points removed successfully.");
						retrieve_layanan();
					}
				}
			});
		}
	});

	$("#btadd_layanan").click(function(e) {
		$("#btadd_layanan").hide();
		$("#layanan-scroll").hide();
		$("#upload_layanan").fadeIn("fast");
	});
	
	$("#btcancel_layanan").click(function(e) {
		$("#btadd_layanan").fadeIn("fast");
		$("#layanan-scroll").fadeIn("fast");
		$("#upload_layanan").hide();
	});
	
	$("#btcanceled_layanan").click(function(e) {
		$("#btadd_layanan").fadeIn("fast");
		$("#layanan-scroll").fadeIn("fast");
		$("#edit_layanan").hide();
	});
	
	$("#btsaveed_layanan").click(function(e) 
	{
		var id = $("#hid_id_layanan").val();
		var pict = $("#cbedLayananIcon").val();
		var title = $("#txtedlayanan_name").val();
		var text = $("#txtedlayanan_text").val();
		var visible = ($("#chkedlayanan_visible").prop("checked") == true) ? "1" : "0";
		
		if (title=="")
			$("#error-edit-layanan").html("Please fill slider name first."); 
		else
		{
			$.ajax({
				url:base_url+"home/edit_layanan",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&pict="+pict+"&name="+title+"&text="+text+"&visible="+visible,
				success: function(result)
				{
					console.log(result);
					retrieve_layanan();
					//Close
					$("#btcanceled_layanan").trigger("click");
				}
			});
		}
	});
	
	$("#btsave_layanan").click(function(e) 
	{
		var pict = $("#cbLayananIcon").val();
		var title = $("#txtlayanan_name").val();
		var text = $("#txtlayanan_text").val();
		var visible=(document.getElementById("chklayanan_visible").checked)?1:0;
		
		if (pict=="")
			$("#error-layanan").html("Please attach the file first.");
		else if (title=="")
			$("#error-layanan").html("Please fill title first.");
		else if (text=="")
			$("#error-layanan").html("Please fill text first.");
		else
		{
			
			$.ajax({
				url:base_url+"home/insert_layanan",
				dataType:"html",
				type:"POST",
				data:"pict="+pict+"&title="+title+"&text="+text+"&visible="+visible,
				success: function(result)
				{
					//Clear Insert
					$("#txtlayanan_name").val("");
					$("#cbLayananIcon").val("icon-mobile");
					$("#iLayananIcon").attr("class","icon-mobile");
					$("#txtlayanan_text").val("");
					document.getElementById("chkslider_visible1").checked = true;
										
					//retrieve
					retrieve_layanan();
					
					//Close
					$("#btcancel_layanan").trigger("click");
				}
			});
		}
	});
});

$("#btedit-intro").click(function(e)
{
	$("#introWallpaper").click();
});

$("#introWallpaper").change(function(e)
{
	var path=$(this).val();
	if (path!="")
	{
		var imageType = /image.*/;  
		var file = this.files[0];
		
		// check file type
		if (!file.type.match(imageType)) {  
			alert("File \""+file.name+"\" is not a valid image file.");
			return false;	
		} 
		// check file size
		if (parseInt(file.size / 1024) > max_size) {  
			alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			return false;	
		} 
		var size=file.size/1024;
		uploadintro(file);
	}
	// $(".social-box").socialLinkBuilder();
});

var uploadintro = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/intro_change_wallpaper", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  var dt = new Date();
			//   $("#pattern-important-info").css("background-image", "url('" + base_url + "images/patterns/patttern-corporate.png?t="+ dt.getTime() + "')");
			  $("#pattern-important-info").attr("src", "" + base_url + "images/patterns/pattern-profile.jpg?t="+ dt.getTime() + "");
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

$("#btedit-introProfile").click(function(e)
{
	$("#introProfile").click();
});

$("#introProfile").change(function(e)
{
	var path=$(this).val();
	if (path!="")
	{
		var imageType = /image.*/;  
		var file = this.files[0];
		
		// check file type
		if (!file.type.match(imageType)) {  
			alert("File \""+file.name+"\" is not a valid image file.");
			return false;	
		} 
		// check file size
		if (parseInt(file.size / 1024) > max_size) {  
			alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			return false;	
		} 
		var size=file.size/1024;
		uploadintroProfile(file);
	}
	// $(".social-box").socialLinkBuilder();
});

var uploadintroProfile = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/intro_change_wallpaperProfile", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  var dt = new Date();
			//   $("#pattern-important-info").css("background-image", "url('" + base_url + "images/patterns/patttern-corporate.png?t="+ dt.getTime() + "')");
			  $("#pattern-important-info-profile").attr("src", "" + base_url + "images/patterns/pattern-profile.jpg?t="+ dt.getTime() + "");
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}


$("#btedit-catalog").click(function(e)
{
	$("#fileCatalog").click();
});

$("#fileCatalog").change(function(e)
{
	var path=$(this).val();
	if (path!="")
	{
		var imageType = /application.pdf/;  
		var file = this.files[0];
		
		// check file type
		if (!file.type.match(imageType)) {  
			alert("File \""+file.name+"\" is not a valid pdf file.");
			return false;	
		} 
		// check file size
		if (parseInt(file.size / 10000) > max_size) {  
			alert("File \""+file.name+"\" maximum " + (max_size / 10000) + " KB.");
			return false;	
		} 
		var size=file.size/10000;
		uploadFileCatalog(file);
	}
	// $(".social-box").socialLinkBuilder();
});

var uploadFileCatalog = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/upload_file_catalog", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  var dt = new Date();
			//   $("#pattern-important-info").css("background-image", "url('" + base_url + "images/patterns/patttern-corporate.png?t="+ dt.getTime() + "')");
			$("#file-katalog").attr("data-href", "" + base_url + "images/upload/catalog.pdf?t="+ dt.getTime() + "");
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

$("#btedit-terms").click(function(e)
{
	$("#fileTerms").click();
});

$("#fileTerms").change(function(e)
{
	var path=$(this).val();
	if (path!="")
	{
		var imageType = /application.pdf/;  
		var file = this.files[0];
		
		// check file type
		if (!file.type.match(imageType)) {  
			alert("File \""+file.name+"\" is not a valid pdf file.");
			return false;	
		} 
		// check file size
		if (parseInt(file.size / 10000) > max_size) {  
			alert("File \""+file.name+"\" maximum " + (max_size / 10000) + " KB.");
			return false;	
		} 
		var size=file.size/10000;
		uploadFileTerms(file);
	}
	// $(".social-box").socialLinkBuilder();
});

var uploadFileTerms = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/upload_file_terms", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  var dt = new Date();
			//   $("#pattern-important-info").css("background-image", "url('" + base_url + "images/patterns/patttern-corporate.png?t="+ dt.getTime() + "')");
			$("#file-terms").attr("data-href", "" + base_url + "images/upload/terms.pdf?t="+ dt.getTime() + "");
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}




//Show Settings if admin
if (admin==1)
{
	$(".welcome-planc").show();
	$("#div-login").show();
    $("#div-login").animate({left:'0'},140);
}
else if (admin==2)
{
	$(".welcome-planc").show();
	$(".div-settings").fadeIn("fast");
	$(".bar").addClass("active");
}
else {
	$(".welcome-planc").hide();
}

