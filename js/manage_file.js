// Javascript Document

// Function
var uploadWallpaper = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/do_change_wallpaper", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);

	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  $("#sourceVideo").prop("src", base_url + "images/home/" + xhr.responseText);
			  document.getElementById('video-player').load();
			  $("#video-player")[0].play();
			  $("#fnchwallpaper").val("");
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		console.log(file);
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

var uploadWallpaper2 = function(file)
{
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('#right_pic').attr('src',e.target.result);
	}
	reader.readAsDataURL(file);
	
	xhr = new XMLHttpRequest();
	xhr.open("post", base_url+"home/do_change_wallpaper2", true);
	
	xhr.upload.addEventListener("progress", function (event) {
		console.log(event);
		if (event.lengthComputable) {

		
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);
	
	xhr.onreadystatechange = function (oEvent) 
	{  
		 if (xhr.readyState === 4) {  
			if (xhr.status === 200) 
			{  
			  alert("Changes saved successfully.");
			  var dt = new Date();
			  $("#inner-intro").css("background-image", "url('" + base_url + "image/full/wallpaper2.jpg?t="+ dt.getTime() + "')");
			} else {  
			  alert("Error "+ xhr.statusText);  
			}  
		  }  
		};  
		
		// Set headers
		xhr.setRequestHeader("X-File-Name", file.fileName);
		xhr.setRequestHeader("X-File-Size", file.fileSize);
		xhr.setRequestHeader("X-File-Type", file.type);
		
		// Send the file (doh)
		xhr.send(file);
	
	}else{
		alert("Your browser doesn't support FileReader object");
	} 		
}

// Event
$(document).ready(function(e)
{
	$("a#link-home").click(function(e)
	{
		if (admin == 2) window.location = base_url + "home/index/admin"; 
		else window.location = base_url + "home"; 
	});

	$("a#link-blog").click(function(e)
	{
		window.location = base_url + "blog"; 
	});

	$("#btedit-wallpaper").click(function(e)
	{
		$("#fnchwallpaper").click();
	});

	$("body").on("change", "#fnchwallpaper", function(e)
	{
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /video.*/;  
			var file = this.files[0];
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 

			// Check extension
			if (!file.name.includes("mp4")) {  
			  alert("File \""+file.name+"\" is not a valid mp4 extension.");
			  return false;	
			} 

			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + max_size + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadWallpaper(file);
		}
	});


	$("#btedit-wallpaper2").click(function(e)
	{
		$("#fnchwallpaper2").click();
	});

	$("#fnchwallpaper2").change(function(e)
	{
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum " + (max_size / 1024) + " KB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadWallpaper2(file);
		}
	});

	// $(".social-box").socialLinkBuilder();
});