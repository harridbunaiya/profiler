// JavaScript Document
// Global Variable
var jml_slide=parseInt($("#hid_jml_slide").val());
var index_slide=0;
var admin=parseInt($("#hid_login").val());
var loadtime = 30;

function show_error(x)
{
	$(".div-errorbox").fadeIn("slow");
	$(".error").html(x);
	window.setTimeout(function(){ $(".div-errorbox").fadeOut("slow"); },3500);
}

//Collapse All
function collapse_all()
{	
    $("#div-change-pass").css("left:","-280px");
	$("#div-change-pass").hide();
	$("#manage-home-box").css("left:","-280px");
	$("#manage-home-box").hide();
	$("#manage-work-box").css("left:","-280px");
	$("#manage-work-box").hide();
	$("#manage-events-box").css("left:","-280px");
	$("#manage-events-box").hide();
	$("#manage-events-detail-box").css("left:","-280px");
	$("#manage-events-detail-box").hide();
}

//Retrieve Front Slider
function retrieve_front_slider()
{
	var left=0;
	$.ajax({
		url:base_url+"home/retrieve_front_slider",
		dataType:"html",
		type:"GET",
		success:function(result)
		{
			var sliderList=$.parseJSON(result);
			var html="";
			var html_box="";
			for (var i=0;i<sliderList.length;i++)
			{
				html+="<div class='bg-css' style='background-image:url(\""+base_url+"assets/slider/"+sliderList[i].imagePath+"\");'><img width='100%' height='100%' src='"+base_url+"assets/slider/"+sliderList[i].imagePath+"' /></div>";	
				
				html_box+="<div  ";
				if (i==0)
				{
					html_box+="class='box activate'";
				}
				else
					html_box+="class='box'";
				
				html_box+=" id='box"+left+"' data-title='"+left+"'></div>";	
				left-=100;
			}
			html_box+="<div class='clear'></div>";
			
			$("#homebg").html(html);
			$("#loadingbox").html(html_box);
			
			$(".box").click(function(e)
			{
				$("#homebg").animate({marginLeft:$(this).attr("data-title")+'%'});
				left=parseInt($(this).attr("data-title"));
				$(".box").removeClass("activate");
				$(this).addClass("activate");
				timer.stop();
				timer.play(true);
			});
		}
	});
}

//Retrieve Front Work
function retrieve_front_work()
{
	var top=160;
	var left=240;
	var hex_left=-150;
	var hex_top=300;
	var tingkat=0;
	var j=0;
	
	$.ajax({
		url:base_url+"home/retrieve_front_image",
		dataType:"html",
		type:"GET",
		success:function(result)
		{
			var imageList=$.parseJSON(result);
			var html="";
			var jml=imageList.length;
			var max_size=max_work_items;
			var jml_slide=(jml/max_size);
			for (var k=0;k<jml_slide;k++)
			{
				html+="<div class='works contents' id='work-panel' style='min-height:665px;' >";
				html+="<div class='innerhex' onMouseOut='hidehex();'></div>";
				
				top=160;
				left=240;
				hex_left=-150;
				hex_top=300;
				tingkat=0;
				j=0;
				for (var i=k*max_size;i<(k+1)*max_size;i++)
				{
					if (j%3==0 && j!=0 && tingkat==0)
					{	
						html+="<div class='clear'></div>";
						tingkat=1; 
						j=0; 
						hex_left=-225; 
						hex_top+=140; 
						top=90; 
						left=165; 
					}
					else if (j%4==0 && j!=0 && tingkat==1)
					{ 
						html+="<div class='clear'></div>"; 
						tingkat=2; 
						j=0; 
						hex_left=-300; 
						hex_top+=140; 
						top=20; 
						left=90;
					}
					else if (j%5==0 && j!=0 && tingkat==2)
					{ 
						html+="<div class='clear'></div>"; 
						tingkat=3; 
						j=0; 
						hex_left=-225; 
						hex_top+=140;
						top=-50; 
						left=165;
					}
					else if (j%4==0 && j!=0 && tingkat==3)
					{ 
						html+="<div class='clear'></div>"; 
						tingkat=4; 
						j=0; 
						hex_left=-150; 
						hex_top+=140;  
						top=-120;
						left=240;
					}
				
					html+="<div class='hexagon hexagon2 hex-item' style='position:relative;float:left;top:"+top+"px;left:"+left+"px;'  title='"+imageList[i].title+"' onMouseOver='showhex(\""+hex_left+"\",\""+hex_top+"\",\""+imageList[i].path+"\",\""+imageList[i].title+"\",\""+imageList[i].desc+"\");' >";
					html+="<div class='hexagon-in1'>";
					html+="<div class='hexagon-in2' style='background-image:url(\""+base_url+"assets/image/"+imageList[i].path+"\");'></div></div></div>";
			
					hex_left+=150;
					left-=10;
					j++;
				}
				html+="</div>";
			}
			html+="<div class='clear'></div>";
			
			$("#front-work").html(html);
			
			//Set new Attribute
			$("#hid_jml").val(jml);
			$("#hid_jml_slide").val(jml_slide);			
			
			if (index_slide+1==jml_slide-1) 
			{
				$("#work-panel").css("margin-left",work_left+"%");
				$("#btwork-next").fadeIn("fast");
			}
			else if (index_slide==jml_slide)
			{
				index_slide=jml_slide-1;
				work_left=index_slide*-100;
				$("#work-panel").css("margin-left",work_left+"%");
				$("#btwork-next").fadeOut("fast");
			}
			else if (index_slide+1==jml_slide)
			{
				$("#work-panel").css("margin-left",work_left+"%");
				$("#btwork-next").fadeOut("fast");
			}
			else
			{	
				//Back to current position
				$("#work-panel").css("margin-left",work_left+"%");
			}
			
			$(".innerhex").click(function(e)
			{
				if ($(this).attr("data-title")!="gray.jpg")
				{
					$("#picturebox").fadeIn("fast");
					$("#picturebox").css("left",window.innerWidth/2 - document.getElementById("picturebox").offsetWidth/2);
					$("#picturebox").css("top",window.innerHeight/2 - document.getElementById("picturebox").offsetHeight/2);
					$("#picturebox").css("background-image","url('"+base_url+"assets/image/"+$(this).attr("data-title")+"')");
					$("#div_title").html($(this).attr("data-collapsed"));
					$("#div_desc").html($(this).attr("data-back-btn-text"));
					$("#bg").fadeIn("fast");
				}
			});
		}
	});
}

/*Retrieve Backend*/
function retrieve_image()
{
	$.ajax({
		url:base_url+"home/retrieve_image",
		dataType:"html",
		type:"POST",
		success:function(result)
		{
			var imageList=$.parseJSON(result);
			var html="";
			for (var i=0;i<imageList.length;i++)
			{
				html+="<div class='slide-img'>";
                html+="<div class='img-polaroid wid' >";
                html+="<div style='background-image:url(\""+base_url+"assets/image/"+imageList[i].imagePath+"\")' class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='"+imageList[i].imageID+"'>";
                html+="<img src='"+base_url+"assets/arrow-up.png' data-id='"+imageList[i].imageID+"' data-value='"+imageList[i].imageIndex+"' class='btwork-up icon mini' /><br/><img src='"+base_url+"assets/arrow-bottom.png' data-id='"+imageList[i].imageID+"' data-value='"+imageList[i].imageIndex+"' class='btwork-down icon mini' /><br/><i data-id='"+imageList[i].imageID+"' data-title='"+imageList[i].imageTitle+"' data-desc='"+imageList[i].imageDesc+"' class='btedit-work icon icon-pencil' title='Edit Title'></i><br/><i data-id='"+imageList[i].imageID+"' class='btchangeimg-work icon icon-picture' title='Edit Picture'></i><br/><i ";
				if (imageList[i].visible==1) 
					html+="class='icon-ok-circle' title='Show'";
				else
					html+="class='icon-ban-circle' title='Hide'";
				html+=" ></i>";
                html+="<input type='file' class='fnchimg' data-id='"+imageList[i].imageID+"' /></span>";
                html+="<span class='btdelete-image spclose back-white' data-value='"+imageList[i].imagePath+"' data-id='"+imageList[i].imageID+"'>X</span>";
                html+="</div></div>";
                html+="<div class='input-prepend input-append dnone' data-id='"+imageList[i].imageID+"'>";
                html+="<span class='add-on mini icon btclose-edit-home' data-id='"+imageList[i].imageID+"'><i class='icon-remove'></i></span>";
                html+="<input type='text' class='txttitle-home mini-input span2' placeholder='Title..' value='"+imageList[i].imageTitle+"' data-id='"+imageList[i].imageID+"' />";
                html+="<span class='add-on mini icon btsave-edit-home' data-id='"+imageList[i].imageID+"'><i class='icon-ok'></i></span></div>";
                html+="<span class='splabel' data-id='"+imageList[i].imageID+"'>"+imageList[i].imageTitle+"</span></div>";
			}
			$("#work-scroll").html(html);
			
			$(".fnchimg").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \"" + file.name + "\" maximum 700 MB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFileImageEdit(file,id);
				}
			});
			
			$(".btwork-up").click(function(e) 
			{
				var imageID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_img_up?imageid="+imageID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_image();
						retrieve_front_work();
					}
				});
			});
			
			$(".btwork-down").click(function(e) 
			{
				var imageID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_img_down?imageid="+imageID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_image();
						retrieve_front_work();
					}
				});
			});
	
			$(".btedit-work").click(function(e) 
			{
				var id=$(this).attr("data-id");
				var title=$(this).attr("data-title");
				var desc=$(this).attr("data-desc");
				
				$("#hid_id_img").val(id);
				$("#txttitle_ed_img").val(title);
				$("#txtdesc_ed_img").val(desc);
				
				$("#work-scroll").hide();
				$("#edit_work").fadeIn("fast");
			});
			
			$(".btdelete-image").click(function(e)
			{
				var image_id=$(this).attr("data-id");
				var filename=$(this).attr("data-value");
				delete_image(image_id,filename);
			});
		}
	});
}

function retrieve_slider()
{
	$.ajax({
		url:base_url+"home/retrieve_slider",
		dataType:"html",
		type:"GET",
		success:function(result)
		{
			var sliderList=$.parseJSON(result);
			var html="";
			for (var i=0;i<sliderList.length;i++)
			{
				html+="<div class='slide-img'>";
                html+="<div class='img-polaroid wid' >";
                html+="<div style='background-image:url(\""+base_url+"assets/slider/"+sliderList[i].imagePath+"\")' class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='"+sliderList[i].imageID+"'>";
                html+="<img src='"+base_url+"assets/arrow-up.png' data-id='"+sliderList[i].imageID+"' data-value='"+sliderList[i].imageIndex+"' class='bthome-up icon mini'/><br/><img src='"+base_url+"assets/arrow-bottom.png' data-id='"+sliderList[i].imageID+"' data-value='"+sliderList[i].imageIndex+"' class='bthome-down icon mini' /><br/><i data-id='"+sliderList[i].imageID+"' class='btedit-home icon icon-pencil' title='Edit Title'></i><br/><i data-id='"+sliderList[i].imageID+"' class='btchangepic-home icon icon-picture' title='Edit Picture'></i><input type='file' class='fnchpic' data-id='"+sliderList[i].imageID+"' />";
                html+="</span>";
                html+="<span class='btdelete-slider spclose back-white' data-id='"+sliderList[i].imageID+"'>X</span>";
                html+="</div></div>";
                html+="<div class='input-prepend input-append dnone' data-id='"+sliderList[i].imageID+"'>";
                html+="<span class='add-on mini icon btclose-edit-home' data-id='"+sliderList[i].imageID+"'><i class='icon-remove'></i></span>";
                html+="<input type='text' class='txttitle-home mini-input span2' placeholder='Title..' value='"+sliderList[i].imageTitle+"' data-id='"+sliderList[i].imageID+"' />";
                html+="<span class='add-on mini icon btsave-edit-home' data-id='"+sliderList[i].imageID+"'><i class='icon-ok'></i></span></div>";
                html+="<span class='splabel' data-id='"+sliderList[i].imageID+"'>"+sliderList[i].imageTitle+"</span></div>";			
			}

			$("#home-scroll").html(html);
			
			$(".bthome-up").click(function(e) 
			{
				var imageID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_up?imageid="+imageID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_slider();
						retrieve_front_slider()
					}
				});
			});
			
			$(".btdelete-slider").click(function(e) 
			{
				var image_id=$(this).attr("data-id");
				var path=$(this).attr("data-value");
				delete_picture(image_id,path);    
			});
			
			
			$(".bthome-down").click(function(e) 
			{
				var imageID=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_down?imageid="+imageID+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_slider();
						retrieve_front_slider()
					}
				});
			});
			
			$(".fnchpic").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).val();
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum 700 MB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFilePictureEdit(file,id);
				}
			});
	
			$(".btchangepic-home").click(function(e)
			{
				var id=$(this).attr("data-id");
				var fnchpic=$(".fnchpic[data-id='"+id+"']");
				fnchpic.click();
			});
			
			$(".btedit-home").click(function(e) {
				var id=$(this).attr("data-id");
				
				$(".splabel[data-id='"+id+"']").hide();
				$(".input-prepend[data-id='"+id+"']").fadeIn("fast");    
			});
			
			$(".btclose-edit-home").click(function(e)
			{
				var id=$(this).attr("data-id");
				$(".input-prepend[data-id='"+id+"']").hide();  
				$(".splabel[data-id='"+id+"']").fadeIn("fast");
			});
			
			$(".btsave-edit-home").click(function(e) 
			{
				var id = $(this).attr("data-id");
				var title=$(".txttitle-home[data-id='"+id+"']").val();
				
				if (title=="")
					alert("Please fill the title first.");
				else
				{
					$.ajax({
						url:base_url+"home/update_slider_title?id="+id+"&title="+title,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							
							$(".btclose-edit-home[data-id='"+id+"']").trigger("click");
							retrieve_slider();
						}
					});
				}
			});
		}
	});
}

/*Uploading Picture*/
$(document).ready(function(e)
{
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
	
	handleFiles = function (files,e){
		// alert(files);
		// Traverse throught all files and check if uploaded file type is image	
		var imageType = /image.*/;  
		var file = files[0];
		// check file type
		if (!file.type.match(imageType)) {  
		  show_error("File \""+file.name+"\" is not a valid image file.");
		  return false;	
		} 
		// check file size
		if (parseInt(file.size / 1024) > max_size) {  
		  show_error("File \""+file.name+"\" maximum 700 MB.");
		  return false;	
		} 
		var size=file.size/1024;
		/*var info = "<div class='preview active-win'><div class='preview-image'><img ></div><div class='progress-holder'><span id='progress'></span></div><span class='percents'></span><div style='float:left;'>Uploaded <span class='up-done'></span> KB of "+parseInt(size)+" KB</div>";
		
		$(".upload-progress").show("fast",function(){
			$(".upload-progress").html(info); 
			
		});*/
		uploadFile(file);
	}
	
	uploadFile = function(file){
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			//$('#right_pic').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/update_right_pic", true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {
				//$("#progress").css("width",(event.loaded / event.total) * 100 + "%");
				
				//alert(((event.loaded / event.total) * 100).toFixed() + "%");
				//$(".percents").html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
				
				$("#sp_percent").html((parseInt(event.loaded / 1024)).toFixed(0));
				//alert((parseInt(event.loaded / 1024)).toFixed(0));
				//$(".up-done").html((parseInt(event.loaded / 1024)).toFixed(0));
			}
			else {
				show_error("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  //$("#progress").css("width","100%");
				  //$(".percents").html("100%");
				  $("#sp_percent").html("100%");
				  //alert((parseInt(file.size / 1024)).toFixed(0));
				  //$(".up-done").html();
				  reader = new FileReader();
				  reader.onload = function(e){
						//alert(e.target.result);
						$('#right_pic').attr('src',e.target.result);
						$("#btcncl-upl").trigger("click");
						$("#sp_percent").html("");
						$('.mid-logo').attr('src',e.target.result);
				  }
				  reader.readAsDataURL(file);
				  
				} else {  
				  alert("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			show_error("Your browser doesnt support FileReader object");
		} 		
	}
});

$(document).ready(function(e)
{
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
	
	uploadFileImageEdit = function(file, data_id)
	{
		var id=data_id;
		
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){

		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/update_image?id="+id, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

			}
			else {
				alert("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  alert("Changes saved successfully.");
				  retrieve_image();
				  retrieve_front_work();
			      $("#btcanceledimg").trigger("click");
				} else {  
				  show_error("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			alert("Your browser doesnt support FileReader object");
		} 		
	}
});

function show_image(files)
{
	var file = files[0];
	if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			$('#image_create').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
	}
}

function show_ed_image(files)
{
	var file = files[0];
	if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			$('#image_edit').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
	}
}


function edit_image(id,path,title,index,desc,visible)
{
	$("#hid_id_img").val(id);
	$("#editimg").fadeIn("slow");
	$("#mngimg").fadeOut("fast");
	$("#txtedtitle_img").val(title);
	$("#txtedindex_img").val(index);
	$("#txteddesc_img").val(desc);
	$("#image_edit").attr("src",base_url+"assets/image/"+path);
	if (visible==1)
		document.getElementById("chkedvisible_img").checked=true;
	else
		document.getElementById("chkedvisible_img").checked=false;
}

function delete_image(id,filename)
{
	var conf=confirm("Are you sure want to delete this image?");
	if (conf==true)
	{
		$.ajax({
			url:base_url+"home/delete_image?imageid="+id+"&filename="+filename,
			dataType:"html",
			type:"POST",
			success:function(result)
			{
				alert("Removed successfully.");
				retrieve_image();
				retrieve_front_work();
			}
		});
	}
}

$(document).ready(function(e)
{
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
	
	uploadFileImage = function(file)
	{
		var title=$("#txttitle_img").val();
		var desc=$("#txtdesc_img").val();
		var visible=(document.getElementById("chkvisible_img").checked==true)?1:0;
		
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			//$('#right_pic').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_img?title="+title+"&desc="+desc+"&visible="+visible, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {
				//$("#progress").css("width",(event.loaded / event.total) * 100 + "%");
				
				//alert(((event.loaded / event.total) * 100).toFixed() + "%");
				//$(".percents").html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
				
				//$("#percent_img").html(((event.loaded / event.total) * 100).toFixed() + "%");
				//alert((parseInt(event.loaded / 1024)).toFixed(0));
				//$(".up-done").html((parseInt(event.loaded / 1024)).toFixed(0));

			}
			else {
				$("#error-work").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  //$("#progress").css("width","100%");
				  //$(".percents").html("100%");
				  //$("#percent").html("100%");
				  //alert((parseInt(file.size / 1024)).toFixed(0));
				  //$(".up-done").html();

                  retrieve_image();
				  retrieve_front_work();
				  $("#btcancelimg").trigger("click");

				} else {  
				  $("#error-work").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error-work").html("Your browser doesnt support FileReader object");
		} 		
	}
	
	$("#btcreateimg").click(function(e) 
	{
		var filename=$("#fnimg").val();
		var title=$("#txttitle_img").val();
		
		//Clean error
		$("#error-work").html("");
		if (filename=="")
			$("#error-work").html("Please attach the file first.");
		else if (title=="")
			$("#error-work").html("Please fill the title first.");
		else 
		{	
			var imageType = /image.*/;  
			var file = document.getElementById("fnimg").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-work").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-work").html("File \""+file.name+"\" maximum 700 MB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileImage(file);
		}
    });
});

function delete_picture(id,path)
{
	var conf=confirm("Are you sure want to delete this slide?");
	if (conf==true)
	{
		$.ajax
		({
			url:base_url+"home/delete_slider?imageid="+id+"&filename="+path,
			dataType:"html",
			type:"POST",
			success:function(result)
			{
				show_error("Removed successfully.");
				retrieve_slider();
				retrieve_front_slider();
			}
		});
	}
}

function edit_picture(id,path,title,index,visible)
{
	$("#hid_id").val(id);
	$("#editpic").fadeIn("slow");
	$("#mngpic").fadeOut("fast");
	$("#txtedtitle").val(title);
	$("#txtedindex").val(index);
	$("#picture_edit").attr("src",base_url+"assets/slider/"+path);
	if (visible==1)
		document.getElementById("chkedvisible").checked=true;
	else
		document.getElementById("chkedvisible").checked=false;
}

function show_picture(files)
{
	var file = files[0];
	if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			$('#picture_create').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
	}
}

function show_ed_picture(files)
{
	var file = files[0];
	if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			$('#picture_edit').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
	}
}


$(document).ready(function(e)
{
	$(".btdelete-slider").click(function(e) 
	{
    	var image_id=$(this).attr("data-id");
		var path=$(this).attr("data-value");
		delete_picture(image_id,path);    
    });
	
	$(".btdelete-image").click(function(e)
	{
		var image_id=$(this).attr("data-id");
		var filename=$(this).attr("data-value");
		delete_image(image_id,filename);
	});
	
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
	
	uploadFilePicture = function(file)
	{
		var title=$("#txttitle").val();
		var visible=(document.getElementById("chkvisible").checked)?1:0;
		
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_slider?title="+title+"&visible="+visible, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

				$("#percent").html(((event.loaded / event.total) * 100).toFixed() + "%");
			}
			else {
				$("#error-home").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  

				  $("#percent").html("100%");
				  
				  //Clean everything..
				  $("#error-home").html("");
				  $("#txttitle").val("");
				  $("#fnpic").val("");

				  if (xhr.responseText!="0")
				  	$("#error-home").html("You can have maximum 4 images.");
				  else
				  {
				  	retrieve_slider();
					retrieve_front_slider();
				  	$("#btcancelpic").trigger("click");
				  }
				} else {  
				  $("#error-home").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error_home").html("Your browser doesnt support FileReader object");
		} 		
	}
	
	$("#btcreatepic").click(function(e) 
	{
		var filename=$("#fnpic").val();
		var title=$("#txttitle").val();
		var visible=(document.getElementById("chkvisible").checked)?1:0;
		
		//Clean error
		$("#error-home").html("");
		if (filename=="")
			$("#error-home").html("Please attach the file first.");
		else if (title=="")
			$("#error-home").html("Please fill the title first.");
		else 
		{
			var imageType = /image.*/;  
			var file = document.getElementById("fnpic").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-home").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-home").html("File \""+file.name+"\" maximum 700 MB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePicture(file);
		}
    });
});

$(document).ready(function(e)
{
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
	
	uploadFilePictureEdit = function(file,data_id)
	{
		var id=data_id;
		
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			//$('#right_pic').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/update_slider_image?id="+id, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

			
			}
			else {
				alert("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  alert("Changes saved successfully.");
				  retrieve_slider();
				  retrieve_front_slider();
				} else {  
				  alert("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			alert("Your browser doesn't support FileReader object");
		} 		
	}
});


function resetbtn()
{
	$("#mnabout").removeClass("active");
	$("#mncontact").removeClass("active");
	$("#mnevents").removeClass("active");
	$("#mnworks").removeClass("active");
}

function scrollto(id)
{
	$('html,body').animate(
	{
		scrollTop: $("#"+id).offset().top-80
	},300);
}

$(document).scroll(function(e)
{
	checking();
	checkTop();
});

function checkTop()
{
	if ($(this).scrollTop()>= $("#about-us").offset().top-100)
	{
		if (document.getElementById("bttop").style.display!="block")
			$("#bttop").fadeIn("slow");
	}
	else
		$("#bttop").fadeOut("slow");
}

function checking()
{
	
	if ($(this).scrollTop()>= $("#contact-us").offset().top-100)
	{
		resetbtn();
		$("#mncontact").addClass("active");
	}else if ($(this).scrollTop()>= $("#events").offset().top-100)
	{
		resetbtn();
		$("#mnevents").addClass("active");
	}else if ($(this).scrollTop()>= $("#works").offset().top-100)
	{
		resetbtn();
		$("#mnworks").addClass("active");
	}else if ($(this).scrollTop()>= $("#about-us").offset().top-100)
	{
		resetbtn();
		$("#mnabout").addClass("active");
	}
	else if ($(this).scrollTop()>= $("#home").offset().top-100)
	{
		resetbtn();
	}
}

function validate()
{
	var error=document.getElementById("errorupload");
	if (document.getElementById("fnupload").value=="")
	{
		error.innerHTML="Please upload your file first.";
		return false;
	}
	else
	{
		$(".div-upload").fadeOut("fast");
		$("#bg").fadeOut("fast");
		window.setTimeout(function(){window.location.reload();},300);
		return true;
	}
}

function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}


/*Slider*/
var left=0;
var slide_left=0;
var page;
var timeout;

function check_button()
{
	if (slide_left==0)
		$("#btslideprev").fadeOut("fast");
	else
		$("#btslideprev").fadeIn("slow");

	if (slide_left==-((page-1)*100))
		$("#btslidenext").fadeOut("fast");
	else
		$("#btslidenext").fadeIn("slow");
}


function slideNext()
{
	var length=$(".box").length;
	
	if (left<=-((length-1)*100))
		left=0;
	else
		left-=100;
	
	$("#homebg").animate({marginLeft:left+'%'},600);	
	$(".box").removeClass("activate");
	$("#box"+left).addClass("activate");
}
	
function showhex(left,top,url,title,desc)
{
	if (url!="gray.jpg")
	{
		$(".innerhex").attr("data-collapsed",title);
		$(".innerhex").attr("data-back-btn-text",desc);
		$(".innerhex").attr("data-title",url);
		$(".innerhex").css("left",left+"px");
		$(".innerhex").css("top",top+"px");
		$(".innerhex").css("background-image","url('"+base_url+"assets/image/"+url+"')");
		$(".innerhex").fadeIn(360);
	}
	else
		$(".innerhex").hide();
}


function hidehex()
{
	$(".innerhex").hide();
}


var work_left=0;


$(document).ready(function()
{	
	$(".fnchimg").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \"" + file.name + "\" maximum 700 MB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileImageEdit(file,id);
		}
	});
	
	$(".btchangeimg-work").click(function(e) 
	{
        var id=$(this).attr("data-id");
		var fnchpic=$(".fnchimg[data-id='"+id+"']");
		fnchpic.click();
    });
	
	$(".btwork-up").click(function(e) 
	{
		var imageID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_img_up?imageid="+imageID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_image();
				retrieve_front_work();
			}
		});
	});
	
	$(".btwork-down").click(function(e) 
	{
		var imageID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_img_down?imageid="+imageID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_image();
				retrieve_front_work();
			}
		});
	});
	
	$("#btcanceledimg").click(function(e) 
	{
     	$("#work-scroll").fadeIn("fast");
		$("#edit_work").hide(); 
    });
	
	$("#btsaveimg").click(function(e) 
	{
        var id = $("#hid_id_img").val();
    	var title=$("#txttitle_ed_img").val();
		var desc=$("#txtdesc_ed_img").val();
		var visible=(document.getElementById("chkvisible_ed_img").checked==true)?1:0;
		
		if (title=="")
			alert("Please fill the title first.");
		else
		{
			$.ajax({
				url:base_url+"home/update_img",
				dataType:"html",
				data:"id="+id+"&title="+title+"&desc="+desc+"&visible="+visible,
				type:"POST",
				success: function(result)
				{
					$("#btcanceledimg").trigger("click");
					retrieve_image();
					retrieve_front_work();
				}
			});
		}
    });
	
	$(".btedit-work").click(function(e) 
	{
		var id=$(this).attr("data-id");
		var title=$(this).attr("data-title");
		var desc=$(this).attr("data-desc");
		
		$("#hid_id_img").val(id);
		$("#txttitle_ed_img").val(title);
		$("#txtdesc_ed_img").val(desc);
		
        $("#work-scroll").hide();
		$("#edit_work").fadeIn("fast");
    });
	
	//Slider Work
	$("#btaddimg").click(function(e) {
		$("#btaddimg").hide();
		$("#work-scroll").hide();
        $("#upload_work").fadeIn("fast");
		
		/*Clear Value*/
		$("#fnimg").val("");
		$("#txttitle_img").val("");
		$("#txtdesc_img").val("");
		document.getElementById("chkvisible_img").checked=true;
    });	
	
	$("#btcancelimg").click(function(e) {
        $("#btaddimg").fadeIn("fast");
		$("#work-scroll").fadeIn("fast");
        $("#upload_work").hide();
    });
	
	$(".btchangepic-home").click(function(e)
	{
		var id=$(this).attr("data-id");
		var fnchpic=$(".fnchpic[data-id='"+id+"']");
		fnchpic.click();
	});
	
	$(".fnchpic").change(function(e)
	{
		var id=$(this).attr("data-id");
		var path=$(this).val();
		if (path!="")
		{
			var imageType = /image.*/;  
			var file = this.files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  alert("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  alert("File \""+file.name+"\" maximum 700 MB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFilePictureEdit(file,id);
		}
	});
	
	$(".btedit-home").click(function(e) {
    	var id=$(this).attr("data-id");
		
		$(".splabel[data-id='"+id+"']").hide();
		$(".input-prepend[data-id='"+id+"']").fadeIn("fast");    
    });
	
	$(".btclose-edit-home").click(function(e)
	{
		var id=$(this).attr("data-id");
		$(".input-prepend[data-id='"+id+"']").hide();  
		$(".splabel[data-id='"+id+"']").fadeIn("fast");
	});
	
	$(".btsave-edit-home").click(function(e) 
	{
		var id = $(this).attr("data-id");
    	var title=$(".txttitle-home[data-id='"+id+"']").val();
		
		if (title=="")
			alert("Please fill the title first.");
		else
		{
			$.ajax({
				url:base_url+"home/update_slider_title?id="+id+"&title="+title,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					
					$(".btclose-edit-home[data-id='"+id+"']").trigger("click");
					retrieve_slider();
				}
			});
		}
    });
	
	$("#btsend_email").click(function(e) 
	{
     	var xmlhttp;
		var status;
		
		var s_name=$("#txts_name").val();
		var s_phone=$("#txts_phone").val();
		var s_email=$("#txts_email").val();
		var s_message=$("#txts_message").val();
		
		var regex=new RegExp("^.+@.+\..+$");
		
		if (s_name=="")
			show_error("Please fill your name first.");
		else if (s_name.length<3)
			show_error("Name contains at least 3 characters.");
		else if (s_email=="")
			show_error("Please fill your email first.");
		else if (!regex.test(s_email))
			show_error("Please enter a valid email.");
		else if (s_message.length=="")
			show_error("Please give your message.");
		else
		{
			if (window.XMLHttpRequest)
				xmlhttp=new XMLHttpRequest();
			else
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.status==200 && xmlhttp.readyState==4)
				{
					status=xmlhttp.responseText;
					show_error("Message successfully sent.");
					
					$("#txts_name").val("");
					$("#txts_phone").val("");
					$("#txts_email").val("");
					$("#txts_message").val("");
				}
			}
			xmlhttp.open("POST",base_url+"home/send_email",true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("s_name="+s_name+"&s_phone="+s_phone+"&s_email="+s_email+"&s_message="+s_message);  
		}
    });
	
	$(".innerhex").click(function(e)
	{
		if ($(this).attr("data-title")!="gray.jpg")
		{
			$("#picturebox").fadeIn("fast");
			$("#picturebox").css("left",window.innerWidth/2 - document.getElementById("picturebox").offsetWidth/2);
			$("#picturebox").css("top",window.innerHeight/2 - document.getElementById("picturebox").offsetHeight/2);
			$("#picturebox").css("background-image","url('"+base_url+"assets/image/"+$(this).attr("data-title")+"')");
			$("#div_title").html($(this).attr("data-collapsed"));
			$("#div_desc").html($(this).attr("data-back-btn-text"));
			$("#bg").fadeIn("fast");
		}
	});
	
	$("#picturebox").hover(function(e) {
        $(".desc-bar").animate({marginTop:'470px'},"slow");
    });
	
	$("#picturebox").mouseleave(function(e) {
        $(".desc-bar").animate({marginTop:'570px'},"slow");
    });
	
	$("#bg,.btclose").click(function(e)
	{
		$("#picturebox").fadeOut("fast");
		$("#pictureevent").fadeOut("fast");
		$(".loginbox").fadeOut("fast");
		$(".bigbox").fadeOut("fast");
		$(".div-upload").fadeOut("fast");
		$("#bg").fadeOut("fast");
	});
	
	
	$(window).keydown(function(e) {
	   if (e.keyCode==27)
	   {
			$("#picturebox").fadeOut("fast");
			$(".loginbox").fadeOut("fast");
			$(".bigbox").fadeOut("fast");
			$(".div-upload").fadeOut("fast");
			$("#bg").fadeOut("fast");
	   }
	});
	
	$("#mnabout,#btfindout").click(function(e)
	{
		resetbtn();
		$("#mnabout").addClass("active");
		scrollto("about-us");
	});
	
	$("#mncontact").click(function(e)
	{
		resetbtn();
		$("#mncontact").addClass("active");
		scrollto("contact-us");
	});
	
	$("#mnevents").click(function(e)
	{
		resetbtn();
		$("#mnevents").addClass("active");
		scrollto("events");
	});
	
	$("#mnworks").click(function(e)
	{
		resetbtn();
		$("#mnworks").addClass("active");
		scrollto("works");
	});
	
	
	$("#bttop,.logo").click(function(e) {
        resetbtn();
		$('html,body').animate(
		{
			scrollTop: 0
		},300);
    });
	
	$("#btchange-pic").click(function(e)
	{
		$(this).hide();
		$(".div-upload").slideDown("slow");
		
	});
	
	$("#btcncl-upl").click(function(e)
	{
		$(".div-upload").slideUp("fast");
		$("#btchange-pic").fadeIn("slow");
	});
	
	$("#btedit-about").click(function(e)
	{
		$(".about-text").slideUp("fast");
		$(".edit-about").slideDown("slow");
		$("#btsave-about").fadeIn("fast");
		$("#btcancel-about").fadeIn("fast");
		$("#btedit-about").fadeOut("fast");
	});
	
	$("#btcancel-about").click(function(e)
	{
		$(".about-text").slideDown("slow");
		$(".edit-about").slideUp("fast");
		$("#btsave-about").fadeOut("fast");
		$("#btcancel-about").fadeOut("fast");
		$("#btedit-about").fadeIn("fast");
	});
	
	$("#btsave-about").click(function(e)
	{
		var xmlhttp;
		var status;
		
		var quote=$("#txtquote").val();
		var simple=$("#txtsimple").val();
		//var content=CKEDITOR.instances.txtcontent.getData();
		var content=$("#txtcontent").val();
		
		var html="";
		
		
		if (window.XMLHttpRequest)
			xmlhttp=new XMLHttpRequest();
		else
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.status==200 && xmlhttp.readyState==4)
			{
				status=xmlhttp.responseText;

				$("#sp_quote_text").html(quote);
				$("#sp_simple_quote").html(simple);
				
				/*Content*/
				var change_page=Math.ceil(content.length/1000);
				
				var c=nl2br(content);
				for (var i=0;i<change_page;i++)
				{					
					html+="<div class='about-box'>";
                    html+="<div class='about-left'>";
                    html+=c.substring((i*1000),(i*1000)+500);                       
                    html+="</div>";
                    html+="<div class='about-right'>";
                    html+=c.substring(((i*1000)+500),(i*1000)+1000); 
                    html+="</div>";
                    html+="<div class='clear'></div>";
                    html+="</div>";
				}
				
				$("#slide_text").html(html);
				$("#slide_text").css("margin-left","0");
				page=change_page;
				slide_left=0;
				check_button();
				//$("#sp_content").html(content);
				$("#btcancel-about").trigger("click");
			}
		}
		xmlhttp.open("POST",base_url+"home/update_about",true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.send("quote="+quote+"&simple="+simple+"&content="+content);
	});
	
	$("#btedit-contact").click(function(e)
	{
		$(".edit-contact").slideDown("slow");
		$(".contact-label").slideUp("fast");
		$("#btsave-contact").fadeIn("fast");
		$("#btcancel-contact").fadeIn("fast");
		$("#btedit-contact").fadeOut("fast");
	});
	
	$("#btcancel-contact").click(function(e)
	{
		$(".edit-contact").slideUp("fast");
		$(".contact-label").slideDown("slow");
		$("#btsave-contact").fadeOut("fast");
		$("#btcancel-contact").fadeOut("fast");
		$("#btedit-contact").fadeIn("fast");
	});
	
	$("#btsave-contact").click(function(e) {
        var xmlhttp;
		var status;
		
		var email=$("#txtemail").val();
		var name_1=$("#txtname_1").val();
		var contact_1=$("#txtmp_1").val();
		var name_2=$("#txtname_2").val();
		var contact_2=$("#txtmp_2").val();
		var fax=$("#txtfax").val();
		var location=$("#txtlocation").val();
				
				
		if (email=="")
			alert("Please fill your email first.");
		else
		{
			if (window.XMLHttpRequest)
				xmlhttp=new XMLHttpRequest();
			else
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			
			xmlhttp.onreadystatechange=function()
			{
				if (xmlhttp.status==200 && xmlhttp.readyState==4)
				{
					status=xmlhttp.responseText;
					
					$("#td_contact1").html(name_1);
					$("#td_mp1").html(contact_1);
					$("#td_contact2").html(name_2);
					$("#td_mp2").html(contact_2);
					$("#td_fax").html(fax);
					$("#td_location").html(nl2br(location));
					
					$("#btcancel-contact").trigger("click");
				}
			}
			xmlhttp.open("POST",base_url+"home/update_contact",true);
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send("email="+email+"&name1="+name_1+"&mp1="+encodeURIComponent(contact_1)+"&name2="+name_2+"&mp2="+encodeURIComponent(contact_2)+"&fax="+fax+"&location="+location);
		}
    });
	
	$("#btedit-home").click(function(e) {
		collapse_all();
		$("#manage-home-box").show();
        $("#manage-home-box").animate({left:'0'},140);
    });
	
	$("#close-home").click(function(e) {
        $("#manage-home-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-home-box").hide();},140);
    });
	
	$("#btedit-work").click(function(e) {
		collapse_all();
		$("#manage-work-box").show();
        $("#manage-work-box").animate({left:'0'},140);
    });
	
	$("#btclose_mng").click(function(e) {
        $(".manage-box").slideUp("slow");
    });
	
	$("#close-work").click(function(e) {
        $("#manage-work-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-work-box").hide();},140);
    });
	
	/*$("#btcancelpic").click(function(e) {
        $("#createpic").fadeOut("fast");
		$("#mngpic").fadeIn("slow");
    });*/
	
	/*$("#btinsertpic").click(function(e) {
        $("#createpic").fadeIn("slow");
		$("#mngpic").fadeOut("fast");
    });*/
	
	$(".num").keydown(function(e) {
        if ((e.keyCode<48 || e.keyCode>57) && e.keyCode!=8 &&e.keyCode!=46 &&e.keyCode!=37 &&e.keyCode!=38 &&e.keyCode!=39 &&e.keyCode!=40 )
		{
			return false;
		}
    });
	
	$("#btcanceledpic").click(function(e) {
        $("#editpic").fadeOut("fast");
		$("#mngpic").fadeIn("slow");
    });
	
	$("#btchangepic").click(function(e)
	{
		$(this).fadeOut("fast");
		$(".file-input").fadeIn("slow");
	});
	
	$("#btcncl_chng").click(function(e)
	{
		$("#btchangepic").fadeIn("slow");
		$(".file-input").fadeOut("fast");
	});
	
	$("#btinsertimg").click(function(e)
	{
		$("#mngimg").fadeOut("fast");
		$("#createimg").fadeIn("slow");
	});
	
	$("#btcancelimg").click(function(e)
	{
		$("#mngimg").fadeIn("slow");
		$("#createimg").fadeOut("fast");
	});
	
	$("#btcanceledimg").click(function(e) {
        $("#mngimg").fadeIn("slow");
		$("#editimg").fadeOut("fast");
    });
	
	
	$("#btchangeimg").click(function(e)
	{
		$(this).fadeOut("fast");
		$(".file-input-img").fadeIn("slow");
	});
	
	$("#btcncl_chng_img").click(function(e)
	{
		$("#btchangeimg").fadeIn("slow");
		$(".file-input-img").fadeOut("fast");
	});
		
	$(".bthome-up").click(function(e) 
	{
        var imageID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_up?imageid="+imageID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_slider();
				retrieve_front_slider()
			}
		});
    });
	
	$(".bthome-down").click(function(e) 
	{
        var imageID=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_down?imageid="+imageID+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_slider();
				retrieve_front_slider()
			}
		});
    });
			
	$(".box").click(function(e)
	{
		$("#homebg").animate({marginLeft:$(this).attr("data-title")+'%'});
		left=parseInt($(this).attr("data-title"));
		$(".box").removeClass("activate");
		$(this).addClass("activate");
		timer.stop();
		timer.play(true);
	});

	/* Slide About Us*/
	$("#btslidenext").click(function(e)
	{
		if (slide_left>-((page-1)*100))
		{
			slide_left-=100;
			$("#slide_text").animate({marginLeft:slide_left+'%'},600);
		}
		
		check_button();
	});
	
	$("#btslideprev").click(function(e)
	{
		if (slide_left<0)
		{
			slide_left+=100;
			$("#slide_text").animate({marginLeft:slide_left+'%'},600);
		}
		
		check_button();
	})
	
	$("#btwork-next").click(function(e) 
	{
		jml_slide=parseInt($("#hid_jml_slide").val());
		if (index_slide<jml_slide-1)
		{
			work_left-=100;
			$("#work-panel").animate({marginLeft:work_left+'%'},600);
			index_slide++;
			//Show Button Prev
			$("#btwork-prev").fadeIn("fast");
		}
		
		if (index_slide>=jml_slide-1)
			$("#btwork-next").fadeOut("fast");
    });
	
	$("#btwork-prev").click(function(e) 
	{
		if (index_slide>0)
		{
			work_left+=100;
			$("#work-panel").animate({marginLeft:work_left+'%'},600);
			index_slide--;
			$("#btwork-next").fadeIn("fast");
		}
		
		if (index_slide<=0)
			$("#btwork-prev").fadeOut("fast");
    });
	
	$("#btlogin").click(function(e) 
	{
    	var username=$("#txtusername").val(); 
		var password=$("#txtpassword").val();
		
		if (username=="")
			$("#error-login").html("Please fill your username first.");
		else if (password=="")
			$("#error-login").html("Please fill your password first.");
		else
		{
			$.ajax({
				url:base_url+"home/do_login",
				data:"id_admin="+username+"&password="+password,
				dataType:"html",
				type:"POST",
				success: function(result)
				{
					switch (parseInt(result))
					{
						case 0:window.location.reload();break;
						case 1:$("#error-login").html("Your username is not registered yet.");break;
						case 2:$("#error-login").html("Your password is incorrect.");break;
						case 3:$("#error-login").html("Your account is not active.");break;
					}				
				}
			});
		}
    });
	
	$(document).keydown(function(e) 
	{
        if(e.keyCode==13)
		{
			$("#btlogin").click();		
		}
    });
});


var timer = $.timer(function() {
	slideNext();
});

timer.set({ time : 6000, autostart : true });

check_button();


//Panel Slider in Events
function load_event_slider()
{
	var pnl=new Array();
	for (var i=0;i<jum_slider;i++)
	{
		pnl.push(new panel_slider("mid-board_"+i,"btleft_"+i,"btright_"+i,"pic"+i));
	}
}

//Check ready state of browser
document.onreadystatechange=function()
{
	if (document.readyState == "complete")
		window.setTimeout(function(){load_event_slider();},loadtime);
};




//Show Settings if admin
if (admin==1)
{
	$("#div-login").show();
    $("#div-login").animate({left:'0'},140);
}
else if (admin==2)
{
	$(".div-settings").fadeIn("fast");
}

//retrieve_slider();
//retrieve_admin();
//retrieve_slider();

