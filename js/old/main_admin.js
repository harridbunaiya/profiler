// JavaScript Document
$(document).ready(function(e)
{
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
	
	handleFilesLogo = function (files,e){
		// alert(files);
		// Traverse throught all files and check if uploaded file type is image	
		var imageType = /image.*/;  
		var file = files[0];
		// check file type
		if (!file.type.match(imageType)) {  
		  show_error("File \""+file.name+"\" is not a valid image file.");
		  return false;	
		} 
		// check file size
		if (parseInt(file.size / 1024) > max_size) {  
		  show_error("File \""+file.name+"\" maximum 700 MB.");
		  return false;	
		} 
		var size=file.size/1024;
		uploadFile(file);
	}
	
	uploadFileLogo = function(file){
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			//$('#right_pic').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/do_change_logo", true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) 
			{
	
			}
			else {
				show_error("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#sp_percent").html("100%");
				  reader = new FileReader();
				  reader.onload = function(e){
						show_error("Logo successfully changed..");
						$('#headlogo').attr('src',e.target.result);
				  }
				  reader.readAsDataURL(file);
				  
				} else {  
				  alert("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			show_error("Your browser doesnt support FileReader object");
		} 		
	}
});

//Upload Web Logo
$(document).ready(function(e)
{
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
	
	handleFilesWebLogo = function (files,e)
	{
		// alert(files);
		// Traverse throught all files and check if uploaded file type is image	
		var imageType = /image.*/;  
		var file = files[0];
		// check file type
		if (!file.type.match(imageType)) {  
		  show_error("File \""+file.name+"\" is not a valid image file.");
		  return false;	
		} 
		// check file size
		if (parseInt(file.size / 1024) > max_size) {  
		  show_error("File \""+file.name+"\" maximum 700 MB.");
		  return false;	
		} 
		var size=file.size/1024;
		uploadFileWebLogo (file);
	}
	
	uploadFileWebLogo = function(file)
	{
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			//$('#right_pic').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/do_change_web_logo", true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) 
			{
	
			}
			else {
				show_error("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  $("#sp_percent").html("100%");
				  reader = new FileReader();
				  reader.onload = function(e){
						show_error("Logo successfully changed..");
						$('#btchangeweblogo').attr('src',e.target.result);
						$('#mid-logo').attr('src',e.target.result);
				  }
				  reader.readAsDataURL(file);
				  
				} else {  
				  alert("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			show_error("Your browser doesnt support FileReader object");
		} 		
	}
});

function retrieve_admin()
{
	$.ajax({
		url:base_url+"home/retrieve_admin",
		dataType:"html",
		type:"GET",
		success: function(result)
		{
			var json=$.parseJSON(result);
			var html="";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img' ><div class='img-polaroid wid_240' ><div class='div-evt' >";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='"+json[i].ID_admin+"'>";
                html+="<i data-id='"+json[i].ID_admin+"' data-title='"+json[i].admin_name+"' class='btedit-admin icon icon-pencil'  data-email='"+json[i].email+"' data-visible='"+json[i].active+"' title='Edit Admin'></i><br/><i ";
				if (json[i].active==1) 
					html+="class='icon-ok-circle' title='Active'";
				else
					html+="class='icon-ban-circle' title='Not active'";
				html+=" ></i></span>";
                html+="<span class='btdelete-admin spclose back-white' data-id='"+json[i].ID_admin+"'>X</span>";
                html+="<span class='events_box' data-id='"+json[i].ID_admin+"' data-title='"+json[i].admin_name+"'>";
				html+=json[i].admin_name;
                html+="<br/>";
                html+=json[i].ID_admin;
                html+="<br/>";
                html+=json[i].email;
                html+="</span></div></div></div>";
			}
			
			$("#admin-scroll").html(html);
			
			$(".btdelete-admin").click(function(e) {
				var id_admin=$(this).attr("data-id");
				
				var conf=confirm("Are you sure want to delete this account?");
				if (conf==true)
				{
					$.ajax({
						url:base_url+"home/remove_admin?id="+id_admin,
						dataType:"html",
						type:"GET",
						success: function(result)
						{
							if (result=="")
							{
								alert("Admin removed successfully.");
								retrieve_admin();
							}
						}
					});
				}
			});
			
			$(".btedit-admin").click(function(e) 
			{
				var id=$(this).attr("data-id");
				var name=$(this).attr("data-title");
				var email=$(this).attr("data-email");
				var visible=$(this).attr("data-visible");
				
				$("#btadd_admin").hide();
				$("#admin-scroll").hide();
				$("#edit_admin").fadeIn("fast");
				
				//set data to component
				$("#txtad_ed_username").val(id);
				$("#txtad_ed_name").val(name);
				$("#txtad_ed_email").val(email);
			});
		}
	});
}


$(document).ready(function(e) 
{
	$("#btsendforgot").click(function(e) {
        var username=$("#txtforgot_username").val();
		var email=$("#txtforgot_email").val();
		var regex=new RegExp("^.+@.+\..+$");
		
		$("#error-forgot").html("");
		if (username=="")
			$("#error-forgot").html("Please fill your username first.");
		else if (email=="")
			$("#error-forgot").html("Please fill your email first.");
		else if (!regex.test(email))
			$("#error-forgot").html("Your email must be like 'operak002@yahoo.com'.");
		else
		{
			$.ajax({
				url:base_url+"home/do_forgot",
				dataType:"html",
				type:"POST",
				data:"username="+username+"&email="+email,
				success: function(result)
				{
					if (parseInt(result)==0)
					{
						show_error("Password has been sent to your email.");
						
						//Clear
						$("#txtforgot_username").val("");
						$("#txtforgot_email").val("");
					}
					else
						$("#error-forgot").html("Your username or email doesn't match in the account.");
				}
			});
		}
    });
	
	$("#btforgot").click(function(e) {
        $("#tbl_forgot").fadeIn("fast");
		$("#tbl_login").hide();
    });
	
	$("#btback_forgot").click(function(e) {
        $("#tbl_login").fadeIn("fast");
		$("#tbl_forgot").hide();
    });
	
	$(".btdelete-admin").click(function(e) {
    	var id_admin=$(this).attr("data-id");
		
		var conf=confirm("Are you sure want to delete this account?");
		if (conf==true)
		{
			$.ajax({
				url:base_url+"home/remove_admin?id="+id_admin,
				dataType:"html",
				type:"GET",
				success: function(result)
				{
					if (result=="")
					{
						alert("Admin removed successfully.");
						retrieve_admin();
					}
				}
			});
		}
    });
	
	$(".btedit-admin").click(function(e) {
		var id=$(this).attr("data-id");
		var name=$(this).attr("data-title");
		var email=$(this).attr("data-email");
		var visible=$(this).attr("data-visible");
		
		$("#btadd_admin").hide();
		$("#admin-scroll").hide();
		$("#edit_admin").fadeIn("fast");
		
		//set data to component
		$("#txtad_ed_username").val(id);
		$("#txtad_ed_name").val(name);
		$("#txtad_ed_email").val(email);
		
		document.getElementById("chkvisible_ed_admin").checked=(visible==1)?true:false;
    });
	
	$("#close-admin").click(function(e) {	
        $("#manage-admin-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-admin-box").hide();},140);
    });
	
	$("#btadd_admin").click(function(e) {
        $("#btadd_admin").hide();
		$("#admin-scroll").hide();
        $("#upload_admin").fadeIn("fast");
    });
	
	$("#btcancel_admin").click(function(e) {
        $("#btadd_admin").fadeIn("fast");
		$("#admin-scroll").fadeIn("fast");
        $("#upload_admin").hide();
    });
	
	$("#btcanceled_admin").click(function(e) {
        $("#btadd_admin").fadeIn("fast");
		$("#admin-scroll").fadeIn("fast");
        $("#edit_admin").hide();
    });
	
	$("#btsaveed_admin").click(function(e) 
	{
    	var username = $("#txtad_ed_username").val();
		var name = $("#txtad_ed_name").val();
		var email = $("#txtad_ed_email").val();
		var visible=(document.getElementById("chkvisible_ed_admin").checked==true)?1:0;
		var regex = new RegExp("^.+@.+\..+$");
		
		if (username=="")
			$("#error-edit-admin").html("Please fill username first."); 
		else if (name=="")
			$("#error-edit-admin").html("Please fill name first.");
		else if (email=="")
			$("#error-edit-admin").html("Please fill email first.");
		else if (!regex.test(email))
			$("#error-edit-admin").html("Please fill the invalid email.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_admin",
				dataType:"html",
				type:"POST",
				data:"username="+username+"&name="+name+"&email="+email+"&visible="+visible,
				success: function(result)
				{
					retrieve_admin();
					//Close
					$("#btcanceled_admin").trigger("click");
				}
			});
		}
    });
	
	$("#btsave_admin").click(function(e) 
	{
    	var username = $("#txtad_username").val();
		var pass = $("#txtad_password").val();
		var name = $("#txtad_name").val();
		var email = $("#txtad_email").val();
		var visible=(document.getElementById("chkad_visible").checked==true)?1:0;
		var regex = new RegExp("^.+@.+\..+$");
		
		if (username=="")
			$("#error-admin").html("Please fill username first."); 
		else if (pass=="")
			$("#error-admin").html("Please fill password first.");
		else if (name=="")
			$("#error-admin").html("Please fill name first.");
		else if (email=="")
			$("#error-admin").html("Please fill email first.");
		else if (!regex.test(email))
			$("#error-admin").html("Please fill the invalid email.");
		else
		{
			$.ajax({
				url:base_url+"home/insert_admin",
				dataType:"html",
				type:"POST",
				data:"username="+username+"&password="+pass+"&name="+name+"&email="+email+"&visible="+visible,
				success: function(result)
				{
					//Clear Insert
					$("#txtad_username").val("");
					$("#txtad_password").val("");
					$("#txtad_name").val("");
					$("#txtad_email").val("");
					
					document.getElementById("chkad_visible").checked==true
					
					//retrieve
					retrieve_admin();
					
					//Close
					$("#btcancel_admin").trigger("click");
				}
			});
		}
    });
	
	
	//----------------------------------------------------
	
	$("#btmanage-admin").click(function(e) {
		collapse_all();
		$("#manage-admin-box").show();
        $("#manage-admin-box").animate({left:'0'},140);
    });
	
	$("#fnchweblogo").change(function(e) 
	{
		var conf=confirm("Are you sure want to change the Web logo?");
		if (conf==true)
		{
			var path=$(this).val();
			if (path!="")
			{
				var imageType = /image.*/;  
				var file = this.files[0];
				
				
				// check file type
				if (!file.type.match(imageType)) {  
				  alert("File \""+file.name+"\" is not a valid image file.");
				  return false;	
				} 
				// check file size
				if (parseInt(file.size / 1024) > max_size) {  
				  alert("File \"" + file.name + "\" maximum 700 MB.");
				  return false;	
				} 
				var size=file.size/1024;
				uploadFileWebLogo (file);
			}
		}
    });
	
	$("#btchangeweblogo").click(function(e) {
    	$("#fnchweblogo").click();    
    });
	
    $("#btchange_logo").click(function(e) {
        $("#fnchlogo").click();
    });
	
	$("#fnchlogo").change(function(e) 
	{
		var conf=confirm("Are you sure want to change the head logo?");
		if (conf==true)
		{
			var path=$(this).val();
			if (path!="")
			{
				var imageType = /image.png/;  
				var file = this.files[0];
				
				
				// check file type
				if (!file.type.match(imageType)) {  
				  alert("File \""+file.name+"\" is not a valid image file.");
				  return false;	
				} 
				// check file size
				if (parseInt(file.size / 1024) > max_size) {  
				  alert("File \"" + file.name + "\" maximum 700 MB.");
				  return false;	
				} 
				var size=file.size/1024;
				uploadFileLogo(file);
			}
		}
    });
	
	$("#btmanage-change").click(function(e) {
		collapse_all();
		$("#div-change-pass").show();
        $("#div-change-pass").animate({left:'0'},140);
    });
	
	$("#close-chpass").click(function(e) {	
        $("#div-change-pass").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-work-box").hide();},140);
    });
	
	$("#btchangepass").click(function(e) 
	{
		var oldpass=$("#txtoldpass").val();
		var newpass=$("#txtnewpass").val();
		var confirmpass=$("#txtconfirm").val();
		
		if (oldpass=="")
			$("#error-chpass").html("Please old password first.");
		else if (newpass=="")
			$("#error-chpass").html("Please new password first.");
		else if (newpass!=confirmpass)
			$("#error-chpass").html("Confirm password doesn't match the new password.");
		else
		{
			$.ajax({
				url:base_url+"home/do_change_pass",
				dataType:"html",
				type:"POST",
				data:"oldpass="+oldpass+"&newpass="+newpass+"&confirm="+confirmpass,
				success: function(result)
				{
					switch(parseInt(result))
					{
						case 0:
							$("#error-chpass").html("Password successfully changed.");
							$("#txtoldpass").val("");
							$("#txtnewpass").val("");
							$("#txtconfirm").val("");
							break;
						case 1:$("#error-chpass").html("Old password doesn't match.");break;
						case 2:$("#error-chpass").html("Confirm password doesn't match the new password.");break;
					}
				}
			});
		}
    });
});