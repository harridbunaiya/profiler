// JavaScript Document
function retrieve_front_evt()
{
	$.ajax({
		url:base_url+"home/retrieve_front_event",
		dataType:"html",
		type:"GET",
		success:function(result)
		{
			console.log(result);
			var part=result.split("<-sp->");
			var json=$.parseJSON(part[0]);
			var json_img=$.parseJSON(part[1]);
			
			var html="";
			jum_slider=0;
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='events title list'><p class='events title'>"+json[i].events_name+"</p></div>";
				html+="<div class='roller-board'>";
				html+="<div class='roller-inner-board'>";
				html+="<div class='left-sign'><img src='"+base_url+"assets/arrow-left.png' id='btleft_"+i+"' class='arrow icon' /></div>";
				html+="<div class='middle-board'>";
				html+="<div id='mid-board_"+i+"'>";
				
				if (json_img[json[i].ID_events]!=null && json_img[json[i].ID_events] !="")
				{
					var json_evt_img=$.parseJSON(json_img[json[i].ID_events]);
					for (var j=0;j<json_evt_img.length;j++)
					{
						html+="<img src='"+base_url+"assets/events/"+json_evt_img[j].imagePath+"'  class='event photo pic"+i+"'/>";
					}
				}
				else
					html+="<center style='margin-top:80px;color:silver;'><h2>No Picture..</h2></center>";
				html+="</div></div>";
				html+="<div class='right-sign'><img src='"+base_url+"assets/arrow-right.png' id='btright_"+i+"' class='arrow icon' /></div><div class='clear'></div></div></div>";
				jum_slider++;
			}
			
			$("#event_section").html(html);
			
			load_event_slider();
			
			$(".photo").click(function(e) {
					var img=$(this).attr("src");
					$("#bg").show();
					$("#pictureevent").fadeIn(); 			
					$("#pictureevent").css("background-image","url('"+img+"')");
					$("#pictureevent").css("left",window.innerWidth/2 - document.getElementById("pictureevent").offsetWidth/2);
					$("#pictureevent").css("top",window.innerHeight/2 - document.getElementById("pictureevent").offsetHeight/2);
					
				});
		}
	});
}

function retrieve_evt()
{
	$.ajax({
		url:base_url+"home/retrieve_event",
		dataType:"html",
		type:"GET",
		success:function(result)
		{
			console.log(result);
			var json=$.parseJSON(result);
			var html="";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img'><div class='img-polaroid wid_240' ><div class='div-evt' >";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='"+json[i].imageID+"'>";
                html+="<img src='"+base_url+"assets/arrow-up.png' data-id='"+json[i].ID_events+"' data-value='"+json[i].events_index+"' class='btevent-up icon mini'/><br/>";
                html+="<img src='"+base_url+"assets/arrow-bottom.png' data-id='"+json[i].ID_events+"' data-value='"+json[i].events_index+"' class='btevent-down icon mini' /><br/>";
                html+="<i data-id='"+json[i].ID_events+"' data-title='"+json[i].events_name+"' class='btedit-event icon icon-pencil' data-desc='"+json[i].events_desc+"' data-date='"+json[i].events_date+"' data-visible='"+json[i].visible+"' title='Edit Title'></i><br/><i ";
				if (json[i].visible==1) 
					html+="class='icon-ok-circle' title='Show'";
				else
					html+="class='icon-ban-circle' title='Hide'";
				html+=" ></i>";
                html+="</span>";
                html+="<span class='btdelete-event spclose back-white' data-id='"+json[i].ID_events+"'>X</span>";
				html+="<span class='events_box' data-id='"+json[i].ID_events+"' data-title='"+json[i].events_name+"'>";
                html+=json[i].events_name;
				html+="</span>";
                html+="</div>";
                html+="</div>";
                html+="</div>";
			}
			
			$("#event-scroll").html(html);
			
			//Add Event
			$(".btdelete-event").click(function(e) 
			{
				var id_evt=$(this).attr("data-id");
				var conf=confirm("Are you sure want to delete this event?");
				if (conf==true)
				{
					$.ajax({
							url:base_url+"home/delete_event?id_event="+id_evt,
							dataType:"html",
							type:"GET",
							success: function(result)
							{
								//Retrieve
								retrieve_evt();
							}
						});
				}
			});
			
			$(".btedit-event").click(function(e) 
			{
				var name=$(this).attr("data-title");
				var desc=$(this).attr("data-desc");
				var date=$(this).attr("data-date");
				var visible=$(this).attr("data-visible");
				var id=$(this).attr("data-id");
				
				$("#btaddevt").hide();
				$("#event-scroll").hide();
				$("#edit_event").fadeIn("fast");
				
				//Set Value
				$("#txtname_ed_evt").val(name);
				$("#txtdesc_ed_evt").val(desc);
				$("#txtdate_ed_evt").val(date);
				$("#hid_id_evt").val(id);
				document.getElementById("chkvisible_ed_evt").checked=(visible==1)?true:false;
			});
			
			$(".btevent-up").click(function(e) 
			{
				var id_events=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_event_up?id="+id_events+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_evt();
						//retrieve_front_work();
					}
				});
			});
			
			$(".btevent-down").click(function(e) 
			{
				var id_events=$(this).attr("data-id");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_event_down?id="+id_events+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_evt();
						//retrieve_front_work();
					}
				});
			});
			
			$(".events_box").click(function(e) 
			{
				var evtname=$(this).attr("data-title");
				var id=$(this).attr("data-id");
		
				$("#hid_id_evt_img").val(id);
				collapse_all();
				$("#manage-events-box").animate({left:'-280'},140);
				window.setTimeout(function(){ $("#manage-events-box").hide();retrieve_evt_img();},140);	
				$("#manage-events-detail-box").show();
				$("#manage-events-detail-box").animate({left:'0'},140);
				$("#lblevtimg").html("<b>Add "+evtname+"'s Image</b>");
				
			});
			
			//Load Front Event Items
			retrieve_front_evt();
		}
	});
}

function retrieve_evt_img()
{
	alert("x");
	var id=$("#hid_id_evt_img").val();
	$.ajax({
		url:base_url+"home/retrieve_event_image?id="+id,
		dataType:"html",
		type:"GET",
		success:function(result)
		{
			console.log(result);
			var json=$.parseJSON(result);
			var html="";
			for (var i=0;i<json.length;i++)
			{
				html+="<div class='slide-img'>";
                html+="<div class='img-polaroid wid' >";
                html+="<div style='background-image:url(\""+base_url+"assets/events/"+json[i].imagePath+"\")' class='div-img'>";
                html+="<span class='spleft back-white-left' style='text-align:center' data-id='"+json[i].detailID+"'>";
                html+="<img src='"+base_url+"assets/arrow-up.png' data-id='"+json[i].detailID+"' data-value='"+json[i].imageIndex+"' data-event='"+json[i].ID_events+"' class='btevtimg-up icon mini'/><br/>";
                html+="<img src='"+base_url+"assets/arrow-bottom.png' data-id='"+json[i].detailID+"' data-value='"+json[i].imageIndex+"' data-event='"+json[i].ID_events+"' class='btevtimg-down icon mini' /><br/>";
                html+="<i data-id='"+json[i].detailID+"' data-title='"+json[i].imageTitle+"' class='btedit-event-img icon icon-pencil' data-desc='"+json[i].imageDesc+"' title='Edit Info'></i><br/>";
                html+="<i data-id='"+json[i].detailID+"' class='btchevtimg icon icon-picture' title='Edit Picture'></i>";
				
				html+="<input type='file' class='fnchevtimg' data-value='"+json[i].imagePath+"' data-id='"+json[i].detailID+"' accept='image/*' /></span>";
                html+="<span class='btdelete-event-image spclose back-white' data-value='"+json[i].imagePath+"' data-id='"+json[i].detailID+"'>X</span></div></div>";
                html+="<span class='splabel' data-id='"+json[i].detailID+"'>"+json[i].imageTitle+"</span></div>";
			}
			
			$("#event-image-scroll").html(html);
			
			$(".btevtimg-up").click(function(e) 
			{
				var id_detail=$(this).attr("data-id");
				var id_evt=$(this).attr("data-event");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_event_image_up?id="+id_detail+"&id_evt="+id_evt+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_evt_img();
						//retrieve_front_work();
					}
				});
			});
			
			$(".btevtimg-down").click(function(e) 
			{
				var id_detail=$(this).attr("data-id");
				var id_evt=$(this).attr("data-event");
				var index=$(this).attr("data-value");
				$.ajax({
					url:base_url+"home/move_event_image_down?id="+id_detail+"&id_evt="+id_evt+"&index="+index,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						retrieve_evt_img();
						//retrieve_front_work();
					}
				});
			});
			
			$(".btedit-event-img").click(function(e) {
                var name=$(this).attr("data-title");
				var desc=$(this).attr("data-desc");
				var id=$(this).attr("data-id");
				
				$("#btaddevtimg").hide();
				$("#event-image-scroll").hide();
				$("#edit_event_image").fadeIn("fast");
				
				//Set Value
				$("#txttitle_ed_evt_img").val(name);
				$("#txtdesc_ed_evt_img").val(desc);
				$("#hid_id_ed_evt_img").val(id);
            });
			
			$(".btdelete-event-image").click(function(e) 
			{
				var id=$(this).attr("data-id");
				var path=$(this).attr("data-value");
				var conf=confirm("Are you sure want to delete this image?");
				if (conf==true)
				{
					$.ajax({
							url:base_url+"home/delete_event_image?id="+id+"&path="+path,
							dataType:"html",
							type:"GET",
							success: function(result)
							{
								//Retrieve
								retrieve_evt_img();
							}
						});
				}
			});
			
			$(".fnchevtimg").change(function(e)
			{
				var id=$(this).attr("data-id");
				var path=$(this).attr("data-value");
				if (path!="")
				{
					var imageType = /image.*/;  
					var file = this.files[0];
					
					
					// check file type
					if (!file.type.match(imageType)) {  
					  alert("File \""+file.name+"\" is not a valid image file.");
					  return false;	
					} 
					// check file size
					if (parseInt(file.size / 1024) > max_size) {  
					  alert("File \""+file.name+"\" maximum 700 MB.");
					  return false;	
					} 
					var size=file.size/1024;
					uploadFileEventImageEdit(file,id,path);
				}
			});
			
			$(".btchevtimg").click(function(e) {
				var id=$(this).attr("data-id");
				$(".fnchevtimg[data-id="+id+"]").click();
			});
			
			//Load Front Event Items
			retrieve_front_evt();
		}
	});
}

$(document).ready(function(e) 
{	
	$("#btsaveimgevt").click(function(e) 
	{
		var id=$("#hid_id_ed_evt_img").val();
		var title=$("#txttitle_ed_evt_img").val();
		var desc=$("#txtdesc_ed_evt_img").val();
		
		if (title=="")
			$("#error-edit-events-image").html("Please fill the image's title first.");
		else
		{
			$.ajax({
				url:base_url+"home/update_event_image_title_desc",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&title="+title+"&desc="+desc,
				success: function(result)
				{
					//Retrieve
					retrieve_evt_img();
					
					//Close Input 
					$("#btcanceledimgevt").trigger("click");
				}
			});
		}
	});

	$("#btcanceledimgevt").click(function(e) {
		$("#btaddevtimg").fadeIn("fast");
		$("#event-image-scroll").fadeIn("fast");
        $("#edit_event_image").hide();
    });
		
	$(".btevent-up").click(function(e) 
	{
		var id_events=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_event_up?id="+id_events+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_evt();
				//retrieve_front_work();
			}
		});
	});
	
	$(".btevent-down").click(function(e) 
	{
		var id_events=$(this).attr("data-id");
		var index=$(this).attr("data-value");
		$.ajax({
			url:base_url+"home/move_event_down?id="+id_events+"&index="+index,
			dataType:"html",
			type:"GET",
			success: function(result)
			{
				retrieve_evt();
				//retrieve_front_work();
			}
		});
	});
	
	$(".events_box").click(function(e) 
	{
		var evtname=$(this).attr("data-title");
		var id=$(this).attr("data-id");
		
		$("#hid_id_evt_img").val(id);
       	collapse_all();
		$("#manage-events-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-events-box").hide();retrieve_evt_img();},140);	
		$("#manage-events-detail-box").show();
        $("#manage-events-detail-box").animate({left:'0'},140);
		$("#lblevtimg").html("<b>Add "+evtname+"'s Image</b>");
    });
	
    $("#btedit-events").click(function(e) {
        collapse_all();
		$("#manage-events-box").show();
        $("#manage-events-box").animate({left:'0'},140);
    });
	
	$("#close-event").click(function(e) {
        $("#manage-events-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-events-box").hide();},140);		
    });
	
	$("#back-event-image").click(function(e) {
        $("#manage-events-detail-box").animate({left:'-280'},140);
		window.setTimeout(function(){ $("#manage-events-detail-box").hide();},140);
		collapse_all();
		$("#manage-events-box").show();
        $("#manage-events-box").animate({left:'0'},140);
    });
	
	//Add Events Detail
	$("#btaddevtimg").click(function(e) 
	{
      	$("#btaddevtimg").hide();
		$("#event-image-scroll").hide();
        $("#upload_event_image").fadeIn("fast");
		
		/*Clear Value*/
		/*$("#txtname_evt").val("");
		document.getElementById("chkvisible_evt").checked=true;*/
    });
	
	$("#btcancelimgevt").click(function(e) {
		$("#btaddevtimg").fadeIn("fast");
		$("#event-image-scroll").fadeIn("fast");
        $("#upload_event_image").hide();
    });
	
	
	//Add Events
	$("#btaddevt").click(function(e) {
		$("#btaddevt").hide();
		$("#event-scroll").hide();
        $("#upload_event").fadeIn("fast");
		
		/*Clear Value*/
		$("#txtname_evt").val("");
		document.getElementById("chkvisible_evt").checked=true;
    });	
	
	$("#btcancelevt").click(function(e) {
		$("#btaddevt").fadeIn("fast");
		$("#event-scroll").fadeIn("fast");
        $("#upload_event").hide();
    });
	
	$("#btcanceledevt").click(function(e) {
		$("#btaddevt").fadeIn("fast");
		$("#event-scroll").fadeIn("fast");
        $("#edit_event").hide();
    });	
	
	$(".btedit-event").click(function(e) {
		var name=$(this).attr("data-title");
		var desc=$(this).attr("data-desc");
		var date=$(this).attr("data-date");
		var visible=$(this).attr("data-visible");
		var id=$(this).attr("data-id");
		
        $("#btaddevt").hide();
		$("#event-scroll").hide();
        $("#edit_event").fadeIn("fast");
		
		//Set Value
		$("#txtname_ed_evt").val(name);
		$("#txtdesc_ed_evt").val(desc);
		$("#txtdate_ed_evt").val(date);
		$("#hid_id_evt").val(id);
		document.getElementById("chkvisible_ed_evt").checked=(visible==1)?true:false;
    });
	
	$("#btsaveevt").click(function(e) 
	{
		var id=$("#hid_id_evt").val();
		var name=$("#txtname_ed_evt").val();
		var date=$("#txtdate_ed_evt").val();
		var desc=$("#txtdesc_ed_evt").val();
		var visible=(document.getElementById("chkvisible_ed_evt").checked==true)?1:0;
		
		if (name=="")
			$("#error-edit-events").html("Please fill the event's name first.");
		else if (date=="")
			$("#error-edit-events").html("Please fill the event's date first.");
		else
		{
			$.ajax({
				url:base_url+"home/edit_event",
				dataType:"html",
				type:"POST",
				data:"id="+id+"&name="+name+"&date="+date+"&desc="+desc+"&visible="+visible,
				success: function(result)
				{
					//Retrieve
					retrieve_evt();
					
					//Close Input 
					$("#btcanceledevt").trigger("click");
				}
			});
		}
	});
	
	$(".photo").click(function(e) 
	{
		var img=$(this).attr("src");
		$("#bg").show();
		$("#pictureevent").fadeIn(); 			
		$("#pictureevent").css("background-image","url('"+img+"')");
		$("#pictureevent").css("left",window.innerWidth/2 - document.getElementById("pictureevent").offsetWidth/2);
		$("#pictureevent").css("top",window.innerHeight/2 - document.getElementById("pictureevent").offsetHeight/2);
		
	});
	
	$("#btcreateevt").click(function(e) 
	{
		var name=$("#txtname_evt").val();
		var date=$("#txtdate_evt").val();
		var desc=$("#txtdesc_evt").val();
		var visible=(document.getElementById("chkvisible_evt").checked==true)?1:0;
		
		if (name=="")
			$("#error-events").html("Please fill the event's name first.");
		else if (date=="")
			$("#error-events").html("Please fill the event's date first.");
		else
		{
			$.ajax({
				url:base_url+"home/insert_event",
				dataType:"html",
				type:"POST",
				data:"name="+name+"&date="+date+"&desc="+desc+"&visible="+visible,
				success: function(result)
				{
					//Clear Input 
					$("#txtname_evt").val("");
					$("#txtdate_evt").val("");
					$("#txtdesc_evt").val("");
					
					//Retrieve
					retrieve_evt();
					
					//Close Input 
					$("#btcancelevt").trigger("click");
				}
			});
		}
    });
	
	$(".btdelete-event").click(function(e) 
	{
		var id_evt=$(this).attr("data-id");
		var conf=confirm("Are you sure want to delete this event?");
		if (conf==true)
		{
			$.ajax({
					url:base_url+"home/delete_event?id_event="+id_evt,
					dataType:"html",
					type:"GET",
					success: function(result)
					{
						//Retrieve
						retrieve_evt();
					}
				});
		}
    });
	
	$(".date-text").datepicker();
});

$(document).ready(function(e)
{
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
	
	uploadFileEventImageEdit = function(file, data_id,p)
	{
		var id=data_id;
		var path=p;
		
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){

		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/update_event_image?id="+id+"&path="+path, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {

			}
			else {
				alert("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  alert("Changes saved successfully.");
			      retrieve_evt_img();
				} else {  
				  alert("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			alert("Your browser doesnt support FileReader object");
		} 		
	}
});

$(document).ready(function(e)
{
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
	
	uploadFileEventImage = function(file)
	{
		var title=$("#txttitle_evt_img").val();
		var desc=$("#txtdesc_evt_img").val();
		var id=$("#hid_id_evt_img").val();
		
		// check if browser supports file reader object 
		if (typeof FileReader !== "undefined"){
		//alert("uploading "+file.name);  
		reader = new FileReader();
		reader.onload = function(e){
			//alert(e.target.result);
			//$('#right_pic').attr('src',e.target.result);
		}
		reader.readAsDataURL(file);
		
		xhr = new XMLHttpRequest();
		xhr.open("post", base_url+"home/insert_evt_img?title="+title+"&desc="+desc+"&id="+id, true);
		
		xhr.upload.addEventListener("progress", function (event) {
			console.log(event);
			if (event.lengthComputable) {
				//$("#progress").css("width",(event.loaded / event.total) * 100 + "%");
				
				//alert(((event.loaded / event.total) * 100).toFixed() + "%");
				//$(".percents").html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
				
				//$("#percent_img").html(((event.loaded / event.total) * 100).toFixed() + "%");
				//alert((parseInt(event.loaded / 1024)).toFixed(0));
				//$(".up-done").html((parseInt(event.loaded / 1024)).toFixed(0));

			}
			else {
				$("#error-event-image").html("Failed to compute file upload length");
			}
		}, false);
		
		xhr.onreadystatechange = function (oEvent) 
		{  
			 if (xhr.readyState === 4) {  
				if (xhr.status === 200) 
				{  
				  //$("#progress").css("width","100%");
				  //$(".percents").html("100%");
				  //$("#percent").html("100%");
				  //alert((parseInt(file.size / 1024)).toFixed(0));
				  //$(".up-done").html();
				  //Clear Input
				  $("#txttitle_evt_img").val("");
				  $("#txtdesc_evt_img").val("")
				  $("#fnevtimg").val("");
				  
				  retrieve_evt_img();
				  $("#btcancelimgevt").trigger("click");

				} else {  
				  $("#error-event-image").html("Error "+ xhr.statusText);  
				}  
			  }  
			};  
			
			// Set headers
			xhr.setRequestHeader("X-File-Name", file.fileName);
			xhr.setRequestHeader("X-File-Size", file.fileSize);
			xhr.setRequestHeader("X-File-Type", file.type);
			
			// Send the file (doh)
			xhr.send(file);
		
		}else{
			$("#error-event-image").html("Your browser doesnt support FileReader object");
		} 		
	}
	
	$("#btcreateimgevt").click(function(e) 
	{
		var filename=$("#fnevtimg").val();
		var title=$("#txttitle_evt_img").val();
		var desc=$("#txtdesc_evt_img").val();
		
		//Clean error
		$("#error-event-image").html("");
		if (filename=="")
			$("#error-event-image").html("Please attach the file first.");
		else if (title=="")
			$("#error-event-image").html("Please fill the title first.");
		else 
		{	
			var imageType = /image.*/;  
			var file = document.getElementById("fnevtimg").files[0];
			
			
			// check file type
			if (!file.type.match(imageType)) {  
			  $("#error-event-image").html("File \""+file.name+"\" is not a valid image file.");
			  return false;	
			} 
			// check file size
			if (parseInt(file.size / 1024) > max_size) {  
			  $("#error-event-image").html("File \""+file.name+"\" maximum 700 MB.");
			  return false;	
			} 
			var size=file.size/1024;
			uploadFileEventImage(file);
		}
    });
});