// JavaScript Document
/*Slider - Event*/
function panel_slider(board,left,right,pic)
{
	this.jml_slide=1;
	
	//Count total
	var pictures=$("."+pic);
	this.total_width=0;
	for (var i=0;i<pictures.length;i++)
	{
		this.total_width+=pictures[i].offsetWidth;
	}

	this.board_width=$("#"+board).width();
	
	this.max_slide=Math.ceil(this.total_width/this.board_width);
	this.panel_left=0;
	this.board=board;
	this.left=left;
	this.right=right;
	
	
	this.slide_right=slide_right;
	this.slide_left=slide_left;
	
	document.getElementById(left).style.display="none";
	var that=this;
	
	document.getElementById(right).onclick=function(){that.slide_right();};
	document.getElementById(left).onclick=function(){that.slide_left();};
	
	if (that.max_slide<=1) $("#"+right).hide();
	function slide_right() 
	{
		/* Slide Event*/
		if (this.jml_slide<this.max_slide)
		{
			$("#"+left).fadeIn("fast");
			this.panel_left-=100;
			$("#"+this.board).animate({marginLeft:this.panel_left+'%'},600);
			this.jml_slide++;
		}
		
		if (this.jml_slide>=this.max_slide)
			$("#"+right).fadeOut("fast");
	}
	
	function slide_left() 
	{
		/* Slide Event*/
		if (this.jml_slide>1)
		{
			$("#"+right).fadeIn("fast");
			this.panel_left+=100;
			$("#"+this.board).animate({marginLeft:this.panel_left+'%'},600);
			this.jml_slide--;
		}
		
		if (this.jml_slide<=1)
			$("#"+left).fadeOut("fast");
	}
	
}