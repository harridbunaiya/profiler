-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2020 at 03:57 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `profiler`
--

-- --------------------------------------------------------

--
-- Table structure for table `msabout`
--

CREATE TABLE `msabout` (
  `id` int(11) NOT NULL,
  `quote_text` varchar(200) DEFAULT '',
  `simple_quote` varchar(500) DEFAULT '',
  `content` longtext,
  `content2` text,
  `picture` varchar(200) DEFAULT '',
  `box1` varchar(50) DEFAULT NULL,
  `box2` varchar(50) DEFAULT NULL,
  `box3` varchar(50) DEFAULT NULL,
  `box4` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msabout`
--

INSERT INTO `msabout` (`id`, `quote_text`, `simple_quote`, `content`, `content2`, `picture`, `box1`, `box2`, `box3`, `box4`) VALUES
(1, 'Our mission and values', 'Lorem ipsum dolor sit amet consectetur adipiscing elitsed do eiusmod tempor incididunt utlabore et dolore magna aliqua.\nUtenim ad minim veniam quisolor in reprehenderite  nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequate.', 'AGUNG TENT dengan nama perusahaan PT. Triputra Darma Dyaksa, (sebelumnya bernama CV. Agung Elektric) bergerak dibidang usaha jasa sewa Tenda Roder, Tenda Dome Geodesic, Tenda Sarnafil, dan Flooring sejak 2009.\n\nSelain sewa, AGUNG TENT juga memproduksi Tenda Roder dengan ukuran standard ataupun custom untuk keperluan gudang, marketing gallery, event durasi tahunan, dll.\n\ntest', 'AGUNG TENT selalu mengutamakan kepuasan pelanggan dengan turut serta mensukseskan event-event yang diselanggarakan. Tim kerja AGUNG TENT dibekali skill yang mumpuni sehingga hasil kerja cepat, tepat, rapi, dan efektif.\n\nAGUNG TENT melakukan maintanence Tenda Roder dan produk lainnya dengan rutin. Dengan sarana dan prasana produksi milik sendiri menjadikan produk AGUNG TENT selalu bersih dan terawat.', '843caf7e6190b37530e00261b8241f8d.jpg', '47', '250', '150', '150');

-- --------------------------------------------------------

--
-- Table structure for table `msabout_quote`
--

CREATE TABLE `msabout_quote` (
  `id` int(11) NOT NULL,
  `quote_text` varchar(255) DEFAULT NULL,
  `content` text,
  `title1` varchar(100) DEFAULT NULL,
  `subtitle1` varchar(255) DEFAULT NULL,
  `title2` varchar(100) DEFAULT NULL,
  `subtitle2` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msabout_quote`
--

INSERT INTO `msabout_quote` (`id`, `quote_text`, `content`, `title1`, `subtitle1`, `title2`, `subtitle2`) VALUES
(1, 'Butuh Tenda Roder untuk Permanen / Gudang?', 'AGUNG TENT juga memproduksi dan menjual Tenda Roder untuk keperluan gudang sementara, marketing gallery, ataupun keperluan lainnya. Harga jual Tenda Roder akan jauh lebih murah daripada sewa dalam jangka waktu bulanan. Tenda Roder juga tidak memerlukan IMB dalam pembangunannya. AGUNG TENT memberikan pilihan Tenda Roder Baru dan Tenda Roder Second.Tenda Roder Second menggunakan rangka second tapi menggunakan atap dan dinding yang baru.', '10', 'Years Established', '250', 'Clients');

-- --------------------------------------------------------

--
-- Table structure for table `msabout_us`
--

CREATE TABLE `msabout_us` (
  `id` int(11) NOT NULL,
  `quote_text` varchar(255) DEFAULT NULL,
  `simple_quote` varchar(255) DEFAULT NULL,
  `content` text,
  `picture` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msabout_us`
--

INSERT INTO `msabout_us` (`id`, `quote_text`, `simple_quote`, `content`, `picture`) VALUES
(1, 'We’ve Been Building For Over 10 Years', 'Iskandar - 0812 9830 427\nIan - 08567920165/081286508055 \nAgung 0821 1607 0610\n', 'AGUNG TENT selalu mengutamakan kepuasan pelanggan dengan turut serta mensukseskan event-event yang diselanggarakan. Tim kerja AGUNG TENT dibekali skill yang mumpuni sehingga hasil kerja cepat, tepat, rapi, dan efektif. \n\nAGUNG TENT melakukan maintanence Tenda Roder dan produk lainnya dengan rutin. Dengan sarana dan prasana produksi milik sendiri menjadikan produk AGUNG TENT selalu bersih dan terawat', '60407912f6ff9625cfc61985a25d24e2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `msadmin`
--

CREATE TABLE `msadmin` (
  `ID_admin` varchar(100) NOT NULL,
  `pass` varchar(100) DEFAULT '',
  `admin_name` varchar(80) DEFAULT '',
  `email` varchar(80) DEFAULT '',
  `created_date` datetime DEFAULT NULL,
  `active` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msadmin`
--

INSERT INTO `msadmin` (`ID_admin`, `pass`, `admin_name`, `email`, `created_date`, `active`) VALUES
('operag', 'NFkCHEdCh/AW7lT4IgnotdHl0phXA2nkq4rC1Welo3cfPh8eom8gq3fWKOgGzF6cni/ZVCHIf14HoxEBWdf7dA==', 'Administrator', 'operak004@gmail.com', '2013-10-31 08:21:37', 1),
('administrator', 'RRrU4Zid+ofCjlZZy15xSTIqpz0R+WKhUNLIOe8aQcWd/ePrTiXvYSziHfAXmudlcZ4M55VYPAPtLPaYlXYsVw==', 'Administrator', 'operak004@gmail.com', '2018-03-10 08:13:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `msblog`
--

CREATE TABLE `msblog` (
  `ID_blog` int(11) NOT NULL,
  `ID_blogcategory` int(11) NOT NULL,
  `ID_tags` int(11) NOT NULL,
  `title` varchar(250) DEFAULT '',
  `image` varchar(250) DEFAULT '',
  `link` varchar(250) DEFAULT '',
  `creator` varchar(250) DEFAULT '',
  `created_date` datetime DEFAULT NULL,
  `visible` smallint(6) DEFAULT '0',
  `blog_text` longtext
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msblog`
--

INSERT INTO `msblog` (`ID_blog`, `ID_blogcategory`, `ID_tags`, `title`, `image`, `link`, `creator`, `created_date`, `visible`, `blog_text`) VALUES
(1, 2, 0, 'Slide Image Post.', '92da13bd5d09b194943cd6f6b7d31966.jpg', '', 'Tanjordy', '2016-09-27 07:26:28', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.'),
(22, 1, 0, 'The complete solution.', '6cb40e2ef88049fea2a43ae9a34949d0.jpg', '', 'TanJordy', '2018-03-19 02:05:19', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.'),
(23, 3, 0, 'Young, Sweet and only Seventeen', 'd9590097bcb086329830efa719da7097.jpg', '', 'TanJordy', '2018-03-22 09:16:15', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.'),
(24, 2, 0, 'Another Beginning', '4bb0aa00fb2a79d6aa5e1a05c968c656.jpg', '', 'Tanjordy', '2018-03-30 10:40:46', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.'),
(26, 2, 0, 'Young, Sweet and only', '8e98f1bf1d28643210a8f9b7348f18b4.jpg', '123', 'admin', '2019-06-16 00:00:00', 1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\n'),
(37, 2, 0, '1', '92da13bd5d09b194943cd6f6b7d31966.jpg', '', 'Tanjordy', '2016-09-27 07:26:28', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.'),
(38, 1, 0, '2', '6cb40e2ef88049fea2a43ae9a34949d0.jpg', '', 'TanJordy', '2018-03-19 02:05:19', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.'),
(39, 3, 0, '3', 'd9590097bcb086329830efa719da7097.jpg', '', 'TanJordy', '2018-03-22 09:16:15', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.'),
(40, 2, 0, '4', '4bb0aa00fb2a79d6aa5e1a05c968c656.jpg', '', 'Tanjordy', '2018-03-30 10:40:46', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.'),
(41, 2, 0, '5', '8e98f1bf1d28643210a8f9b7348f18b4.jpg', '123', 'admin', '2019-06-16 00:00:00', 1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `msblogcategory`
--

CREATE TABLE `msblogcategory` (
  `ID_blogcategory` int(11) NOT NULL,
  `seq_no` int(11) NOT NULL,
  `blogcategory_code` varchar(100) NOT NULL,
  `blogcategory_name` varchar(150) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msblogcategory`
--

INSERT INTO `msblogcategory` (`ID_blogcategory`, `seq_no`, `blogcategory_code`, `blogcategory_name`, `is_active`, `created_date`) VALUES
(1, 1, 'design', 'Design', 1, '2019-06-16 00:00:00'),
(2, 2, 'photos', 'Photos', 1, '2019-06-16 00:00:00'),
(3, 3, 'videos', 'Videos', 1, '2019-06-16 00:00:00'),
(4, 4, 'lifestyle', 'Lifestyle', 1, '2019-06-16 00:00:00'),
(5, 6, 'technology', 'Technology', 1, '2019-06-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `msblogtags`
--

CREATE TABLE `msblogtags` (
  `ID_tags` int(11) NOT NULL,
  `seq_no` int(11) NOT NULL,
  `tags_code` varchar(100) NOT NULL,
  `tags_name` varchar(150) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msblogtags`
--

INSERT INTO `msblogtags` (`ID_tags`, `seq_no`, `tags_code`, `tags_name`, `is_active`, `created_date`) VALUES
(1, 1, 'corporate', 'Corporate', 1, '2019-06-16 00:00:00'),
(2, 2, 'theme', 'Theme', 0, '2019-06-16 00:00:00'),
(3, 3, 'css3', 'CSS 3', 1, '2019-06-16 00:00:00'),
(4, 4, 'premium', 'Premium', 1, '2019-06-16 00:00:00'),
(5, 5, 'html5', 'HTML 5', 1, '2019-06-16 00:00:00'),
(6, 6, 'business', 'Business', 1, '2019-06-16 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mscategory`
--

CREATE TABLE `mscategory` (
  `ID_category` int(11) NOT NULL,
  `seq_no` int(11) DEFAULT '0',
  `category_code` varchar(100) DEFAULT '',
  `category_name` varchar(150) DEFAULT '',
  `is_active` int(11) DEFAULT '0',
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mscategory`
--

INSERT INTO `mscategory` (`ID_category`, `seq_no`, `category_code`, `category_name`, `is_active`, `created_date`) VALUES
(1, 1, 'cat-1', 'Tenda Dome', 1, '2016-09-18 01:55:38'),
(2, 2, 'cat-2', 'Tenda Roder', 1, '2016-09-18 01:55:46'),
(3, 3, 'cat-3', 'Flooring', 1, '2016-09-18 01:55:46'),
(15, 4, 'test', 'test', 1, '2020-03-10 15:09:09');

-- --------------------------------------------------------

--
-- Table structure for table `msconfig`
--

CREATE TABLE `msconfig` (
  `id` int(11) NOT NULL,
  `logo_dark` varchar(250) DEFAULT '',
  `logo_light` varchar(250) DEFAULT '',
  `title` varchar(250) DEFAULT '',
  `favicon` varchar(250) DEFAULT '',
  `why_pict` varchar(250) DEFAULT NULL,
  `customer_pict` varchar(250) DEFAULT NULL,
  `video_pict` varchar(250) DEFAULT NULL,
  `video_text` varchar(2000) DEFAULT NULL,
  `video_link` varchar(250) DEFAULT NULL,
  `cv_path` varchar(250) DEFAULT NULL,
  `blog_link` varchar(250) DEFAULT NULL,
  `team_text` varchar(2000) DEFAULT NULL,
  `like_text` varchar(2000) DEFAULT NULL,
  `facebook_link` varchar(250) DEFAULT NULL,
  `twitter_link` varchar(250) DEFAULT NULL,
  `google_link` varchar(250) DEFAULT NULL,
  `linkedin_link` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msconfig`
--

INSERT INTO `msconfig` (`id`, `logo_dark`, `logo_light`, `title`, `favicon`, `why_pict`, `customer_pict`, `video_pict`, `video_text`, `video_link`, `cv_path`, `blog_link`, `team_text`, `like_text`, `facebook_link`, `twitter_link`, `google_link`, `linkedin_link`) VALUES
(1, 'logo-dark.png', 'logo-light.png', 'Edo Andrefson', 'favicon.png', '860220841e99c2e949f1e1ec003aa0b2.jpg', 'customer.jpg', 'video.jpg', 'Lihat video lebih lengkap Richie The Farmer', 'b6d37cdfe07574d2337b69232f9a310e.mp4', 'cv.pdf', 'http://localhost:8080/tanjordy/blog', 'Meet our professional team members', 'Classical Latin literature from 45 BC, making it over 2000 years old Richard McClintock The generated.', 'https://google.com', 'https://wa.me/6283813066993?text=Halo, apakah ada yang bisa kami bantu ?', 'https://instagram.com', 'undefined');

-- --------------------------------------------------------

--
-- Table structure for table `mscontact`
--

CREATE TABLE `mscontact` (
  `id` int(11) NOT NULL,
  `email` varchar(100) DEFAULT '',
  `email2` varchar(100) DEFAULT '',
  `contactMsg` varchar(500) DEFAULT '',
  `mp1` varchar(100) DEFAULT '',
  `location` text,
  `langitude` varchar(12) DEFAULT NULL,
  `latitude` varchar(12) DEFAULT NULL,
  `corporation_name` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mscontact`
--

INSERT INTO `mscontact` (`id`, `email`, `email2`, `contactMsg`, `mp1`, `location`, `langitude`, `latitude`, `corporation_name`) VALUES
(1, 'hello@tendaguna.com', 'undefined', 'Open Everyday a\na', '+62 811 2220 205', 'jalan H. Muchtar Raya No.165 Kreo, Larangan, Tangerang, Banten. 15156', '106.57536506', '-6.072066463', 'Open Everyday');

-- --------------------------------------------------------

--
-- Table structure for table `mscustomer`
--

CREATE TABLE `mscustomer` (
  `ID_customer` int(11) NOT NULL,
  `seq_no` int(11) DEFAULT '0',
  `brand_name` varchar(250) DEFAULT '',
  `brand_path` varchar(250) DEFAULT '',
  `is_active` int(11) DEFAULT '0',
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mscustomer`
--

INSERT INTO `mscustomer` (`ID_customer`, `seq_no`, `brand_name`, `brand_path`, `is_active`, `created_date`) VALUES
(2, 2, 'cubico', 'eba957b2f07bf2e39a14da6664b270f3.png', 1, '2016-09-18 07:04:17'),
(3, 1, 'iame1', '1e6255bc1968ec6a17ea5b0970f5afe9.png', 1, '2016-09-18 07:04:17'),
(4, 3, 'hexagon', '57d989f72bcbcb4c6e67eb8de4574fad.png', 1, '2016-09-18 07:04:17'),
(5, 4, 'coffee', 'fa7f8cd517a5b69fbf0ad1904ddf5fe2.png', 1, '2016-09-18 07:04:17'),
(6, 5, 'mates', '8e1a2334822b5c91eba01ae02d06dad6.png', 1, '2016-09-18 07:04:17'),
(11, 7, 'iame', 'ee29192190f3cd2b76f51c951500c469.png', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `msevents`
--

CREATE TABLE `msevents` (
  `ID_events` int(10) NOT NULL,
  `events_name` varchar(250) DEFAULT '',
  `events_img` varchar(250) DEFAULT '',
  `events_desc` longtext,
  `events_date` datetime DEFAULT NULL,
  `events_index` int(11) DEFAULT '0',
  `visible` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msevents`
--

INSERT INTO `msevents` (`ID_events`, `events_name`, `events_img`, `events_desc`, `events_date`, `events_index`, `visible`) VALUES
(1, 'Professional Photography', 'fa-camera', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.', NULL, 2, 1),
(2, 'Web Design', 'fa-paper-plane', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.', NULL, 1, 1),
(3, 'Web Development', 'fa-search', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.', NULL, 3, 1),
(4, 'Interface Ux', 'fa-laptop', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.', NULL, 6, 1),
(5, 'Android Apps', 'fa-file', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.', NULL, 4, 1),
(6, 'Iphone Apps', 'fa-phone', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.', NULL, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `msfact`
--

CREATE TABLE `msfact` (
  `ID_fact` int(11) NOT NULL,
  `bg_pict` varchar(250) NOT NULL DEFAULT '0',
  `fact_name` varchar(250) NOT NULL,
  `awards` int(11) DEFAULT '0',
  `clients` int(11) DEFAULT '0',
  `projects` int(11) DEFAULT '0',
  `teams` int(11) DEFAULT '0',
  `seq_no` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msfact`
--

INSERT INTO `msfact` (`ID_fact`, `bg_pict`, `fact_name`, `awards`, `clients`, `projects`, `teams`, `seq_no`, `is_active`, `created_date`) VALUES
(1, '1.jpg', 'partner 1', 1, 0, 0, 0, 8, 1, '2016-09-17 23:32:59'),
(2, '2.jpg', 'partner 2', 0, 0, 0, 0, 1, 1, NULL),
(3, '3.jpg', 'partner 3', 0, 0, 0, 0, 3, 1, NULL),
(4, '4.jpg', 'partner 4', 0, 0, 0, 0, 5, 1, NULL),
(5, '5.jpg', 'partner 5', 0, 0, 0, 0, 6, 1, NULL),
(6, '6.jpg', 'partner 6', 0, 0, 0, 0, 9, 1, NULL),
(7, '7.jpg', 'partner 7', 0, 0, 0, 0, 7, 1, NULL),
(8, '8.jpg', 'partner 8', 0, 0, 0, 0, 2, 1, NULL),
(12, '65f44742e25ddcf880933760e6a0a43e.png', 'test', 0, 0, 0, 0, 4, 1, '2019-06-26 20:49:17');

-- --------------------------------------------------------

--
-- Table structure for table `mshows`
--

CREATE TABLE `mshows` (
  `ID_hows` int(11) NOT NULL,
  `seq_no` smallint(6) DEFAULT '0',
  `how_pict` varchar(150) DEFAULT '',
  `how_title` varchar(150) DEFAULT '',
  `how_subtitle` varchar(250) NOT NULL,
  `how_text` varchar(250) DEFAULT '',
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mshows`
--

INSERT INTO `mshows` (`ID_hows`, `seq_no`, `how_pict`, `how_title`, `how_subtitle`, `how_text`, `created_date`) VALUES
(2, 1, 'fa-briefcase', 'Professional', 'Professional', 'Tenaga kerja professional dan terlatih, cara kerja efektif dan efisien dalam membangun Tenda Roder', '2016-09-17 10:45:09'),
(4, 2, 'fa-clock-o', 'Berpengalaman', 'Berpengalaman', 'Agung Tent telah berpengalaman menyewakan Tenda Roder selama 10 tahun melayani berbagai event di kota-kota besar di Indonesia', '2016-09-17 10:46:01'),
(15, 3, 'fa-check', ' Berkualitas', ' Berkualitas', 'Kami selalu berusaha memenuhi ekspetasi pelaanggan dengan Tenda Roder yang berkualitas, bersih, dan kuat', '2018-03-13 12:06:09'),
(16, 4, 'fa-bookmark', 'Harga Kompetitif', 'Harga Kompetitif', 'Kami selalu berusaha memenuhi ekspetasi pelaanggan dengan Tenda Roder yang berkualitas, bersih, dan kuat', '2018-03-13 12:06:09');

-- --------------------------------------------------------

--
-- Table structure for table `msicon`
--

CREATE TABLE `msicon` (
  `ID_icon` int(11) NOT NULL,
  `icon_class` varchar(120) DEFAULT '',
  `icon_name` varchar(120) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msicon`
--

INSERT INTO `msicon` (`ID_icon`, `icon_class`, `icon_name`) VALUES
(74, 'fa-comments-o', 'Icon chat'),
(73, 'fa-volume-control-phone', 'Icon dial'),
(72, 'fa-map-pin', 'Icon map pin'),
(71, 'fa-play', 'Icon play'),
(70, 'fa-fighter-jet', 'Icon jet'),
(69, 'fa-plane', 'Icon airplane'),
(68, 'fa-glass', 'Icon wine'),
(67, 'fa-bicycle', 'Icon bike'),
(66, 'fa-venus', 'Icon profile female'),
(65, 'fa-mars', 'Icon profile male'),
(64, 'fa-anchor', 'Icon anchor'),
(63, 'fa-recycle', 'Icon recycle'),
(62, 'fa-exclamation', 'Icon caution'),
(61, 'fa-flask', 'Icon beaker'),
(60, 'fa-fort-awesome', 'Icon fort'),
(59, 'fa-microphone', 'Icon mic'),
(58, 'fa-area-chart', 'Icon areagraph'),
(57, 'fa-compass', 'Icon circle compass'),
(56, 'fa-search-plus', 'Icon magnifying glass'),
(55, 'fa-paint-brush', 'Icon paintbrush'),
(54, 'fa-scissors', 'Icon scissors'),
(53, 'fa-bath', 'Icon bath'),
(52, 'fa-cog', 'Icon cog'),
(51, 'fa-pencil', 'Icon pencil'),
(50, 'fa-cubes', 'Icon cubes'),
(49, 'fa-lightbulb-o', 'Icon lightbulb'),
(48, 'fa-tags', 'Icon pricetags'),
(47, 'fa-paperclip', 'Icon attachment'),
(46, 'fa-paper-plane', 'Icon paperplane'),
(45, 'fa-key', 'Icon key'),
(44, 'fa-cogs', 'Icon gears'),
(43, 'fa-binoculars', 'Icon telescope'),
(42, 'fa-map-signs', 'Icon streetsign'),
(41, 'fa-envelope', 'Icon envelope'),
(40, 'fa-shopping-basket', 'Icon basket'),
(39, 'fa-puzzle-piece', 'Icon puzzle'),
(38, 'fa-map', 'Icon map'),
(37, 'fa-flag', 'Icon flag'),
(36, 'fa-trophy', 'Icon trophy'),
(35, 'fa-shield', 'Icon shield'),
(34, 'fa-bullhorn', 'Icon megaphone'),
(33, 'fa-lock', 'Icon lock'),
(32, 'fa-hourglass', 'Icon hourglass'),
(31, 'fa-map-marker', 'Icon marker'),
(30, 'fa-arrows-alt', 'Icon adjustments'),
(29, 'fa-edit', 'Icon edit'),
(28, 'fa-search-plus', 'Icon focus'),
(27, 'fa-expand', 'Icon expand'),
(26, 'fa-th', 'Icon grid'),
(25, 'fa-ba-chart', 'Icon bargraph'),
(24, 'fa-gift', 'Icon gift'),
(23, 'fa-google-wallet', 'Icon wallet'),
(22, 'fa-briefcase', 'Icon briefcase'),
(21, 'fa-wrench', 'Icon toolbox'),
(20, 'fa-print', 'Icon printer'),
(19, 'fa-camera', 'Icon camera'),
(18, 'fa-video-camera', 'Icon video'),
(17, 'fa-image-o', 'Icon pictures'),
(16, 'fa-picture-o', 'Icon picture'),
(15, 'fa-line-chart', 'Icon presentation'),
(14, 'fa-calendar', 'Icon calendar'),
(13, 'fa-globe', 'Icon browser'),
(12, 'fa-book', 'Icon book open'),
(11, 'fa-bookmark', 'Icon bookmark'),
(10, 'fa-newspaper-o', 'Icon newspaper'),
(9, 'fa-clipboard', 'Icon clipboard'),
(8, 'fa-search', 'Icon search'),
(7, 'fa-file-text', 'Icon documents'),
(6, 'fa-file', 'Icon document'),
(5, 'fa-phone', 'Icon phone'),
(4, 'fa-tablet', 'Icon tablet'),
(3, 'fa-desktop', 'Icon desktop'),
(2, 'fa-laptop', 'Icon laptop'),
(1, 'fa-mobile', 'Icon mobile'),
(75, 'fa-heart', 'Icon heart'),
(76, 'fa-cloud', 'Icon cloud'),
(77, 'fa-upload', 'Icon upload'),
(78, 'fa-download', 'Icon download'),
(79, 'fa-bullseye', 'Icon target'),
(80, 'fa-check', 'Icon check'),
(81, 'fa-pie-chart', 'Icon piechart'),
(82, 'fa-tachometer', 'Icon speedometer'),
(83, 'fa-address-card', 'Icon card'),
(84, 'fa-id-badge', 'Icon badge'),
(85, 'fa-life-ring', 'Icon lifesaver'),
(86, 'fa-clock-o', 'Icon clock'),
(87, 'fa-camera-retro', 'Icon aperture'),
(88, 'fa-quote-right', 'Icon quote'),
(89, 'fa-star-o', 'Icon star'),
(90, 'fa-ellipsis-h', 'Icon ellipsis'),
(91, 'fa-refresh', 'Icon refresh'),
(92, 'fa-smile-o', 'Icon happy'),
(93, 'fa-frown-o', 'Icon sad'),
(94, 'fa-facebook', 'Icon facebook'),
(95, 'fa-twitter', 'Icon twitter'),
(96, 'fa-google-plus', 'Icon googleplus'),
(97, 'fa-rss', 'Icon rss'),
(98, 'fa-tumblr', 'Icon tumblr'),
(99, 'fa-linkedin', 'Icon linkedin'),
(100, 'fa-dribbble', 'Icon dribbble');

-- --------------------------------------------------------

--
-- Table structure for table `msicon_mdi`
--

CREATE TABLE `msicon_mdi` (
  `ID_icon` int(11) NOT NULL,
  `icon_class` varchar(120) NOT NULL,
  `icon_name` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msicon_mdi`
--

INSERT INTO `msicon_mdi` (`ID_icon`, `icon_class`, `icon_name`) VALUES
(1, 'mdi-access-point', 'access point'),
(2, 'mdi-access-point-network', 'access point network'),
(3, 'mdi-account', 'account'),
(4, 'mdi-account-alert', 'account alert'),
(5, 'mdi-account-box', 'account box'),
(6, 'mdi-account-box-outline', 'box outline'),
(7, 'mdi-account-card-details', 'account card details'),
(8, 'mdi-account-check', 'account check'),
(9, 'mdi-account-circle', 'account circle'),
(10, 'mdi-account-convert', 'account convert'),
(11, 'mdi-account-edit', 'account edit'),
(12, 'mdi-account-key', 'account key'),
(13, 'mdi-account-location', 'account location'),
(14, 'mdi-account-minus', 'account minus'),
(15, 'mdi-account-multiple', 'account multiple'),
(16, 'mdi-account-multiple-minus', 'account multiple minus'),
(17, 'mdi-account-multiple-outline', 'account multiple outline'),
(18, 'mdi-account-multiple-plus', 'account multiple plus'),
(19, 'mdi-account-network', 'account network'),
(20, 'mdi-account-off', 'account off'),
(21, 'mdi-account-outline', 'account outline'),
(22, 'mdi-account-outline', 'account outline'),
(23, 'mdi-account-plus', 'account plus'),
(24, 'mdi-account-remove', 'account remove'),
(25, 'mdi-account-search', 'account search'),
(26, 'mdi-account-settings', 'account settings'),
(27, 'mdi-account-settings-variant', 'account settings variant'),
(28, 'mdi-account-star', 'account star'),
(29, 'mdi-account-star-variant', 'account star variant'),
(30, 'mdi-account-switch', 'account switch'),
(31, 'mdi-adjust', 'adjust'),
(32, 'mdi-air-conditioner', 'air conditioner'),
(33, 'mdi-airballoon', 'airballoon'),
(34, 'mdi-airplane', 'airplane'),
(35, 'mdi-airplane-landing', 'airplane landing'),
(36, 'mdi-airplane-off', 'airplane off'),
(37, 'mdi-airplane-takeoff', 'airplane takeoff'),
(38, 'mdi-airplay', 'airplay'),
(39, 'mdi-alarm', 'alarm'),
(40, 'mdi-alarm-check', 'alarm-check'),
(41, 'mdi-alarm-multiple', 'alarm multiple'),
(42, 'mdi-alarm-off', 'alarm off'),
(43, 'mdi-alarm-plus', 'alarm plus'),
(44, 'mdi-alarm-snooze', 'alarm snooze'),
(45, 'mdi-album', 'album'),
(46, 'mdi-alert', 'alert'),
(47, 'mdi-alert-box', 'alert box'),
(48, 'mdi-alert-circle', 'alert circle'),
(49, 'mdi-alert-circle-outline', 'alert circle outline'),
(50, 'mdi-alert-octagon', 'alert octagon'),
(51, 'mdi-alert-outline', 'alert outline'),
(52, 'mdi-alert-octagram', 'alert octagram'),
(53, 'mdi-all-inclusive', 'all inclusive'),
(54, 'mdi-alpha', 'alpha'),
(55, 'mdi-alphabetical', 'alphabetical'),
(56, 'mdi-altimeter', 'altimeter'),
(57, 'mdi-amazon', 'amazon'),
(58, 'mdi-amazon-clouddrive', 'amazon cloud drive'),
(59, 'mdi-ambulance', 'ambulance'),
(60, 'mdi-amplifier', 'amplifier'),
(61, 'mdi-anchor', 'anchor'),
(62, 'mdi-android', 'android'),
(63, 'mdi-android-debug-bridge', 'android debug bridge'),
(64, 'mdi-android-studio', 'android studio'),
(65, 'mdi-angular', 'angular'),
(66, 'mdi-angularjs', 'angularjs'),
(67, 'mdi-animation', 'animation'),
(68, 'mdi-apple', 'apple'),
(69, 'mdi-apple-finder', 'apple finder'),
(70, 'mdi-apple-ios', 'apple ios'),
(71, 'mdi-apple-keyboard-caps', 'apple keyboard caps'),
(72, 'mdi-apple-keyboard-command', 'apple keyboard command'),
(73, 'mdi-apple-keyboard-control', 'apple keyboard control'),
(74, 'mdi-apple-keyboard-option', 'apple keyboard option'),
(75, 'mdi-apple-keyboard-shift', 'apple keyboard shift'),
(76, 'mdi-apple-mobileme', 'apple mobileme'),
(77, 'mdi-apple-safari', 'apple safari'),
(78, 'mdi-application', 'application'),
(79, 'mdi-apps', 'apps'),
(80, 'mdi-archive', 'archive'),
(81, 'mdi-arrange-bring-forward', 'mdi arrange bring forward'),
(82, 'mdi-arrange-bring-to-front', 'mdi arrange bring to front'),
(83, 'mdi-arrange-send-backward', 'mdi arrange send backward'),
(84, 'mdi-arrange-send-to-back', 'mdi arrange send to back'),
(85, 'mdi-arrow-all', 'mdi arrow all'),
(86, 'mdi-arrow-bottom-left', 'mdi arrow bottom left'),
(87, 'mdi-arrow-bottom-right', 'mdi arrow bottom right'),
(88, 'mdi-arrow-compress', 'mdi arrow compress'),
(89, 'mdi-arrow-compress-all', 'mdi arrow compress all'),
(90, 'mdi-arrow-down', 'mdi arrow down'),
(91, 'mdi-arrow-down-bold', 'mdi arrow down bold'),
(92, 'mdi-arrow-down-bold-circle', 'mdi arrow down bold circle'),
(93, 'mdi-arrow-down-bold-hexagon-outline', 'mdi arrow down bold hexagon outline'),
(94, 'mdi-arrow-down-box', 'mdi arrow down box'),
(95, 'mdi-arrow-down-drop-circle', 'mdi arrow down drop circle'),
(96, 'mdi-arrow-down-drop-circle-outline', 'mdi arrow down drop circle outline'),
(97, 'mdi-arrow-expand', 'mdi arrow expand'),
(98, 'mdi-arrow-expand-all', 'mdi arrow expand all'),
(99, 'mdi-arrow-left', 'mdi arrow left'),
(100, 'mdi-arrow-left-bold', 'mdi arrow left bold'),
(101, 'mdi-arrow-left-bold-circle', 'mdi arrow left bold circle'),
(102, 'mdi-arrow-left-bold-circle-outline', 'mdi arrow left bold circle outline'),
(103, 'mdi-arrow-left-bold-hexagon-outline', 'mdi arrow left bold hexagon outline'),
(104, 'mdi-arrow-left-box', 'mdi arrow left box'),
(105, 'mdi-arrow-left-drop-circle', 'mdi arrow left drop circle'),
(106, 'mdi-arrow-left-drop-circle-outline', 'mdi arrow left drop circle outline'),
(107, 'mdi-arrow-right', 'mdi arrow right'),
(108, 'mdi-arrow-right-bold', 'mdi arrow right bold'),
(109, 'mdi-arrow-right-bold-circle', 'mdi arrow right bold circle'),
(110, 'mdi-arrow-right-bold-circle-outline', 'mdi arrow right bold circle outline'),
(111, 'mdi-arrow-right-bold-hexagon-outline', 'mdi arrow right bold hexagon outline'),
(112, 'mdi-arrow-right-box', 'mdi arrow right box'),
(113, 'mdi-arrow-right-drop-circle', 'mdi arrow right drop circle'),
(114, 'mdi-arrow-right-drop-circle-outline', 'mdi arrow right drop circle outline'),
(115, 'mdi-arrow-top-left', 'mdi arrow top left'),
(116, 'mdi-arrow-top-right', 'mdi arrow top right'),
(117, 'mdi-arrow-up', 'mdi arrow up'),
(118, 'mdi-arrow-up-bold', 'mdi- arrow up bold'),
(119, 'mdi-arrow-up-bold-circle', 'mdi arrow up bold circle'),
(120, 'mdi-arrow-up-bold-circle-outline', 'mdi arrow up bold circle outline'),
(121, 'mdi-arrow-up-bold-hexagon-outline', 'mdi arrow up bold hexagon outline'),
(122, 'mdi-arrow-up-drop-circle', 'mdi arrow up drop circle'),
(123, 'mdi-arrow-up-box', 'mdi arrow up box'),
(124, 'mdi-arrow-up-drop-circle-outline', 'mdi arrow up drop circle outline'),
(125, 'mdi-assistant', 'mdi assistant'),
(126, 'mdi-asterisk', 'mdi asterisk'),
(127, 'mdi-at', 'mdi at'),
(128, 'mdi-attachment', 'mdi attachment'),
(129, 'mdi-audiobook', 'mdi audiobook'),
(130, 'mdi-auto-fix', 'mdi auto fix');

-- --------------------------------------------------------

--
-- Table structure for table `msimage`
--

CREATE TABLE `msimage` (
  `imageID` int(11) NOT NULL,
  `ID_category` int(11) NOT NULL,
  `imagePath` varchar(250) DEFAULT NULL,
  `imageTitle` varchar(250) DEFAULT '',
  `imageIndex` int(11) DEFAULT '0',
  `imageDesc` longtext,
  `visible` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msimage`
--

INSERT INTO `msimage` (`imageID`, `ID_category`, `imagePath`, `imageTitle`, `imageIndex`, `imageDesc`, `visible`) VALUES
(84, 2, '05ac85aea0263b68a626a515876d06ee.jpg', 'tenda roder', 12, 'tenda roder', 1),
(83, 2, '31227d7d82d41743c302136b3700bab9.jpg', 'tenda roder', 10, 'tenda roder', 1),
(85, 2, 'b35dcced716b46554ead494b4211632b.jpg', 'tenda roder', 13, 'tenda roder', 1),
(86, 3, 'abc58c18017de3d17fe3a69e83de5514.jpg', 'Flooring', 14, 'Flooring', 1),
(82, 1, '05f52ecf9ce323f71755ba68a4414ab5.jpg', 'tenda dome', 11, 'tenda dome', 1),
(100, 3, '2ec0819654db0fcf326150fb2472ce76.jpg', 'Flooring', 28, 'Flooring', 1);

-- --------------------------------------------------------

--
-- Table structure for table `msimage_detail`
--

CREATE TABLE `msimage_detail` (
  `detailID` int(11) NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `imagePath` varchar(250) DEFAULT '',
  `imageTitle` varchar(250) DEFAULT '',
  `imageIndex` int(11) DEFAULT NULL,
  `imageDesc` varchar(250) DEFAULT '',
  `visible` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msimage_detail`
--

INSERT INTO `msimage_detail` (`detailID`, `imageID`, `imagePath`, `imageTitle`, `imageIndex`, `imageDesc`, `visible`) VALUES
(1, 42, '36.jpg', 'Simple Portfolio Title', 1, 'Pre-wedding', 1),
(2, 42, '37.jpg', 'Simple Portfolio Title', 2, 'Pre-wedding', 1),
(3, 42, '38.jpg', 'Simple Portfolio Title', 3, 'Pre-wedding', 1),
(4, 42, '39.jpg', 'Simple Portfolio Title', 4, 'Pre-wedding', 1),
(5, 42, '40.jpg', 'Simple Portfolio Title', 5, 'Pre-wedding', 1),
(6, 42, '41.jpg', 'Simple Portfolio Title', 6, 'Pre-wedding', 1),
(7, 42, '42.jpg', 'Simple Portfolio Title', 7, 'Pre-wedding', 1),
(8, 42, '43.jpg', 'Simple Portfolio Title', 8, 'Pre-wedding', 1),
(9, 42, '44.jpg', 'Simple Portfolio Title', 9, 'Pre-wedding', 1),
(10, 42, '45.jpg', 'Simple Portfolio Title', 10, 'Pre-wedding', 1),
(11, 42, '46.jpg', 'Simple Portfolio Title', 11, 'Pre-wedding', 1),
(12, 42, '47.jpg', 'Simple Portfolio Title', 12, 'Pre-wedding', 1),
(13, 42, '48.jpg', 'Simple Portfolio Title', 13, 'Pre-wedding', 1),
(70, 64, 'b1918b3071d6f76ef94eccb6ebaf5333.jpg', 'Brand,Product, Etc', 62, '', 1),
(15, 43, '02.jpg', 'Simple Portfolio Title', 15, 'Family', 1),
(16, 43, '03.jpg', 'Simple Portfolio Title', 16, 'Family', 1),
(17, 43, '04.jpg', 'Simple Portfolio Title', 17, 'Family', 1),
(18, 43, '05.jpg', 'Simple Portfolio Title', 18, 'Family', 1),
(19, 43, '06.jpg', 'Simple Portfolio Title', 19, 'Family', 1),
(20, 44, '07.jpg', 'Simple Portfolio Title', 20, 'Portrait', 1),
(21, 44, '08.jpg', 'Simple Portfolio Title', 21, 'Portrait', 1),
(22, 44, '12.jpg', 'Simple Portfolio Title', 22, 'Portrait', 1),
(23, 44, '13.jpg', 'Simple Portfolio Title', 23, 'Portrait', 1),
(24, 44, '14.jpg', 'Simple Portfolio Title', 24, 'Portrait', 1),
(25, 44, '15.jpg', 'Simple Portfolio Title', 25, 'Portrait', 1),
(26, 44, '16.jpg', 'Simple Portfolio Title', 26, 'Portrait', 1),
(27, 44, '17.jpg', 'Simple Portfolio Title', 27, 'Portrait', 1),
(28, 44, '18.jpg', 'Simple Portfolio Title', 28, 'Portrait', 1),
(29, 44, '19.jpg', 'Simple Portfolio Title', 29, 'Portrait', 1),
(30, 44, '20.jpg', 'Simple Portfolio Title', 30, 'Portrait', 1),
(31, 44, '21.jpg', 'Simple Portfolio Title', 31, 'Portrait', 1),
(32, 44, '22.jpg', 'Simple Portfolio Title', 32, 'Portrait', 1),
(33, 44, '23.jpg', 'Simple Portfolio Title', 33, 'Portrait', 1),
(34, 15, '09.jpg', 'Simple Portfolio Title', 34, 'Portrait', 1),
(35, 16, '10.jpg', 'Simple Portfolio Title', 35, 'Portrait', 1),
(36, 45, '11.jpg', 'Simple Portfolio Title', 36, 'Pre-wedding', 1),
(37, 45, '24.jpg', 'Simple Portfolio Title', 37, 'Pre-wedding', 1),
(38, 45, '25.jpg', 'Simple Portfolio Title', 38, 'Pre-wedding', 1),
(39, 45, '26.jpg', 'Simple Portfolio Title', 39, 'Pre-wedding', 1),
(40, 45, '27.jpg', 'Simple Portfolio Title', 40, 'Pre-wedding', 1),
(41, 45, '28.jpg', 'Simple Portfolio Title', 41, 'Pre-wedding', 1),
(42, 45, '29.jpg', 'Simple Portfolio Title', 42, 'Pre-wedding', 1),
(43, 45, '30.jpg', 'Simple Portfolio Title', 43, 'Pre-wedding', 1),
(44, 45, '31.jpg', 'Simple Portfolio Title', 44, 'Pre-wedding', 1),
(45, 45, '32.jpg', 'Simple Portfolio Title', 45, 'Pre-wedding', 1),
(46, 45, '33.jpg', 'Simple Portfolio Title', 46, 'Pre-wedding', 1),
(47, 45, '34.jpg', 'Simple Portfolio Title', 47, 'Pre-wedding', 1),
(48, 45, '35.jpg', 'Simple Portfolio Title', 48, 'Pre-wedding', 1),
(49, 45, '40.jpg', 'Simple Portfolio Title', 49, 'Pre-wedding', 1),
(59, 45, '37.jpg', 'Simple Portfolio Title', 50, 'Pre-wedding', 1),
(58, 45, '36.jpg', 'Simple Portfolio Title', 51, 'Pre-wedding', 1),
(60, 45, '38.jpg', 'Simple Portfolio Title', 52, 'Pre-wedding', 1),
(61, 45, '39.jpg', 'Simple Portfolio Title', 53, 'Pre-wedding', 1),
(62, 45, '41.jpg', 'Simple Portfolio Title', 54, 'Pre-wedding', 1),
(63, 45, '42.jpg', 'Simple Portfolio Title', 55, 'Pre-wedding', 1),
(64, 45, '43.jpg', 'Simple Portfolio Title', 56, 'Pre-wedding', 1),
(65, 45, '44.jpg', 'Simple Portfolio Title', 57, 'Pre-wedding', 1),
(66, 45, '45.jpg', 'Simple Portfolio Title', 58, 'Pre-wedding', 1),
(67, 45, '46.jpg', 'Simple Portfolio Title', 59, 'Pre-wedding', 1),
(68, 45, '47.jpg', 'Simple Portfolio Title', 60, 'Pre-wedding', 1),
(69, 45, '48.jpg', 'Simple Portfolio Title', 61, 'Pre-wedding', 1),
(72, 64, 'e14a2df2f00b1900fc7a27c5c0f93430.jpg', 'Brand,Product, Etc', 63, '', 1),
(73, 64, 'b873d59871471503b8b1625accf3415e.jpg', 'Brand,Product, Etc', 64, '', 1),
(74, 64, 'c5bb0c6cc51dad43c0d4adb0101c5de3.jpg', 'Brand,Product, Etc', 65, '', 1),
(75, 64, '9c33f6509060cfaddcc76f11c1f21377.jpg', 'Brand,Product, Etc', 66, '', 1),
(76, 64, 'da3d558acfc95085db5e0bd904733ff2.jpg', 'Brand,Product, Etc', 67, '', 1),
(77, 67, '581707179a7018fdbc7b7f6736d52020.jpg', 'Brand, Product, Etc', 68, '', 1),
(78, 67, '5b4745078acb2fb92ac03d6f50dceb91.jpg', 'Brand, Product, Etc', 69, '', 1),
(79, 67, 'f0a339f233f0c2dd257ab9312a92eece.jpg', 'Brand, Product, Etc', 70, '', 1),
(80, 67, '3d3cf7945d0b1b1b0e89ce6cbea0b0fb.jpg', 'Brand, Product, Etc', 71, '', 1),
(81, 67, 'd8535f5039f8b50ddbf52219ba4e6361.jpg', 'Brand, Product, Etc', 72, '', 1),
(82, 67, '16da60aff2b121e1bb48f0a77b64a494.jpg', 'Brand, Product, Etc', 74, '', 1),
(83, 67, '88ea114d36096c375be8bbe745396e5a.jpg', 'Brand, Product, Etc', 75, '', 1),
(84, 43, '958cc81c798a5bf52d1060ec77af52e2.jpg', 'Family', 76, '', 1),
(85, 43, '9315fecdd401798fc91188ea426c64e6.jpg', 'family', 73, '', 1),
(86, 43, 'ed9c1f3d294e3cc1eb99de6dc77ed79a.jpg', 'family', 77, '', 1),
(89, 82, '3b159354e927ffe0ac8414e9b14b14ac.jpg', 'test', 80, 'test', 1),
(88, 99, '9a2296ab3dcf85e789bebfd9bfc2bbf4.jpg', 'aa', 79, 'aa', 1),
(90, 100, 'fec5fda5f81a88f9460a0a8b1ac959e1.jpg', 'bbb', 81, 'b', 1);

-- --------------------------------------------------------

--
-- Table structure for table `msintro_image`
--

CREATE TABLE `msintro_image` (
  `id` int(11) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msintro_image`
--

INSERT INTO `msintro_image` (`id`, `content`) VALUES
(1, '<h1>abcd</h1>');

-- --------------------------------------------------------

--
-- Table structure for table `msnewsletter`
--

CREATE TABLE `msnewsletter` (
  `ID_newsletter` int(11) NOT NULL,
  `seq_no` int(12) NOT NULL,
  `contact_email` varchar(120) NOT NULL,
  `visible` varchar(2) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msnewsletter`
--

INSERT INTO `msnewsletter` (`ID_newsletter`, `seq_no`, `contact_email`, `visible`, `created_date`) VALUES
(1, 1, 'test', '1', '2019-12-14 11:42:34');

-- --------------------------------------------------------

--
-- Table structure for table `msplan_detail`
--

CREATE TABLE `msplan_detail` (
  `ID_plan_detail` int(11) NOT NULL,
  `ID_plan` int(11) DEFAULT NULL,
  `seq_no` smallint(6) DEFAULT NULL,
  `detail_text` varchar(250) DEFAULT '',
  `imagePath` varchar(255) DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msplan_detail`
--

INSERT INTO `msplan_detail` (`ID_plan_detail`, `ID_plan`, `seq_no`, `detail_text`, `imagePath`, `kategori`, `created_date`) VALUES
(12, 2, 2, 'Tenda Roder Transparan', 'de967b8ecf244335f1aaf8c8c8a002fa.jpg', 'opsi', '2020-03-13 15:36:40'),
(13, 2, 3, '10 m', '6362df10853c669a4d3d264383a7989c.jpg', 'lebar', '2020-03-13 15:38:52'),
(14, 2, 4, '15 m', '3778e0508ac2012db23e884f65e0c4f6.jpg', 'lebar', '2020-03-13 15:39:34'),
(11, 2, 1, 'Tenda Roder', 'c442ad8b362b8e703da0e16cc75604e3.jpg', 'opsi', '2020-03-13 15:36:22'),
(15, 2, 5, '20 m', 'b96581010968c86caf147957ee63fec5.jpg', 'lebar', '2020-03-13 15:39:49'),
(16, 2, 6, 'Panjang Tenda Roder / jarak antar tiang per 5 meter', 'e0ac3ec0dae67c1937e4adf43a87fd75.jpg', 'panjang', '2020-03-13 15:40:29'),
(17, 2, 7, 'Panel Kaca', '0aefa2b46db08faebc7c4adfbe3e658a.jpg', 'aksesoris', '2020-03-13 15:48:13'),
(19, 2, 8, 'Pintu Kaca', '6bbdd8155bc72e474be98b541ef23350.jpg', 'aksesoris', '2020-03-13 15:53:00'),
(20, 2, 9, 'Beton / Rel', 'deef79842281379b5a0b96ce981567fa.jpg', 'aksesoris', '2020-03-13 15:53:22'),
(21, 3, 1, '6 m', '8699b342ce1b6b6adad77cc0dd5a9ff1.jpg', 'lebar', '2020-03-13 16:19:48'),
(22, 3, 2, '10 m', 'f4d8e778cc521f4c1af8308e4ff5847e.jpg', 'lebar', '2020-03-13 16:20:02'),
(23, 3, 3, '15 m', 'd7723f1dfcd3f396ee0337d66a98cb53.jpg', 'lebar', '2020-03-13 16:20:17'),
(24, 3, 4, '20 m', '37225d00af7e4941ea17572ad9763b79.jpg', 'lebar', '2020-03-13 16:20:35'),
(25, 9, 1, 'Tenda Roder dengan Flooring karpet hijau', '73bc0fec17336950207fa121967827ec.jpg', 'opsi', '2020-03-13 16:24:48'),
(26, 9, 2, 'Tenda Roder dengan flooring karpet merah', 'a1db72c8ebf2ca27422b3e899a3104bb.jpg', 'opsi', '2020-03-13 16:25:03');

-- --------------------------------------------------------

--
-- Table structure for table `msplan_header`
--

CREATE TABLE `msplan_header` (
  `ID_plan` int(11) NOT NULL,
  `seq_no` smallint(6) NOT NULL DEFAULT '0',
  `plan_name` varchar(120) DEFAULT '',
  `plan_detail` text,
  `spec_title` varchar(255) DEFAULT NULL,
  `spec_desc` text,
  `plan_img` varchar(250) DEFAULT '',
  `currency` varchar(20) DEFAULT '',
  `price` double DEFAULT NULL,
  `per_each` varchar(30) DEFAULT '',
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msplan_header`
--

INSERT INTO `msplan_header` (`ID_plan`, `seq_no`, `plan_name`, `plan_detail`, `spec_title`, `spec_desc`, `plan_img`, `currency`, `price`, `per_each`, `created_date`) VALUES
(2, 1, 'Tenda Hangar', 'Tenda semi permanen yang kerap dijadikan pilihan untuk menyelenggarakan event-event besar. Konstruksinya tidak membutuhkan tiang di bagian tengah tenda. Hal ini membuat kapasitas tenda roder menjadi lebih luas', 'Spesifikasi Tenda Hangar', 'Ada beberapa ukuran tenda roder yang kami tawarkan. Mulai dari 10 m, 15 m dan 20 m, semua bisa disewa sesuai kebutuhan. Tenda Roder memiliki 2 jenis terpal: Tenda Roder Putih dan Tenda Roder Transparan', '28abc2446b83f3fac9ea747742b5f8d4.jpg', 'IDR', 500000, 'hour <b>(70$)</b>', '2016-09-17 14:21:20'),
(3, 2, 'Tenda Kerucut', 'Tenda berbentuk dome dengan struktur yang terdiri dari pipa-pipa besi dengan cover PVC Tarpaulin yang melapisi. Tidak adanya tiang di bagian tengah tenda membuat kapasitas tenda dome geodesic terasa lebih luas', 'Spesifikasi Tenda Kerucut', 'Kami menyediakan beberapa ukuran tenda dome geodesic yang bisa dipilih sesuai kebutuhan. Diameter ukuran yang tersedia di antaranya adalah 6 m, 10 m, 15 m dan 20 m', '8bf9e2fdb5da09dd89e9df6d7f93345a.jpg', 'IDR', 300000, 'hour (50$)', '2016-09-17 14:22:06'),
(9, 4, 'Pelengkap', 'Pelengkap', 'Pelengkap', 'Pelengkap', 'ff109170aae2376b4e76dda0a3c1451f.jpg', 'IDR', 2500000, 'Session (5 hours)', '2018-03-21 11:22:06'),
(14, 3, 'Tenda Limas', ' Tenda Limas', 'Spesifikasi Tenda Limas', 'Spesifikasi Tenda Limas', '9f568a00c2e163f5341b21440c7d67b5.jpg', '', NULL, '', '2020-06-11 19:02:47');

-- --------------------------------------------------------

--
-- Table structure for table `msroder`
--

CREATE TABLE `msroder` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `spec_title` varchar(255) DEFAULT NULL,
  `spec_desc` varchar(255) DEFAULT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msroder`
--

INSERT INTO `msroder` (`id`, `title`, `subtitle`, `spec_title`, `spec_desc`, `created_date`) VALUES
(1, '1Tenda Roder', '1Tenda semi permanen yang kerap dijadikan pilihan untuk menyelenggarakan event-event besar. Konstruksinya tidak membutuhkan tiang di bagian tengah tenda. Hal ini membuat kapasitas tenda roder menjadi lebih luas', '1Spesifikasi Tenda Roder', '1Ada beberapa ukuran tenda roder yang kami tawarkan. Mulai dari 10 m, 15 m dan 20 m, semua bisa disewa sesuai kebutuhan. Tenda Roder memiliki 2 jenis terpal: Tenda Roder Putih dan Tenda Roder Transparan', '2020-03-10 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `msroder_opsi`
--

CREATE TABLE `msroder_opsi` (
  `id` int(11) NOT NULL,
  `imagePath` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `imageIndex` int(6) DEFAULT NULL,
  `visible` smallint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `msservice`
--

CREATE TABLE `msservice` (
  `ID_services` int(10) NOT NULL,
  `services_name` varchar(250) NOT NULL,
  `services_img` varchar(250) NOT NULL,
  `services_desc` longtext NOT NULL,
  `services_date` datetime NOT NULL,
  `services_index` int(11) NOT NULL,
  `visible` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msservice`
--

INSERT INTO `msservice` (`ID_services`, `services_name`, `services_img`, `services_desc`, `services_date`, `services_index`, `visible`) VALUES
(1, 'Free Consultation', 'fa-comments-o', 'Bebas melakukan konsultasi kepada kami.', '0000-00-00 00:00:00', 2, 1),
(2, 'Best Quality', 'fa-check', 'Kami memberikan kualitas terbaik.', '0000-00-00 00:00:00', 4, 1),
(3, 'Affordable Prices', 'fa-tags', 'Kami memberikan harga yang terjangkau.', '2019-06-16 00:00:00', 3, 1),
(4, 'Go Green', 'fa-recycle', 'Barang yang kami pakai dapat di daur ulang.', '2019-12-22 11:19:14', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `msslider`
--

CREATE TABLE `msslider` (
  `imageID` int(11) NOT NULL,
  `imagePath` varchar(250) DEFAULT '',
  `imageTitle` varchar(250) DEFAULT '',
  `content` text,
  `imageIndex` int(11) DEFAULT '0',
  `visible` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msslider`
--

INSERT INTO `msslider` (`imageID`, `imagePath`, `imageTitle`, `content`, `imageIndex`, `visible`) VALUES
(20, '20966f5b35808787fd0eecb0db74d9be.jpg', 'Slider 2', '', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `msslider_service`
--

CREATE TABLE `msslider_service` (
  `ID_slider` int(11) NOT NULL,
  `imagePath` varchar(250) NOT NULL,
  `slider_name` varchar(250) NOT NULL,
  `content` text NOT NULL,
  `link` varchar(255) NOT NULL DEFAULT '#',
  `imageIndex` int(11) NOT NULL,
  `visible` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `msslider_service`
--

INSERT INTO `msslider_service` (`ID_slider`, `imagePath`, `slider_name`, `content`, `link`, `imageIndex`, `visible`) VALUES
(5, '3739135ddca1235c9c6744f7958d9290.jpg', 'Tenda Roder', 'Tenda Roder Ukuran 10m, 15m, dan 20m', 'test', 1, 1),
(6, '6d40221ad246ce8a257c74dd0fd4c9df.jpg', 'Tenda Dome Geodesic', 'Tenda Dome Geodesic Ukuran 6m, 10m, 15m, dan 20m', '#', 2, 1),
(7, '0d4f7ac521baf614d2cddafa15f4f046.jpg', 'Flooring', 'Flooring / Lantai Tenda Dengan Luas Menyesuaikan Ukuran Tenda', '#', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `msteam`
--

CREATE TABLE `msteam` (
  `ID_team` int(11) NOT NULL,
  `seq_no` int(11) DEFAULT '0',
  `name` varchar(120) DEFAULT '',
  `position_name` varchar(120) DEFAULT '',
  `photo_path` varchar(250) DEFAULT '',
  `facebook_link` varchar(200) DEFAULT '',
  `twitter_link` varchar(200) DEFAULT '',
  `linkedin_link` varchar(200) DEFAULT '',
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `msteam`
--

INSERT INTO `msteam` (`ID_team`, `seq_no`, `name`, `position_name`, `photo_path`, `facebook_link`, `twitter_link`, `linkedin_link`, `created_date`) VALUES
(5, 1, 'Tan Jordy Christian Kurnianto', 'Founder', '88bb4d95eb57e6f9ab8174471460e10c.jpg', 'https://www.facebook.com/jordy.christiankurnianto', 'www.twitter.com/tanjordy', '', '2016-09-18 09:17:33'),
(6, 2, 'Fransiskus Edbert', 'Co Founder', '02.jpg', 'https://www.facebook.com/fransiskus.edbert?fref=ts', '', '', '2016-09-18 09:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `mstenda_service`
--

CREATE TABLE `mstenda_service` (
  `id` int(11) NOT NULL,
  `seq_no` smallint(6) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstenda_service`
--

INSERT INTO `mstenda_service` (`id`, `seq_no`, `title`, `subtitle`, `created_date`) VALUES
(1, 2, 'Mudah Bongkar Pasang', 'Tenda Roder menggunakan sistem knock-down sehingga mudah dibongkar pasang', '2020-03-10 00:00:00'),
(2, 3, 'Proses Pemasangan Cepat', 'Dengan sistem knock-down pemasangan Tenda Roder menjadi cepat', '2020-03-10 00:00:00'),
(3, 1, 'Kapasitas Tenda Besar', 'Tenda Roder tidak menggunakan tiang tengah di bagian dalamnya sehingga kapasitas Tenda Roder dapat digunakan dengan maksimal', '2020-03-10 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mstestimoni`
--

CREATE TABLE `mstestimoni` (
  `ID_testimoni` int(11) NOT NULL,
  `seq_no` int(11) NOT NULL DEFAULT '0',
  `person_name` varchar(150) DEFAULT '',
  `testimoni_text` varchar(5000) DEFAULT '',
  `corporation_name` varchar(150) DEFAULT '',
  `person_photo` varchar(150) DEFAULT '',
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mstestimoni`
--

INSERT INTO `mstestimoni` (`ID_testimoni`, `seq_no`, `person_name`, `testimoni_text`, `corporation_name`, `person_photo`, `created_date`) VALUES
(1, 2, 'Nydia Muliawijaya', '&quot;It is going to be hard to pick my favorite photos with so many good ones to choose from&quot;', 'Front End', '1.jpg', '2016-09-18 01:24:16'),
(2, 1, 'Jonathan &amp; Catherine', '&quot;They are definitely show us how important it is to capture memories&quot;', 'Web Design', '2.jpg', '2016-09-18 01:31:05'),
(3, 3, 'Kelvin &amp; Imelda', '&quot;It is been so lovely to see our memory taken by them, their photos are gonna be a worth return ticket to the past!&quot;', 'UX Design', '2.jpg', '2016-09-18 01:32:01'),
(13, 4, 'Rina', '&quot;The photographer, Jerry was sooo friendly, he even told us which food was the best in the city! I’m definitely gonna pay a visit again somedays!&quot;', 'Graha', '2.jpg', '2016-10-01 19:47:20'),
(25, 7, 'Harrid', '&quot;The photographer, Jerry was sooo friendly, he even told us which food was the best in the city! I’m definitely gonna pay a visit again somedays!&quot;', 'kmis', 'a90a7e98f4231484dd1232b32b4241a2.jpg', '2019-05-31 14:22:52'),
(26, 5, 'Julid', '&quot;The photographer, Jerry was sooo friendly, he even told us which food was the best in the city! I’m definitely gonna pay a visit again somedays!&quot;', 'photographer', '2.jpg', '2019-05-31 14:23:13'),
(27, 6, 'Dessy', '&quot;The photographer, Jerry was sooo friendly, he even told us which food was the best in the city! I’m definitely gonna pay a visit again somedays!&quot;', 'Penyanyi', '2.jpg', '2019-05-31 14:24:17');

-- --------------------------------------------------------

--
-- Table structure for table `mstitle_master`
--

CREATE TABLE `mstitle_master` (
  `id` int(11) NOT NULL,
  `title1` varchar(100) DEFAULT NULL,
  `title2` varchar(100) DEFAULT NULL,
  `title3` varchar(100) DEFAULT NULL,
  `title4` varchar(100) DEFAULT NULL,
  `title5` varchar(100) DEFAULT NULL,
  `title6` varchar(100) DEFAULT NULL,
  `subtitle1` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstitle_master`
--

INSERT INTO `mstitle_master` (`id`, `title1`, `title2`, `title3`, `title4`, `title5`, `title6`, `subtitle1`) VALUES
(1, '1Get a Quote For Your Event\na', '1Mengapa Memilih Agung Tent', '1Gallery', '1Kelebihan Tenda Roder', '1Events', 'Request a free\nconsultation with us', '1Tenda Roder dirancang untuk event outdoor yang membutuhkan ruang yang besar');

-- --------------------------------------------------------

--
-- Table structure for table `mswhy`
--

CREATE TABLE `mswhy` (
  `ID_why` int(11) NOT NULL,
  `seq_no` smallint(6) DEFAULT '0',
  `why_side` varchar(2) DEFAULT '0',
  `why_title` varchar(120) DEFAULT '',
  `why_pict` varchar(150) DEFAULT '',
  `why_text` varchar(1000) DEFAULT '',
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mswhy`
--

INSERT INTO `mswhy` (`ID_why`, `seq_no`, `why_side`, `why_title`, `why_pict`, `why_text`, `created_date`) VALUES
(2, 1, 'L', 'WE ARE CREATIVE', 'icon-genius', 'We capture your moments in the way that even words cannot explain. Who needs words when you got pictures?', '2016-09-17 21:15:40'),
(3, 2, 'L', 'WE ARE PHOTOGRAPHY', 'icon-camera', 'Photography is our passion. We will make sure we share our passion with you by providing ONLY a talented photographer', '2016-09-17 21:15:40'),
(4, 3, 'L', 'WE LOVE FEEDBACK', 'icon-chat', 'Is there anything have you ever dreamt of for your holiday? Wedding proposal? Any special events? Share with us and we are about to make it happen.', '2016-09-17 21:15:40'),
(5, 4, 'R', 'WE ARE WORTH IT', 'icon-trophy', 'When it comes to price, we are certain that our service is well worth trying', '2016-09-17 21:53:47'),
(6, 5, 'R', 'WE CONNECT PEOPLE', 'icon-happy', 'We believe that our service is a long-term proposition. We want you to feel comfortable enough to use our service repeatedly over the years. Do not hesitate to ask for anything from our photographers. They will do their best to help.', '2016-09-17 21:53:47'),
(7, 6, 'R', 'WE ARE EVERYWHERE', 'icon-map', 'We are Mr. And Mrs. Worldwide. Tell u which place you want to go and we will make it happen for you', '2016-09-17 21:53:47');

-- --------------------------------------------------------

--
-- Table structure for table `trbooking`
--

CREATE TABLE `trbooking` (
  `ID_booking` int(11) NOT NULL,
  `book_date` datetime DEFAULT NULL,
  `places` varchar(1000) DEFAULT '',
  `categoryID` int(11) DEFAULT '0',
  `contact_name` varchar(100) DEFAULT '',
  `contact_phone` varchar(30) DEFAULT '',
  `contact_email` varchar(120) DEFAULT '',
  `notes` text,
  `status` varchar(2) DEFAULT 'P',
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trbooking`
--

INSERT INTO `trbooking` (`ID_booking`, `book_date`, `places`, `categoryID`, `contact_name`, `contact_phone`, `contact_email`, `notes`, `status`, `created_date`) VALUES
(1, '2016-09-28 00:00:00', 'Gunung Rinjani', 1, 'test', 'test', 'test@test.com', 'test', 'A', '2016-09-26 07:43:33');

-- --------------------------------------------------------

--
-- Table structure for table `trevents_detail`
--

CREATE TABLE `trevents_detail` (
  `detailID` varchar(100) NOT NULL,
  `ID_events` varchar(100) DEFAULT NULL,
  `imagePath` varchar(250) DEFAULT '',
  `imageTitle` varchar(250) DEFAULT '',
  `imageIndex` int(11) DEFAULT '0',
  `imageDesc` varchar(250) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trevents_detail`
--

INSERT INTO `trevents_detail` (`detailID`, `ID_events`, `imagePath`, `imageTitle`, `imageIndex`, `imageDesc`) VALUES
('IMG0000012', 'EVT0000001', 'facility.jpg', 'Outbound', 1, ''),
('IMG0000013', 'EVT0000001', 'facility.jpg', 'Halang Rintang', 2, ''),
('IMG0000011', 'EVT0000001', 'facility.jpg', 'Touring', 3, ''),
('IMG0000024', 'EVT0000001', 'facility.jpg', 'Seminar', 4, ''),
('IMG0000014', 'EVT0000001', 'facility.jpg', 'Sports & Competition', 5, ''),
('IMG0000036', 'EVT0000001', 'facility.jpg', 'Flying Fox', 6, ''),
('IMG0000015', 'EVT0000001', 'facility.jpg', 'Fun Game', 7, ''),
('IMG0000017', 'EVT0000001', 'facility.jpg', 'Senam & Berenang', 8, ''),
('IMG0000019', 'EVT0000002', 'facility.jpg', 'Audience', 1, ''),
('IMG0000021', 'EVT0000002', 'facility.jpg', 'Our works', 2, ''),
('IMG0000022', 'EVT0000002', 'facility.jpg', 'Kolam Renang', 3, ''),
('IMG0000023', 'EVT0000002', 'facility.jpg', 'Cover Depan', 4, ''),
('IMG0000034', 'EVT0000002', 'facility.jpg', 'Pemukaan Pameran', 5, ''),
('IMG0000032', 'EVT0000002', 'facility.jpg', 'Proses Pemasangan Karya', 6, ''),
('IMG0000033', 'EVT0000002', 'facility.jpg', 'Proses Pemasangan', 7, ''),
('IMG0000031', 'EVT0000002', 'facility.jpg', 'Proses Pemasangan', 8, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `msabout`
--
ALTER TABLE `msabout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `msabout_quote`
--
ALTER TABLE `msabout_quote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `msabout_us`
--
ALTER TABLE `msabout_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `msadmin`
--
ALTER TABLE `msadmin`
  ADD PRIMARY KEY (`ID_admin`);

--
-- Indexes for table `msblog`
--
ALTER TABLE `msblog`
  ADD PRIMARY KEY (`ID_blog`);

--
-- Indexes for table `msblogcategory`
--
ALTER TABLE `msblogcategory`
  ADD PRIMARY KEY (`ID_blogcategory`);

--
-- Indexes for table `msblogtags`
--
ALTER TABLE `msblogtags`
  ADD PRIMARY KEY (`ID_tags`);

--
-- Indexes for table `mscategory`
--
ALTER TABLE `mscategory`
  ADD PRIMARY KEY (`ID_category`);

--
-- Indexes for table `msconfig`
--
ALTER TABLE `msconfig`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mscontact`
--
ALTER TABLE `mscontact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mscustomer`
--
ALTER TABLE `mscustomer`
  ADD PRIMARY KEY (`ID_customer`);

--
-- Indexes for table `msevents`
--
ALTER TABLE `msevents`
  ADD PRIMARY KEY (`ID_events`);

--
-- Indexes for table `msfact`
--
ALTER TABLE `msfact`
  ADD PRIMARY KEY (`ID_fact`);

--
-- Indexes for table `mshows`
--
ALTER TABLE `mshows`
  ADD PRIMARY KEY (`ID_hows`);

--
-- Indexes for table `msicon`
--
ALTER TABLE `msicon`
  ADD PRIMARY KEY (`ID_icon`);

--
-- Indexes for table `msicon_mdi`
--
ALTER TABLE `msicon_mdi`
  ADD PRIMARY KEY (`ID_icon`);

--
-- Indexes for table `msimage`
--
ALTER TABLE `msimage`
  ADD PRIMARY KEY (`imageID`);

--
-- Indexes for table `msimage_detail`
--
ALTER TABLE `msimage_detail`
  ADD PRIMARY KEY (`detailID`);

--
-- Indexes for table `msintro_image`
--
ALTER TABLE `msintro_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `msnewsletter`
--
ALTER TABLE `msnewsletter`
  ADD PRIMARY KEY (`ID_newsletter`);

--
-- Indexes for table `msplan_detail`
--
ALTER TABLE `msplan_detail`
  ADD PRIMARY KEY (`ID_plan_detail`);

--
-- Indexes for table `msplan_header`
--
ALTER TABLE `msplan_header`
  ADD PRIMARY KEY (`ID_plan`);

--
-- Indexes for table `msroder`
--
ALTER TABLE `msroder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `msroder_opsi`
--
ALTER TABLE `msroder_opsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `msservice`
--
ALTER TABLE `msservice`
  ADD PRIMARY KEY (`ID_services`);

--
-- Indexes for table `msslider`
--
ALTER TABLE `msslider`
  ADD PRIMARY KEY (`imageID`);

--
-- Indexes for table `msslider_service`
--
ALTER TABLE `msslider_service`
  ADD PRIMARY KEY (`ID_slider`);

--
-- Indexes for table `msteam`
--
ALTER TABLE `msteam`
  ADD PRIMARY KEY (`ID_team`);

--
-- Indexes for table `mstenda_service`
--
ALTER TABLE `mstenda_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mstestimoni`
--
ALTER TABLE `mstestimoni`
  ADD PRIMARY KEY (`ID_testimoni`);

--
-- Indexes for table `mstitle_master`
--
ALTER TABLE `mstitle_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mswhy`
--
ALTER TABLE `mswhy`
  ADD PRIMARY KEY (`ID_why`);

--
-- Indexes for table `trbooking`
--
ALTER TABLE `trbooking`
  ADD PRIMARY KEY (`ID_booking`);

--
-- Indexes for table `trevents_detail`
--
ALTER TABLE `trevents_detail`
  ADD PRIMARY KEY (`detailID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `msabout`
--
ALTER TABLE `msabout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `msabout_quote`
--
ALTER TABLE `msabout_quote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `msabout_us`
--
ALTER TABLE `msabout_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `msblog`
--
ALTER TABLE `msblog`
  MODIFY `ID_blog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `msblogcategory`
--
ALTER TABLE `msblogcategory`
  MODIFY `ID_blogcategory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `msblogtags`
--
ALTER TABLE `msblogtags`
  MODIFY `ID_tags` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mscategory`
--
ALTER TABLE `mscategory`
  MODIFY `ID_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `msconfig`
--
ALTER TABLE `msconfig`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mscontact`
--
ALTER TABLE `mscontact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mscustomer`
--
ALTER TABLE `mscustomer`
  MODIFY `ID_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `msevents`
--
ALTER TABLE `msevents`
  MODIFY `ID_events` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `msfact`
--
ALTER TABLE `msfact`
  MODIFY `ID_fact` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `mshows`
--
ALTER TABLE `mshows`
  MODIFY `ID_hows` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `msicon`
--
ALTER TABLE `msicon`
  MODIFY `ID_icon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `msicon_mdi`
--
ALTER TABLE `msicon_mdi`
  MODIFY `ID_icon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `msimage`
--
ALTER TABLE `msimage`
  MODIFY `imageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `msimage_detail`
--
ALTER TABLE `msimage_detail`
  MODIFY `detailID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `msintro_image`
--
ALTER TABLE `msintro_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `msnewsletter`
--
ALTER TABLE `msnewsletter`
  MODIFY `ID_newsletter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `msplan_detail`
--
ALTER TABLE `msplan_detail`
  MODIFY `ID_plan_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `msplan_header`
--
ALTER TABLE `msplan_header`
  MODIFY `ID_plan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `msroder`
--
ALTER TABLE `msroder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `msroder_opsi`
--
ALTER TABLE `msroder_opsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `msservice`
--
ALTER TABLE `msservice`
  MODIFY `ID_services` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `msslider`
--
ALTER TABLE `msslider`
  MODIFY `imageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `msslider_service`
--
ALTER TABLE `msslider_service`
  MODIFY `ID_slider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `msteam`
--
ALTER TABLE `msteam`
  MODIFY `ID_team` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `mstenda_service`
--
ALTER TABLE `mstenda_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mstestimoni`
--
ALTER TABLE `mstestimoni`
  MODIFY `ID_testimoni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `mstitle_master`
--
ALTER TABLE `mstitle_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mswhy`
--
ALTER TABLE `mswhy`
  MODIFY `ID_why` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `trbooking`
--
ALTER TABLE `trbooking`
  MODIFY `ID_booking` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
